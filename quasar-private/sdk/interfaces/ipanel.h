#pragma once
#include "../../stdafx.h"

using VPANEL = unsigned int;

class IPanel
{
public:
	const char* GetName(VPANEL vguiPanel)
	{
		using fn = const char* (__thiscall*)(void*, VPANEL);
		return call_virtual<fn>(this, 36)(this, vguiPanel);
	}
};