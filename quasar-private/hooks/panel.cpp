#include "hooks.h"
#include "imgui_render.h"
#include "../features/sounds.h"
#include "../features/visuals.h"

void hooks::painttraverse_h(IPanel* thisptr, PVOID, VPANEL vguiPanel, bool forceRepaint, bool allowForce)
{
	if (!_o_painttraverse)
		return;
	
	_o_painttraverse(thisptr, vguiPanel, forceRepaint, allowForce);

	static VPANEL vpanel = 0;
	if (!vpanel)
		if (fnv::hash_runtime(thisptr->GetName(vguiPanel)) == FNV("MatSystemTopPanel"))
			vpanel = vguiPanel;

	if (vguiPanel == vpanel)
	{
		g_pPlayerList->update();
		g_pSounds->update();
		g_pImRender->begin_scene();
		g_pVisuals->run();
		g_pImRender->end_scene();
	}
}
