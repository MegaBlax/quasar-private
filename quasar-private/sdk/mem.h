#pragma once
#include "../stdafx.h"

class mem
{
public:
	mem();
	bool init();
	MODULEINFO dx9();
	MODULEINFO gameoverlay();
	MODULEINFO client();
	MODULEINFO engine();
	MODULEINFO vstdlib();
	MODULEINFO vguimatsurface();
	MODULEINFO materialsystem();
	MODULEINFO vphysics();
	MODULEINFO filesystem();
	MODULEINFO panorama();
	MODULEINFO vgui2();
	MODULEINFO inputsystem();
	MODULEINFO tier0();
	MODULEINFO steamapi();
	MODULEINFO studiorender();
	MODULEINFO matchmaking();
private:
	HANDLE _hProcess;
	MODULEINFO _shaderapidx9;
	MODULEINFO _gameoverlay;
	MODULEINFO _client;
	MODULEINFO _engine;
	MODULEINFO _vstdlib;
	MODULEINFO _vguimatsurface;
	MODULEINFO _materialsystem;
	MODULEINFO _vphysics;
	MODULEINFO _filesystem;
	MODULEINFO _panorama;
	MODULEINFO _vgui2;
	MODULEINFO _inputsystem;
	MODULEINFO _tier0;
	MODULEINFO _steamapi;
	MODULEINFO _studiorender;
	MODULEINFO _matchmaking;
};

extern std::unique_ptr<mem> g_pMemory;