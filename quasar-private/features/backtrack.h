#pragma once
#include "../sdk/cs_player.h"
#include "../sdk/mathlib.h"

#define TIME_TO_TICKS(dt) ((int)( 0.5f + (float)(dt) / g_pInterfaces->globalvarsbase()->interval_per_tick))
#define TICKS_TO_TIME(t) (g_pInterfaces->globalvarsbase()->interval_per_tick * (t) )

static auto extrapolate_position(const vector& pos, const vector& velocity) -> vector {
    return pos + (velocity * g_pInterfaces->globalvarsbase()->interval_per_tick);
}

struct record_backup_t;
struct record_t;

using record_ptr = std::shared_ptr<record_t>;

struct record_t
{
    float simtime = 0.f;
    cs_player* entity{};
    vector hitbox;
    vector angles;
    vector origin;
    vector velocity;
    std::array<matrix3x4_t, MAXSTUDIOBONES> bone_matrix;
    bool valid{};
    // store_restore
    vector angles_abs;
    vector origin_abs;
    vector vec_mins;
    vector vec_maxs;
    std::array<float, 24> m_flPoseParameter{};

    static record_ptr get_new(cs_player* entity)
	{
        auto r = std::make_shared<record_t>();
        r->entity = entity;
        r->simtime = entity->simulationtime();
        r->velocity = entity->vecvelocity();
        r->angles_abs = entity->GetAbsAngles();
        r->origin_abs = entity->GetAbsOrigin();
        r->angles = entity->eyeangles();
        if (globals::backtrack::active)
        {
            entity->setabsangles(vector(0, r->angles.y, 0));
            entity->setabsorigin(entity->vecorigin());
            entity->invalidatebonecache();
            r->bone_matrix = entity->bonematrix();
            entity->setabsangles(r->angles_abs);
            entity->setabsorigin(r->origin_abs);
        }
        else
        {
            r->bone_matrix = entity->bonematrix();
        }

        r->hitbox = r->bone_matrix[BONE_HEAD].at(3);
        r->origin = entity->vecorigin();
        r->vec_mins = entity->GetCollideable()->OBBMins();
        r->vec_maxs = entity->GetCollideable()->OBBMaxs();
        r->m_flPoseParameter = entity->poseparameter();
        r->valid = false;
        return std::move(r);
    }
};

class backtrack
{
public:
    backtrack();
    std::deque<record_ptr>& record(int index);
    bool is_tick_valid(int tickcount);
    void clear();
	
   void fsn();
   void create_move(CUserCmd* cmd);
    
private:
    float lerp_time();
    bool get_best_tick(int index, float best_fov, record_ptr* record);
    bool get_last_tick(int index, record_ptr* record);
private:
    std::array<std::deque<record_ptr>, 64> _records;
    record_ptr _best_record;
    CUserCmd* _cmd;
    cs_player* _local_player;
    int _best_target;
};

extern std::unique_ptr<backtrack> g_pBacktrack;