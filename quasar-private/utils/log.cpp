#include "log.h"
#include "utils.h"
#include "custom_winapi.h"

std::unique_ptr<logmanager> g_pLogManager;

logmanager::logmanager()
{
	const std::wstring folder_path = g_pUtils->GetLogsPath();
	
	if (!std::filesystem::is_directory(folder_path))
	{
		std::filesystem::remove(folder_path);
		std::filesystem::create_directory(folder_path);
	}
	
	_filename = folder_path + xorstr_(L"\\logs.log");
}

void logmanager::message(const wchar_t* format, ...)
{
	wchar_t buffer[MAX_BUFFER_SIZE];
	va_list va;
	va_start(va, format);
	_vsnwprintf_s(buffer, MAX_BUFFER_SIZE, format, va);
	va_end(va);
	
	write(LogID::message, buffer);
}

void logmanager::error(const wchar_t* format, ...)
{
	wchar_t buffer[MAX_BUFFER_SIZE];
	va_list va;
	va_start(va, format);
	_vsnwprintf_s(buffer, MAX_BUFFER_SIZE, format, va);
	va_end(va);
	
	write(LogID::error, buffer);
}

void logmanager::error(HRESULT hr, DWORD lastError, const wchar_t* format, ...)
{
	wchar_t buffer[MAX_BUFFER_SIZE];
	wchar_t code[MAX_PATH];
	va_list va;
	va_start(va, format);
	_vsnwprintf_s(buffer, MAX_BUFFER_SIZE, format, va);
	va_end(va);

	if (HRESULT_CODE(hr) > 0 && lastError > 0)
		swprintf_s(code, L"hr: [%d] err: [%d]", HRESULT_CODE(hr), lastError);
	else if (HRESULT_CODE(hr) > 0)
		swprintf_s(code, L"hr: [%d]", HRESULT_CODE(hr));
	else
		swprintf_s(code, L"err: [%d]", lastError);
	
	write(LogID::error, L"%s %s", buffer, code);
}

void logmanager::write(int flag, const wchar_t* format, ...) 
{
	wchar_t buffer[MAX_BUFFER_SIZE];
	static FILE* stream;
	struct tm current_tm {};
	__time64_t long_time;

	_time64(&long_time);
	_localtime64_s(&current_tm, &long_time);
	
	va_list va;
	va_start(va, format);
	_vsnwprintf_s(buffer, MAX_BUFFER_SIZE, format, va);
	va_end(va);
	const errno_t _error = _wfopen_s(&stream, _filename.c_str(), L"a");

	if (_error == NULL) {
		fwprintf_s(stream, (flag == LogID::message ? xorstr_(L"[%d:%d:%d][%d:%d] %s\n") : xorstr_(L"[%d:%d:%d][%d:%d] Error: %s\n")), current_tm.tm_mday, current_tm.tm_mon + 1,
			current_tm.tm_year + 1900, current_tm.tm_hour, current_tm.tm_min, buffer);

		fclose(stream);
	}

#ifdef _DEBUG
	if (flag == LogID::error)
		g_pWinApi->dwMessageBoxW(nullptr, buffer, xorstr_(L"Error"), MB_OK | MB_ICONERROR);
#endif // DEBUG

}