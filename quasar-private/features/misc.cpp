#include "misc.h"
#include "../sdk/cs_player.h"
#include "engineprediction.h"
#include "imgui_render.h"

std::unique_ptr<misc> g_pMisc;

misc::misc(): _autoaccept(nullptr), _hit_time(0.f)
{
}

void misc::reveal_ranks(CUserCmd* cmd)
{
	if (!globals::misc::rank)
		return;
	if (cmd->buttons & IN_SCORE)
		g_pInterfaces->baseclientdll()->DispatchUserMessage(CS_UM_ServerRankRevealAll, 0, 0, nullptr);
}

void misc::bunnyhop(CUserCmd* cmd)
{
	if (!globals::misc::bunnyhop)
		return;

	cs_player* local_player = cs_player::get_local_player();
	if (!local_player || !local_player->is_alive())
		return;

	const auto movetype = local_player->movetype();
	const auto flags = g_pPrediction->flags();

	if (!(cmd->buttons & IN_JUMP) || movetype == MOVETYPE_LADDER || movetype == MOVETYPE_NOCLIP)
		return;

	static bool last_jumped = false, should_fake = false;
	if (!last_jumped && should_fake)
	{
		should_fake = false;
		cmd->buttons |= IN_JUMP;
	}
	else if (cmd->buttons & IN_JUMP)
	{
		if (flags & FL_ONGROUND)
		{
			last_jumped = true;
			should_fake = true;
		}
		else
		{
			cmd->buttons &= ~IN_JUMP;
			last_jumped = false;
		}
	}
	else
	{
		last_jumped = false;
		should_fake = false;
	}
}

void misc::strafe(CUserCmd* cmd)
{
	if (!globals::misc::bunnyhop || !globals::misc::strafe)
		return;

	cs_player* local_player = cs_player::get_local_player();
	if (!local_player || !local_player->is_alive())
		return;

	const auto movetype = local_player->movetype();
	const auto flags = g_pPrediction->flags();
	if (flags & FL_ONGROUND && !(cmd->buttons & IN_JUMP))
		return;

	if (movetype == MOVETYPE_LADDER || movetype == MOVETYPE_NOCLIP)
		return;

	static bool backward_style = false;
	if (cmd->buttons & IN_FORWARD)
		backward_style = false;
	else if (cmd->buttons & IN_BACK)
		backward_style = true;

	if (cmd->buttons & IN_FORWARD || cmd->buttons & IN_BACK || cmd->buttons & IN_MOVELEFT || cmd->buttons & IN_MOVERIGHT)
		return;

	if (cmd->mousedx <= 1 && cmd->mousedx >= -1)
		return;

	if (backward_style)
		cmd->sidemove = cmd->mousedx < 0 ? 450.f : -450.f;
	else
		cmd->sidemove = cmd->mousedx < 0 ? -450.f : 450.f;
}

void misc::autoaccept()
{
	if (_autoaccept == nullptr)
		_autoaccept = reinterpret_cast<AutoAccept>(g_pOffsets->autoaccept());

	IMatchSession* match_session = g_pInterfaces->matchframework()->GetMatchSession();
	if (!match_session)
		return;

	KeyValues* session_settings = match_session->GetSessionSettings();
	if (!session_settings)
		return;

	const std::string state = session_settings->GetString(xorstr_("game/mmqueue"));
	if (state.find(xorstr_("reserved")) == std::string::npos)
		return;

	FLASHWINFO flash{ sizeof(FLASHWINFO), globals::vars::window, FLASHW_TRAY | FLASHW_TIMERNOFG, 0, 0 };
	FlashWindowEx(&flash);
	if (globals::misc::res_window)
		ShowWindow(globals::vars::window, SW_RESTORE);

	_autoaccept("");
}

void misc::player_hurt()
{
	_hit_time = g_pInterfaces->globalvarsbase()->curtime + 0.25f;
}

void misc::hitmarker() const
{
	if (_hit_time < g_pInterfaces->globalvarsbase()->curtime)
		return;
	
	int width, height;
	g_pInterfaces->engineclient()->GetScreenSize(width, height);
	float x = (float)width / 2.f;
	float y = (float)height / 2.f;
	float size = 7.f;
	
	g_pImRender->add_line(ImVec2(x - size, y - size),ImVec2(x - 2,y - 2),IM_COL32_WHITE);
	g_pImRender->add_line(ImVec2(x - size, y + size),ImVec2(x - 2,y + 2),IM_COL32_WHITE);
	g_pImRender->add_line(ImVec2(x + size, y + size),ImVec2(x + 2,y + 2),IM_COL32_WHITE);
	g_pImRender->add_line(ImVec2(x + size, y - size),ImVec2(x  + 2,y - 2),IM_COL32_WHITE);
}

void misc::killeffect() const
{
	/*IMaterial* healthboost_material = g_pInterfaces->materialsystem()->FindMaterial("effects\\healthboost", TEXTURE_GROUP_CLIENT_EFFECTS);
	const auto time_remaining = _hit_time - g_pInterfaces->globalvarsbase()->curtime;
	int width, height;
	g_pInterfaces->engineclient()->GetScreenSize(width, height);
	if (time_remaining > 0.f)
	{
		auto health_boost_material_var = healthboost_material->FindVar("$c0_x", false);
		health_boost_material_var->SetFloatValue(time_remaining);
		auto render_context = g_pInterfaces->materialsystem()->GetRenderContext();
		render_context->DrawScreenSpaceRectangle(healthboost_material, 0, 0, width, height, 0, 0, (float)width, (float)height, width, height);
		render_context->Release();
	}*/
}
