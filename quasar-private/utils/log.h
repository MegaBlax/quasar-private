#pragma once
#include "../stdafx.h"

enum LogID
{
	message,
	error
};

class logmanager
{
public:
	logmanager();
	void message(const wchar_t* format, ...);
	void error(const wchar_t* format, ...);
	void error(HRESULT hr, DWORD lastError, const wchar_t* format, ...);
private:
	void write(int flag, const wchar_t* format, ...);
	std::wstring _filename;
};

extern std::unique_ptr<logmanager> g_pLogManager;