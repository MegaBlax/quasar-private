#pragma once
class IHandleEntity;
class vector;

class ICollideable
{
public:
	virtual IHandleEntity* GetEntityHandle() = 0;
	virtual vector& OBBMins() const = 0;
	virtual vector& OBBMaxs() const = 0;
};
