#include "hooks.h"
#include "../sdk/cs_player.h"

constexpr auto MAX_COORD_FLOAT = 16384.0f;
constexpr auto MIN_COORD_FLOAT = -MAX_COORD_FLOAT;

struct RenderableInfo_t {
	IClientRenderable* m_pRenderable;
	byte pad[18];
	uint16_t m_Flags;
	uint16_t m_Flags2;
};

int hooks::listleavesinbox_h(ISpatialQuery* thisptr, PVOID, const vector& mins, const vector& maxs, unsigned short* pList, int listMax)
{
	if (!_o_listleavesinbox)
		return 0;
	
	if (!g_pInterfaces->engineclient()->IsActiveApp())
		return _o_listleavesinbox(thisptr, mins, maxs, pList, listMax);
	
	if (uintp(_ReturnAddress()) == g_pOffsets->listleaves()) {
		if (const auto info = *reinterpret_cast<RenderableInfo_t**>(uintp(_AddressOfReturnAddress()) + 0x14); info && info->m_pRenderable) {
			if (const auto ent = (cs_player*)info->m_pRenderable->GetIClientUnknown()->GetBaseEntity(); ent && ent->is_player()) {
				
				info->m_Flags &= ~0x100;
				info->m_Flags2 |= 0x40;

				static vector min{ MIN_COORD_FLOAT, MIN_COORD_FLOAT, MIN_COORD_FLOAT };
				static vector max{ MAX_COORD_FLOAT, MAX_COORD_FLOAT, MAX_COORD_FLOAT };

				return _o_listleavesinbox(thisptr, std::cref(min), std::cref(max), pList, listMax);
			}
		}
	}

	return _o_listleavesinbox(thisptr, mins, maxs, pList, listMax);
}