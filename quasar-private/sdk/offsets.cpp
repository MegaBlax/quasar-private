#include "offsets.h"
#include "mem.h"
#include "../utils/utils.h"

std::unique_ptr<offsets> g_pOffsets;

ptrdiff_t dwGetCallAddress(ptrdiff_t dwAddress)
{
	return *reinterpret_cast<ptrdiff_t*>(dwAddress + 1) + dwAddress + 5;
}

offsets::offsets()
{
	_shaderapidx9_sig = nullptr;
	_present_sig = 0;
	_reset_sig = 0;
	_matrix_sig = 0;
	_reloading_sig = 0;
	_maptexture_sig = 0;
	_playerres_sig = 0;
	_radarbase_sig = 0;
	_hudelement_sig = nullptr;
	_hud_sig = nullptr;
	_smoke_sig = nullptr;
	_has_c4_sig = nullptr;
	_autoaccept_sig = nullptr;
	_keyvaluessystem_sig = 0;
	_loadfrombuffer_sig = 0;
	_getstring_sig = 0;
	_savetofile_sig = 0;
	_setabsorigin_sig = nullptr;
	_setabsangle_sig = nullptr;
	_invalidbonecache_sig = 0;
	_randomseed_sig = nullptr;
	_currentcmd_sig = 0;
	_listleaves_sig = 0;
}

bool offsets::init()
{
	_shaderapidx9_sig = **reinterpret_cast<DWORD***>(g_pUtils->PatternScan(g_pMemory->dx9(), xorstr_("A1 ? ? ? ? 50 8B 08 FF 51 0C")) + 0x1);
	_present_sig = reinterpret_cast<uintp>(g_pUtils->PatternScan(g_pMemory->gameoverlay(), "FF 15 ? ? ? ? 8B F8 85 DB") + 0x2);
	_reset_sig = reinterpret_cast<uintp>(g_pUtils->PatternScan(g_pMemory->gameoverlay(), "C7 45 ? ? ? ? ? FF 15 ? ? ? ? 8B F8") + 0x9);
	_matrix_sig = *reinterpret_cast<DWORD*>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("0F 10 05 ? ? ? ? 8D 85 ? ? ? ? B9")) + 0x3) + 0xB0;
	_reloading_sig = *reinterpret_cast<DWORD*>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("C6 87 ? ? ? ? ? 8B 06 8B CE FF 90")) + 0x2);
	_hudelement_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 53 8B 5D 08 56 57 8B F9 33 F6 39 77 28"));
	_hud_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("B9 ? ? ? ? E8 ? ? ? ? 85 C0 0F 84 ? ? ? ? 83 C0 EC 89")) + 0x1;
	_smoke_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 83 EC 08 8B 15 ? ? ? ? 0F 57 C0"));
	_has_c4_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("56 8B F1 85 F6 74 31"));
	_autoaccept_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 83 E4 F8 8B 4D 08 BA ? ? ? ? E8 ? ? ? ? 85 C0 75 12"));
	
	_keyvaluessystem_sig = reinterpret_cast<DWORD>(GetProcAddress(HMODULE(g_pMemory->vstdlib().lpBaseOfDll), xorstr_("KeyValuesSystem")));
	_loadfrombuffer_sig = reinterpret_cast<DWORD>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 83 E4 F8 83 EC 34 53 8B 5D 0C 89 4C 24 04")));
	_getstring_sig = reinterpret_cast<DWORD>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 83 E4 C0 81 EC ? ? ? ? 53 8B 5D 08")));
	_savetofile_sig = reinterpret_cast<DWORD>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 51 53 8B ? ? 56 8B ? ? 57 8B ? ? 57 8B ? 68")));
	_setabsangle_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 83 E4 F8 83 EC 64 53 56 57 8B F1 E8"));
	_setabsorigin_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 83 E4 F8 51 53 56 57 8B F1 E8"));
	_invalidbonecache_sig = reinterpret_cast<DWORD>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("80 3D ? ? ? ? ? 74 16 A1 ? ? ? ? 48 C7 81")));
	_randomseed_sig = g_pUtils->PatternScan(g_pMemory->client(), xorstr_("55 8B EC 83 E4 F8 83 EC 70 6A 58"));
	_currentcmd_sig = *reinterpret_cast<DWORD*>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("89 BE ? ? ? ? E8 ? ? ? ? 85 FF")) + 0x2);
	_listleaves_sig = reinterpret_cast<uintp>(g_pUtils->PatternScan(g_pMemory->client(), "56 52 FF 50 18") + 0x5);
	
	return _shaderapidx9_sig && _present_sig && _reset_sig && _matrix_sig && _reloading_sig && _hudelement_sig
		&& _hud_sig && _smoke_sig && _has_c4_sig && _autoaccept_sig && _keyvaluessystem_sig && _loadfrombuffer_sig && _getstring_sig
		&& _savetofile_sig && _setabsangle_sig && _setabsorigin_sig && _invalidbonecache_sig && _randomseed_sig && _currentcmd_sig;
}

DWORD* offsets::shaderapi() const
{
	return _shaderapidx9_sig;
}

uintp offsets::present()
{
	return _present_sig;
}

uintp offsets::reset()
{
	return _reset_sig;
}

DWORD offsets::matrix() const
{
	return _matrix_sig;
}

DWORD offsets::reloading() const
{
	return _reloading_sig;
}

DWORD offsets::radarbase() const
{
	return _radarbase_sig;
}

uint8_t* offsets::hudelement() const
{
	return _hudelement_sig;
}

uint8_t* offsets::hud() const
{
	return _hud_sig;
}

uint8_t* offsets::smoke() const
{
	return _smoke_sig;
}

uint8_t* offsets::hasc4() const
{
	return _has_c4_sig;
}

uint8_t* offsets::autoaccept() const
{
	return _autoaccept_sig;
}

DWORD offsets::keyvaluessystem() const
{
	return _keyvaluessystem_sig;
}

DWORD offsets::loadfrombuffer() const
{
	return _loadfrombuffer_sig;
}

DWORD offsets::getstring() const
{
	return _getstring_sig;
}

DWORD offsets::savetofile() const
{
	return _savetofile_sig;
}

uint8_t* offsets::setabsorigin() const
{
	return _setabsorigin_sig;
}

uint8_t* offsets::setabsangles() const
{
	return _setabsangle_sig;
}

DWORD offsets::invalidbonecache() const
{
	return _invalidbonecache_sig;
}

uint8_t* offsets::randomseed() const
{
	return _randomseed_sig;
}

DWORD offsets::currentcmd() const
{
	return _currentcmd_sig;
}

uintp offsets::listleaves() const
{
	return _listleaves_sig;
}
