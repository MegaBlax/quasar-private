#include "mathlib.h"

//-----------------------------------------------------------------------------
// VMatrix inlines.
//-----------------------------------------------------------------------------

VMatrix::VMatrix(
    float m00, float m01, float m02, float m03,
    float m10, float m11, float m12, float m13,
    float m20, float m21, float m22, float m23,
    float m30, float m31, float m32, float m33)
{
    Init(
        m00, m01, m02, m03,
        m10, m11, m12, m13,
        m20, m21, m22, m23,
        m30, m31, m32, m33
    );
}


VMatrix::VMatrix(const matrix3x4_t& matrix3x4)
{
    Init(matrix3x4);
}


//-----------------------------------------------------------------------------
// Creates a matrix where the X axis = forward
// the Y axis = left, and the Z axis = up
//-----------------------------------------------------------------------------
VMatrix::VMatrix(const vector& xAxis, const vector& yAxis, const vector& zAxis)
{
    Init(
        xAxis.x, yAxis.x, zAxis.x, 0.0f,
        xAxis.y, yAxis.y, zAxis.y, 0.0f,
        xAxis.z, yAxis.z, zAxis.z, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    );
}


void VMatrix::Init(
    float m00, float m01, float m02, float m03,
    float m10, float m11, float m12, float m13,
    float m20, float m21, float m22, float m23,
    float m30, float m31, float m32, float m33
)
{
    m[0][0] = m00;
    m[0][1] = m01;
    m[0][2] = m02;
    m[0][3] = m03;

    m[1][0] = m10;
    m[1][1] = m11;
    m[1][2] = m12;
    m[1][3] = m13;

    m[2][0] = m20;
    m[2][1] = m21;
    m[2][2] = m22;
    m[2][3] = m23;

    m[3][0] = m30;
    m[3][1] = m31;
    m[3][2] = m32;
    m[3][3] = m33;
}


//-----------------------------------------------------------------------------
// Initialize from a 3x4
//-----------------------------------------------------------------------------
void VMatrix::Init(const matrix3x4_t& matrix3x4)
{
    memcpy(m, matrix3x4.Base(), sizeof(matrix3x4_t));

    m[3][0] = 0.0f;
    m[3][1] = 0.0f;
    m[3][2] = 0.0f;
    m[3][3] = 1.0f;
}

//-----------------------------------------------------------------------------
// vector3DMultiplyPosition treats src2 as if it's a point (adds the translation)
//-----------------------------------------------------------------------------
// NJS: src2 is passed in as a full vector rather than a reference to prevent the need
// for 2 branches and a potential copy in the body.  (ie, handling the case when the src2
// reference is the same as the dst reference ).
void vector3DMultiplyPosition(const VMatrix& src1, const vector& src2, vector& dst)
{
    dst[0] = src1[0][0] * src2.x + src1[0][1] * src2.y + src1[0][2] * src2.z + src1[0][3];
    dst[1] = src1[1][0] * src2.x + src1[1][1] * src2.y + src1[1][2] * src2.z + src1[1][3];
    dst[2] = src1[2][0] * src2.x + src1[2][1] * src2.y + src1[2][2] * src2.z + src1[2][3];
}

//-----------------------------------------------------------------------------
// Methods related to the basis vectors of the matrix
//-----------------------------------------------------------------------------

vector VMatrix::GetForward() const
{
    return vector(m[0][0], m[1][0], m[2][0]);
}

vector VMatrix::GetLeft() const
{
    return vector(m[0][1], m[1][1], m[2][1]);
}

vector VMatrix::GetUp() const
{
    return vector(m[0][2], m[1][2], m[2][2]);
}

void VMatrix::SetForward(const vector &vForward)
{
    m[0][0] = vForward.x;
    m[1][0] = vForward.y;
    m[2][0] = vForward.z;
}

void VMatrix::SetLeft(const vector &vLeft)
{
    m[0][1] = vLeft.x;
    m[1][1] = vLeft.y;
    m[2][1] = vLeft.z;
}

void VMatrix::SetUp(const vector &vUp)
{
    m[0][2] = vUp.x;
    m[1][2] = vUp.y;
    m[2][2] = vUp.z;
}

void VMatrix::GetBasisvectors(vector &vForward, vector &vLeft, vector &vUp) const
{
    vForward.Init(m[0][0], m[1][0], m[2][0]);
    vLeft.Init(m[0][1], m[1][1], m[2][1]);
    vUp.Init(m[0][2], m[1][2], m[2][2]);
}

void VMatrix::SetBasisvectors(const vector &vForward, const vector &vLeft, const vector &vUp)
{
    SetForward(vForward);
    SetLeft(vLeft);
    SetUp(vUp);
}


//-----------------------------------------------------------------------------
// Methods related to the translation component of the matrix
//-----------------------------------------------------------------------------

vector VMatrix::GetTranslation() const
{
    return vector(m[0][3], m[1][3], m[2][3]);
}

vector& VMatrix::GetTranslation(vector &vTrans) const
{
    vTrans.x = m[0][3];
    vTrans.y = m[1][3];
    vTrans.z = m[2][3];
    return vTrans;
}

void VMatrix::SetTranslation(const vector &vTrans)
{
    m[0][3] = vTrans.x;
    m[1][3] = vTrans.y;
    m[2][3] = vTrans.z;
}


//-----------------------------------------------------------------------------
// appply translation to this matrix in the input space
//-----------------------------------------------------------------------------
void VMatrix::PreTranslate(const vector &vTrans)
{
    vector tmp;
    vector3DMultiplyPosition(*this, vTrans, tmp);
    m[0][3] = tmp.x;
    m[1][3] = tmp.y;
    m[2][3] = tmp.z;
}


//-----------------------------------------------------------------------------
// appply translation to this matrix in the output space
//-----------------------------------------------------------------------------
void VMatrix::PostTranslate(const vector &vTrans)
{
    m[0][3] += vTrans.x;
    m[1][3] += vTrans.y;
    m[2][3] += vTrans.z;
}

const matrix3x4_t& VMatrix::As3x4() const
{
    return *((const matrix3x4_t*)this);
}

matrix3x4_t& VMatrix::As3x4()
{
    return *((matrix3x4_t*)this);
}

void VMatrix::CopyFrom3x4(const matrix3x4_t &m3x4)
{
    memcpy(m, m3x4.Base(), sizeof(matrix3x4_t));
    m[3][0] = m[3][1] = m[3][2] = 0;
    m[3][3] = 1;
}

void VMatrix::Set3x4(matrix3x4_t& matrix3x4) const
{
    memcpy(matrix3x4.Base(), m, sizeof(matrix3x4_t));
}


//-----------------------------------------------------------------------------
// Matrix Math operations
//-----------------------------------------------------------------------------
const VMatrix& VMatrix::operator+=(const VMatrix &other)
{
    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
            m[i][j] += other.m[i][j];
        }
    }

    return *this;
}

VMatrix VMatrix::operator+(const VMatrix &other) const
{
    VMatrix ret;
    for(int i = 0; i < 16; i++) {
        ((float*)ret.m)[i] = ((float*)m)[i] + ((float*)other.m)[i];
    }
    return ret;
}

VMatrix VMatrix::operator-(const VMatrix &other) const
{
    VMatrix ret;

    for(int i = 0; i < 4; i++) {
        for(int j = 0; j < 4; j++) {
            ret.m[i][j] = m[i][j] - other.m[i][j];
        }
    }

    return ret;
}

VMatrix VMatrix::operator-() const
{
    VMatrix ret;
    for(int i = 0; i < 16; i++) {
        ((float*)ret.m)[i] = -((float*)m)[i];
    }
    return ret;
}

//-----------------------------------------------------------------------------
// vector transformation
//-----------------------------------------------------------------------------


vector VMatrix::operator*(const vector &vVec) const
{
    vector vRet;
    vRet.x = m[0][0] * vVec.x + m[0][1] * vVec.y + m[0][2] * vVec.z + m[0][3];
    vRet.y = m[1][0] * vVec.x + m[1][1] * vVec.y + m[1][2] * vVec.z + m[1][3];
    vRet.z = m[2][0] * vVec.x + m[2][1] * vVec.y + m[2][2] * vVec.z + m[2][3];

    return vRet;
}

vector VMatrix::VMul4x3(const vector &vVec) const
{
    vector vResult;
    vector3DMultiplyPosition(*this, vVec, vResult);
    return vResult;
}


vector VMatrix::VMul4x3Transpose(const vector &vVec) const
{
    vector tmp = vVec;
    tmp.x -= m[0][3];
    tmp.y -= m[1][3];
    tmp.z -= m[2][3];

    return vector(
        m[0][0] * tmp.x + m[1][0] * tmp.y + m[2][0] * tmp.z,
        m[0][1] * tmp.x + m[1][1] * tmp.y + m[2][1] * tmp.z,
        m[0][2] * tmp.x + m[1][2] * tmp.y + m[2][2] * tmp.z
    );
}

vector VMatrix::VMul3x3(const vector &vVec) const
{
    return vector(
        m[0][0] * vVec.x + m[0][1] * vVec.y + m[0][2] * vVec.z,
        m[1][0] * vVec.x + m[1][1] * vVec.y + m[1][2] * vVec.z,
        m[2][0] * vVec.x + m[2][1] * vVec.y + m[2][2] * vVec.z
    );
}

vector VMatrix::VMul3x3Transpose(const vector &vVec) const
{
    return vector(
        m[0][0] * vVec.x + m[1][0] * vVec.y + m[2][0] * vVec.z,
        m[0][1] * vVec.x + m[1][1] * vVec.y + m[2][1] * vVec.z,
        m[0][2] * vVec.x + m[1][2] * vVec.y + m[2][2] * vVec.z
    );
}


void VMatrix::V3Mul(const vector &vIn, vector &vOut) const
{
	float rw = 1.0f / (m[3][0] * vIn.x + m[3][1] * vIn.y + m[3][2] * vIn.z + m[3][3]);
    vOut.x = (m[0][0] * vIn.x + m[0][1] * vIn.y + m[0][2] * vIn.z + m[0][3]) * rw;
    vOut.y = (m[1][0] * vIn.x + m[1][1] * vIn.y + m[1][2] * vIn.z + m[1][3]) * rw;
    vOut.z = (m[2][0] * vIn.x + m[2][1] * vIn.y + m[2][2] * vIn.z + m[2][3]) * rw;
}

//-----------------------------------------------------------------------------
// Other random stuff
//-----------------------------------------------------------------------------
void VMatrix::Identity()
{
    m[0][0] = 1.0f; m[0][1] = 0.0f; m[0][2] = 0.0f; m[0][3] = 0.0f;
    m[1][0] = 0.0f; m[1][1] = 1.0f; m[1][2] = 0.0f; m[1][3] = 0.0f;
    m[2][0] = 0.0f; m[2][1] = 0.0f; m[2][2] = 1.0f; m[2][3] = 0.0f;
    m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;
}


bool VMatrix::IsIdentity() const
{
    return
        m[0][0] == 1.0f && m[0][1] == 0.0f && m[0][2] == 0.0f && m[0][3] == 0.0f &&
        m[1][0] == 0.0f && m[1][1] == 1.0f && m[1][2] == 0.0f && m[1][3] == 0.0f &&
        m[2][0] == 0.0f && m[2][1] == 0.0f && m[2][2] == 1.0f && m[2][3] == 0.0f &&
        m[3][0] == 0.0f && m[3][1] == 0.0f && m[3][2] == 0.0f && m[3][3] == 1.0f;
}

vector VMatrix::ApplyRotation(const vector &vVec) const
{
    return VMul3x3(vVec);
}

void MatrixSetColumn(const vector& in, int column, matrix3x4_t& out)
{
    out[0][column] = in.x;
    out[1][column] = in.y;
    out[2][column] = in.z;
}

void CrossProduct(const vector& v1, const vector& v2, vector& cross)
{
    Assert(v1 != cross);
    Assert(v2 != cross);
    cross[0] = v1[1] * v2[2] - v1[2] * v2[1];
    cross[1] = v1[2] * v2[0] - v1[0] * v2[2];
    cross[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

float VectorNormalize(vector& vec)
{
    const float radius = sqrtf(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
    // FLT_EPSILON is added to the radius to eliminate the possibility of divide by zero.
    const float iradius = 1.f / (radius + FLT_EPSILON);

    vec.x *= iradius;
    vec.y *= iradius;
    vec.z *= iradius;

    return radius;
}

void VectorVectors(const vector& forward, vector& right, vector& up)
{
    vector tmp;

    if (forward[0] == 0 && forward[1] == 0)
    {
        // pitch 90 degrees up/down from identity
        right[0] = 0;
        right[1] = -1;
        right[2] = 0;
        up[0] = -forward[2];
        up[1] = 0;
        up[2] = 0;
    }
    else
    {
        tmp[0] = 0; tmp[1] = 0; tmp[2] = 1.0;
        CrossProduct(forward, tmp, right);
        VectorNormalize(right);
        CrossProduct(right, forward, up);
        VectorNormalize(up);
    }
}

void VectorTransform(const vector& in1, const matrix3x4_t& in2, vector& out)
{
    out[0] = in1.Dot(in2[0]) + in2[0][3];
    out[1] = in1.Dot(in2[1]) + in2[1][3];
    out[2] = in1.Dot(in2[2]) + in2[2][3];
}

void VectorAngles(const vector& forward, qangle& angles)
{
    if (forward[1] == 0.0f && forward[0] == 0.0f)
    {
        angles[0] = (forward[2] > 0.0f) ? 270.0f : 90.0f; // Pitch (up/down)
        angles[1] = 0.0f;                                 // Yaw left/right
    }
    else
    {
        angles[0] = atan2(-forward[2], forward.Length2D()) * -180.f / M_PI_F;
        angles[1] = atan2(forward[1], forward[0]) * 180.f / M_PI_F;

        if (angles[1] > 90) angles[1] -= 180;
        else if (angles[1] < 90) angles[1] += 180;
        else if (angles[1] == 90) angles[1] = 0;
    }
    angles[2] = 0.0f;
}

void AngleVectors(const qangle& angles, vector* forward)
{
    Assert(forward);

    const V4SF vx =
    {
        {
        DEG2RAD(angles[0]),
        DEG2RAD(angles[1]),
        0.f,
        0.f
        },
    };
    V4SF sin4, cos4;

    sincos_ps(vx.v, &sin4.v, &cos4.v);
    forward->x = cos4.v.m128_f32[0] * cos4.v.m128_f32[1];
    forward->y = cos4.v.m128_f32[0] * sin4.v.m128_f32[1];
    forward->z = -sin4.v.m128_f32[0];
}

void AngleVectors(const qangle& angles, vector* forward, vector* right, vector* up)
{
    const V4SF vx =
    {
        {
        DEG2RAD(angles[0]),
        DEG2RAD(angles[1]),
        DEG2RAD(angles[2]),
        0.f
        },
    };
    V4SF sin4, cos4;
    sincos_ps(vx.v, &sin4.v, &cos4.v);
    forward->x = cos4.v.m128_f32[0] * cos4.v.m128_f32[1];
    forward->y = cos4.v.m128_f32[0] * sin4.v.m128_f32[1];
    forward->z = -sin4.v.m128_f32[0];

    right->x = -1 * sin4.v.m128_f32[2] * sin4.v.m128_f32[0] * cos4.v.m128_f32[1] + -1 * cos4.v.m128_f32[2] * -sin4.v.m128_f32[1];
    right->y = -1 * sin4.v.m128_f32[2] * sin4.v.m128_f32[0] * sin4.v.m128_f32[1] + -1 * cos4.v.m128_f32[2] * cos4.v.m128_f32[1];
    right->z = -1 * sin4.v.m128_f32[2] * cos4.v.m128_f32[0];

    up->x = cos4.v.m128_f32[2] * sin4.v.m128_f32[0] * cos4.v.m128_f32[1] + -sin4.v.m128_f32[2] * -sin4.v.m128_f32[1];
    up->y = cos4.v.m128_f32[2] * sin4.v.m128_f32[0] * sin4.v.m128_f32[1] + -sin4.v.m128_f32[2] * cos4.v.m128_f32[1];
    up->z = cos4.v.m128_f32[2] * cos4.v.m128_f32[0];
}

void VectorMatrix(const vector& forward, matrix3x4_t& matrix)
{
    vector right, up;
    VectorVectors(forward, right, up);

    MatrixSetColumn(forward, 0, matrix);
    MatrixSetColumn(-right, 1, matrix);
    MatrixSetColumn(up, 2, matrix);
}

// assume in2 is a rotation and rotate the input vector
void VectorRotate(const vector& in1, const matrix3x4_t& in2, float* out)
{
    out[0] = DotProduct(in1, in2[0]);
    out[1] = DotProduct(in1, in2[1]);
    out[2] = DotProduct(in1, in2[2]);
}

float Bias(float x, float biasAmt)
{
    // WARNING: not thread safe
    static float lastAmt = -1;
    static float lastExponent = 0;
    if (lastAmt != biasAmt)
    {
        lastExponent = log(biasAmt) * -1.4427f; // (-1.4427 = 1 / log(0.5))
    }
    return pow(x, lastExponent);
}


float Gain(float x, float biasAmt)
{
    // WARNING: not thread safe
    if (x < 0.5)
        return 0.5f * Bias(2 * x, 1 - biasAmt);
	
    return 1 - 0.5f * Bias(2 - 2 * x, 1 - biasAmt);
}


float SmoothCurve(float x, const float mult)
{
	return (x - cos(x * M_PI_F)) * mult;
}


inline float MovePeak(float x, float flPeakPos)
{
    // Todo: make this higher-order?
    if (x < flPeakPos)
        return x * 0.5f / flPeakPos;
	
    return 0.5f + 0.5f * (x - flPeakPos) / (1 - flPeakPos);
}


float SmoothCurve_Tweak(float x, float flPeakPos, float flPeakSharpness)
{
    float flMovedPeak = MovePeak(x, flPeakPos);
    float flSharpened = Gain(flMovedPeak, flPeakSharpness);
    return SmoothCurve(flSharpened);
}

bool Intersect(vector start, vector end, vector a, vector b, float radius)
{
    const auto dist = segment_to_segment(a, b, start, end);
    return (dist < radius);
}

float AngleNormalize(float angle)
{
    angle = fmodf(angle, 360.0f);
    if (angle > 180)
    {
        angle -= 360;
    }
    if (angle < -180)
    {
        angle += 360;
    }
    return angle;
}

void ClampAngles(qangle& angles)
{
    if (angles.y > 180.0f)
        angles.y = 180.0f;
    else if (angles.y < -180.0f)
        angles.y = -180.0f;

    if (angles.x > 89.0f)
        angles.x = 89.0f;
    else if (angles.x < -89.0f)
        angles.x = -89.0f;

    angles.z = 0;
}

void NormalizeAngles(qangle& angles)
{
    while (angles.x > 89.0f)
        angles.x -= 180.f;
    while (angles.x < -89.0f)
        angles.x += 180.f;
    while (angles.y > 180.f)
        angles.y -= 360.f;
    while (angles.y < -180.f)
        angles.y += 360.f;
}