#pragma once
#include "../stdafx.h"

namespace netvars
{
	namespace cs_player
	{
		bool init();
		extern int m_iHealth;
		extern int m_lifeState;
		extern int m_iTeamNum;
		extern int m_bIsPlayerGhost;
		extern int m_vecOrigin;
		extern int m_vecViewOffset;
		extern int m_aimPunchAngle;
		extern int m_nTickBase;
		extern int m_iShotsFired;
		extern int m_flFlashDuration;
		extern int m_flFlashMaxAlpha;
		extern int m_fFlags;
		extern int m_vecVelocity;
		extern int m_bSpotted;
		extern int m_flSimulationTime;
		extern int m_hObserverTarget;
		extern int m_iObserverMode;
		extern int m_bGunGameImmunity;
		extern int m_bIsScoped;
		extern int m_nSurvivalTeam;
		extern int m_angEyeAngles;
	}

	namespace cs_weapon
	{
		bool init();
		extern int m_hActiveWeapon;
		extern int m_iItemDefinitionIndex;
		extern int m_iClip1;
		extern int m_flNextPrimaryAttack;
		extern int m_flNextAttack;
		extern int m_fLastShotTime;
	}

	namespace cs_planted_c4
	{
		bool init();
		//extern int m_flTimerLength;
		extern int m_bBombTicking;
		//extern int m_nBombSite;
		//extern int m_flDefuseCountDown;
		extern int m_flC4Blow;
		extern int m_hBombDefuser;
		extern int m_flDefuseLength;
		extern int m_bBombDefused;
	}

	namespace dt_base_animating
	{
		bool init();
		extern int m_flPoseParameter;
	}

	namespace cs_player_res
	{
		bool init();
		extern int m_iCompTeammateColor;
		extern int m_iTotalCashSpent;
		extern int m_iCashSpentThisRound;
	}

	namespace DT_CSGameRulesProxy
	{
		extern int m_bIsValveDS;
	}

	bool init();
}
