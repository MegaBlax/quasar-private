#include "hooks.h"
#include "../features/norecoil.h"

void hooks::runcommand_h(IPrediction* thisptr, PVOID, IClientEntity* player, CUserCmd* cmd, IMoveHelper* helper)
{
	if (!_o_runcommand)
		return;
	
	if (!g_pInterfaces->engineclient()->IsActiveApp())
		return _o_runcommand(thisptr, player, cmd, helper);
	
	_o_runcommand(thisptr, player, cmd, helper);
	
	g_pNorecoil->runcommand(player);
}
