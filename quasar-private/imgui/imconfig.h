//-----------------------------------------------------------------------------
// COMPILE-TIME OPTIONS FOR DEAR IMGUI
// Runtime options (clipboard callbacks, enabling various features, etc.) can generally be set via the ImGuiIO structure.
// You can use ImGui::SetAllocatorFunctions() before calling ImGui::CreateContext() to rewire memory allocation functions.
//-----------------------------------------------------------------------------
// A) You may edit imconfig.h (and not overwrite it when updating Dear ImGui, or maintain a patch/branch with your modifications to imconfig.h)
// B) or add configuration directives in your own file and compile with #define IMGUI_USER_CONFIG "myfilename.h"
// If you do so you need to make sure that configuration settings are defined consistently _everywhere_ Dear ImGui is used, which include
// the imgui*.cpp files but also _any_ of your code that uses Dear ImGui. This is because some compile-time options have an affect on data structures.
// Defining those options in imconfig.h will ensure every compilation unit gets to see the same data structure layouts.
// Call IMGUI_CHECKVERSION() from your .cpp files to verify that the data structures your files are using are matching the ones imgui.cpp is using.
//-----------------------------------------------------------------------------

#pragma once
#include <vector>
#include <string>

struct ImVec2;
struct ImVec4;
typedef int ImGuiDataType;
typedef int ImGuiComboFlags;
typedef int ImGuiColorEditFlags;
typedef unsigned int ImU32;
typedef void* ImTextureID;

namespace ImGui
{
    void QSameLine();
    void AddBorderIsWindowMoving();
	ImU32 QGetColorU32(const ImVec4& col, float alpha);
	bool QSliderScalar(const char* label, float width, ImGuiDataType data_type, void* p_data, const void* p_min, const void* p_max, const char* format);
	bool QBeginCombo(const char* label, float width, const char* preview_value, ImGuiComboFlags flags);
    void QTabs(const std::vector<const char*>& items, const ImVec2& size_arg, int* current_item);
    void QSubTabs(const std::vector<const char*>& items, int* current_item, float height);
    bool QTabButton(const char* label, ImTextureID user_texture_id, const ImVec2& size_arg, bool active);
    bool QSubTabButton(const char* label, const ImVec2& size_arg, bool active);
    bool QCheckbox(const char* label, bool* v);
    bool QSliderFloat(const char* label, float width, float* v, float v_min, float v_max, const char* format = "%.1f");
    bool QSliderInt(const char* label, float width, int* v, int v_min, int v_max, const char* format = "%d");
    bool QPCombo(const char* label, float width, const std::vector<std::pair<int, const char*>>& items, int* current_item);
    bool QCombo(const char* label, float width, const std::vector<const char*>& items, int* current_item);
    bool QListBox(const char* label, int* current_item, const std::vector<std::wstring>& items, float height);
    bool QColorEdit4(const char* label, float col[4], ImGuiColorEditFlags flags = 224);
    bool QColorPicker4(const char* label, float col[4], ImGuiColorEditFlags flags, const float* ref_col);
    void QHotkey(const char* label, int* key, const ImVec2& size_arg);
    void QHotkey(const char* label, int* key, int* mode, const ImVec2& size_arg);
    bool QRadioButton(const char* label, bool active);
    bool QRadioButton(const char* label, int* v, int v_button);
    bool QButton(const char* label, const ImVec2& size_arg);
}

