#pragma once
#include "iclientunknown.h"
#include "iclientrenderable.h"
#include "iclientnetworkable.h"
#include "iclientthinkable.h"

class IClientEntity : public IClientUnknown , public IClientRenderable , public IClientNetworkable, public IClientThinkable
{
public:
	virtual void			Release(void) = 0;
	const vector&			GetAbsOrigin()
	{
		using fn = const vector & (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 10)(this);
	}
	const qangle&			GetAbsAngles()
	{
		using fn = const qangle & (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 11)(this);
	}
};
