#pragma once
#include "color.h"

class ConVar
{
public:
	char pad_0x0000[0x4]; //0x0000
	ConVar* pNext; //0x0004
	__int32 bRegistered; //0x0008
	char* pszName; //0x000C
	char* pszHelpString; //0x0010
	__int32 nFlags; //0x0014
	char pad_0x0018[0x4]; //0x0018
	ConVar* pParent; //0x001C
	char* pszDefaultValue; //0x0020
	char* strString; //0x0024
	__int32 StringLength; //0x0028
	float fValue; //0x002C
	__int32 nValue; //0x0030
	__int32 bHasMin; //0x0034
	float fMinVal; //0x0038
	__int32 bHasMax; //0x003C
	float fMaxVal; //0x0040
	void* fnChangeCallback; //0x0044

	int GetFlags()
	{
		using fn = int(__thiscall*)(void*);
		return call_virtual<fn>(this, 8)(this);
	}

	Color GetColor()
	{
		using fn = Color(__thiscall*)(void*);
		return call_virtual<fn>(this, 10)(this);
	}

	const char* GetString()
	{
		using fn = const char* (__thiscall*)(void*);
		return call_virtual<fn>(this, 11)(this);
	}

	float GetFloat()
	{
		using fn = float(__thiscall*)(void*);
		return call_virtual<fn>(this, 12)(this);
	}

	int GetInt()
	{
		using fn = int(__thiscall*)(void*);
		return call_virtual<fn>(this, 13)(this);
	}

	void SetValue(const char* value)
	{
		using fn = void(__thiscall*)(void*, const char*);
		call_virtual<fn>(this, 14)(this, value);
	}

	void SetValue(float value)
	{
		using fn = void(__thiscall*)(void*, float);
		call_virtual<fn>(this, 15)(this, value);
	}

	void SetValue(int value)
	{
		using fn = void(__thiscall*)(void*, int);
		call_virtual<fn>(this, 16)(this, value);
	}

	void SetValue(Color value)
	{
		using OriginalFn = void(__thiscall*)(void*, Color);
		call_virtual<OriginalFn>(this, 17)(this, value);
	}
};
