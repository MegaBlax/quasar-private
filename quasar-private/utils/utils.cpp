#include "utils.h"
#include "custom_winapi.h"
#include "../sdk/interfaces.h"
#include "../sdk/offsets.h"
#include "../sdk/enums.h"
#include "../sdk/mem.h"

std::unique_ptr<utils> g_pUtils;

std::wstring AnsiToWstring(const std::string& input, const DWORD locale = CP_ACP) 
{
	wchar_t buf[2048] = { 0 };
	MultiByteToWideChar(locale, 0, input.c_str(), int(input.length()), buf, ARRAYSIZE(buf));
	return buf;
}

std::string WstringToAnsi(const std::wstring& input, const DWORD locale = CP_ACP) 
{
	char buf[2048] = { 0 };
	WideCharToMultiByte(locale, 0, input.c_str(), int(input.length()), buf, ARRAYSIZE(buf), nullptr, nullptr);
	return buf;
}

float mm_sqrtf(const float arg)
{
	const V4SF vx =
	{
		{
		arg,
		0.f,
		0.f,
		0.f
		},
	};
	const v4sf mmsqrt = _mm_rsqrt_ss(vx.v);
	const v4sf cache = _mm_mul_ss(vx.v, mmsqrt);
	return _mm_cvtss_f32(cache);
}

void utils::SinCos(const float radians, float* sine, float* cosine)
{
	const V4SF vx =
	{
		{
		radians,
		0.f,
		0.f,
		0.f
		},
	};
	V4SF sin4, cos4;
	sincos_ps(vx.v, &sin4.v, &cos4.v);

	*sine = _mm_cvtss_f32(sin4.v);
	*cosine = _mm_cvtss_f32(cos4.v);
}

std::wstring utils::GetFolderPath() 
{
	TCHAR* buffer;
	size_t len;
	
	_wdupenv_s(&buffer, &len, xorstr_(L"SystemDrive"));

	return buffer;
}

std::wstring utils::GetLogsPath()
{
	std::wstring path = GetFolderPath() + xorstr_(L"\\Quasar\\Logs");

	return path.c_str();
}

std::wstring utils::GetConfigsPath()
{
	std::wstring path = GetFolderPath() + xorstr_(L"\\Quasar\\Configuration");
	
	return path.c_str();
}

std::wstring utils::GetIconsPath()
{
	std::wstring path = GetFolderPath() + xorstr_(L"\\Quasar\\Icons");

	return path.c_str();
}

void utils::CreateAllDirectories()
{
	const std::wstring directory = GetFolderPath() + xorstr_(L"\\Quasar");
	const std::wstring log = directory + xorstr_(L"\\Logs");
	const std::wstring cfg = directory + xorstr_(L"\\Configuration");

	if (!std::filesystem::is_directory(directory))
	{
		std::filesystem::remove(directory);
		std::filesystem::create_directory(directory);
	}
	if (!std::filesystem::is_directory(log))
	{
		std::filesystem::remove(log);
		std::filesystem::create_directory(log);
	}
	if (!std::filesystem::is_directory(cfg))
	{
		std::filesystem::remove(cfg);
		std::filesystem::create_directory(cfg);
	}
}

std::wstring utils::UTF8ToWstring(const std::string& str) 
{
	return AnsiToWstring(str, CP_UTF8);
}

std::string utils::WstringToUTF8(const std::wstring& str) 
{
	return WstringToAnsi(str, CP_UTF8);
}

bool screen_transform(const vector& point, ImVec2& screen)
{
	static VMatrix* _w2sMatrix = nullptr;

	if (!g_pOffsets->matrix())
		return false;

	if (!_w2sMatrix)
		_w2sMatrix = reinterpret_cast<VMatrix*>(g_pOffsets->matrix());

	const VMatrix w2sMatrix = *_w2sMatrix;
	screen.x = w2sMatrix.m[0][0] * point.x + w2sMatrix.m[0][1] * point.y + w2sMatrix.m[0][2] * point.z + w2sMatrix.m[0][3];
	screen.y = w2sMatrix.m[1][0] * point.x + w2sMatrix.m[1][1] * point.y + w2sMatrix.m[1][2] * point.z + w2sMatrix.m[1][3];

	const float w = w2sMatrix.m[3][0] * point.x + w2sMatrix.m[3][1] * point.y + w2sMatrix.m[3][2] * point.z + w2sMatrix.m[3][3];

	if (w < 0.001f) {
		screen.x *= 100000;
		screen.y *= 100000;
		return true;
	}

	const float invw = 1.0f / w;
	screen.x *= invw;
	screen.y *= invw;

	return false;
}

std::uint8_t* utils::PatternScan(const MODULEINFO& mod, const char* signature)
{
	static auto pattern_to_byte = [](const char* pattern)
	{
		auto bytes = std::vector<int>{};
		const auto start = const_cast<char*>(pattern);
		const auto end = const_cast<char*>(pattern) + strlen(pattern);

		for (auto current = start; current < end; ++current) {
			if (*current == '?') {
				++current;
				if (*current == '?')
					++current;
				bytes.push_back(-1);
			}
			else {
				bytes.push_back(strtoul(current, &current, 16));
			}
		}
		return bytes;
	};

	const auto sizeOfImage = mod.SizeOfImage;
	auto patternBytes = pattern_to_byte(signature);
	const auto scanBytes = reinterpret_cast<std::uint8_t*>(mod.lpBaseOfDll);

	const auto s = patternBytes.size();
	const auto d = patternBytes.data();

	for (auto i = 0ul; i < sizeOfImage - s; ++i) {
		auto found = true;
		for (auto j = 0ul; j < s; ++j) {
			if (scanBytes[i + j] != d[j] && d[j] != -1) {
				found = false;
				break;
			}
		}
		if (found) {
			return &scanBytes[i];
		}
	}
	return nullptr;
}

void utils::KeyIsDown(const int key, const int type, bool* v)
{
	if (type == 0)
	{
		*v = true;
		return;
	}
	
	if (key > -1 && key < 256)
	{
		if (key < 5)
		{
			if (type == 1)
			{
				if (ImGui::IsMouseReleased(key))
					*v = !*v;
			}
			else
				*v = ImGui::IsMouseDown(key);
		}
		else
		{
			if (type == 1)
			{
				if (ImGui::IsKeyReleased(key))
					*v = !*v;
			}
			else
				*v = ImGui::IsKeyDown(key);
		}
	}
}

bool utils::WorldToScreen(const vector& origin, ImVec2& screen)
{
	if (!screen_transform(origin, screen)) {
		int iScreenWidth, iScreenHeight;
		g_pInterfaces->engineclient()->GetScreenSize(iScreenWidth, iScreenHeight);

		screen.x = static_cast<float>(iScreenWidth) / 2.0f + screen.x * static_cast<float>(iScreenWidth) / 2;
		screen.y = static_cast<float>(iScreenHeight) / 2.0f - screen.y * static_cast<float>(iScreenHeight) / 2;

		return true;
	}
	return false;
}

ImVec2 utils::CenterOnScreen()
{
	int iScreenWidth, iScreenHeight;
	g_pInterfaces->engineclient()->GetScreenSize(iScreenWidth, iScreenHeight);
	if (iScreenWidth <= 0 || iScreenHeight <= 0)
		return ImVec2();
	return ImVec2(static_cast<float>(iScreenWidth) / 2.f, static_cast<float>(iScreenHeight) / 2.f);
}

ImVec2 utils::GetScreenSize()
{
	int iScreenWidth, iScreenHeight;
	g_pInterfaces->engineclient()->GetScreenSize(iScreenWidth, iScreenHeight);
	if (iScreenWidth <= 0 || iScreenHeight <= 0)
		return ImVec2();
	return ImVec2(static_cast<float>(iScreenWidth), static_cast<float>(iScreenHeight));
}

qangle utils::GetViewAngles()
{
	qangle viewangles;
	g_pInterfaces->engineclient()->GetViewAngles(viewangles);
	return viewangles;
}

qangle utils::CalculateAngle(const vector& src, const vector& dst)
{
	qangle angles;
	auto delta = src - dst;
	VectorAngles(delta, angles);
	Clamp(delta);
	return angles;
}

bool utils::Clamp(qangle& angles)
{
	qangle a = angles;
	NormalizeAngles(a);
	ClampAngles(a);
	if (!a.IsValid())
		return false;

	angles = a;
	return true;
}

float utils::GetFoV(const qangle& viewangle, const qangle& eye, const qangle& spot)
{
	qangle delta = CalculateAngle(eye, spot) - viewangle;
	if (!Clamp(delta))
		return 0.f;
	return delta.Length2D();
}

float utils::GetRealDistanceFOV(const qangle& viewangle, const qangle& eye, const qangle& spot)
{
	const float distance = eye.DistTo(spot);
	qangle angle = CalculateAngle(eye, spot);
	vector aimingAt;
	AngleVectors(viewangle, &aimingAt);
	aimingAt *= distance;

	vector aimAt;
	AngleVectors(angle, &aimAt);
	aimAt *= distance;

	return aimingAt.DistTo(aimAt);
}

void utils::FixMovement(CUserCmd* cmd, const qangle& angle)
{
	if (!angle.IsValid())
		return;

	vector view_fwd, view_right, view_up, cmd_fwd, cmd_right, cmd_up;
	const qangle viewangles = cmd->viewangles;

	AngleVectors(angle, &view_fwd, &view_right, &view_up);
	AngleVectors(viewangles, &cmd_fwd, &cmd_right, &cmd_up);

	const float v8 = mm_sqrtf(view_fwd.x * view_fwd.x + view_fwd.y * view_fwd.y);
	const float v10 = mm_sqrtf(view_right.x * view_right.x + view_right.y * view_right.y);
	const float v12 = mm_sqrtf(view_up.z * view_up.z);

	const vector norm_view_fwd(1.f / v8 * view_fwd.x, 1.f / v8 * view_fwd.y, 0.f);
	const vector norm_view_right(1.f / v10 * view_right.x, 1.f / v10 * view_right.y, 0.f);
	const vector norm_view_up(0.f, 0.f, 1.f / v12 * view_up.z);

	const float v14 = mm_sqrtf(cmd_fwd.x * cmd_fwd.x + cmd_fwd.y * cmd_fwd.y);
	const float v16 = mm_sqrtf(cmd_right.x * cmd_right.x + cmd_right.y * cmd_right.y);
	const float v18 = mm_sqrtf(cmd_up.z * cmd_up.z);

	const vector norm_cmd_fwd(1.f / v14 * cmd_fwd.x, 1.f / v14 * cmd_fwd.y, 0.f);
	const vector norm_cmd_right(1.f / v16 * cmd_right.x, 1.f / v16 * cmd_right.y, 0.f);
	const vector norm_cmd_up(0.f, 0.f, 1.f / v18 * cmd_up.z);

	const float v22 = norm_view_fwd.x * cmd->forwardmove;
	const float v26 = norm_view_fwd.y * cmd->forwardmove;
	const float v28 = norm_view_fwd.z * cmd->forwardmove;
	const float v24 = norm_view_right.x * cmd->sidemove;
	const float v23 = norm_view_right.y * cmd->sidemove;
	const float v25 = norm_view_right.z * cmd->sidemove;
	const float v30 = norm_view_up.x * cmd->upmove;
	const float v27 = norm_view_up.z * cmd->upmove;
	const float v29 = norm_view_up.y * cmd->upmove;

	cmd->forwardmove = norm_cmd_fwd.x * v24 + norm_cmd_fwd.y * v23 + norm_cmd_fwd.z * v25
		+ (norm_cmd_fwd.x * v22 + norm_cmd_fwd.y * v26 + norm_cmd_fwd.z * v28)
		+ (norm_cmd_fwd.y * v30 + norm_cmd_fwd.x * v29 + norm_cmd_fwd.z * v27);

	cmd->sidemove = norm_cmd_right.x * v24 + norm_cmd_right.y * v23 + norm_cmd_right.z * v25
		+ (norm_cmd_right.x * v22 + norm_cmd_right.y * v26 + norm_cmd_right.z * v28)
		+ (norm_cmd_right.x * v29 + norm_cmd_right.y * v30 + norm_cmd_right.z * v27);

	cmd->upmove = norm_cmd_up.x * v23 + norm_cmd_up.y * v24 + norm_cmd_up.z * v25
		+ (norm_cmd_up.x * v26 + norm_cmd_up.y * v22 + norm_cmd_up.z * v28)
		+ (norm_cmd_up.x * v30 + norm_cmd_up.y * v29 + norm_cmd_up.z * v27);

	cmd->forwardmove = clamp(cmd->forwardmove, -450.f, 450.f);
	cmd->sidemove = clamp(cmd->sidemove, -450.f, 450.f);
	cmd->upmove = clamp(cmd->upmove, -320.f, 320.f);
}

int utils::RandomInt(const int min, const int max)
{
	static RandomIntEx _random = reinterpret_cast<RandomIntEx>(GetProcAddress(HMODULE(g_pMemory->vstdlib().lpBaseOfDll), xorstr_("RandomInt")));

	return _random(min, max);
}

float utils::RandomFloat(const float min, const float max)
{
	static RandomFloatEx _random = reinterpret_cast<RandomFloatEx>(GetProcAddress(HMODULE(g_pMemory->vstdlib().lpBaseOfDll), xorstr_("RandomFloat")));

	return _random(min, max);
}

void utils::RandomSeed(const int vl)
{
	static RandomSeedEx _seed = reinterpret_cast<RandomSeedEx>(GetProcAddress(HMODULE(g_pMemory->vstdlib().lpBaseOfDll), xorstr_("RandomSeed")));

	_seed(vl);
}

DWORD utils::FindHudElement(const char* name)
{
	static FindHudElementEx _element = reinterpret_cast<FindHudElementEx>(g_pOffsets->hudelement());
	return _element(*reinterpret_cast<DWORD**>(g_pOffsets->hud()), name);
}

const char* utils::GetWeaponName(const int index)
{
	switch (index)
	{
	case weapon_deagle:
		return "deagle";
	case weapon_elite:
		return "elite";
	case weapon_fiveseven:
		return "five7";
	case weapon_glock:
		return "glock18";
	case weapon_ak47:
		return "ak47";
	case weapon_aug:
		return "aug";
	case weapon_awp:
		return "awp";
	case weapon_famas:
		return "famas";
	case weapon_g3sg1:
		return "g3sg1";
	case weapon_galilar:
		return "galil";
	case weapon_m249:
		return "m249";
	case weapon_m4a1:
		return "m4a4";
	case weapon_mac10:
		return "mac10";
	case weapon_p90:
		return "p90";
	case weapon_mp5:
		return "mp5-sd";
	case weapon_ump:
		return "ump45";
	case weapon_xm1014:
		return "xm1014";
	case weapon_bizon:
		return "bizon";
	case weapon_mag7:
		return "mag7";
	case weapon_negev:
		return "negev";
	case weapon_sawedoff:
		return "sawed-off";
	case weapon_tec9:
		return "tec9";
	case weapon_hkp2000:
		return "p2000";
	case weapon_mp7:
		return "mp7";
	case weapon_mp9:
		return "mp9";
	case weapon_nova:
		return "nova";
	case weapon_p250:
		return "p250";
	case weapon_scar20:
		return "scar20";
	case weapon_sg556:
		return "sg556";
	case weapon_ssg08:
		return "ssg08";
	case weapon_m4a1_silencer:
		return "m4a1-s";
	case weapon_usp_silencer:
		return "usp-s";
		//case weapon_revolver:
		//	return ("revolver");
	case weapon_cz75a:
		return "cz75";
	default:
		return "";
	}
}

char const* utils::EncryptString(char const* szAddress, uint64 ullCrypt)
{
	if (!szAddress || !*szAddress)
		return nullptr;
	if (!ullCrypt)
		return nullptr;
	if (szAddress[0] == ':')
		return nullptr;
	if (szAddress[0] == '$')
		return nullptr;

	static unsigned char s_chData[256];
	const int nLen = strlen(szAddress);
	if (nLen >= ARRAYSIZE(s_chData) / 2 - 1)
		return nullptr;

	// Copy the address
	s_chData[0] = '$';
	for (int j = 0; j < nLen; ++j)
	{
		const uint8 uiVal = uint8(szAddress[j]) ^ uint8(reinterpret_cast<uint8*>(&ullCrypt)[j % sizeof(uint64)]);
		snprintf((char*)(s_chData + 1 + 2 * j), 3, "%02X", (uint32)uiVal);
	}
	return (char*)s_chData;
}

char const* utils::DecryptString(char const* szAddress, uint64 ullCrypt)
{
	if (!szAddress || !*szAddress)
		return nullptr;
	if (!ullCrypt)
		return nullptr;
	if (szAddress[0] != '$')
		return nullptr;

	static unsigned char s_chData[256];
	const int nLen = strlen(szAddress);
	if (nLen * 2 + 2 >= ARRAYSIZE(s_chData))
		return nullptr;

	// Copy the address
	for (int j = 0; j < nLen / 2; ++j)
	{
		uint32 uiVal;
		if (!sscanf_s(szAddress + 1 + 2 * j, "%02X", &uiVal))
			return nullptr;
		if (uiVal > 0xFF)
			return nullptr;
		uiVal = uint8(uiVal) ^ uint8(reinterpret_cast<uint8*>(&ullCrypt)[j % sizeof(uint64)]);
		if (!uiVal)
			return nullptr;
		s_chData[j] = uiVal;
	}
	s_chData[nLen / 2] = 0;
	return (char*)s_chData;
}