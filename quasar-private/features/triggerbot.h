#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"
#include "../sdk/cs_player.h"
#include "../utils/utils.h"

class triggerbot
{
public:
	void run(CUserCmd* cmd, byte*& packet);

private:
	bool find_target(cs_player*& data);
	cs_player* _local_player = nullptr;
	cs_player* _entity_player = nullptr;
	CUserCmd* _cmd = nullptr;

	bool _in_attack = false;
	float _delay_time = 0.f;
};

extern std::unique_ptr<triggerbot> g_pTriggerbot;