#pragma once

struct DrawModelState_t
{
	studiohdr_t* m_pStudioHdr;
	PVOID* m_pStudioHWData;
	IClientRenderable* m_pRenderable;
	const matrix3x4_t* m_pModelToWorld;
	void* m_decals;
	int	m_drawFlags;
	int	m_lod;
};

struct ModelRenderInfo_t
{
	vector origin;
	qangle angles;
	char pad[0x4];
	IClientRenderable* pRenderable;
	const model_t* pModel;
	const matrix3x4_t* pModelToWorld;
	const matrix3x4_t* pLightingOffset;
	const vector* pLightingOrigin;
	int flags;
	int entity_index;
	int skin;
	int body;
	int hitboxset;
	ModelInstanceHandle_t instance;

	ModelRenderInfo_t(): pad{}, pRenderable(nullptr), pModel(nullptr), pModelToWorld(nullptr), pLightingOffset(nullptr),
	                     pLightingOrigin(nullptr),
	                     flags(0),
	                     entity_index(0), skin(0),
	                     body(0), hitboxset(0),
	                     instance(0)
	{
	}
};

enum OverrideType_t
{
	OVERRIDE_NORMAL = 0,
	OVERRIDE_BUILD_SHADOWS,
	OVERRIDE_DEPTH_WRITE,
};

class IVModelRender
{
public:
	void ForcedMaterialOverride(IMaterial* mat)
	{
		using fn = void(__thiscall*)(void*, IMaterial*, OverrideType_t, int);
		return call_virtual<fn>(this, 1)(this, mat, OVERRIDE_NORMAL, 0);
	}
	void RemoveAllDecals(ModelInstanceHandle_t handle)
	{
		using fn = void(__thiscall*)(void*, ModelInstanceHandle_t);
		return call_virtual<fn>(this, 10)(this, handle);
	}
	void DrawModelExecute(IMatRenderContext* ctx, DrawModelState_t& state, const ModelRenderInfo_t& pInfo, matrix3x4_t* pCustomBoneToWorld = nullptr)
	{
		using fn = void(__thiscall*)(void*, IMatRenderContext*, DrawModelState_t&, const ModelRenderInfo_t&, matrix3x4_t*);
		return call_virtual<fn>(this, 21)(this, ctx, state, pInfo, pCustomBoneToWorld);
	}
};