#include <imgui.h>
#include <imgui_menu.h>
#include <imgui_internal.h>
#include <imgui_render.h>
#include "../globals.h"
#include "../sdk/interfaces.h"
#include "../sdk/cs_player.h"

#include "../utils/configuration.h"
#include "../utils/utils.h"

std::unique_ptr<imgui_menu> g_pMenu;

enum : int
{
	LEGIT = 0,
	VISUALS,
	MISC,
	CONFIG,
	MAX
};

std::vector<std::pair<int,const char*>> vec_weapons =
{
	{ weapon_usp_silencer, "USP-S" },
	{ weapon_hkp2000,"P2000" },
	{ weapon_glock,"Glock-18" },
	{ weapon_elite,"Dual berettas" },
	{ weapon_p250,"P250" },
	{ weapon_fiveseven,"Five7" },
	{ weapon_tec9,"Tec9" },
	{ weapon_cz75a,"CZ75" },
	{ weapon_deagle,"Deagle" },
	{ weapon_nova,"Nova" },
	{ weapon_xm1014,"Xm1014" },
	{ weapon_sawedoff,"Sawed-off" },
	{ weapon_mag7,"Mag7" },
	{ weapon_m249,"M249" },
	{ weapon_negev,"Negev" },
	{ weapon_mp9,"MP9" },
	{ weapon_mac10,"Mac10" },
	{ weapon_mp7,"MP7" },
	{ weapon_mp5,"MP5-SD" },
	{ weapon_ump,"UMP45" },
	{ weapon_p90,"P90" },
	{ weapon_bizon,"Bizon" },
	{ weapon_famas, "Famas" },
	{ weapon_galilar, "Galil" },
	{ weapon_m4a1, "M4A4" },
	{ weapon_m4a1_silencer, "M4A1-S" },
	{ weapon_ak47,"AK47" },
	{ weapon_ssg08, "SSG08" },
	{ weapon_aug, "Aug" },
	{ weapon_sg556, "SG556" },
	{ weapon_awp, "Awp" },
	{ weapon_scar20, "Scar20" },
	{ weapon_g3sg1, "G3SG1" },
};

std::vector<std::pair<int, const char*>> vec_hitbox =
{
	{ HITBOX_HEAD, "Head" },
	{ HITBOX_NECK, "Neck" },
	{ HITBOX_STOMACH, "Stomach" },
	{ HITBOX_CHEST, "Chest" },
	{ HITBOX_UPPER_CHEST, "Upper chest" },
};

imgui_menu::imgui_menu()
{
	// enemy
	globals::visuals::team_t[0].color[int(PCOLOR::BOX_V)] = { 0.8f,0.0f,0.f,1.f };
	globals::visuals::team_t[0].color[int(PCOLOR::BOX_I)] = { 0.0f,0.0f,0.0f,1.f };
	globals::visuals::team_t[0].color[int(PCOLOR::CHAMS_V)] = { 0.8f,0.0f,0.f,1.f };
	globals::visuals::team_t[0].color[int(PCOLOR::CHAMS_I)] = { 0.0f,0.0f,0.0f,1.f };
	globals::visuals::team_t[0].color[int(PCOLOR::STEPS)] = { 1.0f,1.0f,1.0f,1.0f };
	globals::visuals::team_t[0].color[int(PCOLOR::NAME)] = { 0.9f,0.9f,0.9f,1.0f };
	globals::visuals::team_t[0].color[int(PCOLOR::WEAPON)] = { 0.9f,0.9f,0.9f,1.0f };
	globals::visuals::team_t[0].color[int(PCOLOR::CHAMS_BT)] = { 0.6f,0.6f,0.6f,0.3f };

	// ally
	globals::visuals::team_t[1].color[int(PCOLOR::BOX_V)] = { 0.f,0.8f,0.f,1.f };
	globals::visuals::team_t[1].color[int(PCOLOR::BOX_I)] = { 0.0f,0.0f,0.0f,1.f };
	globals::visuals::team_t[1].color[int(PCOLOR::CHAMS_V)] = { 0.f,0.8f,0.f,1.f };
	globals::visuals::team_t[1].color[int(PCOLOR::CHAMS_I)] = { 0.0f,0.0f,0.0f,1.f };
	globals::visuals::team_t[1].color[int(PCOLOR::STEPS)] = { 1.0f,1.0f,1.0f,1.0f };
	globals::visuals::team_t[1].color[int(PCOLOR::NAME)] = { 0.9f,0.9f,0.9f,1.0f };
	globals::visuals::team_t[1].color[int(PCOLOR::WEAPON)] = { 0.9f,0.9f,0.9f,1.0f };
	globals::visuals::team_t[1].color[int(PCOLOR::CHAMS_BT)] = { 0.6f,0.6f,0.6f,0.3f };

	globals::visuals::color[int(COLOR::FOV)] = { 0.0f,1.0f,0.0f,0.1f };
	globals::visuals::color[int(COLOR::CROSSHAIR)] = { 1.0f,1.0f,1.0f,1.0f };
	globals::visuals::color[int(COLOR::RECOIL)] = { 1.0f,0.0f,0.0f,1.0f };

	
}

void imgui_menu::run()
{
	if (!globals::menu::active)
		return;

	const ImGuiWindowFlags flags = ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoTitleBar;
#ifdef _DEBUG
	ImGui::ShowDemoWindow(nullptr);
#endif
	ImGui::SetNextWindowPos(globals::menu::pos, ImGuiCond_Once);
	ImGui::SetNextWindowSize(ImVec2(800, 500), ImGuiCond_Once);
	ImGui::Begin(xorstr_("Quasar [0.2.6]"), nullptr, flags | ImGuiWindowFlags_NoBackground);
	{
		ImVec2 winpos = ImVec2(ImGui::GetWindowPos().x + 4,ImGui::GetWindowPos().y + 1);
		ImVec2 winsize = ImGui::GetWindowSize();
		ImDrawList* drawlist = ImGui::GetWindowDrawList();

		draw_background(winpos, winsize, drawlist);
		ImGui::PushAllowKeyboardFocus(false);
		ImGui::BeginChild(xorstr_("tabs"), ImVec2(0, 40));
		{
			ImGui::SetCursorPosX(ImGui::GetCursorPosX() + 150);
			ImGui::QTabs({ xorstr_("Legit"),xorstr_("Visuals"),xorstr_("Misc"),xorstr_("Config") }, ImVec2(125,40), &globals::menu::index);
		}
		ImGui::EndChild();
		
		ImGui::BeginChild(xorstr_("sub"), ImVec2(150, 0));
		{
			switch (globals::menu::index)
			{
			case LEGIT:
			{
				ImGui::QSubTabs({ xorstr_("Aimbot"), xorstr_("Misc") }, &globals::menu::sub_index1, 43);
				break;
			}
			case VISUALS:
			{
				ImGui::QSubTabs({ xorstr_("Enemy"),xorstr_("Team"),xorstr_("Misc") }, &globals::menu::sub_index2, 43);
				break;
			}
			case MISC:
			{
				break;
			}
			case CONFIG:
			{
				break;
			}
			default:
				break;
			}
			
		}
		ImGui::EndChild();

		ImGui::SameLine();
		ImGui::BeginChild(xorstr_("main"), ImVec2(0, 0));
		{
			switch (globals::menu::index)
			{
			case LEGIT:
			{
				draw_legit();
				break;
			}
			case VISUALS:
			{
				draw_visuals();
				break;
			}
			case MISC:
			{
				draw_misc();
				break;
			}
			case CONFIG:
			{
				draw_config();
				break;
			}
			default:
				break;
			}
		}
		ImGui::EndChild();
		ImGui::PopAllowKeyboardFocus();
	}
	ImGui::End();
	
}

void imgui_menu::toggle()
{
	if (!globals::menu::active && g_pInterfaces->clientstate()->IsInGame())
	{
		cs_player* local_player = cs_player::get_local_player();
		if (local_player && local_player->is_alive())
		{
			_itemindex = g_pPlayerList->get_player_data(g_pInterfaces->engineclient()->GetLocalPlayer())->itemindex;
		}
	}
	if (globals::hotkey::menu <= KEY_FIRST || globals::hotkey::menu > KEY_LAST)
		globals::hotkey::menu = KEY_INSERT;

	const int menu_key = g_pInterfaces->inputsystem()->ButtonCodeToVirtualKey((ButtonCode_t)globals::hotkey::menu);
	if (ImGui::IsKeyPressed(menu_key, false))
	{
		globals::menu::active = !globals::menu::active;
		globals::config::arr = g_pConfig->getarray();
	}
	const std::string wt = std::string(xorstr_("Quasar Build time [")) + __DATE__ + "] [" + __TIME__ + "]";
	ImGui::GetForegroundDrawList()->AddText(ImVec2(10, 5), IM_COL32_WHITE, wt.c_str());
}

void imgui_menu::draw_legit()
{
	const float width = ImGui::GetContentRegionMax().x / 2;
	const float e_width = width - 30;
	switch (globals::menu::sub_index1)
	{
	case 0:
	{
		ImGui::BeginChild(xorstr_("global"), ImVec2(width, 0), true);
		{
			ImGui::QCheckbox(xorstr_("Active (global)"), &globals::aimbot::active);
			ImGui::QCheckbox(xorstr_("Smoke check"), &globals::aimbot::smoke_check);
			ImGui::QCheckbox(xorstr_("Flash check"), &globals::aimbot::flash_check);
			ImGui::QCheckbox(xorstr_("Jump check"), &globals::aimbot::jump_check);
			ImGui::QCheckbox(xorstr_("Zoom check"), &globals::aimbot::zoom_check);
			ImGui::QCheckbox(xorstr_("Friendly fire"), &globals::aimbot::ffa_mode);
			ImGui::QPCombo(xorstr_("Weapon"), 150, vec_weapons, &_itemindex);
			ImGui::QCombo(xorstr_("Activation type"), 150, { xorstr_("Always"), xorstr_("Hold key"), xorstr_("Toggle key") }, &globals::aimbot::key_mode);
		}
		ImGui::EndChild();
		ImGui::SameLine();
		ImGui::BeginChild(xorstr_("weapon"), ImVec2(width - 15, 0), true);
		{
			ImGui::QCheckbox(xorstr_("Active weapon"), &globals::aimbot::weapon_t[_itemindex].active);
			ImGui::QCheckbox(xorstr_("Smart hitbox"), &globals::aimbot::weapon_t[_itemindex].smart);
			ImGui::QCheckbox(xorstr_("Multipoints"), &globals::aimbot::weapon_t[_itemindex].multipoints);
			ImGui::QCheckbox(xorstr_("Auto delay"), &globals::aimbot::weapon_t[_itemindex].auto_delay);
			ImGui::QCheckbox(xorstr_("Silent first bullet"), &globals::aimbot::weapon_t[_itemindex].first_bullet_silent);
			if (!globals::aimbot::weapon_t[_itemindex].smart)
				ImGui::QPCombo(xorstr_("Hitbox"), 150, vec_hitbox, &globals::aimbot::weapon_t[_itemindex].hitbox);

			ImGui::QSliderFloat(xorstr_("FoV"), e_width, &globals::aimbot::weapon_t[_itemindex].fov, 0.5f, 60.f, xorstr_("%.1f"));

			ImGui::QSliderFloat(xorstr_("Second FoV"), e_width, &globals::aimbot::weapon_t[_itemindex].second_fov, 0.5f, 60.f, xorstr_("%.1f"));

			ImGui::QSliderFloat(xorstr_("Smooth"), e_width, &globals::aimbot::weapon_t[_itemindex].smooth, 1.f, 20.f, xorstr_("%.1f"));

			if (!globals::aimbot::weapon_t[_itemindex].auto_delay && !globals::aimbot::weapon_t[_itemindex].first_bullet_silent)
				ImGui::QSliderFloat(xorstr_("Delay before first shot"), e_width, &globals::aimbot::weapon_t[_itemindex].first_shot_delay, 0.f, 1.f);

			ImGui::QSliderFloat(xorstr_("Delay after kill"), e_width, &globals::aimbot::weapon_t[_itemindex].kill_delay, 0.f, 1.f);
			if (globals::aimbot::weapon_t[_itemindex].first_bullet_silent)
				ImGui::QSliderInt(xorstr_("Silent chance"), e_width, &globals::aimbot::weapon_t[_itemindex].chance, 30, 100);
		}
		ImGui::EndChild();
		break;
	}
	case 1:
	{
		ImGui::BeginChild(xorstr_("triggerbot/rcs"), ImVec2(width, 0), true);
		{
			ImGui::QCheckbox(xorstr_("Triggerbot active"), &globals::triggerbot::active);
			ImGui::QCheckbox(xorstr_("Smoke check"), &globals::triggerbot::smoke_check);
			ImGui::QCheckbox(xorstr_("Flash check"), &globals::triggerbot::flash_check);
			ImGui::QCheckbox(xorstr_("Zoom check"), &globals::triggerbot::zoom_check);
			ImGui::QCombo(xorstr_("Activation type"), 150, { xorstr_("Always"), xorstr_("Hold key"), xorstr_("Toggle key") }, &globals::triggerbot::key_mode);
			ImGui::Separator();
			ImGui::QCheckbox(xorstr_("RCS"), &globals::rcs::active);
			ImGui::QSliderFloat(xorstr_("RCS bullet start"), width - 15, &globals::rcs::start, 1, 10, xorstr_("%.0f"));
			ImGui::QSliderFloat(xorstr_("RCS pitch"), width - 15, &globals::rcs::pitch, 0.f, 2.f, xorstr_("%.2f"));
			ImGui::QSliderFloat(xorstr_("RCS yaw"), width - 15, &globals::rcs::yaw, 0.f, 2.f, xorstr_("%.2f"));
		}
		ImGui::EndChild();
		ImGui::SameLine();
		ImGui::BeginChild(xorstr_("backtrack"), ImVec2(width - 15, 0), true);
		{
			ImGui::QCheckbox(xorstr_("Backtrack active"), &globals::backtrack::active);
			ImGui::QSliderInt(xorstr_("Backtrack time"),e_width, &globals::backtrack::time, 100, 200);
		}
		ImGui::EndChild();
		break;
	}
	default:
		break;
	}
}

void imgui_menu::draw_visuals()
{
	const float width = ImGui::GetContentRegionMax().x;
	const float e_width = width - 20;

	switch (globals::menu::sub_index2)
	{
	case 0:
	{
		ImGui::BeginChild(xorstr_("player 1"), ImVec2(width - 7, 0), true);
		{
			ImGui::QCheckbox(xorstr_("Active"), &globals::visuals::team_t[0].active);
			ImGui::QCheckbox(xorstr_("Visible check"), &globals::visuals::team_t[0].visible_check);

			ImGui::QCheckbox(xorstr_("Box"), &globals::visuals::team_t[0].box);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##col_box_1"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::BOX_V)]);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##col_box_2"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::BOX_I)]);

			ImGui::QCheckbox(xorstr_("Name"), &globals::visuals::team_t[0].name);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colname"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::NAME)]);

			ImGui::QCheckbox(xorstr_("Weapon"), &globals::visuals::team_t[0].weapon);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colweapon"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::WEAPON)]);

			ImGui::QCheckbox(xorstr_("Healthbar"), &globals::visuals::team_t[0].health);
			ImGui::QCheckbox(xorstr_("Steps"), &globals::visuals::team_t[0].steps);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colsteps"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::STEPS)]);

			ImGui::QSliderFloat(xorstr_("Corner size"), e_width, &globals::visuals::team_t[0].corner_size, 10, 50, xorstr_("%.0f"));
			ImGui::QSliderFloat(xorstr_("Steps size"), e_width, &globals::visuals::team_t[0].steps_size, 10.f, 50.f, xorstr_("%.0f"));

			ImGui::QCheckbox(xorstr_("Chams active"), &globals::visuals::team_t[0].chams);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colchams_1"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::CHAMS_V)]);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colchams_2"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::CHAMS_I)]);

			ImGui::QCheckbox(xorstr_("Chams visible check"), &globals::visuals::team_t[0].chams_visible_check);
			ImGui::QCheckbox(xorstr_("Chams flat"), &globals::visuals::team_t[0].chams_flat);

			ImGui::QCheckbox(xorstr_("Chams backtrack"), &globals::visuals::team_t[0].chams_backtrack);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colchams_bt"), (float*)&globals::visuals::team_t[0].color[int(PCOLOR::CHAMS_BT)]);
		}
		ImGui::EndChild();
		break;
	}
	case 1:
	{
		ImGui::BeginChild(xorstr_("player 2"), ImVec2(width - 7, 0), true);
		{
			ImGui::QCheckbox(xorstr_("Active"), &globals::visuals::team_t[1].active);
			ImGui::QCheckbox(xorstr_("Visible check"), &globals::visuals::team_t[1].visible_check);

			ImGui::QCheckbox(xorstr_("Box"), &globals::visuals::team_t[1].box);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##col_box_1"), (float*)&globals::visuals::team_t[1].color[int(PCOLOR::BOX_V)]);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##col_box_2"), (float*)&globals::visuals::team_t[1].color[int(PCOLOR::BOX_I)]);

			ImGui::QCheckbox(xorstr_("Name"), &globals::visuals::team_t[1].name);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colname"), (float*)&globals::visuals::team_t[1].color[int(PCOLOR::NAME)]);

			ImGui::QCheckbox(xorstr_("Weapon"), &globals::visuals::team_t[1].weapon);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colweapon"), (float*)&globals::visuals::team_t[1].color[int(PCOLOR::WEAPON)]);

			ImGui::QCheckbox(xorstr_("Healthbar"), &globals::visuals::team_t[1].health);
			ImGui::QCheckbox(xorstr_("Steps"), &globals::visuals::team_t[1].steps);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colsteps"), (float*)&globals::visuals::team_t[1].color[int(PCOLOR::STEPS)]);

			ImGui::QSliderFloat(xorstr_("Corner size"), e_width, &globals::visuals::team_t[1].corner_size, 10, 50, xorstr_("%.0f"));
			ImGui::QSliderFloat(xorstr_("Steps size"), e_width, &globals::visuals::team_t[1].steps_size, 10.f, 50.f, xorstr_("%.0f"));

			ImGui::QCheckbox(xorstr_("Chams active"), &globals::visuals::team_t[1].chams);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colchams_1"), (float*)&globals::visuals::team_t[1].color[int(PCOLOR::CHAMS_V)]);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##colchams_2"), (float*)&globals::visuals::team_t[1].color[int(PCOLOR::CHAMS_I)]);

			ImGui::QCheckbox(xorstr_("Chams visible check"), &globals::visuals::team_t[1].chams_visible_check);
			ImGui::QCheckbox(xorstr_("Chams flat"), &globals::visuals::team_t[1].chams_flat);
		}
		ImGui::EndChild();
		break;
	}
	case 2:
	{
		ImGui::BeginChild(xorstr_("wut_misc"), ImVec2(width - 7, 0), true);
		{
			ImGui::QCheckbox(xorstr_("Draw FoV"), &globals::visuals::fov);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##col_fov"), (float*)&globals::visuals::color[int(COLOR::FOV)]);
			ImGui::QCheckbox(xorstr_("Sniper crosshair"), &globals::visuals::sniper_crosshair);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##col_crosshair"), (float*)&globals::visuals::color[int(COLOR::CROSSHAIR)]);
			ImGui::QCheckbox(xorstr_("Recoil crosshair"), &globals::visuals::recoil_crosshair);
			ImGui::SameLine();
			ImGui::QColorEdit4(xorstr_("##col_recoil"), (float*)&globals::visuals::color[int(COLOR::RECOIL)]);
		}
		ImGui::EndChild();
		break;
	}
	default:
		break;
	}
}

void imgui_menu::draw_misc()
{
	const float width = ImGui::GetContentRegionMax().x / 2;
	const float e_width = width - 30;
	ImGui::BeginChild(xorstr_("misc_"), ImVec2(width, 0), true);
	{
		ImGui::QCheckbox(xorstr_("Reveal ranks"), &globals::misc::rank);
		ImGui::QCheckbox(xorstr_("Bunnyhop"), &globals::misc::bunnyhop);
		ImGui::QCheckbox(xorstr_("Bunnyhop autostrafe"), &globals::misc::strafe);
		ImGui::QCheckbox(xorstr_("Auto accept"), &globals::misc::autoaccept);
		if (globals::misc::autoaccept)
			ImGui::QCheckbox(xorstr_("Restore window"), &globals::misc::res_window);
		ImGui::QCheckbox(xorstr_("Hitmarker"), &globals::misc::hitmarker);
	}
	ImGui::EndChild();
	ImGui::SameLine();
	ImGui::BeginChild(xorstr_("radar"), ImVec2(width - 15, 0), true);
	{
		ImGui::QCheckbox(xorstr_("Radar"), &globals::radar::active);
		ImGui::QSliderFloat(xorstr_("Radar size"), e_width,&globals::radar::size, 100.f, 300.f, xorstr_("%.0f"));
		ImGui::QSliderFloat(xorstr_("Radar zoom"), e_width, &globals::radar::zoom, 0.5f, 1.5f, xorstr_("%.2f"));
		ImGui::QSliderFloat(xorstr_("Radar alpha"), e_width, &globals::radar::alpha, 0.5f, 1.f, xorstr_("%.2f"));
		ImGui::QSliderFloat(xorstr_("Radar scale icons"), e_width , &globals::radar::icon_scale, 2.5f, 10.f, xorstr_("%.1f"));
	}
	ImGui::EndChild();
	
}

void imgui_menu::draw_config()
{
	const float width = ImGui::GetContentRegionMax().x / 2;
	const float e_width = width - 30;
	ImGui::BeginChild(xorstr_("configs"), ImVec2(width, 0), true);
	{
		const float w = ImGui::GetWindowContentRegionWidth() * 0.5f - ImGui::GetStyle().FramePadding.x;
		//const float h = ImGui::GetContentRegionMax().y * 0.5f;
		ImGui::QListBox(xorstr_("##Configs"), &globals::config::index, globals::config::arr, 0);
		const bool valid = !globals::config::arr.empty();

		if (ImGui::QButton(xorstr_("Save"), ImVec2(w, 25.f)))
		{
			if (valid)
				g_pConfig->save(globals::config::arr[globals::config::index]);
		}
		ImGui::SameLine();
		if ((globals::menu::load = ImGui::QButton(xorstr_("Load"), ImVec2(w, 25.f))))
		{
			if (valid)
				g_pConfig->load(globals::config::arr[globals::config::index]);
		}

		if (ImGui::QButton(xorstr_("Refresh"), ImVec2(w, 25.f)))
		{
			globals::config::arr.clear();
			globals::config::arr = g_pConfig->getarray();
		}
		ImGui::SameLine();
		if (ImGui::QButton(xorstr_("Delete"), ImVec2(w, 25.f)))
		{
			if (valid)
			{
				g_pConfig->del(globals::config::index);
				if (globals::config::index > 0)
					globals::config::index--;
			}

		}
	}
	ImGui::EndChild();
	ImGui::SameLine();
	ImGui::BeginChild(xorstr_("buttons"), ImVec2(width - 15, 0), true);
	{
		const float w = ImGui::GetWindowContentRegionWidth() * 0.5f - ImGui::GetStyle().FramePadding.x;
		ImGui::InputTextEx(xorstr_("##Name"), nullptr, globals::config::name, 32, ImVec2(ImGui::GetWindowContentRegionWidth(), 0), 0, nullptr, nullptr);
		if (ImGui::Button(xorstr_("Create"), ImVec2(w, 25.f)))
		{
			if (strlen(globals::config::name) > 3)
			{
				g_pConfig->save(g_pUtils->UTF8ToWstring(globals::config::name));
			}
		}
		ImGui::SameLine();
		if (ImGui::Button(xorstr_("Clear"), ImVec2(w, 25.f)))
			memset(globals::config::name, 0, sizeof globals::config::name);
		ImGui::Separator();
		ImGui::QHotkey("Menu key", &globals::hotkey::menu, ImVec2(w, 25.f));
		ImGui::QHotkey("Aimbot key", &globals::hotkey::aimbot, ImVec2(w, 0));
		ImGui::QHotkey("Triggerbot key", &globals::hotkey::triggerbot, ImVec2(w, 0));
	}
	ImGui::EndChild();
}

void imgui_menu::draw_background(const ImVec2& pos, const ImVec2& size, ImDrawList* drawlist) const
{
	const ImU32 bg_tl = ImGui::GetColorU32(g_pImRender->get_color(ImCustomColor_WindowBg_TL));
	const ImU32 bg_tr = ImGui::GetColorU32(g_pImRender->get_color(ImCustomColor_WindowBg_TR));
	const ImU32 bg_br = ImGui::GetColorU32(g_pImRender->get_color(ImCustomColor_WindowBg_BR));
	const ImU32 bg_bl = ImGui::GetColorU32(g_pImRender->get_color(ImCustomColor_WindowBg_BL));
	const ImU32 line = ImGui::GetColorU32(g_pImRender->get_color(ImCustomColor_Line));
	drawlist->AddRectFilledMultiColor(pos, pos + size, bg_tl, bg_tr, bg_br, bg_bl);
	drawlist->AddRect(pos, pos + size - ImVec2(8, 2), IM_COL32_BLACK);
	drawlist->AddRect(pos + ImVec2(1,1), pos + size - ImVec2(9, 3), line);
	drawlist->AddLine(ImVec2(pos.x + 2, pos.y + 48), ImVec2(pos.x + size.x - 10, pos.y + 48), line);
	drawlist->AddLine(ImVec2(pos.x + 152, pos.y + 49), ImVec2(pos.x + 152, pos.y + size.y - 4), line);
}
