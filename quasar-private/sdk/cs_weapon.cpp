#include "cs_weapon.h"
#include "netvars.h"
#include "enums.h"
#include "offsets.h"
#include "interfaces.h"

short cs_weapon::itemindex()
{
	return *reinterpret_cast<short*>(reinterpret_cast<DWORD>(this) + netvars::cs_weapon::m_iItemDefinitionIndex);
}

int cs_weapon::ammo()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_weapon::m_iClip1);
}

int cs_weapon::type()
{
	const weapondata_t* weapon_data = g_pInterfaces->weaponsystem()->GetWeaponData(this->itemindex());
	const int weapon_type = weapon_data->WeaponType;
	const std::string bullet_type = weapon_data->szBulletType;
	const std::string weapon_class = weapon_data->szWeaponClass;
	
	if (weapon_type == 0 && bullet_type.empty() && weapon_class.empty())
		return (int)CSWeaponType::WEAPONTYPE_C4;
	if (weapon_type == 4 && weapon_class.find(xorstr_("primary_sniper")) != std::string::npos)
		return (int)CSWeaponType::WEAPONTYPE_SNIPER_RIFLE;
	if (weapon_type == 5 && weapon_class.find(xorstr_("primary_mg")) != std::string::npos)
		return (int)CSWeaponType::WEAPONTYPE_MACHINEGUN;

	return weapon_type;
}

float cs_weapon::nextprimaryattack()
{
	return *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_weapon::m_flNextPrimaryAttack);
}

float cs_weapon::lastshottime()
{
	return *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_weapon::m_fLastShotTime);
}

bool cs_weapon::is_reloading()
{
	return *reinterpret_cast<bool*>(reinterpret_cast<DWORD>(this) + g_pOffsets->reloading());
}

bool cs_weapon::can_fire(const float nextattack)
{
	if (this->is_reloading() || this->ammo() < 1)
		return false;

	return nextprimaryattack() < globals::vars::server_time && nextattack < globals::vars::server_time;
}

float cs_weapon::get_inaccuracy()
{
	using fn = float(__thiscall*)(void*);
	return call_virtual<fn>(this, 482)(this);
}

float cs_weapon::get_spread()
{
	using fn = float(__thiscall*)(void*);
	return call_virtual<fn>(this, 452)(this);
}

void cs_weapon::update_accuracy_penalty()
{
	using fn = void(__thiscall*)(void*);
	call_virtual<fn>(this, 482)(this); // +2
}

bool cs_weapon::is_shootable()
{
	switch (this->itemindex())
	{
	case weapon_deagle:
	case weapon_elite:
	case weapon_fiveseven:
	case weapon_glock:
	case weapon_ak47:
	case weapon_aug:
	case weapon_awp:
	case weapon_famas:
	case weapon_g3sg1:
	case weapon_galilar:
	case weapon_m249:
	case weapon_m4a1:
	case weapon_mac10:
	case weapon_p90:
	case weapon_mp5:
	case weapon_ump:
	case weapon_xm1014:
	case weapon_bizon:
	case weapon_mag7:
	case weapon_negev:
	case weapon_sawedoff:
	case weapon_tec9:
	case weapon_taser:
	case weapon_hkp2000:
	case weapon_mp7:
	case weapon_mp9:
	case weapon_nova:
	case weapon_p250:
	case weapon_scar20:
	case weapon_sg556:
	case weapon_ssg08:
	case weapon_m4a1_silencer:
	case weapon_usp_silencer:
	//case weapon_revolver:
	case weapon_cz75a:
		return true;
	default:
		return false;
	}
}

bool cs_weapon::is_cz75()
{
	return this->itemindex() == weapon_cz75a;
}

bool cs_weapon::is_revolver()
{
	return this->itemindex() == weapon_revolver;
}

bool cs_weapon::is_pistol()
{
	switch (this->itemindex())
	{
	case weapon_deagle:
	case weapon_elite:
	case weapon_fiveseven:
	case weapon_glock:
	case weapon_tec9:
	case weapon_hkp2000:
	case weapon_p250:
	case weapon_usp_silencer:
		return true;
	default:
		return false;
	}
}

bool cs_weapon::is_shotgun()
{
	switch (this->itemindex())
	{
	case weapon_xm1014:
	case weapon_mag7:
	case weapon_sawedoff:
	case weapon_nova:
		return true;
	default:
		return false;
	}
}

bool cs_weapon::is_heavy()
{
	switch (this->itemindex())
	{
	case weapon_m249:
	case weapon_negev:
		return true;
	default:
		return false;
	}
}

bool cs_weapon::is_smg()
{
	switch (this->itemindex())
	{
	case weapon_mac10:
	case weapon_p90:
	case weapon_mp5:
	case weapon_ump:
	case weapon_bizon:
	case weapon_mp7:
	case weapon_mp9:
		return true;
	default:
		return false;
	}
}

bool cs_weapon::is_rifle()
{
	switch (this->itemindex())
	{
	case weapon_ak47:
	case weapon_aug:
	case weapon_famas:
	case weapon_galilar:
	case weapon_m4a1:
	case weapon_sg556:
	case weapon_m4a1_silencer:
		return true;
	default:
		return false;
	}
}

bool cs_weapon::is_sniper()
{
	switch (this->itemindex())
	{
	case weapon_awp:
	case weapon_ssg08:
		return true;
	default:
		return false;
	}
}

bool cs_weapon::is_autosniper()
{
	switch (this->itemindex())
	{
	case weapon_g3sg1:
	case weapon_scar20:
		return true;
	default:
		return false;
	}
}

const char* cs_weapon::name()
{
	switch (this->itemindex())
	{
	case weapon_deagle:
		return "deagle";
	case weapon_elite:
		return "elite";
	case weapon_fiveseven:
		return "five7";
	case weapon_glock:
		return "glock18";
	case weapon_ak47:
		return "ak47";
	case weapon_aug:
		return "aug";
	case weapon_awp:
		return "awp";
	case weapon_famas:
		return "famas";
	case weapon_g3sg1:
		return "g3sg1";
	case weapon_galilar:
		return "galil";
	case weapon_m249:
		return "m249";
	case weapon_m4a1:
		return "m4a4";
	case weapon_mac10:
		return "mac10";
	case weapon_p90:
		return "p90";
	case weapon_mp5:
		return "mp5-sd";
	case weapon_ump:
		return "ump45";
	case weapon_xm1014:
		return "xm1014";
	case weapon_bizon:
		return "bizon";
	case weapon_mag7:
		return "mag7";
	case weapon_negev:
		return "negev";
	case weapon_sawedoff:
		return "sawed-off";
	case weapon_tec9:
		return "tec9";
	case weapon_hkp2000:
		return "p2000";
	case weapon_mp7:
		return "mp7";
	case weapon_mp9:
		return "mp9";
	case weapon_nova:
		return "nova";
	case weapon_p250:
		return "p250";
	case weapon_scar20:
		return "scar20";
	case weapon_sg556:
		return "sg556";
	case weapon_ssg08:
		return "ssg08";
	case weapon_flashbang:
		return "flashbang";
	case weapon_hegrenade:
		return "he-grenade";
	case weapon_smokegrenade:
		return "smoke grenade";
	case weapon_molotov:
		return "molotov";
	case weapon_decoy:
		return "decoy";
	case weapon_incgrenade:
		return "inc-grenade";
	case weapon_c4:
		return "c4";
	case weapon_healthshot:
		return "healthshot";
	case weapon_m4a1_silencer:
		return "m4a1-s";
	case weapon_usp_silencer:
		return "usp-s";
	case weapon_revolver:
		return "revolver";
	case weapon_cz75a:
		return "cz75";
	case weapon_knife_ct:
	case weapon_knife_t:
	case weapon_knife_bayonet:
	case weapon_knife_css:
	case weapon_knife_flip:
	case weapon_knife_gut:
	case weapon_knife_karambit:
	case weapon_knife_m9_bayonet:
	case weapon_knife_tactical:
	case weapon_knife_falchion:
	case weapon_knife_survival_bowie:
	case weapon_knife_butterfly:
	case weapon_knife_push:
	case weapon_knife_ursus:
	case weapon_knife_gypsy_jackknife:
	case weapon_knife_stiletto:
	case weapon_knife_widowmaker:
		return "knife";
	default:
		return "";
	}
}