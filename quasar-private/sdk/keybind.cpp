//===== Copyright � 1996-2005, Valve Corporation, All rights reserved. ======//
//
// Purpose: 
//
// $NoKeywords: $
//
//===========================================================================//

#include "keybind.h"
#include "interfaces.h"

std::unique_ptr<CKeyBindings> keybind;

//-----------------------------------------------------------------------------
// Set a key binding
//-----------------------------------------------------------------------------
void CKeyBindings::SetBinding(ButtonCode_t code, const char* pBinding)
{
	if (code == BUTTON_CODE_INVALID || code == KEY_NONE)
		return;
	
	// free old bindings
	if (!m_KeyInfo[code].empty())
	{
		// Exactly the same, don't re-bind and fragment memory
		if (!_stricmp(m_KeyInfo[code].c_str(), pBinding))
			return;
		
		m_KeyInfo[code] = "";
	}

	// allocate memory for new binding
	m_KeyInfo[code] = pBinding;
}

void CKeyBindings::SetBinding(const char* pButtonName, const char* pBinding)
{
	ButtonCode_t code = g_pInterfaces->inputsystem()->StringToButtonCode(pButtonName);
	SetBinding(code, pBinding);
}

void CKeyBindings::Unbind(ButtonCode_t code)
{
	if (code != KEY_NONE && code != BUTTON_CODE_INVALID)
	{
		m_KeyInfo[code] = "";
	}
}

void CKeyBindings::Unbind(const char* pButtonName)
{
	ButtonCode_t code = g_pInterfaces->inputsystem()->StringToButtonCode(pButtonName);
	Unbind(code);
}

void CKeyBindings::UnbindAll()
{
	for (int i = 0; i < BUTTON_CODE_LAST; i++)
	{
		m_KeyInfo[i] = "";
	}
}


//-----------------------------------------------------------------------------
// Count number of lines of bindings we'll be writing
//-----------------------------------------------------------------------------
int CKeyBindings::GetBindingCount() const
{
	int	nCount = 0;
	for (int i = 0; i < BUTTON_CODE_LAST; i++)
	{
		if (m_KeyInfo[i].length())
		{
			nCount++;
		}
	}

	return nCount;
}


//-----------------------------------------------------------------------------
// Writes lines containing "bind key value"
//-----------------------------------------------------------------------------
//void CKeyBindings::WriteBindings(CUtlBuffer& buf)
//{
//	for (int i = 0; i < BUTTON_CODE_LAST; i++)
//	{
//		if (m_KeyInfo[i].Length())
//		{
//			const char* pButtonCode = g_pInputSystem->ButtonCodeToString((ButtonCode_t)i);
//			buf.Printf("bind \"%s\" \"%s\"\n", pButtonCode, m_KeyInfo[i].Get());
//		}
//	}
//}


//-----------------------------------------------------------------------------
// Returns the keyname to which a binding string is bound.  E.g., if 
// TAB is bound to +use then searching for +use will return "TAB"
//-----------------------------------------------------------------------------
const char* CKeyBindings::ButtonNameForBinding(const char* pBinding)
{
	const char* pBind = pBinding;
	if (pBinding[0] == '+')
	{
		++pBind;
	}

	for (int i = 0; i < BUTTON_CODE_LAST; i++)
	{
		if (!m_KeyInfo[i].length())
			continue;

		if (m_KeyInfo[i][0] == '+')
		{
			if (!_stricmp(&m_KeyInfo[i][1], pBind))
				return g_pInterfaces->inputsystem()->ButtonCodeToString((ButtonCode_t)i);
		}
		else
		{
			if (!_stricmp(m_KeyInfo[i].c_str(), pBind))
				return g_pInterfaces->inputsystem()->ButtonCodeToString((ButtonCode_t)i);
		}
	}

	return NULL;
}

const char* CKeyBindings::GetBindingForButton(ButtonCode_t code)
{
	if (m_KeyInfo[code].empty())
		return NULL;

	return m_KeyInfo[code].c_str();
}