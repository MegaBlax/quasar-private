#pragma once
#include "../stdafx.h"

class CSGameRulesProxy
{
public:
	CSGameRulesProxy();
	bool init();
	bool IsValveDS() const;
private:
	DWORD thisptr;
};

extern std::unique_ptr<CSGameRulesProxy> g_pGameRules;