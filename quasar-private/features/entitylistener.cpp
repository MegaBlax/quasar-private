#include "entitylistener.h"
#include "../sdk/cs_player.h"

entity_listener* g_pEntitymanager;

void entity_listener::OnEntityCreated(IClientEntity* pEntity)
{
	const auto ent_index = pEntity->EntIndex();

	if (ent_index > 0 && ent_index < 65)
	{
		g_pPlayerList->add_player(pEntity, ent_index);
	}
	else
	{
		const int class_id = pEntity->GetClientClass()->m_ClassID;
		if (class_id == CC4 || class_id == CPlantedC4)
			g_pPlayerList->add_object(pEntity, ent_index);
	}
}

void entity_listener::OnEntityDeleted(IClientEntity* pEntity)
{
	const auto ent_index = pEntity->EntIndex();
	if (ent_index > 0 && ent_index < 65)
	{
		g_pPlayerList->remove_player(pEntity);
	}
	else
	{
		g_pPlayerList->remove_object(pEntity);
	}
}