#include "hooks.h"
#include "../utils/utils.h"
#include "../sdk/cs_gamerules.h"
#include "../features/norecoil.h"
#include "../features/sounds.h"
#include "../features/aimbot.h"
#include "../features/triggerbot.h"
#include "../features/backtrack.h"
#include "../features/engineprediction.h"
#include "../features/misc.h"

void hooks::createmove_h(IBaseClientDLL* thisptr, int sequence_number, float input_sample_frametime, bool active, byte* packet)
{
	if (!_o_createmove)
		return;
	
	_o_createmove(thisptr,sequence_number, input_sample_frametime, active);
	if (g_pInterfaces->engineclient()->IsPlayingDemo() || !g_pInterfaces->engineclient()->IsActiveApp() || 
		g_pInterfaces->engineclient()->Con_IsVisible() || !g_pInterfaces->gameui()->InGame())
		return;
	
	CUserCmd* cmd = g_pInterfaces->input()->GetUserCmd(sequence_number);
	CVerifiedUserCmd* verified = g_pInterfaces->input()->GetVerifiedUserCmd(sequence_number);
	if (!cmd || !cmd->command_number || !verified)
		return;
	
	// Engine prediction start
	g_pPrediction->run(cmd);

	g_pAimbot->run(cmd, packet);
	g_pTriggerbot->run(cmd, packet);
	g_pNorecoil->run(cmd);
	g_pBacktrack->create_move(cmd);
	
	g_pMisc->reveal_ranks(cmd);
	g_pMisc->bunnyhop(cmd);
	g_pMisc->strafe(cmd);
	
	// Engine prediction end
	g_pPrediction->end();
	// Fix viewangles
	g_pUtils->Clamp(cmd->viewangles);
	g_pUtils->FixMovement(cmd, cmd->viewangles);

	verified->m_cmd = *cmd;
	verified->m_crc = cmd->GetChecksum();
}

__declspec(naked) void hooks::createmove_proxy_h(IBaseClientDLL* thisptr, PVOID, int sequence_number, float input_sample_frametime, bool active)
{
	__asm
	{
		push ebp
		mov  ebp, esp
		push ebx
		push esp
		push dword ptr[active]
		push dword ptr[input_sample_frametime]
		push dword ptr[sequence_number]
		push dword ptr[thisptr]
		call createmove_h
		pop  ebx
		pop  ebp
		retn 0Ch
	}
}

void hooks::framestagenotify_h(IBaseClientDLL* thisptr, PVOID, ClientFrameStage_t curStage)
{
	if (!_o_framestagenotify)
		return;
	
	if (g_pInterfaces->engineclient()->IsPlayingDemo() || !g_pInterfaces->engineclient()->IsActiveApp())
		return _o_framestagenotify(thisptr, curStage);

	if (g_pInterfaces->clientstate()->IsInGame())
	{
		switch (curStage)
		{
		case FRAME_NET_UPDATE_POSTDATAUPDATE_START:
		{
			g_pNorecoil->fsn();
			break;
		}
		case FRAME_NET_UPDATE_END:
		{
			g_pBacktrack->fsn();
			break;
		}
		default:
			break;
		}
	}

	_o_framestagenotify(thisptr, curStage);
}

void hooks::levelinitpostentity_h(IBaseClientDLL* thisptr)
{
	if (!_o_levelinitpostentity)
		return;
	
	g_pAimbot->clear();
	g_pBacktrack->clear();
	g_pPrediction->clear();
	g_pSounds->clear();
	g_pPlayerList->clear();

	globals::vars::game_type = g_pInterfaces->gametypes()->GetCurrentGameType();
	globals::vars::game_mode = g_pInterfaces->gametypes()->GetCurrentGameMode();
	globals::vars::valve_ds = g_pGameRules->IsValveDS();
	globals::vars::server_tickrate = g_pInterfaces->globalvarsbase()->GetServerTickrate();
	
	_o_levelinitpostentity(thisptr);
}

bool hooks::dispatchusermessage_h(IBaseClientDLL* thisptr, PVOID, int msg_type, int32 nFlags, int size, PVOID msg)
{
	return _o_dispatchusermessage(thisptr, msg_type, nFlags, size, msg);
}
