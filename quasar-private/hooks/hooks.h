#pragma once
#include "../stdafx.h"
#include "../utils/vmt.h"
#include "../sdk/interfaces.h"
#include "../sdk/enums.h"

using present_t = HRESULT(__stdcall*)(IDirect3DDevice9*, const RECT*, const RECT*, HWND, const RGNDATA*);
using reset_t = HRESULT(__stdcall*)(IDirect3DDevice9*, D3DPRESENT_PARAMETERS*);
using painttraverse_t = void(__thiscall*)(IPanel*, VPANEL, bool, bool);
using lockcursor_t = void(__thiscall*)(ISurface*);
using framestagenotify_t = void(__thiscall*)(IBaseClientDLL*, ClientFrameStage_t);
using runcommand_t = void(__thiscall*)(IPrediction*, IClientEntity*, CUserCmd*, IMoveHelper*);
using drawmodelexecute_t = void(__thiscall*)(IVModelRender*, IMatRenderContext*, DrawModelState_t&, const ModelRenderInfo_t&, matrix3x4_t*);
using overrideview_t = void(__thiscall*)(IClientMode*, CViewSetup*);
//using renderview_t = void(__thiscall*)(IViewRender*, const CViewSetup&, const CViewSetup&, int, int);
using createmove_t = void(__thiscall*)(IBaseClientDLL*,int, float, bool);
using levelinitpostentity_t = void(__thiscall*)(IBaseClientDLL*);
using listleavesinbox_t = int(__thiscall*)(ISpatialQuery*, const vector&, const vector&, unsigned short*, int);
//using render2deffectsprehud = void(__thiscall*)(IViewRender*, const CViewSetup&);
using dispatchusermessage_t = bool(__thiscall*)(IBaseClientDLL*, int, int32, int, PVOID);

enum hook_index
{
	LEVELINITPOSTENTITY = 6,
	LISTLEAVESINBOX = 6,
	//RENDERVIEW = 6,
	//RESET = 16,
	//PRESENT = 17,
	OVERRIDEVIEW = 18,
	RUNCOMMAND = 19,
	DRAWMODELEXECUTE = 21,
	CREATEMOVE = 22,
	PAINTTRAVERSE = 41,
	//OVERRIDEMOUSEINPUT = 23,
	FRAMESTAGENOTIFY = 37,
	DISPATCHUSERMESSAGE = 38,
	//RENDER2DEFFECTSPREHUD = 39,
	LOCKCURSOR = 67,
	//DRAWINDEXEDPRIMITIVE = 82,
};

class hooks
{
public:
	hooks();
	bool init() const;
	bool reset() const;
	static LRESULT __stdcall wndproc_h(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
	static HRESULT __stdcall present_h(IDirect3DDevice9* device, const RECT* pSourceRect, const RECT* pDestRect, HWND hDestWindowOverride, const RGNDATA* pDirtyRegion);
	static HRESULT __stdcall reset_h(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* pPresentationParameters);
	static void __fastcall createmove_proxy_h(IBaseClientDLL* thisptr, PVOID, int sequence_number, float input_sample_frametime, bool active);
	static void __stdcall createmove_h(IBaseClientDLL* thisptr, int sequence_number, float input_sample_frametime, bool active, byte* packet);
	static void __fastcall painttraverse_h(IPanel* thisptr, PVOID, VPANEL vguiPanel, bool forceRepaint, bool allowForce);
	//static void __fastcall overridemouseinput_h(IClientMode* thisptr, PVOID, float* x, float* y);
	static void __fastcall lockcursor_h(ISurface* thisptr, PVOID);
	static void __fastcall framestagenotify_h(IBaseClientDLL* thisptr, PVOID, ClientFrameStage_t curStage);
	static void __fastcall runcommand_h(IPrediction* thisptr, PVOID, IClientEntity* player, CUserCmd* cmd, IMoveHelper* helper);
	static void __fastcall drawmodelexecute_h(IVModelRender* thisptr, PVOID, IMatRenderContext* ctx, DrawModelState_t& state, const ModelRenderInfo_t& pInfo, matrix3x4_t* pCustomBoneToWorld);
	static void __fastcall overrideview_h(IClientMode* thisptr, PVOID, CViewSetup* pSetup);
	//static void __fastcall renderview_h(IViewRender* thisptr, PVOID, const CViewSetup& view, const CViewSetup& hudViewSetup, int nClearFlags, int whatToDraw);
	static void __fastcall levelinitpostentity_h(IBaseClientDLL* thisptr);
	static int __fastcall listleavesinbox_h(ISpatialQuery* thisptr, PVOID, const vector& mins, const vector& maxs, unsigned short* pList, int listMax);
	//static void __fastcall render2deffectsprehud_h(IViewRender* thisptr, PVOID, const CViewSetup& view);
	static bool __fastcall dispatchusermessage_h(IBaseClientDLL* thisptr, PVOID, int msg_type, int32 nFlags, int size, PVOID msg);

private:
	std::unique_ptr<virtual_hook> _hooks;

	inline static WNDPROC _o_wndproc;
	inline static present_t _o_present;
	inline static reset_t _o_reset;
	inline static createmove_t _o_createmove;
	inline static painttraverse_t _o_painttraverse;
	inline static framestagenotify_t _o_framestagenotify;
	inline static lockcursor_t _o_lockcursor;
	inline static runcommand_t _o_runcommand;
	inline static drawmodelexecute_t _o_drawmodelexecute;
	inline static overrideview_t _o_overrideview;
	//inline static renderview_t _o_renderview;
	inline static levelinitpostentity_t _o_levelinitpostentity;
	inline static listleavesinbox_t _o_listleavesinbox;
	//inline static render2deffectsprehud _o_render2deffectsprehud;
	inline static dispatchusermessage_t _o_dispatchusermessage;
};

extern std::unique_ptr<hooks> g_pHooks;