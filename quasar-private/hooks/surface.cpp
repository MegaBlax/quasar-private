#include "hooks.h"

void hooks::lockcursor_h(ISurface* thisptr, PVOID)
{
	if (!_o_lockcursor)
		return;
	
	_o_lockcursor(thisptr);
	
	if (globals::menu::active)
		g_pInterfaces->surface()->UnlockCursor();
}