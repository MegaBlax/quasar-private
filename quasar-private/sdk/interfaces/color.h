#pragma once

class Color
{
public:
	// constructors
	Color()
	{
		*reinterpret_cast<int*>(this) = 0;
	}
	Color(int color32)
	{
		*reinterpret_cast<int*>(this) = color32;
	}
	Color(int _r, int _g, int _b)
	{
		SetColor(_r, _g, _b, 255);
	}
	Color(int _r, int _g, int _b, int _a)
	{
		SetColor(_r, _g, _b, _a);
	}

	explicit Color(float color[])
	{
		SetColor(static_cast<int>(color[0]), static_cast<int>(color[1]), static_cast<int>(color[2]), static_cast<int>(color[3]));
	}

	void SetColor(int _r, int _g, int _b, int _a = 255)
	{
		_color[0] = static_cast<unsigned char>(_r);
		_color[1] = static_cast<unsigned char>(_g);
		_color[2] = static_cast<unsigned char>(_b);
		_color[3] = static_cast<unsigned char>(_a);
	}

	void GetColor(int& _r, int& _g, int& _b, int& _a) const
	{
		_r = _color[0];
		_g = _color[1];
		_b = _color[2];
		_a = _color[3];
	}

	void SetRawColor(int color32)
	{
		*reinterpret_cast<int*>(this) = color32;
	}

	void SetAlpha(int alpha)
	{
		_color[3] = static_cast<unsigned char>(alpha);
	}

	int GetRawColor()
	{
		return *reinterpret_cast<int*>(this);
	}

	int r() const { return _color[0]; }
	int g() const { return _color[1]; }
	int b() const { return _color[2]; }
	int a() const { return _color[3]; }

	unsigned char& operator[](int index)
	{
		return _color[index];
	}

	const unsigned char& operator[](int index) const
	{
		return _color[index];
	}

	bool operator == (const Color& rhs) const
	{
		return *(int*)this == *(int*)& rhs;
	}

	bool operator != (const Color& rhs) const
	{
		return !(operator==(rhs));
	}

	static Color FromHSB(float hue, float saturation, float brightness)
	{
		auto h = hue == 1.0f ? 0 : hue * 6.0f;
		auto f = h - static_cast<int>(h);
		auto p = brightness * (1.0f - saturation);
		auto q = brightness * (1.0f - saturation * f);
		auto t = brightness * (1.0f - (saturation * (1.0f - f)));

		if (h < 1)
		{
			return Color(
				static_cast<unsigned char>(brightness * 255),
				static_cast<unsigned char>(t * 255),
				static_cast<unsigned char>(p * 255)
			);
		}
		if (h < 2)
		{
			return Color(
				static_cast<unsigned char>(q * 255),
				static_cast<unsigned char>(brightness * 255),
				static_cast<unsigned char>(p * 255)
			);
		}
		if (h < 3)
		{
			return Color(
				static_cast<unsigned char>(p * 255),
				static_cast<unsigned char>(brightness * 255),
				static_cast<unsigned char>(t * 255)
			);
		}
		if (h < 4)
		{
			return Color(
				static_cast<unsigned char>(p * 255),
				static_cast<unsigned char>(q * 255),
				static_cast<unsigned char>(brightness * 255)
			);
		}
		if (h < 5)
		{
			return Color(
				static_cast<unsigned char>(t * 255),
				static_cast<unsigned char>(p * 255),
				static_cast<unsigned char>(brightness * 255)
			);
		}
		return Color(
			static_cast<unsigned char>(brightness * 255),
			static_cast<unsigned char>(p * 255),
			static_cast<unsigned char>(q * 255)
		);
	}

	unsigned int U32() const
	{
		return ((_color[3] & 0xff) << 24) + ((_color[2] & 0xff) << 16) + ((_color[1] & 0xff) << 8) + (_color[0] & 0xff);
	}
	static Color Black()
	{
		return Color(0, 0, 0, 255);
	}
	static Color White()
	{
		return Color(255, 255, 255, 255);
	}
	static Color Red()
	{
		return Color(255, 0, 0, 255);
	}
	static Color Green()
	{
		return Color(0, 128, 0, 255);
	}
	static Color Blue()
	{
		return Color(0, 0, 255, 255);
	}
	static Color Lime()
	{
		return Color(0, 255, 0, 255);
	}
	static Color Yellow()
	{
		return Color(255, 255, 0, 255);
	}
	static Color Cyan()
	{
		return Color(0, 255, 255, 255);
	}
	static Color Magenta()
	{
		return Color(255, 0, 255, 255);
	}
	static Color Silver()
	{
		return Color(192, 192, 192, 255);
	}
	static Color Gray()
	{
		return Color(128, 128, 128, 255);
	}
	static Color Maroon()
	{
		return Color(128, 0, 0, 255);
	}
	static Color Olive()
	{
		return Color(128, 128, 0, 255);
	}
	static Color Purple()
	{
		return Color(128, 0, 128, 255);
	}
	static Color Teal()
	{
		return Color(0, 128, 128, 255);
	}
	static Color Navy()
	{
		return Color(0, 0, 128, 255);
	}
	static Color DarkRed()
	{
		return Color(139, 0, 0, 255);
	}
	static Color Brown()
	{
		return Color(165, 42, 42, 255);
	}
	static Color Firebrick()
	{
		return Color(178, 34, 34, 255);
	}
	static Color Crimson()
	{
		return Color(220, 20, 60, 255);
	}
	static Color IndianRed()
	{
		return Color(205, 92, 92, 255);
	}
	static Color LightCoral()
	{
		return Color(240, 128, 128, 255);
	}
	static Color DarkSalmon()
	{
		return Color(233, 150, 122, 255);
	}
	static Color Salmon()
	{
		return Color(250, 128, 114, 255);
	}
	static Color LightSalmon()
	{
		return Color(255, 160, 122, 255);
	}
	static Color OrangeRed()
	{
		return Color(255, 69, 0, 255);
	}
	static Color DarkOrange()
	{
		return Color(255, 140, 0, 255);
	}
	static Color Orange()
	{
		return Color(255, 165, 0, 255);
	}
	static Color Gold()
	{
		return Color(255, 215, 0, 255);
	}
	static Color DarkGoldenRod()
	{
		return Color(184, 134, 11, 255);
	}
	static Color GoldenRod()
	{
		return Color(218, 165, 32, 255);
	}
	static Color YellowGreen()
	{
		return Color(154, 205, 50, 255);
	}
	static Color DarkOliveGreen()
	{
		return Color(85, 107, 47, 255);
	}

private:
	unsigned char _color[4]{};
};