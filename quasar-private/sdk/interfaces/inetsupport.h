//===== Copyright c 1996-2009, Valve Corporation, All rights reserved. ======//
//
// Purpose: 
//
// $NoKeywords: $
//===========================================================================//

#ifndef INETSUPPORT_H
#define INETSUPPORT_H

#ifdef _WIN32
#pragma once
#endif
#include "iappsystem.h"

struct ClientInfo_t
{
	int m_nSignonState;	// client signon state
	int m_nSocket;		// client socket
	INetChannel* m_pNetChannel;

	int m_numHumanPlayers;	// Number of human players on the server
};

class INetSupport : public IAppSystem
{
public:
	// Get engine build number
	virtual int GetEngineBuildNumber() = 0;

	// Get client info
	virtual void GetClientInfo(ClientInfo_t* pClientInfo)
	{
		using fn = void(__thiscall*)(PVOID, ClientInfo_t*);
		call_virtual<fn>(this, 11)(this, pClientInfo);
	}
};

#endif // INETSUPPORT_H
