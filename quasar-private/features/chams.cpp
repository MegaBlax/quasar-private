#include "chams.h"
#include "../sdk/cs_player.h"
#include "../utils/utils.h"
#include "backtrack.h"

std::unique_ptr<chams> g_pChams;

IMaterial* create_material(const char* type, const bool ignorez, bool wireframe = false)
{
	const int rnd = g_pUtils->RandomInt(0, 999);
	char mat_name[25];
	sprintf_s(mat_name, sizeof(mat_name), xorstr_("custom_material_%i.vmt"), rnd);

	KeyValues* kv2 = new KeyValues(type);
	kv2->SetString(xorstr_("$basetexture"), xorstr_("VGUI/white_additive"));
	kv2->SetInt(xorstr_("$model"), 1);
	kv2->SetInt(xorstr_("$nocull"), 1);
	kv2->SetInt(xorstr_("$nofog"), 1);
	kv2->SetInt(xorstr_("$ignorez"), ignorez);
	kv2->SetInt(xorstr_("$znearer"), 1);
	kv2->SetInt(xorstr_("$halflambert"), 1);
	kv2->SetInt(xorstr_("$wireframe"), wireframe);
	//kv2->SaveToFile((DWORD)g_pInterfaces->filesystem(), "check_material", nullptr, false);
	
	return g_pInterfaces->materialsystem()->CreateMaterial(mat_name, kv2);
}

chams::chams(): _material{}, _original(nullptr)
{
}

bool chams::init(const drawmodelexecute_t& original)
{
	_material[int(MATERIAL::TEXTURED)] = create_material(xorstr_("VertexLitGeneric"), false);
	_material[int(MATERIAL::TEXTURED_I)] = create_material(xorstr_("VertexLitGeneric"), true);
	_material[int(MATERIAL::FLAT)] = create_material(xorstr_("UnlitGeneric"), false);
	_material[int(MATERIAL::FLAT_I)] = create_material(xorstr_("UnlitGeneric"), true);
	_original = original;

	return _material[int(MATERIAL::TEXTURED)] && _material[int(MATERIAL::TEXTURED_I)] && _material[int(MATERIAL::FLAT)] && _material[int(MATERIAL::FLAT_I)];
}

void chams::create_new_mat()
{
	_material[int(MATERIAL::TEXTURED)] = create_material(xorstr_("VertexLitGeneric"), false);
	_material[int(MATERIAL::TEXTURED_I)] = create_material(xorstr_("VertexLitGeneric"), true);
	_material[int(MATERIAL::FLAT)] = create_material(xorstr_("UnlitGeneric"), false);
	_material[int(MATERIAL::FLAT_I)] = create_material(xorstr_("UnlitGeneric"), true);
}

void chams::run(IVModelRender* thisptr, IMatRenderContext* ctx, DrawModelState_t& state, const ModelRenderInfo_t& pInfo, matrix3x4_t* pCustomBoneToWorld)
{
	if (g_pInterfaces->engineclient()->IsTakingScreenshot() || !thisptr || !ctx || !pInfo.pModel || !pCustomBoneToWorld)
		return;
	if (!_material[0] || !_material[1] || !_material[2] || !_material[3])
		return;

	const std::string model_name = pInfo.pModel->name;
	if (model_name.find(xorstr_("models/player")) == std::string::npos)
		return;

	cs_player* local_player = cs_player::get_local_player();
	cs_player* entity_player = cs_player::get_entity_player(pInfo.entity_index);

	if (local_player->teamindex() < TEAM_TERRORIST)
		return;
	if (entity_player == local_player)
		return;
	if (!entity_player || !entity_player->is_alive() || entity_player->IsDormant())
		return;
	
	const int teamid = entity_player->is_enemy(local_player) ? int(TEAM::ENEMY) : int(TEAM::ALLY);
	if (!globals::visuals::team_t[teamid].active)
		return;
	if (globals::visuals::team_t[teamid].chams_backtrack)
		draw_backtrack(thisptr, ctx, state, pInfo, teamid);
	if (!globals::visuals::team_t[teamid].chams)
		return;

	IMaterial* visible_mat = globals::visuals::team_t[teamid].chams_flat ? _material[int(MATERIAL::FLAT)] : _material[int(MATERIAL::TEXTURED)];
	IMaterial* ignorez_mat = globals::visuals::team_t[teamid].chams_flat ? _material[int(MATERIAL::FLAT_I)] : _material[int(MATERIAL::TEXTURED_I)];

	const ImVec4 vis_col = globals::visuals::team_t[teamid].color[int(PCOLOR::CHAMS_V)];
	const ImVec4 ign_col = globals::visuals::team_t[teamid].color[int(PCOLOR::CHAMS_I)];

	visible_mat->AlphaModulate(vis_col.w);
	visible_mat->ColorModulate(vis_col.x, vis_col.y, vis_col.z);

	ignorez_mat->AlphaModulate(ign_col.w);
	ignorez_mat->ColorModulate(ign_col.x, ign_col.y, ign_col.z);

	if (!globals::visuals::team_t[teamid].chams_visible_check)
	{
		g_pInterfaces->modelrender()->ForcedMaterialOverride(ignorez_mat);

		_original(thisptr, ctx, state, pInfo, pCustomBoneToWorld);
	}

	g_pInterfaces->modelrender()->ForcedMaterialOverride(visible_mat);

	_original(thisptr, ctx, state, pInfo, pCustomBoneToWorld);
}

void chams::draw_backtrack(IVModelRender* thisptr, IMatRenderContext* ctx, DrawModelState_t& state, const ModelRenderInfo_t& pInfo, const int teamid)
{
	const auto index = pInfo.entity_index;
	if (!index)
		return;

	const auto& draw_chams = [&](std::array<matrix3x4_t, MAXSTUDIOBONES> bmatrix)
	{
		IMaterial* mat = _material[int(MATERIAL::FLAT)];
		const ImVec4 col = globals::visuals::team_t[teamid].color[int(PCOLOR::CHAMS_BT)];
		mat->AlphaModulate(0.3f);
		mat->ColorModulate(col.x, col.y, col.z);
		g_pInterfaces->modelrender()->ForcedMaterialOverride(mat);
		_original(thisptr, ctx, state, pInfo, bmatrix.data());
		g_pInterfaces->modelrender()->ForcedMaterialOverride(nullptr);
	};

	const auto record = &g_pBacktrack->record(index);
	if (!record || record->empty())
		return;

	if (record && !record->empty() && g_pBacktrack->is_tick_valid(TIME_TO_TICKS(record->back()->simtime)))
	{
		draw_chams(record->back()->bone_matrix);
	}
}