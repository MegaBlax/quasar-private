#pragma once
#include "iappsystem.h"
#include "imaterial.h"
#include "refcount.h"
#include "../tier1/keyvalues.h"

// These are given to FindMaterial to reference the texture groups that show up on the 
#define TEXTURE_GROUP_LIGHTMAP						"Lightmaps"
#define TEXTURE_GROUP_WORLD							"World textures"
#define TEXTURE_GROUP_MODEL							"Model textures"
#define TEXTURE_GROUP_VGUI							"VGUI textures"
#define TEXTURE_GROUP_PARTICLE						"Particle textures"
#define TEXTURE_GROUP_DECAL							"Decal textures"
#define TEXTURE_GROUP_SKYBOX						"SkyBox textures"
#define TEXTURE_GROUP_CLIENT_EFFECTS				"ClientEffect textures"
#define TEXTURE_GROUP_OTHER							"Other textures"
#define TEXTURE_GROUP_PRECACHED						"Precached"				// TODO: assign texture groups to the precached materials
#define TEXTURE_GROUP_CUBE_MAP						"CubeMap textures"
#define TEXTURE_GROUP_RENDER_TARGET					"RenderTargets"
#define TEXTURE_GROUP_UNACCOUNTED					"Unaccounted textures"	// Textures that weren't assigned a texture group.
//#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER		"Static Vertex"
#define TEXTURE_GROUP_STATIC_INDEX_BUFFER			"Static Indices"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_DISP		"Displacement Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_COLOR	"Lighting Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_WORLD	"World Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_MODELS	"Model Verts"
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_OTHER	"Other Verts"
#define TEXTURE_GROUP_DYNAMIC_INDEX_BUFFER			"Dynamic Indices"
#define TEXTURE_GROUP_DYNAMIC_VERTEX_BUFFER			"Dynamic Verts"
#define TEXTURE_GROUP_DEPTH_BUFFER					"DepthBuffer"
#define TEXTURE_GROUP_VIEW_MODEL					"ViewModel"
#define TEXTURE_GROUP_PIXEL_SHADERS					"Pixel Shaders"
#define TEXTURE_GROUP_VERTEX_SHADERS				"Vertex Shaders"
#define TEXTURE_GROUP_RENDER_TARGET_SURFACE			"RenderTarget Surfaces"
#define TEXTURE_GROUP_MORPH_TARGETS					"Morph Targets"

typedef unsigned short MaterialHandle_t;

class Texture_t {
public:
	char pad[0xC];
	LPDIRECT3DTEXTURE9 texture_ptr;
};

class ITexture {
public:
	virtual const char* GetName(void) const = 0;
	virtual int GetMappingWidth() const = 0;
	virtual int GetMappingHeight() const = 0;
	virtual int GetActualWidth() const = 0;
	virtual int GetActualHeight() const = 0;
	virtual int GetNumAnimationFrames() const = 0;
	virtual bool IsTranslucent() const = 0;
	virtual bool IsMipmapped() const = 0;

	char pad[0x4C];
	Texture_t** m_pTextureHandles;
};

enum ClearFlags_t
{
	VIEW_CLEAR_COLOR = 0x1,
	VIEW_CLEAR_DEPTH = 0x2,
	VIEW_CLEAR_FULL_TARGET = 0x4,
	VIEW_NO_DRAW = 0x8,
	VIEW_CLEAR_OBEY_STENCIL = 0x10, // Draws a quad allowing stencil test to clear through portals
	VIEW_CLEAR_STENCIL = 0x20,
};

enum ImageFormat
{
	IMAGE_FORMAT_UNKNOWN = -1,
	IMAGE_FORMAT_RGBA8888 = 0,
	IMAGE_FORMAT_ABGR8888,
	IMAGE_FORMAT_RGB888,
	IMAGE_FORMAT_BGR888,
	IMAGE_FORMAT_RGB565,
	IMAGE_FORMAT_I8,
	IMAGE_FORMAT_IA88,
	IMAGE_FORMAT_P8,
	IMAGE_FORMAT_A8,
	IMAGE_FORMAT_RGB888_BLUESCREEN,
	IMAGE_FORMAT_BGR888_BLUESCREEN,
	IMAGE_FORMAT_ARGB8888,
	IMAGE_FORMAT_BGRA8888,
	IMAGE_FORMAT_DXT1,
	IMAGE_FORMAT_DXT3,
	IMAGE_FORMAT_DXT5,
	IMAGE_FORMAT_BGRX8888,
	IMAGE_FORMAT_BGR565,
	IMAGE_FORMAT_BGRX5551,
	IMAGE_FORMAT_BGRA4444,
	IMAGE_FORMAT_DXT1_ONEBITALPHA,
	IMAGE_FORMAT_BGRA5551,
	IMAGE_FORMAT_UV88,
	IMAGE_FORMAT_UVWQ8888,
	IMAGE_FORMAT_RGBA16161616F,
	IMAGE_FORMAT_RGBA16161616,
	IMAGE_FORMAT_UVLX8888,
	IMAGE_FORMAT_R32F,			// Single-channel 32-bit floating point
	IMAGE_FORMAT_RGB323232F,
	IMAGE_FORMAT_RGBA32323232F,

	// Depth-stencil texture formats for shadow depth mapping
	IMAGE_FORMAT_NV_DST16,		// 
	IMAGE_FORMAT_NV_DST24,		//
	IMAGE_FORMAT_NV_INTZ,		// Vendor-specific depth-stencil texture
	IMAGE_FORMAT_NV_RAWZ,		// formats for shadow depth mapping 
	IMAGE_FORMAT_ATI_DST16,		// 
	IMAGE_FORMAT_ATI_DST24,		//
	IMAGE_FORMAT_NV_NULL,		// Dummy format which takes no video memory

	// Compressed normal map formats
	IMAGE_FORMAT_ATI2N,			// One-surface ATI2N / DXN format
	IMAGE_FORMAT_ATI1N,			// Two-surface ATI1N format


	NUM_IMAGE_FORMATS
};

class IMatRenderContext : public IRefCounted
{
public:
	virtual void BeginRender() = 0;
	virtual void EndRender() = 0;

	virtual void Flush(bool flushHardware = false) = 0;

	virtual void BindLocalCubemap(ITexture* pTexture) = 0;

	// pass in an ITexture (that is build with "rendertarget" "1") or
	// pass in NULL for the regular backbuffer.
	virtual void SetRenderTarget(ITexture* pTexture) = 0;
	virtual ITexture* GetRenderTarget(void) = 0;
	
	void GetViewport(int& x, int& y, int& width, int& height)
	{
		using fn = void(__thiscall*)(PVOID, int&, int&, int&, int&);
		call_virtual<fn>(this, 40)(this, x, y, width, height);
	}
	
	void DrawScreenSpaceRectangle(
		IMaterial* pMaterial,
		int destx, int desty,
		int width, int height,
		float src_texture_x0, float src_texture_y0,	// which texel you want to appear at destx/y
		float src_texture_x1, float src_texture_y1,	// which texel you want to appear at destx+width-1, desty+height-1
		int src_texture_width, int src_texture_height, // needed for fixup
		PVOID pClientRenderable = nullptr,
		int nXDice = 1,
		int nYDice = 1)
	{
		using fn = void(__thiscall*)(PVOID, IMaterial*,int, int,int, int,float, float,float, float, int, int, PVOID,int,int);
		call_virtual<fn>(this, 114)(this, pMaterial, destx, desty, width, height, src_texture_x0, src_texture_y0,
		                            src_texture_x1, src_texture_y1, src_texture_width, src_texture_height,
		                            pClientRenderable, nXDice, nYDice);
	}

	void PushRenderTargetAndViewport()
	{
		using fn = void(__thiscall*)(PVOID);
		call_virtual<fn>(this, 119)(this);
	}
	
	void PopRenderTargetAndViewport()
	{
		using fn = void(__thiscall*)(PVOID);
		call_virtual<fn>(this, 120)(this);
	}
};

class IMaterialSystem : public IAppSystem
{
public:
	/*bool OverrideConfig(const MaterialSystem_Config_t& config, bool bForceUpdate)
	{
		using fn = bool(__thiscall*)(PVOID, const MaterialSystem_Config_t&, bool);
		return call_virtual<fn>(this, 21)(this, config, bForceUpdate);
	}*/
	ImageFormat GetBackBufferFormat()
	{
		using fn = ImageFormat(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 36)(this);
	}
	IMaterial* CreateMaterial(const char* pMaterialName, KeyValues* pVMTKeyValues)
	{
		using fn = IMaterial * (__thiscall*)(PVOID, const char*, KeyValues*);
		return call_virtual<fn>(this, 83)(this, pMaterialName, pVMTKeyValues);
	}

	IMaterial* FindMaterial(char const* pMaterialName, const char* pTextureGroupName, bool complain = true, const char* pComplainPrefix = nullptr)
	{
		using fn = IMaterial * (__thiscall*)(PVOID, char const*, const char*, bool, const char*);
		return call_virtual<fn>(this, 84)(this, pMaterialName, pTextureGroupName, complain, pComplainPrefix);
	}

	MaterialHandle_t FirstMaterial()
	{
		using fn = MaterialHandle_t(__thiscall*)(PVOID);
		return call_virtual <fn>(this, 86)(this);
	}

	MaterialHandle_t NextMaterial(MaterialHandle_t h)
	{
		using fn = MaterialHandle_t(__thiscall*)(PVOID, MaterialHandle_t);
		return call_virtual <fn>(this, 87)(this, h);
	}

	MaterialHandle_t InvalidMaterial()
	{
		using fn = MaterialHandle_t(__thiscall*)(PVOID);
		return call_virtual <fn>(this, 88)(this);
	}

	IMaterial* GetMaterial(MaterialHandle_t h)
	{
		using fn = IMaterial * (__thiscall*)(PVOID, MaterialHandle_t);
		return call_virtual <fn>(this, 89)(this, h);
	}
	
	ITexture* FindTexture(char const* pTextureName, const char* pTextureGroupName, bool complain = true, int nAdditionalCreationFlags = 0)
	{
		using fn = ITexture* (__thiscall*)(PVOID, char const*, const char*, bool, int);
		return call_virtual <fn>(this, 91)(this, pTextureName, pTextureGroupName, complain, nAdditionalCreationFlags);
	}

	bool IsTextureLoaded(char const* pTextureName)
	{
		using fn = bool(__thiscall*)(PVOID, char const*);
		return call_virtual<fn>(this, 92)(this, pTextureName);
	}

	ITexture* CreateProceduralTexture(const char* pTextureName, const char* pTextureGroupName, int w, int h,
	                                          ImageFormat fmt, int nFlags)
	{
		using fn = ITexture*(__thiscall*)(PVOID, const char*, const char*, int, int, ImageFormat, int);
		return call_virtual<fn>(this, 93)(this, pTextureName, pTextureGroupName, w, h, fmt, nFlags);
	}

	void BeginRenderTargetAllocation()
	{
		using fn = void(__thiscall*)(PVOID);
		return call_virtual <fn>(this, 94)(this);
	}
	void EndRenderTargetAllocation() // Simulate an Alt-Tab in here, which causes a release/restore of all resources
	{
		using fn = void(__thiscall*)(PVOID);
		return call_virtual <fn>(this, 95)(this);
	}
	
	IMatRenderContext* GetRenderContext()
	{
		using fn = IMatRenderContext * (__thiscall*)(PVOID);
		return call_virtual <fn>(this, 115)(this);
	}
};

class ITextureManager
{
public:
	ITexture* FindOrLoadTexture(const char* pTextureName, const char* pTextureGroupName, int nAdditionalCreationFlags = 0)
	{
		using fn = ITexture*(__thiscall*)(PVOID, const char*, const char*, int);
		return call_virtual<fn>(this, 7)(this, pTextureName, pTextureGroupName, nAdditionalCreationFlags);
	}
	bool IsTextureLoaded(const char* pTextureName)
	{
		using fn = bool(__thiscall*)(PVOID, const char*);
		return call_virtual<fn>(this, 28)(this, pTextureName);
	}
	int	FindNext(int iIndex, ITexture** ppTexture)
	{
		using fn = int (__thiscall*)(PVOID, int, ITexture **);
		return call_virtual <fn>(this, 31)(this, iIndex, ppTexture);
	}
};

class CMatSystemTexture
{
public:
	CMatSystemTexture();
	~CMatSystemTexture();

	void SetId(int id) { m_ID = id; };

	CRC32_t	GetCRC() const;
	void SetCRC(CRC32_t val);

	void SetMaterial(const char* pFileName);
	void SetMaterial(IMaterial* pMaterial);

	// This is used when we want different rendering state sharing the same procedural texture (fonts)
	void ReferenceOtherProcedural(CMatSystemTexture* pTexture, IMaterial* pMaterial);

	IMaterial* GetMaterial() { return m_pMaterial; }
	int Width() const { return m_iWide; }
	int Height() const { return m_iTall; }

	bool IsProcedural(void) const;
	void SetProcedural(bool proc);

	bool IsReference() const { return m_Flags & TEXTURE_IS_REFERENCE; }

	void SetTextureRGBA(const char* rgba, int wide, int tall, ImageFormat format);
	void SetSubTextureRGBA(int drawX, int drawY, unsigned const char* rgba, int subTextureWide, int subTextureTall);

	float	m_s0, m_t0, m_s1, m_t1;

	void CreateRegen(int nWidth, int nHeight, ImageFormat format);
	void ReleaseRegen();
	void CleanUpMaterial();

	ITexture* GetTextureValue();

	enum
	{
		TEXTURE_IS_PROCEDURAL = 0x1,
		TEXTURE_IS_REFERENCE = 0x2
	};

	CRC32_t				m_crcFile;
	IMaterial* m_pMaterial;
	ITexture* m_pTexture;
	int					m_iWide;
	int					m_iTall;
	int					m_ID;
	int					m_Flags;
	/*CFontTextureRegen*/ PVOID m_pRegen;
};

class ITextureDictionary
{
public:
	// Create, destroy textures
	virtual int	CreateTexture(bool procedural = false) = 0;
	virtual void DestroyTexture(int id) = 0;
	virtual void DestroyAllTextures() = 0;

	// Is this a valid id?
	virtual bool IsValidId(int id) const = 0;

	// Binds a material to a texture
	virtual void BindTextureToFile(int id, const char* pFileName) = 0;

	// Binds a material to a texture
	virtual void BindTextureToMaterial(int id, IMaterial* pMaterial) = 0;

	// Binds a material to a texture
	virtual void BindTextureToMaterialReference(int id, int referenceId, IMaterial* pMaterial) = 0;

	// Texture info
	virtual IMaterial* GetTextureMaterial(int id) = 0;
	virtual PVOID GetTextureHandle(int id) = 0;
	virtual void GetTextureSize(int id, int& iWide, int& iTall) = 0;
	virtual void GetTextureTexCoords(int id, float& s0, float& t0, float& s1, float& t1) = 0;

	virtual void SetTextureRGBA(int id, const char* rgba, int wide, int tall) = 0;
	virtual void SetTextureRGBAEx(int id, const char* rgba, int wide, int tall, ImageFormat format) = 0;

	virtual int	FindTextureIdForTextureFile(char const* pFileName) = 0;
	virtual void SetSubTextureRGBA(int id, int drawX, int drawY, unsigned const char* rgba, int subTextureWide, int subTextureTall) = 0;
	virtual void SetSubTextureRGBAEx(int id, int drawX, int drawY, unsigned const char* rgba, int subTextureWide, int subTextureTall, ImageFormat fmt) = 0;
	virtual void UpdateSubTextureRGBA(int, int, int, unsigned const char*, int, int, ImageFormat) = 0;

	
};