#pragma once
#include "interfaces.h"

class netvarmanager
{
public:
	bool init();
	std::uint16_t get_offset(const fnv::hash& hash);

private:
	void dump(RecvTable* recv_table, const char* name = "", std::uint16_t offset = 0);
	struct stored_data
	{
		RecvProp* prop;
		std::uint16_t offset;
	};

	std::unordered_map<fnv::hash, stored_data> _database;
};

extern std::unique_ptr<netvarmanager> g_pNetvarManager;