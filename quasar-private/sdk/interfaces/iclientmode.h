#pragma once

class IClientMode final
{
public:
	virtual	~IClientMode() = default;
	bool CreateMove(float flInputSampleTime, CUserCmd* cmd)
	{
		using fn = bool(__thiscall*)(void*, float, CUserCmd*);
		return call_virtual<fn>(this, 24)(this, flInputSampleTime, cmd);
	}
};