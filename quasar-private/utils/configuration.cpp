#include "configuration.h"
#include "utils.h"
#include "../json/json.h"
#include "../sdk/interfaces.h"

std::unique_ptr<configuration> g_pConfig;

using json = Json::Value;

namespace str_defines
{
	std::string ally = xorstr_("ally");
	std::string enemy = xorstr_("enemy");
	std::string visible = xorstr_("visible");
	std::string invisible = xorstr_("invisible");

	std::string visuals = xorstr_("visuals");
	std::string aimbot = xorstr_("aimbot");
	std::string triggerbot = xorstr_("triggerbot");
	std::string backtrack = xorstr_("backtrack");
	std::string misc = xorstr_("misc");
	std::string colors = xorstr_("colors");
	std::string hotkeys = xorstr_("hotkeys");
	std::string preview = xorstr_("esp_preview");
	std::string fstatus = xorstr_("features_status");
	std::string menu = xorstr_("menu");
}

void save_value(json& j, std::any value)
{
	const auto& type = value.type();

	if (type == typeid(int*)) 
	{
		const auto& v = *std::any_cast<int*>(value);
		j = v;
	}
	else if (type == typeid(bool*)) 
	{
		const auto& v = *std::any_cast<bool*>(value);
		j = v;
	}
	else if (type == typeid(float*)) 
	{
		const auto& v = *std::any_cast<float*>(value);
		j = v;
	}
	else if (type == typeid(double*)) 
	{
		const auto& v = *std::any_cast<double*>(value);
		j = v;
	}
	else if (type == typeid(std::string*)) 
	{
		const auto& v = *std::any_cast<std::string*>(value);
		j = v;
	}
	else if (type == typeid(ImVec2*))
	{
		auto& v = *std::any_cast<ImVec2*>(value);
		j["x"] = v.x;
		j["y"] = v.y;
	}
	else if (type == typeid(vector*))
	{
		auto& v = *std::any_cast<vector*>(value);
		j["x"] = v.x;
		j["y"] = v.y;
		j["z"] = v.z;
	}
	else if (type == typeid(ImVec4*))
	{
		const auto& v = *std::any_cast<ImVec4*>(value);
		j["r"] = v.x;
		j["g"] = v.y;
		j["b"] = v.z;
		j["a"] = v.w;
	}
}

auto load_value(json& j, std::any value) -> void
{
	if (j.empty())
		return;

	const auto& type = value.type();

	if (type == typeid(int*)) 
	{
		auto& v = *std::any_cast<int*>(value);
		v = j.asInt();
	}
	else if (type == typeid(bool*)) 
	{
		auto& v = *std::any_cast<bool*>(value);
		v = j.asBool();
	}
	else if (type == typeid(float*)) 
	{
		auto& v = *std::any_cast<float*>(value);
		v = j.asFloat();
	}
	else if (type == typeid(double*)) 
	{
		auto& v = *std::any_cast<double*>(value);
		v = j.asDouble();
	}
	else if (type == typeid(std::string*)) 
	{
		auto& v = *std::any_cast<std::string*>(value);
		v = j.asString();
	}
	else if (type == typeid(ImVec2*))
	{
		auto& v = *std::any_cast<ImVec2*>(value);
		v.x = j["x"].asFloat();
		v.y = j["y"].asFloat();
	}
	else if (type == typeid(vector*))
	{
		auto& v = *std::any_cast<vector*>(value);
		v.x = j["x"].asFloat();
		v.y = j["y"].asFloat();
		v.z = j["z"].asFloat();
	}
	else if (type == typeid(ImVec4*))
	{
		auto& v = *std::any_cast<ImVec4*>(value);
		v.x = j["r"].asFloat();
		v.y = j["g"].asFloat();
		v.z = j["b"].asFloat();
		v.w = j["a"].asFloat();
	}
}

bool configuration::init()
{
	_path = g_pUtils->GetConfigsPath();
	if (!std::filesystem::is_directory(_path.c_str()))
	{
		std::filesystem::remove(_path.c_str());
		std::filesystem::create_directory(_path.c_str());
	}
	_path += xorstr_(L"\\");

	return true;
}

std::vector<std::wstring> configuration::getarray() const
{
	std::vector<std::wstring> out;
	for (auto& p : std::filesystem::recursive_directory_iterator(_path.c_str()))
	{
		const auto& path = p.path();
		if (path.extension() != xorstr_(L".json"))
			continue;
		std::wstring filename = path.filename();
		filename.erase(filename.length() - 5, 5);
		out.emplace_back(filename);
	}

	return out;
}

void configuration::del(int id) const
{
	const std::wstring file_name = _path + globals::config::arr[id] + xorstr_(L".json");
	std::filesystem::remove(file_name.c_str());
	globals::config::arr.erase(globals::config::arr.begin() + id);
}

bool configuration::save(const std::wstring& name) const
{
	if (name.empty())
	{
		log_error(xorstr_(L"Empty configuration name"));
		return false;
	}
	json j;

	//visuals
	save_value(j[str_defines::visuals][xorstr_("draw_fov")], &globals::visuals::fov);
	save_value(j[str_defines::visuals][xorstr_("sniper_crosshair")], &globals::visuals::sniper_crosshair);
	save_value(j[str_defines::visuals][xorstr_("recoil_crosshair")], &globals::visuals::recoil_crosshair);

	// enemy visuals
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("active")], &globals::visuals::team_t[int(TEAM::ENEMY)].active);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("visible_check")], &globals::visuals::team_t[int(TEAM::ENEMY)].visible_check);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("box")], &globals::visuals::team_t[int(TEAM::ENEMY)].box);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("corner_size")], &globals::visuals::team_t[int(TEAM::ENEMY)].corner_size);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("healthbar")], &globals::visuals::team_t[int(TEAM::ENEMY)].health);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ENEMY)].name);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ENEMY)].weapon);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ENEMY)].steps);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("steps_size")], &globals::visuals::team_t[int(TEAM::ENEMY)].steps_size);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("chams")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("chams_flat")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams_flat);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams_backtrack);
	save_value(j[str_defines::visuals][str_defines::enemy][xorstr_("chams_visible_check")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams_visible_check);

	// ally visuals
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("active")], &globals::visuals::team_t[int(TEAM::ALLY)].active);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("visible_check")], &globals::visuals::team_t[int(TEAM::ALLY)].visible_check);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("box")], &globals::visuals::team_t[int(TEAM::ALLY)].box);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("corner_size")], &globals::visuals::team_t[int(TEAM::ALLY)].corner_size);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("healthbar")], &globals::visuals::team_t[int(TEAM::ALLY)].health);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ALLY)].name);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ALLY)].weapon);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ALLY)].steps);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("steps_size")], &globals::visuals::team_t[int(TEAM::ALLY)].steps_size);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams")], &globals::visuals::team_t[int(TEAM::ALLY)].chams);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_flat")], &globals::visuals::team_t[int(TEAM::ALLY)].chams_flat);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ALLY)].chams_backtrack);
	save_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_visible_check")], &globals::visuals::team_t[int(TEAM::ALLY)].chams_visible_check);

	//aimbot
	save_value(j[str_defines::aimbot][xorstr_("active")], &globals::aimbot::active);
	save_value(j[str_defines::aimbot][xorstr_("flash_check")], &globals::aimbot::flash_check);
	save_value(j[str_defines::aimbot][xorstr_("smoke_check")], &globals::aimbot::smoke_check);
	save_value(j[str_defines::aimbot][xorstr_("jump_check")], &globals::aimbot::jump_check);
	save_value(j[str_defines::aimbot][xorstr_("friendly_fire")], &globals::aimbot::ffa_mode);
	save_value(j[str_defines::aimbot][xorstr_("key_mode")], &globals::aimbot::key_mode);

	for (auto it = 0; it < 64; it++)
	{
		std::string weapon_name = g_pUtils->GetWeaponName(it);
		if (weapon_name.empty())
			continue;

		save_value(j[str_defines::aimbot][weapon_name][xorstr_("active")], &globals::aimbot::weapon_t[it].active);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("smart")], &globals::aimbot::weapon_t[it].smart);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("multipoints")], &globals::aimbot::weapon_t[it].multipoints);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("auto_delay")], &globals::aimbot::weapon_t[it].auto_delay);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("first_bullet_silent")], &globals::aimbot::weapon_t[it].first_bullet_silent);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("hitbox")], &globals::aimbot::weapon_t[it].hitbox);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("smooth")], &globals::aimbot::weapon_t[it].smooth);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("first_fov")], &globals::aimbot::weapon_t[it].fov);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("second_fov")], &globals::aimbot::weapon_t[it].second_fov);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("kill_delay")], &globals::aimbot::weapon_t[it].kill_delay);
		save_value(j[str_defines::aimbot][weapon_name][xorstr_("silent_chance")], &globals::aimbot::weapon_t[it].chance);
	}

	// triggerbot
	save_value(j[str_defines::triggerbot][xorstr_("active")], &globals::triggerbot::active);
	save_value(j[str_defines::triggerbot][xorstr_("flash_check")], &globals::triggerbot::flash_check);
	save_value(j[str_defines::triggerbot][xorstr_("smoke_check")], &globals::triggerbot::smoke_check);
	save_value(j[str_defines::triggerbot][xorstr_("zoom_check")], &globals::triggerbot::zoom_check);
	save_value(j[str_defines::triggerbot][xorstr_("key_mode")], &globals::triggerbot::key_mode);

	// backtrack
	save_value(j[str_defines::backtrack][xorstr_("active")], &globals::backtrack::active);
	save_value(j[str_defines::backtrack][xorstr_("time")], &globals::backtrack::time);

	//misc
	save_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("active")], &globals::rcs::active);
	save_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("start")], &globals::rcs::start);
	save_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("pitch")], &globals::rcs::pitch);
	save_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("yaw")], &globals::rcs::yaw);
	save_value(j[str_defines::misc][xorstr_("radar")][xorstr_("active")], &globals::radar::active);
	save_value(j[str_defines::misc][xorstr_("radar")][xorstr_("pos")], &globals::radar::pos);
	save_value(j[str_defines::misc][xorstr_("radar")][xorstr_("size")], &globals::radar::size);
	save_value(j[str_defines::misc][xorstr_("radar")][xorstr_("zoom")], &globals::radar::zoom);
	save_value(j[str_defines::misc][xorstr_("radar")][xorstr_("alpha")], &globals::radar::alpha);
	save_value(j[str_defines::misc][xorstr_("radar")][xorstr_("icon_scale")], &globals::radar::icon_scale);
	save_value(j[str_defines::misc][xorstr_("misc")][xorstr_("bunnyhop")], &globals::misc::bunnyhop);
	save_value(j[str_defines::misc][xorstr_("misc")][xorstr_("autostrafe")], &globals::misc::strafe);
	save_value(j[str_defines::misc][xorstr_("misc")][xorstr_("autoaccept")], &globals::misc::autoaccept);
	save_value(j[str_defines::misc][xorstr_("misc")][xorstr_("restore_window")], &globals::misc::res_window);
	save_value(j[str_defines::misc][xorstr_("misc")][xorstr_("rank")], &globals::misc::rank);
	save_value(j[str_defines::misc][xorstr_("misc")][xorstr_("hitmarker")], &globals::misc::hitmarker);

	//colors
	save_value(j[str_defines::colors][xorstr_("fov")], &globals::visuals::color[int(COLOR::FOV)]);
	save_value(j[str_defines::colors][xorstr_("sniper_crosshair")], &globals::visuals::color[int(COLOR::CROSSHAIR)]);
	save_value(j[str_defines::colors][xorstr_("recoil")], &globals::visuals::color[int(COLOR::RECOIL)]);

	// enemy color
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("box_visible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::BOX_V)]);
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("box_invisible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::BOX_I)]);
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("chams_visible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::CHAMS_V)]);
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("chams_invisible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::CHAMS_I)]);
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::CHAMS_BT)]);
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::STEPS)]);
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::NAME)]);
	save_value(j[str_defines::colors][str_defines::enemy][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::WEAPON)]);


	// ally color
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("box_visible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::BOX_V)]);
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("box_invisible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::BOX_I)]);
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("chams_visible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::CHAMS_V)]);
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("chams_invisible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::CHAMS_I)]);
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::CHAMS_BT)]);
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::STEPS)]);
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::NAME)]);
	save_value(j[str_defines::colors][str_defines::ally][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::WEAPON)]);

	// hotkeys
	save_value(j[str_defines::hotkeys][xorstr_("menu")], &globals::hotkey::menu);
	save_value(j[str_defines::hotkeys][xorstr_("aimbot")], &globals::hotkey::aimbot);
	save_value(j[str_defines::hotkeys][xorstr_("triggerbot")], &globals::hotkey::triggerbot);

	// menu
	save_value(j[str_defines::menu][xorstr_("position")], &globals::menu::pos);

	// walkbot
	

	const std::wstring filename = _path + name + xorstr_(L".json");
	std::ofstream write = std::ofstream(filename.c_str());
	if (write.good())
	{
		write << j;
		write.close();
	}
	else
		return false;

	g_pInterfaces->cvar()->ConsoleColorPrintf(Color::Green(), xorstr_("save: %ls\n"), filename.c_str());
	
	return true;
}

bool configuration::load(const std::wstring& name) const
{
	if (name.empty())
	{
		log_error(xorstr_(L"Empty configuration name"));
		return false;
	}
	const std::wstring filename = _path + name + xorstr_(L".json");
	std::ifstream read = std::ifstream(filename.c_str());
	if (read.good())
	{
		json j;
		Json::CharReaderBuilder builder;
		JSONCPP_STRING errs;
		if (!parseFromStream(builder, read, &j, &errs)) {
			log_error(g_pUtils->UTF8ToWstring(errs).c_str());
			return false;
		}
		read.close();

		//visuals
		load_value(j[str_defines::visuals][xorstr_("draw_fov")], &globals::visuals::fov);
		load_value(j[str_defines::visuals][xorstr_("sniper_crosshair")], &globals::visuals::sniper_crosshair);
		load_value(j[str_defines::visuals][xorstr_("recoil_crosshair")], &globals::visuals::recoil_crosshair);

		// enemy visuals
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("active")], &globals::visuals::team_t[int(TEAM::ENEMY)].active);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("visible_check")], &globals::visuals::team_t[int(TEAM::ENEMY)].visible_check);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("box")], &globals::visuals::team_t[int(TEAM::ENEMY)].box);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("corner_size")], &globals::visuals::team_t[int(TEAM::ENEMY)].corner_size);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("healthbar")], &globals::visuals::team_t[int(TEAM::ENEMY)].health);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ENEMY)].name);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ENEMY)].weapon);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ENEMY)].steps);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("steps_size")], &globals::visuals::team_t[int(TEAM::ENEMY)].steps_size);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("chams")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_flat")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams_flat);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams_backtrack);
		load_value(j[str_defines::visuals][str_defines::enemy][xorstr_("chams_visible_check")], &globals::visuals::team_t[int(TEAM::ENEMY)].chams_visible_check);

		// ally visuals
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("active")], &globals::visuals::team_t[int(TEAM::ALLY)].active);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("visible_check")], &globals::visuals::team_t[int(TEAM::ALLY)].visible_check);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("box")], &globals::visuals::team_t[int(TEAM::ALLY)].box);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("corner_size")], &globals::visuals::team_t[int(TEAM::ALLY)].corner_size);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("healthbar")], &globals::visuals::team_t[int(TEAM::ALLY)].health);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ALLY)].name);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ALLY)].weapon);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ALLY)].steps);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("steps_size")], &globals::visuals::team_t[int(TEAM::ALLY)].steps_size);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams")], &globals::visuals::team_t[int(TEAM::ALLY)].chams);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_flat")], &globals::visuals::team_t[int(TEAM::ALLY)].chams_flat);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ALLY)].chams_backtrack);
		load_value(j[str_defines::visuals][str_defines::ally][xorstr_("chams_visible_check")], &globals::visuals::team_t[int(TEAM::ALLY)].chams_visible_check);

		//aimbot
		load_value(j[str_defines::aimbot][xorstr_("active")], &globals::aimbot::active);
		load_value(j[str_defines::aimbot][xorstr_("flash_check")], &globals::aimbot::flash_check);
		load_value(j[str_defines::aimbot][xorstr_("smoke_check")], &globals::aimbot::smoke_check);
		load_value(j[str_defines::aimbot][xorstr_("jump_check")], &globals::aimbot::jump_check);
		load_value(j[str_defines::aimbot][xorstr_("friendly_fire")], &globals::aimbot::ffa_mode);
		load_value(j[str_defines::aimbot][xorstr_("key_mode")], &globals::aimbot::key_mode);

		for (auto it = 0; it < 64; it++)
		{
			std::string weapon_name = g_pUtils->GetWeaponName(it);
			if (weapon_name.empty())
				continue;

			load_value(j[str_defines::aimbot][weapon_name][xorstr_("active")], &globals::aimbot::weapon_t[it].active);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("smart")], &globals::aimbot::weapon_t[it].smart);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("multipoints")], &globals::aimbot::weapon_t[it].multipoints);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("auto_delay")], &globals::aimbot::weapon_t[it].auto_delay);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("first_bullet_silent")], &globals::aimbot::weapon_t[it].first_bullet_silent);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("hitbox")], &globals::aimbot::weapon_t[it].hitbox);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("smooth")], &globals::aimbot::weapon_t[it].smooth);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("first_fov")], &globals::aimbot::weapon_t[it].fov);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("second_fov")], &globals::aimbot::weapon_t[it].second_fov);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("kill_delay")], &globals::aimbot::weapon_t[it].kill_delay);
			load_value(j[str_defines::aimbot][weapon_name][xorstr_("silent_chance")], &globals::aimbot::weapon_t[it].chance);
		}

		// triggerbot
		load_value(j[str_defines::triggerbot][xorstr_("active")], &globals::triggerbot::active);
		load_value(j[str_defines::triggerbot][xorstr_("key_mode")], &globals::triggerbot::key_mode);
		load_value(j[str_defines::triggerbot][xorstr_("flash_check")], &globals::triggerbot::flash_check);
		load_value(j[str_defines::triggerbot][xorstr_("smoke_check")], &globals::triggerbot::smoke_check);
		load_value(j[str_defines::triggerbot][xorstr_("zoom_check")], &globals::triggerbot::zoom_check);

		// backtrack
		load_value(j[str_defines::backtrack][xorstr_("active")], &globals::backtrack::active);
		load_value(j[str_defines::backtrack][xorstr_("time")], &globals::backtrack::time);

		//misc
		load_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("active")], &globals::rcs::active);
		load_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("start")], &globals::rcs::start);
		load_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("pitch")], &globals::rcs::pitch);
		load_value(j[str_defines::misc][xorstr_("rcs")][xorstr_("yaw")], &globals::rcs::yaw);
		load_value(j[str_defines::misc][xorstr_("radar")][xorstr_("active")], &globals::radar::active);
		load_value(j[str_defines::misc][xorstr_("radar")][xorstr_("pos")], &globals::radar::pos);
		load_value(j[str_defines::misc][xorstr_("radar")][xorstr_("size")], &globals::radar::size);
		load_value(j[str_defines::misc][xorstr_("radar")][xorstr_("zoom")], &globals::radar::zoom);
		load_value(j[str_defines::misc][xorstr_("radar")][xorstr_("alpha")], &globals::radar::alpha);
		load_value(j[str_defines::misc][xorstr_("radar")][xorstr_("icon_scale")], &globals::radar::icon_scale);
		load_value(j[str_defines::misc][xorstr_("misc")][xorstr_("bunnyhop")], &globals::misc::bunnyhop);
		load_value(j[str_defines::misc][xorstr_("misc")][xorstr_("autostrafe")], &globals::misc::strafe);
		load_value(j[str_defines::misc][xorstr_("misc")][xorstr_("autoaccept")], &globals::misc::autoaccept);
		load_value(j[str_defines::misc][xorstr_("misc")][xorstr_("restore_window")], &globals::misc::res_window);
		load_value(j[str_defines::misc][xorstr_("misc")][xorstr_("rank")], &globals::misc::rank);
		load_value(j[str_defines::misc][xorstr_("misc")][xorstr_("hitmarker")], &globals::misc::hitmarker);

		//colors
		load_value(j[str_defines::colors][xorstr_("fov")], &globals::visuals::color[int(COLOR::FOV)]);
		load_value(j[str_defines::colors][xorstr_("sniper_crosshair")], &globals::visuals::color[int(COLOR::CROSSHAIR)]);
		load_value(j[str_defines::colors][xorstr_("recoil")], &globals::visuals::color[int(COLOR::RECOIL)]);

		// enemy color
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("box_visible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::BOX_V)]);
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("box_invisible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::BOX_I)]);
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("chams_visible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::CHAMS_V)]);
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("chams_invisible")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::CHAMS_I)]);
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::CHAMS_BT)]);
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::STEPS)]);
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::NAME)]);
		load_value(j[str_defines::colors][str_defines::enemy][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ENEMY)].color[int(PCOLOR::WEAPON)]);


		// ally color
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("box_visible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::BOX_V)]);
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("box_invisible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::BOX_I)]);
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("chams_visible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::CHAMS_V)]);
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("chams_invisible")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::CHAMS_I)]);
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("chams_backtrack")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::CHAMS_BT)]);
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("steps")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::STEPS)]);
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("name")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::NAME)]);
		load_value(j[str_defines::colors][str_defines::ally][xorstr_("weapon")], &globals::visuals::team_t[int(TEAM::ALLY)].color[int(PCOLOR::WEAPON)]);

		// hotkeys
		load_value(j[str_defines::hotkeys][xorstr_("menu")], &globals::hotkey::menu);
		load_value(j[str_defines::hotkeys][xorstr_("aimbot")], &globals::hotkey::aimbot);
		load_value(j[str_defines::hotkeys][xorstr_("triggerbot")], &globals::hotkey::triggerbot);

		// menu
		save_value(j[str_defines::menu][xorstr_("position")], &globals::menu::pos);
	}
	else
		return false;

	g_pInterfaces->cvar()->ConsoleColorPrintf(Color::Green(), xorstr_("load: %ls\n"), filename.c_str());

	return true;
}
