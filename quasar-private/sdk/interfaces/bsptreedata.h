#pragma once

class ISpatialQuery
{
public:
	// Returns the number of leaves
	virtual int LeafCount() const = 0;
};