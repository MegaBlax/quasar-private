#include "radar.h"
#include "../imgui/imgui_internal.h"
#include "../sdk/offsets.h"
#include "../utils/utils.h"
#include "../utils/custom_winapi.h"
#include "../sdk/cs_player.h"
#include "../sdk/cs_plantedc4.h"

#include "../resources/radar_icons.h"
#include "../imgui/imgui.h"

std::unique_ptr<radar> g_pRadar;

static void SquareConstraint(ImGuiSizeCallbackData* data)
{
	data->DesiredSize = ImVec2(max(data->DesiredSize.x, data->DesiredSize.y), max(data->DesiredSize.x, data->DesiredSize.y));
}

LPDIRECT3DTEXTURE9 FindTexture(const char* name)
{
	return nullptr;
}

ImVec2 GetSize()
{
	return ImVec2();
}

ImVec2 GetPos()
{
	return ImGui::GetWindowPos();
}

void VectorYawRotate(const ImVec2& in, const float flYaw, ImVec2& out)
{
}

void VectorYawRotate2(const ImVec2& center, const float angle, ImVec2* p)
{
}

bool radar::init()
{
	return true;
}

void radar::find_maptexture()
{
}

void radar::run()
{
}

void radar::load_texture()
{
}

ImVec2 radar::world_to_map(const vector& worldpos) const
{
	return ImVec2();
}

ImVec2 radar::map_to_radar(const ImVec2& mappos)
{
	return ImVec2();
}

void radar::getbounds(float& x, float& y, float& wide, float& tall)
{
	
}

ImVec2 radar::is_in_panel(ImVec2& pos)
{
	return ImVec2();
}

ImVec2 radar::world_to_radar(const vector& entity_origin, const vector& local_origin, const float angle, const float zoom = 1.f)
{
	return ImVec2();
}

bool radar::update()
{
	return true;
}