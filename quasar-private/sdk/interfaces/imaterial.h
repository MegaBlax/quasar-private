#pragma once
#include "imaterialvar.h"
//-----------------------------------------------------------------------------
// Shader state flags can be read from the FLAGS materialvar
// Also can be read or written to with the Set/GetMaterialVarFlags() call
// Also make sure you add/remove a string associated with each flag below to CShaderSystem::ShaderStateString in ShaderSystem.cpp
//-----------------------------------------------------------------------------
enum MaterialVarFlags_t
{
	MATERIAL_VAR_DEBUG = (1 << 0),
	MATERIAL_VAR_NO_DEBUG_OVERRIDE = (1 << 1),
	MATERIAL_VAR_NO_DRAW = (1 << 2),
	MATERIAL_VAR_USE_IN_FILLRATE_MODE = (1 << 3),

	MATERIAL_VAR_VERTEXCOLOR = (1 << 4),
	MATERIAL_VAR_VERTEXALPHA = (1 << 5),
	MATERIAL_VAR_SELFILLUM = (1 << 6),
	MATERIAL_VAR_ADDITIVE = (1 << 7),
	MATERIAL_VAR_ALPHATEST = (1 << 8),
//	MATERIAL_VAR_UNUSED					  = (1 << 9),
	MATERIAL_VAR_ZNEARER = (1 << 10),
	MATERIAL_VAR_MODEL = (1 << 11),
	MATERIAL_VAR_FLAT = (1 << 12),
	MATERIAL_VAR_NOCULL = (1 << 13),
	MATERIAL_VAR_NOFOG = (1 << 14),
	MATERIAL_VAR_IGNOREZ = (1 << 15),
	MATERIAL_VAR_DECAL = (1 << 16),
	MATERIAL_VAR_ENVMAPSPHERE = (1 << 17), // OBSOLETE
//	MATERIAL_VAR_UNUSED					  = (1 << 18),
	MATERIAL_VAR_ENVMAPCAMERASPACE = (1 << 19), // OBSOLETE
	MATERIAL_VAR_BASEALPHAENVMAPMASK = (1 << 20),
	MATERIAL_VAR_TRANSLUCENT = (1 << 21),
	MATERIAL_VAR_NORMALMAPALPHAENVMAPMASK = (1 << 22),
	MATERIAL_VAR_NEEDS_SOFTWARE_SKINNING = (1 << 23), // OBSOLETE
	MATERIAL_VAR_OPAQUETEXTURE = (1 << 24),
	MATERIAL_VAR_ENVMAPMODE = (1 << 25), // OBSOLETE
	MATERIAL_VAR_SUPPRESS_DECALS = (1 << 26),
	MATERIAL_VAR_HALFLAMBERT = (1 << 27),
	MATERIAL_VAR_WIREFRAME = (1 << 28),
	MATERIAL_VAR_ALLOWALPHATOCOVERAGE = (1 << 29),
	MATERIAL_VAR_ALPHA_MODIFIED_BY_PROXY = (1 << 30),
	MATERIAL_VAR_VERTEXFOG = (1 << 31),

// NOTE: Only add flags here that either should be read from
// .vmts or can be set directly from client code. Other, internal
// flags should to into the flag enum in IMaterialInternal.h
};

class IMaterial
{
public:
	const char* GetName()
	{
		using fn = const char* (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 0)(this);
	}
	const char* GetTextureGroupName()
	{
		using fn = const char* (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 1)(this);
	}
	IMaterialVar* FindVar( const char *varName, bool* found, const bool complain = true )
	{
		using fn = IMaterialVar* (__thiscall*)(PVOID, const char*, bool*, bool);
		return call_virtual<fn>(this, 11)(this, varName, found, complain);
	}
	bool HasProxy(void)
	{
		using fn = bool(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 21)(this);
	}
	void AlphaModulate(const float alpha)
	{
		using fn = void(__thiscall*)(PVOID, float);
		call_virtual<fn>(this, 27)(this, alpha);
	}
	void ColorModulate(float r, float g, float b)
	{
		using fn = void(__thiscall*)(PVOID, float, float, float);
		call_virtual<fn>(this, 28)(this, r, g, b);
	}
	void SetMaterialVarFlag(MaterialVarFlags_t flag, bool on)
	{
		//using fn = void(__thiscall*)(PVOID, MaterialVarFlags_t, bool);
		//call_virtual<fn>(this, 29)(this, flag, on);
		using fn = void(__thiscall*)(PVOID, int, bool);
		call_virtual<fn>(this, 29)(this, flag, on);
	}
	bool GetMaterialVarFlag(MaterialVarFlags_t flag)
	{
		using fn = bool(__thiscall*)(PVOID, MaterialVarFlags_t);
		return call_virtual<fn>(this, 30)(this, flag);
	}
	IMaterialVar** GetShaderParams(void)
	{
		using fn = IMaterialVar **(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 41)(this);
	}
	bool IsErrorMaterial()
	{
		using fn = bool(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 42)(this);
	}
	float GetAlphaModulation()
	{
		using fn = float(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 44)(this);
	}
	void GetColorModulation(float* r, float* g, float* b)
	{
		using fn = void(__thiscall*)(PVOID, float*, float*, float*);
		call_virtual<fn>(this, 45)(this, r, g, b);
	}
};