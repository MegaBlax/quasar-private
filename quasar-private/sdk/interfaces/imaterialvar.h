#pragma once
class ITexture;

class IMaterialVar
{
public:
	//virtual ITexture* GetTextureValue(void) = 0;
	char const* GetName(void)
	{
		using fn = char const* (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 2)(this);
	}
	void SetFloatValue(const float val)
	{
		using fn = void(__thiscall*)(PVOID, float);
		call_virtual<fn>(this, 4)(this, val);
	}
	
	void SetIntValue(const int val)
	{
		using fn = void(__thiscall*)(PVOID, int);
		call_virtual<fn>(this, 5)(this, val);
	}

	void SetStringValue(char const* val)
	{
		using fn = void(__thiscall*)(PVOID, char const*);
		call_virtual<fn>(this, 6)(this, val);
	}
	
	void SetVecValue( float x, float y, float z )
	{
		using fn = void(__thiscall*)(PVOID,float,float,float);
		call_virtual<fn>(this,11)(this,x,y,z);
	}
};
