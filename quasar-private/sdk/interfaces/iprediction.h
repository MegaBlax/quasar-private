#pragma once

class IPrediction
{
public:
	void Update(int startframe, bool validframe, int incoming_acknowledged, int outgoing_command)
	{
		using fn = void(__thiscall*)(void*, int, bool, int, int);
		call_virtual<fn>(this, 3)(this, startframe, validframe, incoming_acknowledged, outgoing_command);
	}
	void GetLocalViewAngles(qangle& angle)
	{
		using fn = void(__thiscall*)(void*, qangle&);
		call_virtual<fn>(this, 12)(this, angle);
	}
	void SetLocalViewAngles(qangle& angle)
	{
		using fn = void(__thiscall*)(void*, qangle&);
		call_virtual<fn>(this, 13)(this, angle);
	}
	void CheckMovingGround(IClientEntity* player, double frametime)
	{
		using fn = void(__thiscall*)(void*, IClientEntity*, double);
		call_virtual<fn>(this, 18)(this, player, frametime);
	}

	void SetupMove(IClientEntity* player, CUserCmd* ucmd, IMoveHelper* helper, void* movedata)
	{
		using fn = void(__thiscall*)(void*, IClientEntity*, CUserCmd*, IMoveHelper*, void*);
		call_virtual<fn>(this, 20)(this, player, ucmd, helper, movedata);
	}

	void FinishMove(IClientEntity* player, CUserCmd* ucmd, void* movedata)
	{
		using fn = void(__thiscall*)(void*, IClientEntity*, CUserCmd*, void*);
		call_virtual<fn>(this, 21)(this, player, ucmd, movedata);
	}
};