#pragma once
#include "../vector.h"

struct cplane_t
{
	vector	normal;
	float	dist;
	byte	type;			// for fast side tests
	byte	signbits;		// signx + (signy<<1) + (signz<<1)
	byte	pad[2];
};

struct csurface_t
{
	const char* name;
	short		surfaceProps;
	unsigned short	flags;		// BUGBUG: These are declared per surface, not per material, but this database is per-material now
};

struct Ray_t
{
	vector_aligned m_Start;	// starting point, centered within the extents
	vector_aligned m_Delta;	// direction + length of the ray
	vector_aligned m_StartOffset;	// Add this to m_Start to get the actual ray start
	vector_aligned m_Extents;	// Describes an axis aligned box extruded along a ray
	const matrix3x4_t* m_pWorldAxisTransform;
	bool m_IsRay;	// are the extents zero?
	bool m_IsSwept;	// is delta != 0?

	Ray_t() : m_pWorldAxisTransform(nullptr), m_IsRay(false), m_IsSwept(false)
	{
	}

	void Init(vector const& start, vector const& end)
	{
		Assert(&end);
		VectorSubtract(end, start, m_Delta);
		m_IsSwept = (m_Delta.LengthSqr() != 0);
		VectorClear(m_Extents);
		m_pWorldAxisTransform = nullptr;
		m_IsRay = true;
		// Offset m_Start to be in the center of the box...
		VectorClear(m_StartOffset);
		VectorCopy(start, m_Start);
	}

	void Init(vector const& start, vector const& end, vector const& mins, vector const& maxs)
	{
		Assert(&end);
		VectorSubtract(end, start, m_Delta);
		m_pWorldAxisTransform = nullptr;
		m_IsSwept = (m_Delta.LengthSqr() != 0);
		VectorSubtract(maxs, mins, m_Extents);
		m_Extents *= 0.5f;
		m_IsRay = (m_Extents.LengthSqr() < 1e-6);
		// Offset m_Start to be in the center of the box...
		VectorAdd(mins, maxs, m_StartOffset);
		m_StartOffset *= 0.5f;
		VectorAdd(start, m_StartOffset, m_Start);
		m_StartOffset *= -1.0f;
	}

	// compute inverse delta
	vector InvDelta() const
	{
		vector vecInvDelta;
		for (int iAxis = 0; iAxis < 3; ++iAxis)
		{
			if (m_Delta[iAxis] != 0.0f)
			{
				vecInvDelta[iAxis] = 1.0f / m_Delta[iAxis];
			}
			else
			{
				vecInvDelta[iAxis] = FLT_MAX;
			}
		}
		return vecInvDelta;
	}
};