#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"

class event_listener : public IGameEventListener2
{
public:
	event_listener(const std::vector<const char*>& events)
	{
		for (auto& it : events)
		{
			if (!g_pInterfaces->gameeventmanager()->AddListener(this, it, false))
				g_pInterfaces->cvar()->ConsoleColorPrintf(Color::Red(), xorstr_("Failed to register the event: %s!\n"), it); // TODO: Make a conclusion to the logs
		}
	}
	~event_listener()
	{
		g_pInterfaces->gameeventmanager()->RemoveListener(this);
	}
	int GetEventDebugID() override
	{
		return EVENT_DEBUG_ID_INIT;
	}
	bool remove() 
	{
		g_pInterfaces->gameeventmanager()->RemoveListener(this);
		return true;
	}

private:
	void FireGameEvent(IGameEvent* ev) override;
};

extern event_listener* g_pEventmanager;

class match_events : public IMatchEventsSink
{
public:
	match_events()
	{
		g_pInterfaces->matchframework()->GetEventsSubscription()->Subscribe(this);
	}
	~match_events()
	{
		g_pInterfaces->matchframework()->GetEventsSubscription()->Unsubscribe(this);
	}
	void remove()
	{
		g_pInterfaces->matchframework()->GetEventsSubscription()->Unsubscribe(this);
	}
private:
	void OnEvent(KeyValues* pEvent) override;
};

extern match_events* g_pMatcheventmanager;