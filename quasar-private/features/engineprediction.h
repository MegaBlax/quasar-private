#pragma once
#include "../sdk/cs_player.h"

class eprediction
{
public:
    eprediction();
    void run(CUserCmd* cmd);
	void end();
    void clear();
    int flags();
private:
    CUserCmd* _last_cmd;
    CUserCmd* _old_cmd;
    float _old_curtime;
    float _old_frametime;
    int _tickbase;
    int _randomseed;
    int _old_flags;
};

extern std::unique_ptr<eprediction> g_pPrediction;