#include "mem.h"
#include "../utils/custom_winapi.h"

std::unique_ptr<mem> g_pMemory;

mem::mem()
{
	_hProcess = nullptr;
	_shaderapidx9 = {};
	_gameoverlay = {};
	_client = {};
	_engine = {};
	_vstdlib = {};
	_vguimatsurface = {};
	_materialsystem = {};
	_vphysics = {};
	_filesystem = {};
	_panorama = {};
	_vgui2 = {};
	_inputsystem = {};
	_tier0 = {};
	_steamapi = {};
	_studiorender = {};
	_matchmaking = {};
}

auto mem::init() -> bool
{
	_hProcess = _hProcess = g_pWinApi->dwGetCurrentProcess();

	spoofed_GetModuleInfo(FNV("shaderapidx9.dll"), _shaderapidx9);
	if (!_shaderapidx9.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize shaderapidx9.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("gameoverlayrenderer.dll"), _gameoverlay);
	if (!_gameoverlay.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize gameoverlayrenderer.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("client.dll"), _client);
	if (!_client.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize client.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("engine.dll"), _engine);
	if (!_engine.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize engine.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("vstdlib.dll"), _vstdlib);
	if (!_vstdlib.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize vstdlib.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("vguimatsurface.dll"), _vguimatsurface);
	if (!_vguimatsurface.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize vguimatsurface.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("materialsystem.dll"), _materialsystem);
	if (!_materialsystem.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize materialsystem.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("vphysics.dll"), _vphysics);
	if (!_vphysics.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize vphysics.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("filesystem_stdio.dll"), _filesystem);
	if (!_filesystem.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize filesystem_stdio.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("panorama.dll"), _panorama);
	if (!_panorama.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize panorama.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("vgui2.dll"), _vgui2);
	if (!_vgui2.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize vgui2.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("inputsystem.dll"), _inputsystem);
	if (!_inputsystem.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize inputsystem.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("inputsystem.dll"), _inputsystem);
	if (!_inputsystem.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize inputsystem.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("tier0.dll"), _tier0);
	if (!_tier0.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize tier0.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("steam_api.dll"), _steamapi);
	if (!_steamapi.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize steam_api.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("studiorender.dll"), _studiorender);
	if (!_studiorender.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize studiorender.dll"));
		return false;
	}
	spoofed_GetModuleInfo(FNV("matchmaking.dll"), _matchmaking);
	if (!_matchmaking.lpBaseOfDll)
	{
		log_error(xorstr_(L"Failed to initialize matchmaking.dll"));
		return false;
	}

	CloseHandle(_hProcess);

	return true;
}

MODULEINFO mem::dx9() 
{
	return _shaderapidx9;
}

MODULEINFO mem::gameoverlay() 
{
	return _gameoverlay;
}

MODULEINFO mem::client() 
{
	return _client;
}

MODULEINFO mem::engine() 
{
	return _engine;
}

MODULEINFO mem::vstdlib() 
{
	return _vstdlib;
}

MODULEINFO mem::vguimatsurface() 
{
	return _vguimatsurface;
}

MODULEINFO mem::materialsystem() 
{
	return _materialsystem;
}

MODULEINFO mem::vphysics() 
{
	return _vphysics;
}

MODULEINFO mem::filesystem() 
{
	return _filesystem;
}

MODULEINFO mem::panorama() 
{
	return _panorama;
}

MODULEINFO mem::vgui2() 
{
	return _vgui2;
}

MODULEINFO mem::inputsystem() 
{
	return _inputsystem;
}

MODULEINFO mem::tier0() 
{
	return _tier0;
}

MODULEINFO mem::steamapi() 
{
	return _steamapi;
}

MODULEINFO mem::studiorender() 
{
	return _studiorender;
}

MODULEINFO mem::matchmaking() 
{
	return _matchmaking;
}
