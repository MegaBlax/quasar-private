#pragma once
#include <cassert>
#include "vector.h"

// NJS: Inlined to prevent floats from being autopromoted to doubles, as with the old system.
#ifndef RAD2DEG
#define RAD2DEG( x  )  ( (float)(x) * (float)(180.f / M_PI_F) )
#endif

#ifndef DEG2RAD
#define DEG2RAD( x  )  ( (float)(x) * (float)(M_PI_F / 180.f) )
#endif

template <class T>
FORCEINLINE void V_swap(T& x, T& y)
{
	T temp = x;
	x = y;
	y = temp;
}

struct matrix3x4_t
{
	matrix3x4_t() {};
	matrix3x4_t(
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23)
	{
		m_flMatVal[0][0] = m00; m_flMatVal[0][1] = m01; m_flMatVal[0][2] = m02; m_flMatVal[0][3] = m03;
		m_flMatVal[1][0] = m10; m_flMatVal[1][1] = m11; m_flMatVal[1][2] = m12; m_flMatVal[1][3] = m13;
		m_flMatVal[2][0] = m20; m_flMatVal[2][1] = m21; m_flMatVal[2][2] = m22; m_flMatVal[2][3] = m23;
	}
	//-----------------------------------------------------------------------------
	// Creates a matrix where the X axis = forward
	// the Y axis = left, and the Z axis = up
	//-----------------------------------------------------------------------------
	void Init(const vector& xAxis, const vector& yAxis, const vector& zAxis, const vector& vecOrigin)
	{
		m_flMatVal[0][0] = xAxis.x; m_flMatVal[0][1] = yAxis.x; m_flMatVal[0][2] = zAxis.x; m_flMatVal[0][3] = vecOrigin.x;
		m_flMatVal[1][0] = xAxis.y; m_flMatVal[1][1] = yAxis.y; m_flMatVal[1][2] = zAxis.y; m_flMatVal[1][3] = vecOrigin.y;
		m_flMatVal[2][0] = xAxis.z; m_flMatVal[2][1] = yAxis.z; m_flMatVal[2][2] = zAxis.z; m_flMatVal[2][3] = vecOrigin.z;
	}

	//-----------------------------------------------------------------------------
	// Creates a matrix where the X axis = forward
	// the Y axis = left, and the Z axis = up
	//-----------------------------------------------------------------------------
	matrix3x4_t(const vector& xAxis, const vector& yAxis, const vector& zAxis, const vector& vecOrigin)
	{
		Init(xAxis, yAxis, zAxis, vecOrigin);
	}

	void SetOrigin(vector const& p)
	{
		m_flMatVal[0][3] = p.x;
		m_flMatVal[1][3] = p.y;
		m_flMatVal[2][3] = p.z;
	}

	void Invalidate()
	{
		for (auto& i : m_flMatVal)
		{
			for (float& j : i)
			{
				j = std::numeric_limits<float>::infinity();;
			}
		}
	}

	[[nodiscard]] vector GetXAxis()  const { return at(0); }
	[[nodiscard]] vector GetYAxis()  const { return at(1); }
	[[nodiscard]] vector GetZAxis()  const { return at(2); }
	[[nodiscard]] vector GetOrigin() const { return at(3); }

	[[nodiscard]] vector at(int i) const { return vector{ m_flMatVal[0][i], m_flMatVal[1][i], m_flMatVal[2][i] }; }

	float* operator[](int i) { return m_flMatVal[i]; }
	const float* operator[](int i) const { return m_flMatVal[i]; }
	float* Base() { return &m_flMatVal[0][0]; }
	[[nodiscard]] const float* Base() const { return &m_flMatVal[0][0]; }

	float m_flMatVal[3][4];
};

class VMatrix
{
public:
	VMatrix() = default;
	VMatrix(const VMatrix&) = default;
	VMatrix(VMatrix&&) = default;
	
	VMatrix(
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33
	);

	// Creates a matrix where the X axis = forward
	// the Y axis = left, and the Z axis = up
	VMatrix(const vector& forward, const vector& left, const vector& up);

	// Construct from a 3x4 matrix
	VMatrix(const matrix3x4_t& matrix3x4);

	// Set the values in the matrix.
	void  Init(
		float m00, float m01, float m02, float m03,
		float m10, float m11, float m12, float m13,
		float m20, float m21, float m22, float m23,
		float m30, float m31, float m32, float m33
	);


	// Initialize from a 3x4
	void  Init(const matrix3x4_t& matrix3x4);

	// array access
	float* operator[](int i)
	{
		return m[i];
	}

	const float* operator[](int i) const
	{
		return m[i];
	}

	// Get a pointer to m[0][0]
	float* Base()
	{
		return &m[0][0];
	}

	[[nodiscard]] const float* Base() const
	{
		return &m[0][0];
	}

	void  SetLeft(const vector& vLeft);
	void  SetUp(const vector& vUp);
	void  SetForward(const vector& vForward);

	void  GetBasisvectors(vector& vForward, vector& vLeft, vector& vUp) const;
	void  SetBasisvectors(const vector& vForward, const vector& vLeft, const vector& vUp);

	// Get/Set the translation.
	vector& GetTranslation(vector& vTrans) const;
	void  SetTranslation(const vector& vTrans);

	void  PreTranslate(const vector& vTrans);
	void  PostTranslate(const vector& vTrans);

	matrix3x4_t& As3x4();
	[[nodiscard]] const matrix3x4_t& As3x4() const;
	void  CopyFrom3x4(const matrix3x4_t& m3x4);
	void  Set3x4(matrix3x4_t& matrix3x4) const;

	bool operator==(const VMatrix& src) const
	{
		if (*this == src)
			return true;
		for (auto i = 0; i < 4; ++i)
			for (auto j = 0; j < 4; ++j)
				if (m[i][j] != src.m[i][j])
					return false;
		return true;
	}

	bool  operator!=(const VMatrix& src) const { return !(*this == src); }

	// Access the basis vectors.
	[[nodiscard]] vector  GetLeft() const;
	[[nodiscard]] vector  GetUp() const;
	[[nodiscard]] vector  GetForward() const;
	[[nodiscard]] vector  GetTranslation() const;


	// Matrix->vector operations.
	// Multiply by a 3D vector (same as operator*).
	void  V3Mul(const vector& vIn, vector& vOut) const;

	// Multiply by a 4D vector.
	//void  V4Mul( const vector4D &vIn, vector4D &vOut ) const;

	// Applies the rotation (ignores translation in the matrix). (This just calls VMul3x3).
	[[nodiscard]] vector  ApplyRotation(const vector& vVec) const;

	// Multiply by a vector (divides by w, assumes input w is 1).
	vector  operator*(const vector& vVec) const;

	// Multiply by the upper 3x3 part of the matrix (ie: only apply rotation).
	[[nodiscard]] vector  VMul3x3(const vector& vVec) const;

	// Apply the inverse (transposed) rotation (only works on pure rotation matrix)
	[[nodiscard]] vector  VMul3x3Transpose(const vector& vVec) const;

	// Multiply by the upper 3 rows.
	[[nodiscard]] vector  VMul4x3(const vector& vVec) const;

	// Apply the inverse (transposed) transformation (only works on pure rotation/translation)
	[[nodiscard]] vector  VMul4x3Transpose(const vector& vVec) const;


	// Matrix->plane operations.
	//public:
	// Transform the plane. The matrix can only contain translation and rotation.
	//void  TransformPlane( const VPlane &inPlane, VPlane &outPlane ) const;

	// Just calls TransformPlane and returns the result.
	//VPlane  operator*(const VPlane &thePlane) const;

	// Matrix->matrix operations.

	VMatrix& operator=(const VMatrix& mOther) = default;

	// Add two matrices.
	const VMatrix& operator+=(const VMatrix& other);

	// Add/Subtract two matrices.
	VMatrix  operator+(const VMatrix& other) const;
	VMatrix  operator-(const VMatrix& other) const;

	// Negation.
	VMatrix  operator-() const;

	// Return inverse matrix. Be careful because the results are undefined 
	// if the matrix doesn't have an inverse (ie: InverseGeneral returns false).
	//VMatrix  operator~() const;

	// Matrix operations.
	// Set to identity.
	void  Identity();
	[[nodiscard]] bool  IsIdentity() const;
	
	// The matrix.
	float  m[4][4]{};
};

FORCEINLINE float DotProduct(const vector& v1, const vector& v2)
{
	return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

void MatrixSetColumn(const vector& in, int column, matrix3x4_t& out);
void CrossProduct(const vector& v1, const vector& v2, vector& cross);
float VectorNormalize(vector& vec);
void VectorVectors(const vector& forward, vector& right, vector& up);
void VectorTransform(const vector& in1, const matrix3x4_t& in2, vector& out1);
void VectorAngles(const vector& forward, qangle& angles);
void AngleVectors(const qangle& angles, vector* forward);
void AngleVectors(const qangle& angles, vector* forward, vector* right, vector* up);
void VectorMatrix(const vector& forward, matrix3x4_t& matrix);
void VectorRotate(const vector& in1, const matrix3x4_t& in2, float* out);

// Bias takes an X value between 0 and 1 and returns another value between 0 and 1
// The curve is biased towards 0 or 1 based on biasAmt, which is between 0 and 1.
// Lower values of biasAmt bias the curve towards 0 and higher values bias it towards 1.
//
// For example, with biasAmt = 0.2, the curve looks like this:
//
// 1
// |				  *
// |				  *
// |			     *
// |			   **
// |			 **
// |	  	 ****
// |*********
// |___________________
// 0                   1
//
//
// With biasAmt = 0.8, the curve looks like this:
//
// 1
// | 	**************
// |  **
// | * 
// | *
// |* 
// |* 
// |*  
// |___________________
// 0                   1
//
// With a biasAmt of 0.5, Bias returns X.
float Bias(float x, float biasAmt);


// Gain is similar to Bias, but biasAmt biases towards or away from 0.5.
// Lower bias values bias towards 0.5 and higher bias values bias away from it.
//
// For example, with biasAmt = 0.2, the curve looks like this:
//
// 1
// | 				  *
// | 				 *
// | 				**
// |  ***************
// | **
// | *
// |*
// |___________________
// 0                   1
//
//
// With biasAmt = 0.8, the curve looks like this:
//
// 1
// |  		    *****
// |  		 ***
// |  		*
// | 		*
// | 		*
// |   	 ***
// |*****
// |___________________
// 0                   1
float Gain(float x, float biasAmt);


// SmoothCurve maps a 0-1 value into another 0-1 value based on a cosine wave
// where the derivatives of the function at 0 and 1 (and 0.5) are 0. This is useful for
// any fadein/fadeout effect where it should start and end smoothly.
//
// The curve looks like this:
//
// 1
// |  		**
// | 	   *  *
// | 	  *	   *
// | 	  *	   *
// | 	 *		*
// |   **		 **
// |***			   ***
// |___________________
// 0                   1
//
float SmoothCurve(float x, float mult = 0.5f);


// This works like SmoothCurve, with two changes:
//
// 1. Instead of the curve peaking at 0.5, it will peak at flPeakPos.
//    (So if you specify flPeakPos=0.2, then the peak will slide to the left).
//
// 2. flPeakSharpness is a 0-1 value controlling the sharpness of the peak.
//    Low values blunt the peak and high values sharpen the peak.
float SmoothCurve_Tweak(float x, float flPeakPos = 0.5, float flPeakSharpness = 0.5);

// halflife is time for value to reach 50%
inline float ExponentialDecay(float halflife, float dt)
{
	// log(0.5) == -0.69314718055994530941723212145818
	return expf(-0.69314718f / halflife * dt);
}

// decayTo is factor the value should decay to in decayTime
inline float ExponentialDecay(float decayTo, float decayTime, float dt)
{
	return expf(logf(decayTo) / decayTime * dt);
}

// Get the integrated distanced traveled
// decayTo is factor the value should decay to in decayTime
// dt is the time relative to the last velocity update
inline float ExponentialDecayIntegral(float decayTo, float decayTime, float dt)
{
	return (powf(decayTo, dt / decayTime) * decayTime - decayTime) / logf(decayTo);
}

// hermite basis function for smooth interpolation
// Similar to Gain() above, but very cheap to call
// value should be between 0 & 1 inclusive
inline float SimpleSpline(float value)
{
	float valueSquared = value * value;

	// Nice little ease-in, ease-out spline-like curve
	return (3 * valueSquared - 2 * valueSquared * value);
}

// remaps a value in [startInterval, startInterval+rangeInterval] from linear to
// spline using SimpleSpline
inline float SimpleSplineRemapVal(float val, float A, float B, float C, float D)
{
	if (A == B)
		return val >= B ? D : C;
	float cVal = (val - A) / (B - A);
	return C + (D - C) * SimpleSpline(cVal);
}

// remaps a value in [startInterval, startInterval+rangeInterval] from linear to
// spline using SimpleSpline
inline float SimpleSplineRemapValClamped(float val, float A, float B, float C, float D)
{
	if (A == B)
		return val >= B ? D : C;
	float cVal = (val - A) / (B - A);
	cVal = clamp(cVal, 0.0f, 1.0f);
	return C + (D - C) * SimpleSpline(cVal);
}

inline float segment_to_segment(vector s1, vector s2, vector k1, vector k2)
{
	static auto constexpr epsilon = 0.00000001f;

	const auto u = s2 - s1;
	const auto v = k2 - k1;
	const auto w = s1 - k1;

	const auto a = u.Dot(u);
	const auto b = u.Dot(v);
	const auto c = v.Dot(v);
	const auto d = u.Dot(w);
	const auto e = v.Dot(w);
	const auto D = a * c - b * b;
	float sn, sd = D;
	float tn, td = D;

	if (D < epsilon) {
		sn = 0.0f;
		sd = 1.0f;
		tn = e;
		td = c;
	}
	else {
		sn = b * e - c * d;
		tn = a * e - b * d;

		if (sn < 0.0f) {
			sn = 0.0f;
			tn = e;
			td = c;
		}
		else if (sn > sd) {
			sn = sd;
			tn = e + b;
			td = c;
		}
	}

	if (tn < 0.0f) {
		tn = 0.0f;

		if (-d < 0.0f)
			sn = 0.0f;
		else if (-d > a)
			sn = sd;
		else {
			sn = -d;
			sd = a;
		}
	}
	else if (tn > td) {
		tn = td;

		if (-d + b < 0.0f)
			sn = 0.f;
		else if (-d + b > a)
			sn = sd;
		else {
			sn = -d + b;
			sd = a;
		}
	}

	const float sc = abs(sn) < epsilon ? 0.0f : sn / sd;
	const float tc = abs(tn) < epsilon ? 0.0f : tn / td;

	const auto dp = w + u * sc - v * tc;
	return dp.Length();
}

bool Intersect(vector start, vector end, vector a, vector b, float radius); // start, end, min, max, radius
float AngleNormalize(float angle);
void ClampAngles(qangle& angles);
void NormalizeAngles(qangle& angles);