#pragma once
#include <imgui.h>
#include "sdk/enums.h"

namespace globals
{
	namespace vars
	{
		inline HWND window = nullptr;
		inline float fov = 0.0f;
		inline float server_time = 0.0f;
		inline bool unload = false;
		inline bool key_pressed[256] = {};
		inline int game_type = -1;
		inline int game_mode = -1;
		inline bool valve_ds = false;
		inline int server_tickrate = 64;
		inline int client_version = 1165;
		inline int engine_build = 13759;
	}
	namespace walkbot
	{
		inline bool active = false;
		inline bool record = false;
		inline float dist = 100.f;
	}
	namespace menu
	{
		inline bool active = false;
		inline bool load = false;
		inline bool save = false;
		inline int index = 0;
		inline int sub_index1 = 0;
		inline int sub_index2 = 0;
		inline int sub_index3 = 0;
		inline int sub_index4 = 0;
		inline ImVec2 pos = ImVec2(450,200);
	}
	namespace hotkey
	{
		inline int menu = 72; // KEY_INSERT
		inline int aimbot = 107; // MOUSE_LEFT
		inline int triggerbot = 0;
	}
	namespace visuals
	{
		inline bool fov = false;
		inline bool sniper_crosshair = false;
		inline bool recoil_crosshair = false;
		inline bool draw_multipoints = false;
		inline ImVec4 color[int(COLOR::MAX)];
		inline int teamid = 0; // 0 - enemy, 1 - ally
		
		inline struct team_s
		{
			float corner_size = 50;
			int chams_brightness = 255;
			float steps_size = 25.f;
			bool active = false;
			bool visible_check = false;
			bool box = false;
			bool health = false;
			bool name = false;
			bool weapon = false;
			bool steps = false;
			bool chams = false;
			bool chams_flat = false;
			bool chams_visible_check = true;
			bool chams_backtrack = false;
			bool chams_material = false;

			ImVec4 color[int(PCOLOR::MAX)];
		} team_t[2];
	}
	namespace aimbot
	{
		inline bool active = false;
		inline bool smoke_check = false;
		inline bool flash_check = false;
		inline bool jump_check = false;
		inline bool zoom_check = false;
		inline bool ffa_mode = false;
		inline int key_mode = 0;
		
		inline struct weapon_s
		{
			bool active = false;
			bool multipoints = false;
			bool smart = false;
			bool auto_delay = false;
			bool first_bullet_silent = false;
			int hitbox = 0;
			int chance = 100;
			float smooth = 5.f;
			float recoil_smooth = 7.f;
			float fov = 1.f;
			float second_fov = 1.f;
			float first_shot_delay = 0.f;
			float kill_delay = 0.f;
			float multipoints_scale = 1.f;
			int multipoints_value = 4;
		}weapon_t[64];
	}
	namespace triggerbot
	{
		inline bool active = false;
		inline bool smoke_check = false;
		inline bool flash_check = false;
		inline bool zoom_check = false;
		inline int key_mode = 0;
	}
	namespace backtrack
	{
		inline bool active = false;
		inline int time = 200;
	}
	namespace rcs
	{
		inline bool active = false;
		inline float start = 3;
		inline float pitch = 1.25f;
		inline float yaw = 1.75f;
	}
	namespace radar
	{
		inline bool active = false;
		inline ImVec2 pos = ImVec2(200, 200);
		inline float size = 235.f;
		inline float zoom = 1.f;
		inline float alpha = 1.f;
		inline float icon_scale = 7.f;
	}
	namespace misc
	{
		inline bool bunnyhop = false;
		inline bool strafe = false;
		inline bool autoaccept = false;
		inline bool res_window = false;
		inline bool rank = false;
		inline bool hitmarker = false;
	}
	namespace config
	{
		inline int index = 0;
		inline char name[32] = "";
		inline std::vector<std::wstring> arr;
	}
}
