#include "custom_winapi.h"
#include <intrin.h>
#include "fnv_hash.h"
#include "../xorstr.h"

std::unique_ptr<winapi> g_pWinApi;

__forceinline _TEB32* GetTEB()
{
	return reinterpret_cast<_TEB32*>(__readfsdword(0x18));
}

__forceinline _PEB* GetPEB()
{
	auto teb{ GetTEB() };
	if (!teb)
		return nullptr;

	return teb->ProcessEnvironmentBlock;
}

_LIST_ENTRY* GetFirstLoaderModule()
{
	_PEB* const peb = reinterpret_cast<_PEB*>(GetPEB());
	_PEB_LDR_DATA* const ldr_data = peb->Ldr;
	
	return &ldr_data->InLoadOrderModuleList;
}

void __stdcall spoofed_GetModuleInfo(fnv::hash lpModuleName, MODULEINFO& moduleinfo)
{
	moduleinfo = { nullptr, 0, nullptr };

	const PLIST_ENTRY pHeadEntry = GetFirstLoaderModule();

	PLIST_ENTRY pEntry = pHeadEntry->Flink;
	while (pEntry != pHeadEntry)
	{
		// Retrieve the current MODULE_ENTRY
		const PLDR_MODULE pLdrEntry = CONTAINING_RECORD(pEntry, LDR_MODULE, InLoadOrderLinks);
		if (pLdrEntry->SizeOfImage)
		{
			if (pLdrEntry->BaseDllName.Buffer)
			{
				if (fnv::hash_runtime(pLdrEntry->BaseDllName.Buffer) == lpModuleName)
				{
					moduleinfo.EntryPoint = pLdrEntry->EntryPoint;
					moduleinfo.SizeOfImage = pLdrEntry->SizeOfImage;
					moduleinfo.lpBaseOfDll = pLdrEntry->DllBase;
					return;
				}
			}
		}

		// Iterate to the next entry
		pEntry = pEntry->Flink;
	}

}

HMODULE __stdcall spoofed_GetModuleHandleW(IN const LPCWSTR lpModuleName)
{
	const PLIST_ENTRY pHeadEntry = GetFirstLoaderModule();

	PLIST_ENTRY pEntry = pHeadEntry->Flink;
	while (pEntry != pHeadEntry)
	{
		// Retrieve the current MODULE_ENTRY
		const PLDR_MODULE pLdrEntry = CONTAINING_RECORD(pEntry, LDR_MODULE, InLoadOrderLinks);
		if (pLdrEntry->SizeOfImage)
		{
			if (pLdrEntry->BaseDllName.Buffer)
			{
				if (_wcsicmp(pLdrEntry->BaseDllName.Buffer, lpModuleName) == 0)
				{
					return static_cast<HMODULE>(pLdrEntry->DllBase);
				}
			}
		}

		// Iterate to the next entry
		pEntry = pEntry->Flink;
	}
	return nullptr;
}

std::vector<const wchar_t*> __stdcall GetModuleList()
{
	std::vector<const wchar_t*> vec;
	const PLIST_ENTRY pHeadEntry = GetFirstLoaderModule();

	PLIST_ENTRY pEntry = pHeadEntry->Flink;
	while (pEntry != pHeadEntry)
	{
		// Retrieve the current MODULE_ENTRY
		const PLDR_MODULE pLdrEntry = CONTAINING_RECORD(pEntry, LDR_MODULE, InLoadOrderLinks);
		if (pLdrEntry->SizeOfImage)
		{
			if (pLdrEntry->BaseDllName.Buffer)
			{
				if (wcslen(pLdrEntry->BaseDllName.Buffer) > 3)
				{
					vec.push_back(pLdrEntry->BaseDllName.Buffer);
				}
			}
		}

		// Iterate to the next entry
		pEntry = pEntry->Flink;
	}
	return vec;
}

winapi::winapi()
{
	Kernel32Address = GetKernel32Address();
	dwGetModuleHandleA = reinterpret_cast<GetModuleHandleA_t>(GetProcAddress(Kernel32Address, xorstr_("GetModuleHandleA")));
	dwGetModuleHandleW = spoofed_GetModuleHandleW;
	dwLoadLibrary = reinterpret_cast<LoadLibrary_t>(GetProcAddress(Kernel32Address, xorstr_("LoadLibraryA")));

	dwSleep = reinterpret_cast<Sleep_t>(GetProcAddress(Kernel32Address, xorstr_("Sleep")));
	dwGetCurrentProcess = reinterpret_cast<GetCurrentProcess_t>(GetProcAddress(Kernel32Address, xorstr_("GetCurrentProcess")));
	dwCreateFileW = reinterpret_cast<CreateFileW_t>(GetProcAddress(Kernel32Address, xorstr_("CreateFileW")));
	dwCreateFileA = reinterpret_cast<CreateFileA_t>(GetProcAddress(Kernel32Address, xorstr_("CreateFileA")));
	dwReadFile = reinterpret_cast<ReadFile_t>(GetProcAddress(Kernel32Address, xorstr_("ReadFile")));
	dwGetCurrentThreadId = reinterpret_cast<GetCurrentThreadId_t>(GetProcAddress(Kernel32Address, xorstr_("GetCurrentThreadId")));
	dwGetCurrentProcessId = reinterpret_cast<GetCurrentProcessId_t>(GetProcAddress(Kernel32Address, xorstr_("GetCurrentProcessId")));
	dwVirtualProtect = reinterpret_cast<VirtualProtect_t>(GetProcAddress(Kernel32Address, xorstr_("VirtualProtect")));
	dwVirtualQuery = reinterpret_cast<VirtualQuery_t>(GetProcAddress(Kernel32Address, xorstr_("VirtualQuery")));
	dwVirtualAlloc = reinterpret_cast<VirtualAlloc_t>(GetProcAddress(Kernel32Address, xorstr_("VirtualAlloc")));
	dwVirtualFree = reinterpret_cast<VirtualFree_t>(GetProcAddress(Kernel32Address, xorstr_("VirtualFree")));
	dwIsBadCodePtr = reinterpret_cast<IsBadCodePtr_t>(GetProcAddress(Kernel32Address, xorstr_("IsBadCodePtr")));
	dwAttachConsole = reinterpret_cast<AttachConsole_t>(GetProcAddress(Kernel32Address, xorstr_("AttachConsole")));
	dwAllocConsole = reinterpret_cast<AllocConsole_t>(GetProcAddress(Kernel32Address, xorstr_("AllocConsole")));
	dwSwitchToThread = reinterpret_cast<SwitchToThread_t>(GetProcAddress(Kernel32Address, xorstr_("SwitchToThread")));
	dwGetConsoleWindow = reinterpret_cast<GetConsoleWindow_t>(GetProcAddress(Kernel32Address, xorstr_("GetConsoleWindow")));
	dwDisableThreadLibraryCalls = reinterpret_cast<DisableThreadLibraryCalls_t>(GetProcAddress(Kernel32Address, xorstr_("DisableThreadLibraryCalls")));
	dwTerminateThread = reinterpret_cast<TerminateThread_t>(GetProcAddress(Kernel32Address, xorstr_("TerminateThread")));
	dwBeep = reinterpret_cast<Beep_t>(GetProcAddress(Kernel32Address, xorstr_("Beep")));
	dwGetWindowsDirectoryA = reinterpret_cast<GetWindowsDirectoryA_t>(GetProcAddress(Kernel32Address, xorstr_("GetWindowsDirectoryA")));
	dwFreeLibraryAndExitThread = reinterpret_cast<FreeLibraryAndExitThread_t>(GetProcAddress(Kernel32Address, xorstr_("FreeLibraryAndExitThread")));

	const auto User32Address = reinterpret_cast<DWORD>(spoofed_GetModuleHandleW(xorstr_(L"user32.dll")));
	dwCallWindowProcW = reinterpret_cast<CallWindowProcW_t>(GetProcAddress(User32Address, xorstr_("CallWindowProcW")));
	dwShowWindow = reinterpret_cast<ShowWindow_t>(GetProcAddress(User32Address, xorstr_("ShowWindow")));
	dwFindWindowA = reinterpret_cast<FindWindowA_t>(GetProcAddress(User32Address, xorstr_("FindWindowA")));
	dwFindWindowW = reinterpret_cast<FindWindowW_t>(GetProcAddress(User32Address, xorstr_("FindWindowW")));
	dwFlashWindowEx = reinterpret_cast<FlashWindowEx_t>(GetProcAddress(User32Address, xorstr_("FlashWindowEx")));
	dwSetActiveWindow = reinterpret_cast<SetActiveWindow_t>(GetProcAddress(User32Address, xorstr_("SetActiveWindow")));
	dwGetClientRect = reinterpret_cast<GetClientRect_t>(GetProcAddress(User32Address, xorstr_("GetClientRect")));
	dwMessageBoxA = reinterpret_cast<MessageBoxA_t>(GetProcAddress(User32Address, xorstr_("MessageBoxA")));
	dwMessageBoxW = reinterpret_cast<MessageBoxW_t>(GetProcAddress(User32Address, xorstr_("MessageBoxW")));
	dwSetWindowLongW = reinterpret_cast<SetWindowLongW_t>(GetProcAddress(User32Address, xorstr_("SetWindowLongW")));
	dwGetForegroundWindow = reinterpret_cast<GetForegroundWindow_t>(GetProcAddress(User32Address, xorstr_("GetForegroundWindow")));
	dwGetWindowLongW = reinterpret_cast<GetWindowLongW_t>(GetProcAddress(User32Address, xorstr_("GetWindowLongW")));
	dwGetWindowRect = reinterpret_cast<GetWindowRect_t>(GetProcAddress(User32Address, xorstr_("GetWindowRect")));
	dwSetCursor = reinterpret_cast<SetCursor_t>(GetProcAddress(User32Address, xorstr_("SetCursor")));
	dwGetCursor = reinterpret_cast<GetCursor_t>(GetProcAddress(User32Address, xorstr_("GetCursor")));
	dwSetCursorPos = reinterpret_cast<SetCursorPos_t>(GetProcAddress(User32Address, xorstr_("SetCursorPos")));
	dwGetCursorPos = reinterpret_cast<GetCursorPos_t>(GetProcAddress(User32Address, xorstr_("GetCursorPos")));
	dwClientToScreen = reinterpret_cast<ClientToScreen_t>(GetProcAddress(User32Address, xorstr_("ClientToScreen")));
	dwScreenToClient = reinterpret_cast<ScreenToClient_t>(GetProcAddress(User32Address, xorstr_("ScreenToClient")));
	dwSendMessageW = reinterpret_cast<SendMessageW_t>(GetProcAddress(User32Address, xorstr_("SendMessageW")));
	dwGetKeyState = reinterpret_cast<GetKeyState_t>(GetProcAddress(User32Address, xorstr_("GetKeyState")));
	dwIsChild = reinterpret_cast<IsChild_t>(GetProcAddress(User32Address, xorstr_("IsChild")));
	dwSetCapture = reinterpret_cast<SetCapture_t>(GetProcAddress(User32Address, xorstr_("SetCapture")));
	dwReleaseCapture = reinterpret_cast<ReleaseCapture_t>(GetProcAddress(User32Address, xorstr_("ReleaseCapture")));

	const auto Shell32Address = reinterpret_cast<DWORD>(spoofed_GetModuleHandleW(xorstr_(L"shell32.dll")));
	dwShellExecuteA = reinterpret_cast<ShellExecuteA_t>(GetProcAddress(Shell32Address, xorstr_("ShellExecuteA")));

	const auto ntdllAddress = reinterpret_cast<DWORD>(spoofed_GetModuleHandleW(xorstr_(L"ntdll.dll")));
	dwRtlAddVectoredExceptionHandler = reinterpret_cast<AddVectoredExceptionHandler_t>(GetProcAddress(ntdllAddress, xorstr_("RtlAddVectoredExceptionHandler")));
	dwRtlRaiseException = reinterpret_cast<RtlRaiseException_t>(GetProcAddress(ntdllAddress, xorstr_("RtlRaiseException")));
	dwNtContinue = reinterpret_cast<NtContinue_t>(GetProcAddress(ntdllAddress, xorstr_("NtContinue")));
	dwNtRaiseException = reinterpret_cast<NtRaiseException_t>(GetProcAddress(ntdllAddress, xorstr_("NtRaiseException")));
	dwNtReadVirtualMemory = reinterpret_cast<NtReadVirtualMemory_t>(GetProcAddress(ntdllAddress, xorstr_("NtReadVirtualMemory")));
	dwRtlImageNtHeader = reinterpret_cast<RtlImageNtHeader_t>(GetProcAddress(ntdllAddress, xorstr_("RtlImageNtHeader")));

	const auto shlwapiAddress = reinterpret_cast<DWORD>(spoofed_GetModuleHandleW(xorstr_(L"shlwapi.dll")));

	dwPathIsRelativeA = reinterpret_cast<PathIsRelativeA_t>(GetProcAddress(shlwapiAddress, xorstr_("PathIsRelativeA")));
	dwPathIsRelativeW = reinterpret_cast<PathIsRelativeW_t>(GetProcAddress(shlwapiAddress, xorstr_("PathIsRelativeW")));

	unsigned counter = 100;

	DWORD tier0_Address = 0;
	while (!tier0_Address)
	{
		--counter;
		tier0_Address = reinterpret_cast<DWORD>(spoofed_GetModuleHandleW(xorstr_(L"tier0.dll")));
		if (dwSwitchToThread() == 0)
		{
			dwSleep(100);
		}
	}
	if (tier0_Address == 0)
		tier0_Address = reinterpret_cast<DWORD>(dwLoadLibrary(xorstr_("tier0.dll")));
	dwCreateSimpleThread = reinterpret_cast<CreateSimpleThread_t>(GetProcAddress(tier0_Address, xorstr_("CreateSimpleThread")));
	dwAllocateThreadID = reinterpret_cast<AllocateThreadID_t>(GetProcAddress(tier0_Address, xorstr_("AllocateThreadID")));
	dwFreeThreadID = reinterpret_cast<FreeThreadID_t>(GetProcAddress(tier0_Address, xorstr_("FreeThreadID")));
	dwReleaseThreadHandle = reinterpret_cast<ReleaseThreadHandle_t>(GetProcAddress(tier0_Address, xorstr_("ReleaseThreadHandle")));
}

DWORD winapi::GetKernel32Address()
{
	ptrdiff_t teb = (ptrdiff_t)GetPEB();
	teb = *reinterpret_cast<ptrdiff_t*>(teb + 0x0C);
	teb = *reinterpret_cast<ptrdiff_t*>(teb + 0x14);
	teb = *reinterpret_cast<ptrdiff_t*>(teb);
	teb = *reinterpret_cast<ptrdiff_t*>(teb);
	const DWORD kernelAddr = *reinterpret_cast<DWORD*>(teb + 0x10);
	return kernelAddr;
}

PVOID winapi::GetProcAddress(HMODULE dwModule, const char* szProcName) const
{
	return GetProcAddress(reinterpret_cast<DWORD>(dwModule), szProcName);
}

PVOID winapi::GetProcAddress(const DWORD dwModule, const char* szProcName) const
{
	char* module = reinterpret_cast<char*>(dwModule);

	const auto dosHeader = reinterpret_cast<PIMAGE_DOS_HEADER>(module);
	auto ntHeader = reinterpret_cast<PIMAGE_NT_HEADERS>(module + dosHeader->e_lfanew);
	const PIMAGE_OPTIONAL_HEADER optHeader = &ntHeader->OptionalHeader;
	const auto expEntry = static_cast<PIMAGE_DATA_DIRECTORY>(&optHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT]);
	const auto expDir = reinterpret_cast<PIMAGE_EXPORT_DIRECTORY>(module + expEntry->VirtualAddress);

	auto* funcTable = reinterpret_cast<PVOID*>(module + expDir->AddressOfFunctions);
	const auto ordTable = reinterpret_cast<PWORD>(module + expDir->AddressOfNameOrdinals);
	const auto nameTable = reinterpret_cast<PCHAR*>(module + expDir->AddressOfNames);

	void* address = nullptr;

	if (reinterpret_cast<DWORD>(szProcName) >> 16 == 0)
	{
		const WORD ord = LOWORD(szProcName);
		const DWORD ordBase = expDir->Base;

		if (ord < ordBase || ord > ordBase + expDir->NumberOfFunctions)
			return nullptr;

		address = static_cast<PVOID>(module + reinterpret_cast<DWORD>(funcTable[ord - ordBase]));
	}
	else
	{
		for (size_t i = 0; i < expDir->NumberOfNames; ++i)
		{
			if (!strcmp(szProcName, module + reinterpret_cast<DWORD>(nameTable[i])))
			{
				address = static_cast<PVOID>(module + reinterpret_cast<DWORD>(funcTable[ordTable[i]]));
				break;
			}
		}
	}
	if (!address)
	{
		return nullptr;
	}

	if (static_cast<char*>(address) >= reinterpret_cast<char*>(expDir)
		&& (static_cast<char*>(address) < reinterpret_cast<char*>(expDir) + expEntry->Size))
	{
		char* dllName = _strdup(static_cast<char*>(address));

		if (!dllName)
			return nullptr;

		address = nullptr;

		char* funcName = strchr(dllName, '.');
		*funcName++ = 0;

		const size_t len = strlen(dllName) + 1;
		auto* dllNameW = new wchar_t[strlen(dllName) + 1];
		size_t outSize;
		//mbstowcs(dllNameW, dllName, len);
		mbstowcs_s(&outSize, dllNameW, len, dllName, len - 1);

		HMODULE forwardModule = dwGetModuleHandleW(dllNameW);
		if (!forwardModule)
		{
			forwardModule = dwLoadLibrary(dllName);
		}

		if (forwardModule)
			address = GetProcAddress(reinterpret_cast<DWORD>(forwardModule), funcName);

		delete[] dllNameW;
		free(dllName);
		free(funcName);
	}

	return address;
}