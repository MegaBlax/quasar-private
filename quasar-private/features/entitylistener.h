#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"

class entity_listener : public IClientEntityListener
{
public:
	entity_listener()
	{
		if (!g_pInterfaces->cliententitylist()->_entity_listeners.AddToTail(this))
			log_error(xorstr_(L"Failed to initialize IClientEntityListener hook"));
	}
	~entity_listener()
	{
		if (!g_pInterfaces->cliententitylist()->_entity_listeners.FindAndRemove(this))
			log_error(xorstr_(L"Failed to unhook IClientEntityListener. This is fatal"));
	}
	void remove()
	{
		if (!g_pInterfaces->cliententitylist()->_entity_listeners.FindAndRemove(this))
			log_error(xorstr_(L"Failed to unhook IClientEntityListener. This is fatal"));
	}
	
	void OnEntityCreated(IClientEntity* pEntity) override;
	void OnEntityDeleted(IClientEntity* pEntity) override;
};

extern entity_listener* g_pEntitymanager;