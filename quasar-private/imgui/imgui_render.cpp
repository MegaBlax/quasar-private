#include "imgui_render.h"
#include <imgui_internal.h>
#include <imgui_impl_win32.h>
#include <imgui_impl_dx9.h>
#include <d3dx9tex.h>

#include "../resources/res.h"
#include "../sdk/offsets.h"
#include "../utils/utils.h"

std::unique_ptr<imgui_render> g_pImRender;

std::string font_full_path(std::string name)
{
	if (name.empty()) {
		return "";
	}

	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	std::vector<std::string> parts;
	std::stringstream ss(name);
	for (std::string s; ss >> s; parts.push_back(s)){}

	char keyNameBuffer[MAX_PATH];
	char keyValueBuffer[MAX_PATH];

	HKEY fontKey;
	if (RegOpenKeyExA(HKEY_LOCAL_MACHINE, xorstr_(R"(SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts)"), 0, KEY_READ,
	                  &fontKey) == ERROR_SUCCESS) 
	{
		int i = 0;
		int lastError;
		do {
			DWORD keyNameLength = sizeof(keyNameBuffer);
			DWORD keyValueLength = sizeof(keyValueBuffer);

			lastError = RegEnumValueA(fontKey, i, keyNameBuffer, &keyNameLength, nullptr, nullptr,
			                          reinterpret_cast<uint8_t*>(keyValueBuffer), &keyValueLength);
			
			if (lastError == ERROR_SUCCESS) 
			{
				std::string keyName(keyNameBuffer);
				if (keyName.find(xorstr_(" (TrueType)")) != std::string::npos || keyName.find(xorstr_(" (OpenType)")) != std::string::npos) 
				{
					keyName.erase(keyName.length() - 11);
				}

				if (keyName.length() == name.length()) 
				{
					std::transform(keyName.begin(), keyName.end(), keyName.begin(), ::tolower);

					auto foundAll = true;
					for (auto& part : parts) {
						if (keyName.find(part) == std::string::npos) {
							foundAll = false;
							break;
						}
					}

					if (foundAll) 
					{
						char path[MAX_PATH] = { };
						ExpandEnvironmentStringsA((std::string(xorstr_("%windir%\\fonts\\")) + keyValueBuffer).c_str(), path, sizeof(path));
						RegCloseKey(fontKey);

						return path;
					}
				}
			}
			++i;
		} while (lastError != ERROR_NO_MORE_ITEMS);

		RegCloseKey(fontKey);
	}
	return "";
}

imgui_render::imgui_render(): _drawlist(nullptr), _drawlist2(nullptr), _drawlist3(nullptr), _fonts{}, _textures{}
{
}

bool imgui_render::init()
{
	const auto device = reinterpret_cast<IDirect3DDevice9*>(g_pOffsets->shaderapi());
	ImGui::CreateContext();
	if (!ImGui_ImplWin32_Init(globals::vars::window))
	{
		log_error(xorstr_(L"Failed to initialize ImGui_ImplWin32_Init"));
		return false;
	}
	if (!ImGui_ImplDX9_Init(device))
	{
		log_error(xorstr_(L"Failed to initialize ImGui_ImplDX9_Init"));
		return false;
	}

	_drawlist = new ImDrawList(ImGui::GetDrawListSharedData());
	_drawlist2 = new ImDrawList(ImGui::GetDrawListSharedData());
	_drawlist3 = new ImDrawList(ImGui::GetDrawListSharedData());

	_fonts[ImCustomFont_Default] = ImGui::GetIO().Fonts->AddFontFromFileTTF(std::string(g_pUtils->WstringToUTF8(g_pUtils->GetFolderPath()) + R"(\Quasar\Fonts\Arimo.ttf)").c_str(), 13.f);
	_fonts[ImCustomFont_Tabs] = ImGui::GetIO().Fonts->AddFontFromFileTTF(font_full_path(xorstr_("Verdana")).c_str(), 16.f);
	_fonts[ImCustomFont_Hotkey] = ImGui::GetIO().Fonts->AddFontFromFileTTF(font_full_path(xorstr_("Verdana")).c_str(), 12.f);

	if (FAILED(D3DXCreateTextureFromFileInMemoryEx(device, checkmark, sizeof checkmark, D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		D3DUSAGE_DYNAMIC, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &_textures[ImCustomTexture_Checkmark])))
	{
		log_error(xorstr_(L"Failed to initialize texture [checkmark.png]"));
		return false;
	}
	if (FAILED(D3DXCreateTextureFromFileExW(device, std::wstring(g_pUtils->GetIconsPath() + L"\\legit.png").c_str(), D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
			D3DUSAGE_DYNAMIC, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &_textures[ImCustomTexture_Legit])))
	{
		log_error(xorstr_(L"Failed to initialize texture [legit.png]"));
		return false;
	}
	if (FAILED(D3DXCreateTextureFromFileExW(device, std::wstring(g_pUtils->GetIconsPath() + L"\\visuals.png").c_str(), D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		D3DUSAGE_DYNAMIC, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &_textures[ImCustomTexture_Visuals])))
	{
		log_error(xorstr_(L"Failed to initialize texture [visuals.png]"));
		return false;
	}
	if (FAILED(D3DXCreateTextureFromFileExW(device, std::wstring(g_pUtils->GetIconsPath() + L"\\misc.png").c_str(), D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		D3DUSAGE_DYNAMIC, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &_textures[ImCustomTexture_Misc])))
	{
		log_error(xorstr_(L"Failed to initialize texture [misc.png]"));
		return false;
	}
	if (FAILED(D3DXCreateTextureFromFileExW(device, std::wstring(g_pUtils->GetIconsPath() + L"\\config.png").c_str(), D3DX_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT,
		D3DUSAGE_DYNAMIC, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &_textures[ImCustomTexture_Config])))
	{
		log_error(xorstr_(L"Failed to initialize texture [config.png]"));
		return false;
	}
	
	ImGui::StyleColorsDark();

	ImGuiStyle* style = &ImGui::GetStyle();
	ImVec4* colors = style->Colors;

	colors[ImGuiCol_FrameBg] = ImColor(11, 13, 17, 255);
	colors[ImGuiCol_CheckMark] = ImColor(12, 126, 203, 255);
	colors[ImGuiCol_FrameBgActive] = ImColor(11, 100, 145, 255);
	colors[ImGuiCol_SliderGrab] = ImColor(14, 149, 206, 255);
	colors[ImGuiCol_SliderGrabActive] = ImColor(6, 104, 140, 255);
	colors[ImGuiCol_PopupBg] = ImColor(29, 34, 44, 255);
	colors[ImGuiCol_Header] = ImColor(60, 67, 79, 255);
	colors[ImGuiCol_HeaderHovered] = ImColor(50, 57, 69, 255);
	colors[ImGuiCol_HeaderActive] = ImColor(40, 47, 59, 255);
	colors[ImGuiCol_Tab] = ImColor(0, 174, 255, 255);
	colors[ImGuiCol_TabHovered] = ImColor(255, 255, 255, 15);
	colors[ImGuiCol_TabActive] = ImColor(255, 255, 255, 30);
	colors[ImGuiCol_Button] = ImColor(3, 125, 181, 255);
	colors[ImGuiCol_ButtonHovered] = ImColor(13, 160, 228, 255);
	colors[ImGuiCol_ButtonActive] = ImColor(0, 99, 145, 255);
	colors[ImGuiCol_ScrollbarBg] = ImColor(0, 0, 0, 0);
	colors[ImGuiCol_ScrollbarGrab] = ImColor(58, 61, 68, 255);
	colors[ImGuiCol_ScrollbarGrabHovered] = ImColor(93, 96, 101, 255);
	colors[ImGuiCol_ScrollbarGrabActive] = ImColor(69, 73, 79, 255);

	_colors[ImCustomColor_WindowBg_TL] = ImColor(14, 24, 44, 255);
	_colors[ImCustomColor_WindowBg_TR] = ImColor(42, 69, 111, 255);
	_colors[ImCustomColor_WindowBg_BR] = ImColor(7, 30, 74, 255);
	_colors[ImCustomColor_WindowBg_BL] = ImColor(1, 11, 29, 255);
	_colors[ImCustomColor_Line] = ImColor(200, 200, 200, 70);
	_colors[ImCustomColor_Line_Light] = ImColor(200, 200, 200, 90);
	_colors[ImCustomColor_Line_Dark] = ImColor(200, 200, 200, 50);
	_colors[ImCustomColor_Line_Blue] = ImColor(25, 94, 152, 255);
	_colors[ImCustomColor_Text_Shadow] = ImColor(195, 195, 195, 255);
	_colors[ImCustomColor_Hotkey] = ImColor(47, 50, 57, 255);
	_colors[ImCustomColor_Hotkey_Active] = ImColor(14, 16, 21, 255);

	style->WindowRounding = 0;
	style->FrameRounding = 2;
	style->PopupRounding = 2;
	style->GrabMinSize = 8;
	style->WindowPadding.x = 6;
	//style->ItemSpacing.x = 9;

	return true;
}

void imgui_render::begin_scene() const
{
	_drawlist->Clear();
	_drawlist->PushClipRectFullScreen();
}

void imgui_render::end_scene()
{
	_mutex.lock();
	*_drawlist2 = *_drawlist;
	_mutex.unlock();
}

ImDrawList* imgui_render::get_drawlist()
{
	if (_mutex.try_lock())
	{
		*_drawlist3 = *_drawlist2;
		_mutex.unlock();
	}
	return _drawlist3;
}

ImFont* imgui_render::get_font(const ImCustomFont_ id)
{
	if (id > ImCustomFont_COUNT - 1)
		return nullptr;
	
	return _fonts[id];
}

ImVec4 imgui_render::get_color(const ImCustomColor_ id)
{
	if (id > ImCustomColor_COUNT - 1)
		return ImVec4(0, 0, 0, 0);
	
	return _colors[id];
}

ImTextureID imgui_render::get_texture(const ImCustomTexture_ id)
{
	if (id > ImCustomTexture_COUNT - 1)
		return nullptr;

	return _textures[id];
}

void imgui_render::add_line(const ImVec2& a, const ImVec2& b, const ImU32& col, const float thickness) const
{
	_drawlist->AddLine(a, b, col, thickness);
}

void imgui_render::add_rect(const ImVec2& a, const ImVec2& b, const ImU32& col, const float thickness, const float rounding, int rounding_corners_flags) const
{
	_drawlist->AddRect(a, b, col, rounding, rounding_corners_flags, thickness);
}

void imgui_render::add_rect_filled(const ImVec2& a, const ImVec2& b, const ImU32& col, const float rounding, const int rounding_corners_flags) const
{
	_drawlist->AddRectFilled(a, b, col, rounding, rounding_corners_flags);
}

void imgui_render::add_circle(const ImVec2& center, const float radius, const ImU32& col, const int segments, const float thickness) const
{
	_drawlist->AddCircle(center, radius, col, segments, thickness);
}

void imgui_render::add_circle_filled(const ImVec2& center, const float radius, const ImU32& col, const int segments) const
{
	_drawlist->AddCircleFilled(center, radius, col, segments);
}

void imgui_render::add_corner_box(const ImVec2& a, const ImVec2& b, const ImU32& col, const float size, const float thickness) const
{
	const auto lx = (b.x - a.x) / 100 * size;
	const auto ly = (b.y - a.y) / 100 * size;

	add_line(ImVec2(a.x - thickness / 2, a.y), ImVec2(a.x + lx + thickness / 2, a.y), col, thickness);
	add_line(ImVec2(a.x, a.y), ImVec2(a.x, a.y + ly), col, thickness);
	add_line(ImVec2(b.x + thickness / 2, a.y), ImVec2(b.x - lx - thickness / 2, a.y), col, thickness);
	add_line(ImVec2(b.x, a.y), ImVec2(b.x, a.y + ly), col, thickness);
	add_line(ImVec2(b.x + thickness / 2, b.y), ImVec2(b.x - lx - thickness / 2, b.y), col, thickness);
	add_line(ImVec2(b.x, b.y), ImVec2(b.x, b.y - ly), col, thickness);
	add_line(ImVec2(a.x - thickness / 2, b.y), ImVec2(a.x + lx + thickness / 2, b.y), col, thickness);
	add_line(ImVec2(a.x, b.y), ImVec2(a.x, b.y - ly), col, thickness);
}

void imgui_render::add_circle3d(const vector& centre, const float points, const float radius, const float thickness, const ImU32& col) const
{
	const float step = static_cast<float>(M_PI * 2.0f / points);
	for (float a = 0.f; a < (M_PI * 2.0f); a += step)
	{
		vector start(radius * cosf(a) + centre.x, radius * sinf(a) + centre.y, centre.z);
		vector end(radius * cosf(a + step) + centre.x, radius * sinf(a + step) + centre.y, centre.z);
		ImVec2 start2d, end2d;

		if (!g_pUtils->WorldToScreen(start, start2d) || !g_pUtils->WorldToScreen(end, end2d))
			break;

		add_line(ImVec2(start2d.x, start2d.y), ImVec2(end2d.x, end2d.y), col, thickness);
	}
}

void imgui_render::add_text(const ImFont* font, const ImU32& color, const ImU32& shadow_color, const ImVec2& pos, const int flags, const char* format, ...) const
{
	if (!font)
		return;
	if (!font->ContainerAtlas)
		return;
	
	_drawlist->PushTextureID(font->ContainerAtlas->TexID);
	
	char buffer[MAX_BUFFER_SIZE];
	float x = pos.x;
	float y = pos.y;
	
	va_list va;
	va_start(va, format);
	vsnprintf(buffer, MAX_BUFFER_SIZE, format, va);
	va_end(va);

	if (flags & CENTERED_X || flags & CENTERED_Y) {

		const auto size = font->CalcTextSizeA(font->FontSize, FLT_MAX, 0.0f, buffer);
		
		if (flags & CENTERED_X)
			x -= size.x / 2.f;

		if (flags & CENTERED_Y)
			y -= size.y / 2.f;
	}

	if (flags & DROPSHADOW)
		_drawlist->AddText(font, font->FontSize, ImVec2(x + 1, y + 1), shadow_color, buffer);

	if (flags & OUTLINED) {
		_drawlist->AddText(font, font->FontSize, ImVec2(x + 1, y + 1), shadow_color, buffer);
		_drawlist->AddText(font, font->FontSize, ImVec2(x - 1, y - 1), shadow_color, buffer);
		_drawlist->AddText(font, font->FontSize, ImVec2(x + 1, y - 1), shadow_color, buffer);
		_drawlist->AddText(font, font->FontSize, ImVec2(x - 1, y + 1), shadow_color, buffer);
	}
	_drawlist->AddText(font, font->FontSize, ImVec2(x, y), color, buffer);
	_drawlist->PopTextureID();
}
