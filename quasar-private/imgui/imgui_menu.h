#pragma once
#include "../stdafx.h"

class imgui_menu
{
public:
	imgui_menu();
	void run();
	void toggle();
	
private:
	void draw_legit();
	void draw_visuals();
	void draw_misc();
	void draw_config();
	void draw_background(const ImVec2& pos, const ImVec2& size, ImDrawList* drawlist) const;
private:
	int _itemindex = 61;
};

extern std::unique_ptr<imgui_menu> g_pMenu;