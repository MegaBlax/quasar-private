#include "imgui.h"
#include "imgui_internal.h"
#include "imgui_render.h"
#include "../utils/utils.h"
#include "../sdk/key_translation.h"
#include "../sdk/interfaces.h"

static const char* PatchFormatString(const char* fmt)
{
    if (fmt[0] == '%' && fmt[1] == '.' && fmt[2] == '0' && fmt[3] == 'f' && fmt[4] == 0) // Fast legacy path for "%.0f" which is expected to be the most common case.
        return "%d";
    const char* fmt_start = ImParseFormatFindStart(fmt);    // Find % (if any, and ignore %%)
    const char* fmt_end = ImParseFormatFindEnd(fmt_start);  // Find end of format specifier, which itself is an exercise of confidence/recklessness (because snprintf is dependent on libc or user).
    if (fmt_end > fmt_start && fmt_end[-1] == 'f')
    {
#ifndef IMGUI_DISABLE_OBSOLETE_FUNCTIONS
        if (fmt_start == fmt && fmt_end[0] == 0)
            return "%d";
        ImGuiContext& g = *GImGui;
        ImFormatString(g.TempBuffer, IM_ARRAYSIZE(g.TempBuffer), "%.*s%%d%s", (int)(fmt_start - fmt), fmt, fmt_end); // Honor leading and trailing decorations, but lose alignment/precision.
        return g.TempBuffer;
#else
        IM_ASSERT(0 && "DragInt(): Invalid format string!"); // Old versions used a default parameter of "%.0f", please replace with e.g. "%d"
#endif
    }
    return fmt;
}

static float CalcMaxPopupHeightFromItemCount(int items_count)
{
    ImGuiContext& g = *GImGui;
    if (items_count <= 0)
        return FLT_MAX;
    return (g.FontSize + g.Style.ItemSpacing.y) * items_count - g.Style.ItemSpacing.y + (g.Style.WindowPadding.y * 2);
}

static void RenderArrowsForVerticalBar(ImDrawList* draw_list, ImVec2 pos, ImVec2 half_sz, float bar_w, float alpha)
{
    ImU32 alpha8 = IM_F32_TO_INT8_SAT(alpha);
    ImGui::RenderArrowPointingAt(draw_list, ImVec2(pos.x + half_sz.x + 1, pos.y), ImVec2(half_sz.x + 2, half_sz.y + 1), ImGuiDir_Right, IM_COL32(0, 0, 0, alpha8));
    ImGui::RenderArrowPointingAt(draw_list, ImVec2(pos.x + half_sz.x, pos.y), half_sz, ImGuiDir_Right, IM_COL32(255, 255, 255, alpha8));
    ImGui::RenderArrowPointingAt(draw_list, ImVec2(pos.x + bar_w - half_sz.x - 1, pos.y), ImVec2(half_sz.x + 2, half_sz.y + 1), ImGuiDir_Left, IM_COL32(0, 0, 0, alpha8));
    ImGui::RenderArrowPointingAt(draw_list, ImVec2(pos.x + bar_w - half_sz.x, pos.y), half_sz, ImGuiDir_Left, IM_COL32(255, 255, 255, alpha8));
}

void ImGui::QSameLine()
{
    SameLine();
    SetCursorPosX(GetContentRegionMax().x / 2);
}

void ImGui::AddBorderIsWindowMoving()
{
    ImGuiWindow* window = GetCurrentWindow();
    ImGuiContext& g = *GImGui;
    if (!g.MovingWindow)
        return;

    const std::string window_name = window->Name;
    const std::string moving_name = g.MovingWindow->Name;
    if (moving_name.find(window_name) == std::string::npos)
        return;

    GetForegroundDrawList(window)->AddRect(GetWindowPos(), GetWindowPos() + GetWindowSize(), GetColorU32(ImGuiCol_ButtonActive));
}

ImU32 ImGui::QGetColorU32(const ImVec4& col, const float alpha)
{
    ImVec4 c = col;
    c.w = alpha;
    return ColorConvertFloat4ToU32(c);
}

bool ImGui::QSliderScalar(const char* label, const float width, ImGuiDataType data_type, void* p_data, const void* p_min, const void* p_max, const char* format)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;
    
    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    const float w = CalcItemWidth();
    const float h = 8.f;

    const ImVec2 label_size = CalcTextSize(label, NULL, true);
    const ImVec2 cursor_pos = ImVec2(window->DC.CursorPos.x, window->DC.CursorPos.y + label_size.y + style.ItemInnerSpacing.y);
    const ImRect frame_bb(cursor_pos, cursor_pos + ImVec2(width > 0 ? width : w, h));
    const ImRect total_bb(ImVec2(frame_bb.Min.x + 1, frame_bb.Min.y - style.ItemInnerSpacing.y - label_size.y), frame_bb.Max);

    ItemSize(total_bb, style.FramePadding.y);
    if (!ItemAdd(total_bb, id, &frame_bb))
        return false;

    // Default format string when passing NULL
    if (format == NULL)
        format = DataTypeGetInfo(data_type)->PrintFmt;
    else if (data_type == ImGuiDataType_S32 && strcmp(format, "%d") != 0) // (FIXME-LEGACY: Patch old "%.0f" format string to use "%d", read function more details.)
        format = PatchFormatString(format);

    // Tabbing or CTRL-clicking on Slider turns it into an input box
    const bool hovered = ItemHoverable(frame_bb, id);
    bool temp_input_is_active = TempInputIsActive(id);
    bool temp_input_start = false;
    if (!temp_input_is_active)
    {
        const bool focus_requested = FocusableItemRegister(window, id);
        const bool clicked = (hovered && g.IO.MouseClicked[0]);
        if (focus_requested || clicked || g.NavActivateId == id || g.NavInputId == id)
        {
            SetActiveID(id, window);
            SetFocusID(id, window);
            FocusWindow(window);
            g.ActiveIdUsingNavDirMask |= (1 << ImGuiDir_Left) | (1 << ImGuiDir_Right);
            if (focus_requested || (clicked && g.IO.KeyCtrl) || g.NavInputId == id)
            {
                temp_input_start = true;
                FocusableItemUnregister(window);
            }
        }
    }
    if (temp_input_is_active || temp_input_start)
        return TempInputScalar(frame_bb, id, label, data_type, p_data, format);

    // Draw frame
    RenderNavHighlight(frame_bb, id);
    RenderFrame(frame_bb.Min, frame_bb.Max, GetColorU32(ImGuiCol_FrameBg), true, 4.f);

    // Slider behavior
    ImRect grab_bb;
    const bool value_changed = SliderBehavior(frame_bb, id, data_type, p_data, p_min, p_max, format, 1, ImGuiSliderFlags_None, &grab_bb);
    if (value_changed)
        MarkItemEdited(id);

    // Render grab
    if (grab_bb.Max.x > grab_bb.Min.x)
    {
        window->DrawList->AddRectFilled(frame_bb.Min + 1, ImVec2(grab_bb.GetCenter().x/*grab_bb.Max.x + 1*/, frame_bb.Max.y - 1), GetColorU32(ImGuiCol_FrameBgActive), 4.f);
        window->DrawList->AddCircleFilled(grab_bb.GetCenter(), 6.f, GetColorU32(ImGuiCol_SliderGrab), 36);
    	if (g.ActiveId == id)
    		window->DrawList->AddCircleFilled(grab_bb.GetCenter(), 3.f, GetColorU32(ImGuiCol_SliderGrabActive), 36);
    }

    // Display value using user-provided display format so user can add prefix/suffix/decorations to the value.
    char value_buf[64];
    const char* value_buf_end = value_buf + DataTypeFormatString(value_buf, IM_ARRAYSIZE(value_buf), data_type, p_data, format);
    RenderText(ImVec2(total_bb.Min.x + label_size.x + style.ItemInnerSpacing.x, total_bb.Min.y), value_buf, value_buf_end);

    if (label_size.x > 0.0f)
    {
        const ImU32 col_text = GetColorU32(g_pImRender->get_color(ImCustomColor_Text_Shadow));
        window->DrawList->AddText(g_pImRender->get_font(ImCustomFont_Default), 13.f, total_bb.Min, col_text, label);
    }

    return value_changed;
}

bool ImGui::QBeginCombo(const char* label, const float width, const char* preview_value, ImGuiComboFlags flags)
{
    // Always consume the SetNextWindowSizeConstraint() call in our early return paths
    ImGuiContext& g = *GImGui;
    bool has_window_size_constraint = (g.NextWindowData.Flags & ImGuiNextWindowDataFlags_HasSizeConstraint) != 0;
    g.NextWindowData.Flags &= ~ImGuiNextWindowDataFlags_HasSizeConstraint;

    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    IM_ASSERT((flags & (ImGuiComboFlags_NoArrowButton | ImGuiComboFlags_NoPreview)) != (ImGuiComboFlags_NoArrowButton | ImGuiComboFlags_NoPreview)); // Can't use both flags together

    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);

    const ImVec2 label_size = CalcTextSize(label, NULL, true);
    const ImVec2 cursor_pos = ImVec2(window->DC.CursorPos.x, window->DC.CursorPos.y + label_size.y + style.ItemInnerSpacing.y);
    const ImRect frame_bb(cursor_pos, cursor_pos + ImVec2(width, label_size.y + style.FramePadding.y * 2.0f));
    const ImRect total_bb(ImVec2(frame_bb.Min.x + 1, frame_bb.Min.y - style.ItemInnerSpacing.y - label_size.y), frame_bb.Max);

    ItemSize(total_bb, style.FramePadding.y);
    if (!ItemAdd(total_bb, id, &frame_bb))
        return false;

    bool hovered, held;
    bool pressed = ButtonBehavior(frame_bb, id, &hovered, &held);
    bool popup_open = IsPopupOpen(id);

    //const ImU32 frame_col = GetColorU32(hovered ? ImGuiCol_ButtonHovered : ImGuiCol_WindowBg);
    const ImU32 col_line = GetColorU32(g_pImRender->get_color(ImCustomColor_Line));
    const ImU32 col_line_blue = GetColorU32(g_pImRender->get_color(ImCustomColor_Line_Blue));
    const float value_x2 = ImMax(frame_bb.Min.x, frame_bb.Max.x);
    RenderNavHighlight(frame_bb, id);
    if (!(flags & ImGuiComboFlags_NoPreview))
    {
        const ImU32 col = popup_open ? IM_COL32_BLACK : GetColorU32(ImGuiCol_FrameBg);
        window->DrawList->AddRectFilled(frame_bb.Min, ImVec2(value_x2, frame_bb.Max.y), col, style.FrameRounding);
        window->DrawList->AddRect(frame_bb.Min, frame_bb.Max, popup_open ? col_line_blue : col_line);
    }
    if (!(flags & ImGuiComboFlags_NoArrowButton))
    {
        ImU32 bg_col = GetColorU32((popup_open || hovered) ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
        ImU32 text_col = GetColorU32(ImGuiCol_Text);
        window->DrawList->AddRectFilled(ImVec2(value_x2, frame_bb.Min.y), frame_bb.Max, bg_col, style.FrameRounding);
        if (value_x2 - style.FramePadding.x <= frame_bb.Max.x)
            RenderArrow(window->DrawList, ImVec2(value_x2 + style.FramePadding.y, frame_bb.Min.y + style.FramePadding.y), text_col, ImGuiDir_Down, 1.0f);
    }
    RenderFrameBorder(frame_bb.Min, frame_bb.Max, style.FrameRounding);
    if (preview_value != NULL && !(flags & ImGuiComboFlags_NoPreview))
        RenderTextClipped(frame_bb.Min + style.FramePadding, ImVec2(value_x2, frame_bb.Max.y), preview_value, NULL, NULL, ImVec2(0.0f, 0.0f));

    if (label_size.x > 0)
    {
        const ImU32 col_text = GetColorU32(g_pImRender->get_color(ImCustomColor_Text_Shadow));
        window->DrawList->AddText(g_pImRender->get_font(ImCustomFont_Default), 13.f, total_bb.Min, col_text, label);
    }

    if ((pressed || g.NavActivateId == id) && !popup_open)
    {
        if (window->DC.NavLayerCurrent == 0)
            window->NavLastIds[0] = id;
        OpenPopupEx(id);
        popup_open = true;
    }

    if (!popup_open)
        return false;

    if (has_window_size_constraint)
    {
        g.NextWindowData.Flags |= ImGuiNextWindowDataFlags_HasSizeConstraint;
        g.NextWindowData.SizeConstraintRect.Min.x = ImMax(g.NextWindowData.SizeConstraintRect.Min.x, width);
    }
    else
    {
        if ((flags & ImGuiComboFlags_HeightMask_) == 0)
            flags |= ImGuiComboFlags_HeightRegular;
        IM_ASSERT(ImIsPowerOfTwo(flags & ImGuiComboFlags_HeightMask_));    // Only one
        int popup_max_height_in_items = -1;
        if (flags & ImGuiComboFlags_HeightRegular)     popup_max_height_in_items = 8;
        else if (flags & ImGuiComboFlags_HeightSmall)  popup_max_height_in_items = 4;
        else if (flags & ImGuiComboFlags_HeightLarge)  popup_max_height_in_items = 20;
        SetNextWindowSizeConstraints(ImVec2(width, 0.0f), ImVec2(width, CalcMaxPopupHeightFromItemCount(popup_max_height_in_items)));
    }

    char name[16];
    ImFormatString(name, IM_ARRAYSIZE(name), "##Combo_%02d", g.BeginPopupStack.Size); // Recycle windows based on depth

    // Peak into expected window size so we can position it
    if (ImGuiWindow* popup_window = FindWindowByName(name))
        if (popup_window->WasActive)
        {
            ImVec2 size_expected = CalcWindowExpectedSize(popup_window);
            if (flags & ImGuiComboFlags_PopupAlignLeft)
                popup_window->AutoPosLastDirection = ImGuiDir_Left;
            ImRect r_outer = GetWindowAllowedExtentRect(popup_window);
            ImVec2 pos = FindBestWindowPosForPopupEx(frame_bb.GetBL(), size_expected, &popup_window->AutoPosLastDirection, r_outer, frame_bb, ImGuiPopupPositionPolicy_ComboBox);
            SetNextWindowPos(pos);
        }

    // We don't use BeginPopupEx() solely because we have a custom name string, which we could make an argument to BeginPopupEx()
    ImGuiWindowFlags window_flags = ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_Popup | ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoMove;

    // Horizontally align ourselves with the framed text
    PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(style.FramePadding.x, style.WindowPadding.y));
    bool ret = Begin(name, NULL, window_flags);
    PopStyleVar();
    if (!ret)
    {
        EndPopup();
        IM_ASSERT(0);   // This should never happen as we tested for IsPopupOpen() above
        return false;
    }
    return true;
}

void ImGui::QTabs(const std::vector<const char*>& items, const ImVec2& size_arg, int* current_item)
{
    const size_t size = items.size();

    PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(2, 0));
    for (auto index = 0; index < (int)size; ++index)
    {
        const auto active = *current_item == index;
        const auto id = ImCustomTexture_Legit + index;
        ImTextureID texture = g_pImRender->get_texture((ImCustomTexture_)id);
        if (QTabButton(items[index], texture, size_arg, active))
            *current_item = index;

        SameLine();
    }
    PopStyleVar();
}

void ImGui::QSubTabs(const std::vector<const char*>& items, int* current_item, const float height)
{
    const size_t size = items.size();
    const float w = GetWindowContentRegionWidth();
    PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
    for(auto index = 0; index < (int)size; ++index)
    {
        const auto active = *current_item == index;
        if (QSubTabButton(items[index], ImVec2(w, height), active))
            *current_item = index;
    }
    PopStyleVar();
}

bool ImGui::QTabButton(const char* label, ImTextureID user_texture_id, const ImVec2& size_arg, const bool active)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    const ImVec2 label_size = CalcTextSize(label, NULL, true);

    ImVec2 pos = window->DC.CursorPos + ImVec2(0, 1);
    ImVec2 size = CalcItemSize(size_arg, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

    const ImRect bb(pos, pos + size);
    ItemSize(size, style.FramePadding.y);
    if (!ItemAdd(bb, id))
        return false;

    bool hovered, held;
    bool pressed = ButtonBehavior(bb, id, &hovered, &held);
	
    RenderNavHighlight(bb, id);

    const ImU32 col = GetColorU32(active ? ImGuiCol_TabActive : ImGuiCol_TabHovered);
    RenderNavHighlight(bb, id);
    if (active || hovered)
        window->DrawList->AddRectFilled(bb.Min, bb.Max, col);
    if (active)
        window->DrawList->AddRectFilled(ImVec2(bb.Min.x,bb.Max.y), ImVec2(bb.Max.x, bb.Max.y - 3), GetColorU32(ImGuiCol_Tab));
    
    if (user_texture_id)
    {
        const ImVec2 img_pos = ImVec2(bb.Min.x + 5, bb.GetCenter().y - size_arg.y / 4);
        window->DrawList->AddImage(user_texture_id, img_pos, img_pos + size_arg.y / 2);
    }
    RenderTextClipped(bb.Min + style.FramePadding, bb.Max - style.FramePadding, label, NULL, &label_size, style.ButtonTextAlign, &bb);
	
    return pressed;
}

bool ImGui::QSubTabButton(const char* label, const ImVec2& size_arg, const bool active)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    const ImVec2 label_size = CalcTextSize(label, NULL, true);

    ImVec2 pos = window->DC.CursorPos + ImVec2(0,1);
    ImVec2 size = CalcItemSize(size_arg, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

    const ImRect bb(pos, pos + size);
    ItemSize(size, style.FramePadding.y);
    if (!ItemAdd(bb, id))
        return false;

    bool hovered, held;
    bool pressed = ButtonBehavior(bb, id, &hovered, &held);

    const ImU32 col = GetColorU32(active ? ImGuiCol_TabActive : ImGuiCol_TabHovered);
    RenderNavHighlight(bb, id);
	if (active || hovered)
    window->DrawList->AddRectFilled(bb.Min,bb.Max, col);
	if (active)
		window->DrawList->AddRectFilled(bb.Min,ImVec2(bb.Min.x + 3,bb.Max.y), GetColorU32(ImGuiCol_Tab));

    RenderTextClipped(bb.Min + style.FramePadding, bb.Max - style.FramePadding, label, NULL, &label_size, style.ButtonTextAlign, &bb);

    return pressed;
}

bool ImGui::QCheckbox(const char* label, bool* v)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    const ImVec2 label_size = CalcTextSize(label, NULL, true);

    const float square_sz = GetFrameHeight();

    const ImVec2 pos = window->DC.CursorPos;
    const ImRect total_bb(pos, pos + ImVec2(square_sz + (label_size.x > 0.0f ? style.ItemInnerSpacing.x + label_size.x : 0.0f), label_size.y + style.FramePadding.y * 2.0f));
    ItemSize(total_bb, style.FramePadding.y);
    if (!ItemAdd(total_bb, id))
        return false;

    bool hovered, held;
    bool pressed = ButtonBehavior(total_bb, id, &hovered, &held);
    if (pressed)
    {
        *v = !(*v);
        MarkItemEdited(id);
    }

    const ImU32 col_line = GetColorU32(g_pImRender->get_color(ImCustomColor_Line));
    const ImU32 col_line_light = GetColorU32(g_pImRender->get_color(ImCustomColor_Line_Light));
    const ImU32 col_line_dark = GetColorU32(g_pImRender->get_color(ImCustomColor_Line_Dark));
    const ImRect check_bb(pos, pos + ImVec2(square_sz, square_sz));
	
    RenderNavHighlight(total_bb, id);
    RenderFrame(check_bb.Min, check_bb.Max, GetColorU32(ImGuiCol_FrameBg), true, style.FrameRounding);
    window->DrawList->AddRect(check_bb.Min, check_bb.Max, col_line, style.FrameRounding);

    if (hovered)
		window->DrawList->AddRect(check_bb.Min, check_bb.Max, held ? col_line_dark : col_line_light, style.FrameRounding);
	
    if (*v)
    {
        ImTextureID texture = g_pImRender->get_texture(ImCustomTexture_Checkmark);
    	if (texture)
			window->DrawList->AddImage(texture, check_bb.Min + 1, check_bb.Max - 1,ImVec2(0,0),ImVec2(1,1),GetColorU32(ImGuiCol_CheckMark));
    }
	
    if (label_size.x > 0.0f)
        RenderText(ImVec2(check_bb.Max.x + style.ItemInnerSpacing.x, check_bb.Min.y + style.FramePadding.y), label);

    return pressed;
}

bool ImGui::QSliderFloat(const char* label, const float width, float* v, float v_min, float v_max, const char* format)
{
    const auto ret = QSliderScalar(label, width, ImGuiDataType_Float, v, &v_min, &v_max, format);
    *v = clamp(*v, v_min, v_max);
    return ret;
}

bool ImGui::QSliderInt(const char* label, const float width, int* v, int v_min, int v_max, const char* format)
{
    const auto ret = QSliderScalar(label, width, ImGuiDataType_S32, v, &v_min, &v_max, format);
    *v = clamp(*v, v_min, v_max);
    return ret;
}

bool ImGui::QCombo(const char* label, const float width, const std::vector<const char*>& items,  int* current_item)
{
    const size_t size = items.size();
    const char* preview_value = items[*current_item];

    if (!QBeginCombo(label, width, preview_value, ImGuiComboFlags_NoArrowButton))
        return false;
	
    bool value_changed = false;
    for (int index = 0; index < (int)size; ++index)
    {
        PushID((void*)(intptr_t)index);
        const char* item_text = items[index];
        const bool item_selected = (index == *current_item);
        if (Selectable(item_text, item_selected,0,ImVec2(width,0)))
        {
            value_changed = true;
            *current_item = index;
        }
        if (item_selected)
            SetItemDefaultFocus();
        PopID();
    }
    EndCombo();
    return value_changed;
}

bool ImGui::QPCombo(const char* label, const float width, const std::vector<std::pair<int, const char*>>& items, int* current_item)
{
    const char* preview_value = nullptr;
    for (const auto& at : items)
    {
        if (*current_item == at.first)
            preview_value = at.second;
    }
    if (!QBeginCombo(label, width, preview_value, ImGuiComboFlags_NoArrowButton))
        return false;
	
    bool value_changed = false;
    for(auto& it : items)
    {
        PushID((void*)(intptr_t)it.first);
        const char* item_text = it.second;
        const bool item_selected = (it.first == *current_item);
        if (Selectable(item_text, item_selected,0,ImVec2(width,0)))
        {
            value_changed = true;
            *current_item = it.first;
        }
        if (item_selected)
            SetItemDefaultFocus();
        PopID();
    }

    EndCombo();
    return value_changed;
}

bool ImGui::QListBox(const char* label, int* current_item, const std::vector<std::wstring>& items, const float height)
{
    const ImGuiStyle& style = GetStyle();
    ImGuiContext& g = *GImGui;
    const size_t items_size = items.size();
    const ImVec2 size_arg = ImVec2(GetWindowContentRegionWidth(), height > 0.f ? height : (g.Font->FontSize + style.FramePadding.y * 2.f) * items_size);
    
    if (!ListBoxHeader(label, size_arg))
        return false;
	
    bool value_changed = false;
    for (auto index = 0; index < (int)items_size; ++index)
    {
        const bool is_selected = (*current_item == index);
        PushID((void*)(intptr_t)index);
        if (Selectable(g_pUtils->WstringToUTF8(items[index]).c_str(), is_selected))
        {
            *current_item = index;
            value_changed = true;
        }
        if (is_selected)
            SetItemDefaultFocus();   // Set the initial focus when opening the combo (scrolling + for keyboard navigation support in the upcoming navigation branch)
        PopID();
    }

    ListBoxFooter();
    if (value_changed)
        MarkItemEdited(g.CurrentWindow->DC.LastItemId);
    
    return value_changed;
}

bool ImGui::QColorEdit4(const char* label, float col[4], ImGuiColorEditFlags flags)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const float square_sz = GetFrameHeight();
    const float w_full = CalcItemWidth();
    const float w_button = (flags & ImGuiColorEditFlags_NoSmallPreview) ? 0.0f : (square_sz + style.ItemInnerSpacing.x);
    const float w_inputs = w_full - w_button;
    const char* label_display_end = FindRenderedTextEnd(label);
    g.NextItemData.ClearFlags();

    BeginGroup();
    PushID(label);

    // If we're not showing any slider there's no point in doing any HSV conversions
    const ImGuiColorEditFlags flags_untouched = flags;
    if (flags & ImGuiColorEditFlags_NoInputs)
        flags = (flags & (~ImGuiColorEditFlags__DisplayMask)) | ImGuiColorEditFlags_DisplayRGB | ImGuiColorEditFlags_NoOptions;

    // Context menu: display and modify options (before defaults are applied)
    if (!(flags & ImGuiColorEditFlags_NoOptions))
        ColorEditOptionsPopup(col, flags);

    // Read stored options
    if (!(flags & ImGuiColorEditFlags__DisplayMask))
        flags |= (g.ColorEditOptions & ImGuiColorEditFlags__DisplayMask);
    if (!(flags & ImGuiColorEditFlags__DataTypeMask))
        flags |= (g.ColorEditOptions & ImGuiColorEditFlags__DataTypeMask);
    if (!(flags & ImGuiColorEditFlags__PickerMask))
        flags |= (g.ColorEditOptions & ImGuiColorEditFlags__PickerMask);
    if (!(flags & ImGuiColorEditFlags__InputMask))
        flags |= (g.ColorEditOptions & ImGuiColorEditFlags__InputMask);
    flags |= (g.ColorEditOptions & ~(ImGuiColorEditFlags__DisplayMask | ImGuiColorEditFlags__DataTypeMask | ImGuiColorEditFlags__PickerMask | ImGuiColorEditFlags__InputMask));
    IM_ASSERT(ImIsPowerOfTwo(flags & ImGuiColorEditFlags__DisplayMask)); // Check that only 1 is selected
    IM_ASSERT(ImIsPowerOfTwo(flags & ImGuiColorEditFlags__InputMask));   // Check that only 1 is selected

    const bool alpha = (flags & ImGuiColorEditFlags_NoAlpha) == 0;
    const bool hdr = (flags & ImGuiColorEditFlags_HDR) != 0;
    const int components = alpha ? 4 : 3;

    // Convert to the formats we need
    float f[4] = { col[0], col[1], col[2], alpha ? col[3] : 1.0f };
    if ((flags & ImGuiColorEditFlags_InputHSV) && (flags & ImGuiColorEditFlags_DisplayRGB))
        ColorConvertHSVtoRGB(f[0], f[1], f[2], f[0], f[1], f[2]);
    else if ((flags & ImGuiColorEditFlags_InputRGB) && (flags & ImGuiColorEditFlags_DisplayHSV))
    {
        // Hue is lost when converting from greyscale rgb (saturation=0). Restore it.
        ColorConvertRGBtoHSV(f[0], f[1], f[2], f[0], f[1], f[2]);
        if (memcmp(g.ColorEditLastColor, col, sizeof(float) * 3) == 0)
        {
            if (f[1] == 0)
                f[0] = g.ColorEditLastHue;
            if (f[2] == 0)
                f[1] = g.ColorEditLastSat;
        }
    }
    int i[4] = { IM_F32_TO_INT8_UNBOUND(f[0]), IM_F32_TO_INT8_UNBOUND(f[1]), IM_F32_TO_INT8_UNBOUND(f[2]), IM_F32_TO_INT8_UNBOUND(f[3]) };

    bool value_changed = false;
    bool value_changed_as_float = false;

    const ImVec2 pos = window->DC.CursorPos;
    const float inputs_offset_x = (style.ColorButtonPosition == ImGuiDir_Left) ? w_button : 0.0f;
    window->DC.CursorPos.x = pos.x + inputs_offset_x;

    ImGuiWindow* picker_active_window = NULL;
    if (!(flags & ImGuiColorEditFlags_NoSmallPreview))
    {
        const float button_offset_x = ((flags & ImGuiColorEditFlags_NoInputs) || (style.ColorButtonPosition == ImGuiDir_Left)) ? 0.0f : w_inputs + style.ItemInnerSpacing.x;
        window->DC.CursorPos = ImVec2(pos.x + button_offset_x, pos.y);

        const ImVec4 col_v4(col[0], col[1], col[2], alpha ? col[3] : 1.0f);
        if (ColorButton(xorstr_("##ColorButton"), col_v4, flags))
        {
            if (!(flags & ImGuiColorEditFlags_NoPicker))
            {
                // Store current color and open a picker
                g.ColorPickerRef = col_v4;
                OpenPopup(xorstr_("picker"));
                SetNextWindowPos(window->DC.LastItemRect.GetBL() + ImVec2(-1, style.ItemSpacing.y));
            }
        }
        if (!(flags & ImGuiColorEditFlags_NoOptions))
            OpenPopupOnItemClick(xorstr_("context"));

        if (BeginPopup(xorstr_("picker")))
        {
            picker_active_window = g.CurrentWindow;
            if (label != label_display_end)
            {
                TextEx(label, label_display_end);
                Spacing();
            }
            //ImGuiColorEditFlags picker_flags_to_forward = ImGuiColorEditFlags__DataTypeMask | ImGuiColorEditFlags_PickerHueBar | ImGuiColorEditFlags_HDR | ImGuiColorEditFlags_NoAlpha | ImGuiColorEditFlags_AlphaBar;
            //ImGuiColorEditFlags picker_flags = (flags_untouched & picker_flags_to_forward) | ImGuiColorEditFlags_NoLabel | ImGuiColorEditFlags_AlphaPreviewHalf;
            ImGuiColorEditFlags picker_flags = ImGuiColorEditFlags_NoLabel | ImGuiColorEditFlags_PickerHueBar | ImGuiColorEditFlags_NoAlpha | ImGuiColorEditFlags_AlphaBar | ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview;
        	SetNextItemWidth(square_sz * 12.0f); // Use 256 + bar sizes?
            value_changed |= QColorPicker4(xorstr_("##picker"), col, picker_flags, &g.ColorPickerRef.x);
            EndPopup();
        }
    }

    if (label != label_display_end && !(flags & ImGuiColorEditFlags_NoLabel))
    {
        const float text_offset_x = (flags & ImGuiColorEditFlags_NoInputs) ? w_button : w_full + style.ItemInnerSpacing.x;
        window->DC.CursorPos = ImVec2(pos.x + text_offset_x, pos.y + style.FramePadding.y);
        TextEx(label, label_display_end);
    }

    // Convert back
    if (value_changed && picker_active_window == NULL)
    {
        if (!value_changed_as_float)
            for (int n = 0; n < 4; n++)
                f[n] = i[n] / 255.0f;
        if ((flags & ImGuiColorEditFlags_DisplayHSV) && (flags & ImGuiColorEditFlags_InputRGB))
        {
            g.ColorEditLastHue = f[0];
            g.ColorEditLastSat = f[1];
            ColorConvertHSVtoRGB(f[0], f[1], f[2], f[0], f[1], f[2]);
            memcpy(g.ColorEditLastColor, f, sizeof(float) * 3);
        }
        if ((flags & ImGuiColorEditFlags_DisplayRGB) && (flags & ImGuiColorEditFlags_InputHSV))
            ColorConvertRGBtoHSV(f[0], f[1], f[2], f[0], f[1], f[2]);

        col[0] = f[0];
        col[1] = f[1];
        col[2] = f[2];
        if (alpha)
            col[3] = f[3];
    }

    PopID();
    EndGroup();

    // Drag and Drop Target
    // NB: The flag test is merely an optional micro-optimization, BeginDragDropTarget() does the same test.
    if ((window->DC.LastItemStatusFlags & ImGuiItemStatusFlags_HoveredRect) && !(flags & ImGuiColorEditFlags_NoDragDrop) && BeginDragDropTarget())
    {
        bool accepted_drag_drop = false;
        if (const ImGuiPayload* payload = AcceptDragDropPayload(IMGUI_PAYLOAD_TYPE_COLOR_3F))
        {
            memcpy((float*)col, payload->Data, sizeof(float) * 3); // Preserve alpha if any //-V512
            value_changed = accepted_drag_drop = true;
        }
        if (const ImGuiPayload* payload = AcceptDragDropPayload(IMGUI_PAYLOAD_TYPE_COLOR_4F))
        {
            memcpy((float*)col, payload->Data, sizeof(float) * components);
            value_changed = accepted_drag_drop = true;
        }

        // Drag-drop payloads are always RGB
        if (accepted_drag_drop && (flags & ImGuiColorEditFlags_InputHSV))
            ColorConvertRGBtoHSV(col[0], col[1], col[2], col[0], col[1], col[2]);
        EndDragDropTarget();
    }

    // When picker is being actively used, use its active id so IsItemActive() will function on ColorEdit4().
    if (picker_active_window && g.ActiveId != 0 && g.ActiveIdWindow == picker_active_window)
        window->DC.LastItemId = g.ActiveId;

    if (value_changed)
        MarkItemEdited(window->DC.LastItemId);

    return value_changed;
}

bool ImGui::QColorPicker4(const char* label, float col[4], ImGuiColorEditFlags flags, const float* ref_col)
{
    ImGuiContext& g = *GImGui;
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    ImDrawList* draw_list = window->DrawList;
    ImGuiStyle& style = g.Style;
    ImGuiIO& io = g.IO;

    const float width = CalcItemWidth();
    g.NextItemData.ClearFlags();

    PushID(label);
    BeginGroup();

    // Read stored options
    if (!(flags & ImGuiColorEditFlags__PickerMask))
        flags |= ((g.ColorEditOptions & ImGuiColorEditFlags__PickerMask) ? g.ColorEditOptions : ImGuiColorEditFlags__OptionsDefault) & ImGuiColorEditFlags__PickerMask;
    if (!(flags & ImGuiColorEditFlags__InputMask))
        flags |= ((g.ColorEditOptions & ImGuiColorEditFlags__InputMask) ? g.ColorEditOptions : ImGuiColorEditFlags__OptionsDefault) & ImGuiColorEditFlags__InputMask;
    IM_ASSERT(ImIsPowerOfTwo(flags & ImGuiColorEditFlags__PickerMask)); // Check that only 1 is selected
    IM_ASSERT(ImIsPowerOfTwo(flags & ImGuiColorEditFlags__InputMask));  // Check that only 1 is selected
    if (!(flags & ImGuiColorEditFlags_NoOptions))
        flags |= (g.ColorEditOptions & ImGuiColorEditFlags_AlphaBar);

    // Setup
    int components = (flags & ImGuiColorEditFlags_NoAlpha) ? 3 : 4;
    bool alpha_bar = (flags & ImGuiColorEditFlags_AlphaBar) && !(flags & ImGuiColorEditFlags_NoAlpha);
    ImVec2 picker_pos = window->DC.CursorPos;
    float square_sz = 10.f;//GetFrameHeight();
    float bars_width = square_sz; // Arbitrary smallish width of Hue/Alpha picking bars
    float sv_picker_size = ImMax(bars_width * 1, width - (alpha_bar ? 2 : 1) * (bars_width + style.ItemInnerSpacing.x)); // Saturation/Value picking box
    float bar0_pos_x = picker_pos.x + sv_picker_size + style.ItemInnerSpacing.x;
    float bar1_pos_x = bar0_pos_x + bars_width + style.ItemInnerSpacing.x;

    float backup_initial_col[4];
    memcpy(backup_initial_col, col, components * sizeof(float));

    float H = col[0], S = col[1], V = col[2];
    float R = col[0], G = col[1], B = col[2];
    //ImGuiColorEditFlags_InputRGB
    {
        // Hue is lost when converting from greyscale rgb (saturation=0). Restore it.
        ColorConvertRGBtoHSV(R, G, B, H, S, V);
        if (memcmp(g.ColorEditLastColor, col, sizeof(float) * 3) == 0)
        {
            if (S == 0)
                H = g.ColorEditLastHue;
            if (V == 0)
                S = g.ColorEditLastSat;
        }
    }

    bool value_changed = false, value_changed_h = false, value_changed_sv = false;

    //ImGuiColorEditFlags_PickerHueBar
    {
        // SV rectangle logic
        InvisibleButton("sv", ImVec2(sv_picker_size, sv_picker_size));
        if (IsItemActive())
        {
            S = ImSaturate((io.MousePos.x - picker_pos.x) / (sv_picker_size - 1));
            V = 1.0f - ImSaturate((io.MousePos.y - picker_pos.y) / (sv_picker_size - 1));
            value_changed = value_changed_sv = true;
        }
        if (!(flags & ImGuiColorEditFlags_NoOptions))
            OpenPopupOnItemClick("context");

        // Hue bar logic
        SetCursorScreenPos(ImVec2(bar0_pos_x, picker_pos.y));
        InvisibleButton("hue", ImVec2(bars_width, sv_picker_size));
        if (IsItemActive())
        {
            H = ImSaturate((io.MousePos.y - picker_pos.y) / (sv_picker_size - 1));
            value_changed = value_changed_h = true;
        }
    }

    // Alpha bar logic
    if (alpha_bar)
    {
        SetCursorScreenPos(ImVec2(bar1_pos_x, picker_pos.y));
        InvisibleButton("alpha", ImVec2(bars_width, sv_picker_size));
        if (IsItemActive())
        {
            col[3] = 1.0f - ImSaturate((io.MousePos.y - picker_pos.y) / (sv_picker_size - 1));
            value_changed = true;
        }
    }

    // Convert back color to RGB
    if (value_changed_h || value_changed_sv)
    {
        //ImGuiColorEditFlags_InputRGB
        {
            ColorConvertHSVtoRGB(H >= 1.0f ? H - 10 * 1e-6f : H, S > 0.0f ? S : 10 * 1e-6f, V > 0.0f ? V : 1e-6f, col[0], col[1], col[2]);
            g.ColorEditLastHue = H;
            g.ColorEditLastSat = S;
            memcpy(g.ColorEditLastColor, col, sizeof(float) * 3);
        }
    }

    const int style_alpha8 = IM_F32_TO_INT8_SAT(style.Alpha);
    const ImU32 col_black = IM_COL32(0, 0, 0, style_alpha8);
    const ImU32 col_white = IM_COL32(255, 255, 255, style_alpha8);
    const ImU32 col_midgrey = IM_COL32(128, 128, 128, style_alpha8);
    const ImU32 col_hues[6 + 1] = { IM_COL32(255,0,0,style_alpha8), IM_COL32(255,255,0,style_alpha8), IM_COL32(0,255,0,style_alpha8), IM_COL32(0,255,255,style_alpha8), IM_COL32(0,0,255,style_alpha8), IM_COL32(255,0,255,style_alpha8), IM_COL32(255,0,0,style_alpha8) };

    ImVec4 hue_color_f(1, 1, 1, style.Alpha); ColorConvertHSVtoRGB(H, 1, 1, hue_color_f.x, hue_color_f.y, hue_color_f.z);
    ImU32 hue_color32 = ColorConvertFloat4ToU32(hue_color_f);
    ImU32 user_col32_striped_of_alpha = ColorConvertFloat4ToU32(ImVec4(R, G, B, style.Alpha)); // Important: this is still including the main rendering/style alpha!!

    ImVec2 sv_cursor_pos;

    // ImGuiColorEditFlags_PickerHueBar
    {
        // Render SV Square
        draw_list->AddRectFilledMultiColor(picker_pos, picker_pos + ImVec2(sv_picker_size, sv_picker_size), col_white, hue_color32, hue_color32, col_white);
        draw_list->AddRectFilledMultiColor(picker_pos, picker_pos + ImVec2(sv_picker_size, sv_picker_size), 0, 0, col_black, col_black);
        RenderFrameBorder(picker_pos, picker_pos + ImVec2(sv_picker_size, sv_picker_size), 0.0f);
        sv_cursor_pos.x = ImClamp(IM_ROUND(picker_pos.x + ImSaturate(S) * sv_picker_size), picker_pos.x + 2, picker_pos.x + sv_picker_size - 2); // Sneakily prevent the circle to stick out too much
        sv_cursor_pos.y = ImClamp(IM_ROUND(picker_pos.y + ImSaturate(1 - V) * sv_picker_size), picker_pos.y + 2, picker_pos.y + sv_picker_size - 2);

        // Render Hue Bar
        for (int i = 0; i < 6; ++i)
            draw_list->AddRectFilledMultiColor(ImVec2(bar0_pos_x, picker_pos.y + i * (sv_picker_size / 6)), ImVec2(bar0_pos_x + bars_width, picker_pos.y + (i + 1) * (sv_picker_size / 6)), col_hues[i], col_hues[i], col_hues[i + 1], col_hues[i + 1]);
        float bar0_line_y = IM_ROUND(picker_pos.y + H * sv_picker_size);
        RenderFrameBorder(ImVec2(bar0_pos_x, picker_pos.y), ImVec2(bar0_pos_x + bars_width, picker_pos.y + sv_picker_size), 0.0f);
        draw_list->AddLine(ImVec2(bar0_pos_x - 1, bar0_line_y), ImVec2(bar1_pos_x - 3, bar0_line_y), IM_COL32_WHITE, 1);
    }

    // Render cursor/preview circle (clamp S/V within 0..1 range because floating points colors may lead HSV values to be out of range)
    float sv_cursor_rad = 5.0f;
    draw_list->AddCircleFilled(sv_cursor_pos, sv_cursor_rad, user_col32_striped_of_alpha, 12);
    draw_list->AddCircle(sv_cursor_pos, sv_cursor_rad + 1, col_midgrey, 12);
    draw_list->AddCircle(sv_cursor_pos, sv_cursor_rad, col_white, 12);

    // Render alpha bar
    if (alpha_bar)
    {
        float alpha = ImSaturate(col[3]);
        ImRect bar1_bb(bar1_pos_x, picker_pos.y, bar1_pos_x + bars_width, picker_pos.y + sv_picker_size);
        RenderColorRectWithAlphaCheckerboard(draw_list, bar1_bb.Min, bar1_bb.Max, 0, bar1_bb.GetWidth() / 2.0f, ImVec2(0.0f, 0.0f));
        draw_list->AddRectFilledMultiColor(bar1_bb.Min, bar1_bb.Max, user_col32_striped_of_alpha, user_col32_striped_of_alpha, user_col32_striped_of_alpha & ~IM_COL32_A_MASK, user_col32_striped_of_alpha & ~IM_COL32_A_MASK);
        float bar1_line_y = IM_ROUND(picker_pos.y + (1.0f - alpha) * sv_picker_size);
        RenderFrameBorder(bar1_bb.Min, bar1_bb.Max, 0.0f);
        //RenderArrowsForVerticalBar(draw_list, ImVec2(bar1_pos_x - 1, bar1_line_y), ImVec2(bars_triangles_half_sz + 1, bars_triangles_half_sz), bars_width + 2.0f, style.Alpha);
        draw_list->AddLine(ImVec2(bar0_pos_x - 1, bar1_line_y), ImVec2(bar1_pos_x - 3, bar1_line_y), IM_COL32_WHITE, 1);
    }

    EndGroup();

    if (value_changed && memcmp(backup_initial_col, col, components * sizeof(float)) == 0)
        value_changed = false;
    if (value_changed)
        MarkItemEdited(window->DC.LastItemId);

    PopID();

    return value_changed;
}

void ImGui::QHotkey(const char* label, int* key, const ImVec2& size_arg)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return;

    ImGuiContext& g = *GImGui;
    ImGuiIO& io = g.IO;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
	
    const std::string text = g.ActiveId == id ? xorstr_("Press a new key binding...") : label + std::string(": ") + keytranslation->ButtonCode_ButtonCodeToString((ButtonCode_t)*key);
	const ImVec2 label_size = CalcTextSize(text.c_str(), NULL, true);

    ImVec2 pos = window->DC.CursorPos;
    ImVec2 size = CalcItemSize(size_arg, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

    const ImRect bb(pos, pos + size);
    ItemSize(size, style.FramePadding.y);
    if (!ItemAdd(bb, id))
        return;

    const bool hovered = ItemHoverable(bb, id);
    const bool clicked = io.MouseClicked[0];
    
    if (hovered && clicked)
    {
        if (g.ActiveId != id)
        {
            memset(globals::vars::key_pressed, 0, sizeof globals::vars::key_pressed);
            *key = -1;
        }
        SetActiveID(id, window);
        FocusWindow(window);
    }
	
    if (g.ActiveId == id)
    {
    	for(auto index = 0; index < BUTTON_CODE_LAST; ++index)
    	{
    		if (globals::vars::key_pressed[index])
    		{
                *key = index != KEY_ESCAPE ? index : 0;
                ClearActiveID();
    		}
    	}
    }
	
	//if (hovered)
    //    SetTooltip("Press any key to change keybind");

    // Render
    const ImU32 col = GetColorU32(g_pImRender->get_color(g.ActiveId == id ? ImCustomColor_Hotkey_Active : ImCustomColor_Hotkey));
    const ImU32 col_border = GetColorU32(g_pImRender->get_color(g.ActiveId == id ? ImCustomColor_Line_Light : ImCustomColor_Line));
    RenderNavHighlight(bb, id);
    window->DrawList->AddRectFilled(bb.Min, bb.Max, col, style.FrameRounding);
    window->DrawList->AddRect(bb.Min, bb.Max, col_border, style.FrameRounding);
    RenderTextClipped(bb.Min + style.FramePadding, bb.Max - style.FramePadding, text.c_str(), NULL, &label_size, style.ButtonTextAlign, &bb);
}

void ImGui::QHotkey(const char* label, int* key, int* mode, const ImVec2& size_arg)
{
    ImGuiContext& g = *GImGui;
    const std::string name1 = std::string("Hold##") + std::string(label);
    const std::string name2 = std::string("Toggle##") + std::string(label);
    const ImVec2 pos = GetCursorPos();
    const float height = CalcTextSize(label, NULL, true).y;
	
    QHotkey(label, key, size_arg);
    SameLine();
    SetCursorPos(ImVec2(pos.x, pos.y + height + g.Style.ItemInnerSpacing.y * 2.5f));
    PushFont(g_pImRender->get_font(ImCustomFont_Hotkey));
    if (QRadioButton(name1.c_str(), *mode == 0))
        *mode = 0;
    SetCursorPos(ImVec2(pos.x + size_arg.x / 2, pos.y + height + g.Style.ItemInnerSpacing.y * 2.5f));
    if (QRadioButton(name2.c_str(), *mode == 1))
        *mode = 1;
    PopFont();
    Spacing();
}

bool ImGui::QRadioButton(const char* label, bool active)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    const ImVec2 label_size = CalcTextSize(label, NULL, true);

    const float square_sz = GetFrameHeight();
    const ImVec2 pos = window->DC.CursorPos;
    const ImRect check_bb(pos, pos + ImVec2(square_sz, square_sz));
    const ImRect total_bb(pos, pos + ImVec2(square_sz + (label_size.x > 0.0f ? style.ItemInnerSpacing.x + label_size.x : 0.0f), label_size.y + style.FramePadding.y * 2.0f));
    ItemSize(total_bb, style.FramePadding.y);
    if (!ItemAdd(total_bb, id))
        return false;

    bool hovered, held;
    bool pressed = ButtonBehavior(total_bb, id, &hovered, &held);
    if (pressed)
        MarkItemEdited(id);

    RenderNavHighlight(total_bb, id);
    RenderFrame(check_bb.Min, check_bb.Max, GetColorU32(ImGuiCol_PlotLines), true, style.FrameRounding);
    if (!held && !hovered)
        window->DrawList->AddRectFilled(ImVec2(check_bb.Min.x + 1, check_bb.Max.y - 2), ImVec2(check_bb.Max.x - 1, check_bb.Max.y - 1), GetColorU32(ImGuiCol_Button));
    if (active)
        window->DrawList->AddRectFilled(check_bb.Min + ImVec2(1, 1), check_bb.Max - ImVec2(1, 1), GetColorU32(ImGuiCol_CheckMark));
    else if (held || hovered)
        window->DrawList->AddRect(check_bb.Min, check_bb.Max, GetColorU32(ImGuiCol_PlotLinesHovered));

    if (label_size.x > 0.0f)
        RenderText(ImVec2(check_bb.Max.x + style.ItemInnerSpacing.x, check_bb.Min.y + style.FramePadding.y), label);

    return pressed;
}

bool ImGui::QRadioButton(const char* label, int* v, int v_button)
{
    const bool pressed = QRadioButton(label, *v == v_button);
    if (pressed)
        *v = v_button;
    return pressed;
}

bool ImGui::QButton(const char* label, const ImVec2& size_arg)
{
    ImGuiWindow* window = GetCurrentWindow();
    if (window->SkipItems)
        return false;

    ImGuiContext& g = *GImGui;
    const ImGuiStyle& style = g.Style;
    const ImGuiID id = window->GetID(label);
    const ImVec2 label_size = CalcTextSize(label, NULL, true);

    ImVec2 pos = window->DC.CursorPos;
    ImVec2 size = CalcItemSize(size_arg, label_size.x + style.FramePadding.x * 2.0f, label_size.y + style.FramePadding.y * 2.0f);

    const ImRect bb(pos, pos + size);
    ItemSize(size, style.FramePadding.y);
    if (!ItemAdd(bb, id))
        return false;

    bool hovered, held;
    bool pressed = ButtonBehavior(bb, id, &hovered, &held, 0);

    // Render
    const ImVec2 text_pos = bb.GetCenter() - label_size / 2 + (held ? ImVec2(1, 1) : ImVec2(0,0));
    const ImU32 col = GetColorU32(held ? ImGuiCol_ButtonActive : hovered ? ImGuiCol_ButtonHovered : ImGuiCol_Button);
	
    RenderNavHighlight(bb, id);
    window->DrawList->AddRectFilled(bb.Min, bb.Max, col, style.FrameRounding);
    window->DrawList->AddText(text_pos, GetColorU32(ImGuiCol_Text), label);

    return pressed;
}