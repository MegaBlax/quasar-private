#pragma once
#include "../vector.h"
#include "icliententity.h"

class CMoveData
{
public:
    bool    m_bFirstRunOfFunctions : 1;
    bool    m_bGameCodeMovedPlayer : 1;
    int     m_nPlayerHandle;        // edict index on server, client entity handle on client=
    int     m_nImpulseCommand;      // Impulse command issued.
    vector  m_vecViewAngles;        // Command view angles (local space)
    vector  m_vecAbsViewAngles;     // Command view angles (world space)
    int     m_nButtons;             // Attack buttons.
    int     m_nOldButtons;          // From host_client->oldbuttons;
    float   m_flForwardMove;
    float   m_flSideMove;
    float   m_flUpMove;
    float   m_flMaxSpeed;
    float   m_flClientMaxSpeed;
    vector  m_vecVelocity;          // edict::velocity        // Current movement direction.
    vector  m_vecAngles;            // edict::angles
    vector  m_vecOldAngles;
    float   m_outStepHeight;        // how much you climbed this move
    vector  m_outWishVel;           // This is where you tried 
    vector  m_outJumpVel;           // This is your jump velocity
    vector  m_vecConstraintCenter;
    float   m_flConstraintRadius;
    float   m_flConstraintWidth;
    float   m_flConstraintSpeedFactor;
    float   m_flUnknown[5];
    vector  m_vecAbsOrigin;
};

class IGameMovement
{
public:
    virtual			            ~IGameMovement(void) {}

    virtual void	            ProcessMovement(IClientEntity* pPlayer, CMoveData* pMove) = 0;
    virtual void	            Reset(void) = 0;
    virtual void	            StartTrackPredictionErrors(IClientEntity* pPlayer) = 0;
    virtual void	            FinishTrackPredictionErrors(IClientEntity* pPlayer) = 0;
    virtual void	            DiffPrint(char const* fmt, ...) = 0;
    virtual vector const&       GetPlayerMins(bool ducked) const = 0;
    virtual vector const&       GetPlayerMaxs(bool ducked) const = 0;
    virtual vector const&       GetPlayerViewOffset(bool ducked) const = 0;
    virtual bool		        IsMovingPlayerStuck(void) const = 0;
    virtual IClientEntity*      GetMovingPlayer(void) const = 0;
    virtual void		        UnblockPusher(IClientEntity* pPlayer, IClientEntity* pPusher) = 0;
    virtual void                SetupMovementBounds(CMoveData* pMove) = 0;
};

class CGameMovement : public IGameMovement
{
public:
	virtual ~CGameMovement(void) {}
};