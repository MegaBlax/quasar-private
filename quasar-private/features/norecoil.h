#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"

class norecoil
{
public:
	void run(CUserCmd* cmd);
	void runcommand(IClientEntity* player);
	void fsn();

private:
	qangle _old_punch;
	qangle m_aimPunchAngle[128];
};

extern std::unique_ptr<norecoil> g_pNorecoil;