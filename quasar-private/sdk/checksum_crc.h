#pragma once

typedef unsigned long CRC32_t;

class CRC32
{
public:
	static void Init(CRC32_t* pulCRC);
	static void ProcessBuffer(CRC32_t* pulCRC, const void* p, int nBuffer);
	static void Final(CRC32_t* pulCRC);
	static CRC32_t GetTableEntry(unsigned int slot);
	static CRC32_t ProcessSingleBuffer(const void* p, int len);
};


