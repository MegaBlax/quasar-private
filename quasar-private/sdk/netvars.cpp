#include "netvars.h"
#include "netvarmanager.h"

namespace netvars
{
	namespace cs_player
	{
		int m_iHealth;
		int m_lifeState;
		int m_iTeamNum;
		int m_bIsPlayerGhost;
		int m_vecOrigin;
		int m_vecViewOffset;
		int m_aimPunchAngle;
		int m_nTickBase;
		int m_iShotsFired;
		int m_flFlashDuration;
		int m_flFlashMaxAlpha;
		int m_fFlags;
		int m_vecVelocity;
		int m_bSpotted;
		int m_flSimulationTime;
		int m_hObserverTarget;
		int m_iObserverMode;
		int m_bGunGameImmunity;
		int m_bIsScoped;
		int m_nSurvivalTeam;
		int m_angEyeAngles;

		bool init()
		{
			m_iHealth = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_iHealth"));
			m_lifeState = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_lifeState"));
			m_vecViewOffset = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_vecViewOffset[0]"));
			m_aimPunchAngle = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_aimPunchAngle"));
			m_nTickBase = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_nTickBase"));
			m_fFlags = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_fFlags"));
			m_vecVelocity = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_vecVelocity[0]"));
			m_hObserverTarget = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_hObserverTarget"));
			m_iObserverMode = g_pNetvarManager->get_offset(FNV("DT_BasePlayer->m_iObserverMode"));
			m_iTeamNum = g_pNetvarManager->get_offset(FNV("DT_BaseEntity->m_iTeamNum"));
			m_bSpotted = g_pNetvarManager->get_offset(FNV("DT_BaseEntity->m_bSpotted"));
			m_flSimulationTime = g_pNetvarManager->get_offset(FNV("DT_BaseEntity->m_flSimulationTime"));
			m_vecOrigin = g_pNetvarManager->get_offset(FNV("DT_BaseEntity->m_vecOrigin"));
			m_bIsPlayerGhost = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_bIsPlayerGhost"));
			m_iShotsFired = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_iShotsFired"));
			m_flFlashDuration = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_flFlashDuration"));
			m_flFlashMaxAlpha = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_flFlashMaxAlpha"));
			m_bGunGameImmunity = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_bGunGameImmunity"));
			m_bIsScoped = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_bIsScoped"));
			m_nSurvivalTeam = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_nSurvivalTeam"));
			m_angEyeAngles = g_pNetvarManager->get_offset(FNV("DT_CSPlayer->m_angEyeAngles[0]"));
			
			return m_iHealth && m_lifeState && m_vecViewOffset && m_aimPunchAngle && m_nTickBase && m_fFlags &&
				m_vecVelocity && m_hObserverTarget && m_iObserverMode && m_iTeamNum && m_bSpotted && m_flSimulationTime && 
				m_vecOrigin && m_bIsPlayerGhost && m_iShotsFired && m_flFlashDuration && m_flFlashMaxAlpha && m_bGunGameImmunity
				&& m_bIsScoped && m_nSurvivalTeam && m_angEyeAngles;
		}
	}

	namespace cs_weapon
	{
		int m_hActiveWeapon;
		int m_iItemDefinitionIndex;
		int m_iClip1;
		int m_flNextPrimaryAttack;
		int m_flNextAttack;
		int m_fLastShotTime;

		bool init()
		{
			m_hActiveWeapon = g_pNetvarManager->get_offset(FNV("DT_BaseCombatCharacter->m_hActiveWeapon"));
			m_flNextAttack = g_pNetvarManager->get_offset(FNV("DT_BaseCombatCharacter->m_flNextAttack"));
			m_iClip1 = g_pNetvarManager->get_offset(FNV("DT_BaseCombatWeapon->m_iClip1"));
			m_flNextPrimaryAttack = g_pNetvarManager->get_offset(FNV("DT_BaseCombatWeapon->m_flNextPrimaryAttack"));
			m_iItemDefinitionIndex = g_pNetvarManager->get_offset(FNV("DT_BaseAttributableItem->m_iItemDefinitionIndex"));
			m_fLastShotTime = g_pNetvarManager->get_offset(FNV("DT_WeaponCSBase->m_fLastShotTime"));

			return m_hActiveWeapon && m_iItemDefinitionIndex && m_iClip1 && m_flNextPrimaryAttack && m_flNextAttack && m_fLastShotTime;
		}
	}

	namespace cs_planted_c4
	{
		//int m_flTimerLength;
		int m_bBombTicking;
		//int m_nBombSite;
		//int m_flDefuseCountDown;
		int m_flC4Blow;
		int m_hBombDefuser;
		int m_flDefuseLength;
		int m_bBombDefused;

		bool init()
		{
			//m_flTimerLength = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_flTimerLength"));
			m_bBombTicking = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_bBombTicking"));
			//m_nBombSite = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_nBombSite"));
			//m_flDefuseCountDown = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_flDefuseCountDown"));
			m_flC4Blow = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_flC4Blow"));
			m_hBombDefuser = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_hBombDefuser"));
			m_flDefuseLength = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_flDefuseLength"));
			m_bBombDefused = g_pNetvarManager->get_offset(FNV("DT_PlantedC4->m_bBombDefused"));
			return m_bBombTicking && m_flC4Blow && m_hBombDefuser && m_flDefuseLength && m_bBombDefused;
		}
	}

	namespace dt_base_animating
	{
		int m_flPoseParameter;

		bool init()
		{
			m_flPoseParameter = g_pNetvarManager->get_offset(FNV("DT_BaseAnimating->m_flPoseParameter"));
			return m_flPoseParameter;
		}
	}

	namespace cs_player_res
	{
		int m_iCompTeammateColor;
		int m_iTotalCashSpent;
		int m_iCashSpentThisRound;

		bool init()
		{
			m_iCompTeammateColor = g_pNetvarManager->get_offset(FNV("DT_CSPlayerResource->m_iCompTeammateColor"));
			//m_iTotalCashSpent = g_pNetvarManager->get_offset(FNV("DT_CSPlayerResource->m_iTotalCashSpent"));
			//m_iCashSpentThisRound = g_pNetvarManager->get_offset(FNV("DT_CSPlayerResource->m_iCashSpentThisRound"));
			return m_iCompTeammateColor;
		}
		
	}

	namespace DT_CSGameRulesProxy
	{
		int m_bIsValveDS;

		bool init()
		{
			m_bIsValveDS = g_pNetvarManager->get_offset(FNV("DT_CSGameRulesProxy->m_bIsValveDS"));
			return m_bIsValveDS;
		}
	}

	bool init()
	{
		g_pNetvarManager = std::make_unique<netvarmanager>();
		if (!g_pNetvarManager->init())
		{
			log_error(xorstr_(L"Failed to initialize netvarmanager"));
			return false;
		}
		if (!cs_player::init())
		{
			log_error(xorstr_(L"Failed to initialize player natvars"));
			return false;
		}
		if (!cs_weapon::init())
		{
			log_error(xorstr_(L"Failed to initialize weapon natvars"));
			return false;
		}
		if (!cs_planted_c4::init())
		{
			log_error(xorstr_(L"Failed to initialize planted_c4 natvars"));
			return false;
		}
		if (!dt_base_animating::init())
		{
			log_error(xorstr_(L"Failed to initialize animating natvars"));
			return false;
		}
		if (!cs_player_res::init())
		{
			log_error(xorstr_(L"Failed to initialize player resource natvars"));
			return false;
		}
		/*if (!DT_CSGameRulesProxy::init())
		{
			log_error(xorstr_(L"Failed to initialize DT_CSGameRulesProxy natvars"));
			return false;
		}*/
		return true;
	}
}