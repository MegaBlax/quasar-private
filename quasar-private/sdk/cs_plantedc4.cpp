#include "cs_plantedc4.h"
#include "netvars.h"

cs_planted_c4* cs_planted_c4::get_entity(cs_player* entity)
{
	return reinterpret_cast<cs_planted_c4*>(entity);
}

float cs_planted_c4::defuselength()
{
	return *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_planted_c4::m_flDefuseLength);
}

float cs_planted_c4::explosiontimer()
{
	const float C4Blow = *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_planted_c4::m_flC4Blow);
	const auto bomb_time = C4Blow - g_pInterfaces->globalvarsbase()->curtime;
	return bomb_time > 0.f ? bomb_time : 0.f;
}

float cs_planted_c4::defusetimer()
{
	static float defuse_time = -1.f;
	if (!this->defuser())
	{
		defuse_time = -1.f;
	}
	else if (defuse_time == -1.f)
	{
		defuse_time = g_pInterfaces->globalvarsbase()->curtime + this->defuselength();
	}
	if (defuse_time > -1.f && this->defuser())
		return defuse_time - g_pInterfaces->globalvarsbase()->curtime;

	return 0.f;
}

bool cs_planted_c4::ticking()
{
	return *reinterpret_cast<bool*>(reinterpret_cast<DWORD>(this) + netvars::cs_planted_c4::m_bBombTicking);
}

bool cs_planted_c4::defused()
{
	return *reinterpret_cast<bool*>(reinterpret_cast<DWORD>(this) + netvars::cs_planted_c4::m_bBombDefused);
}

CHandle<cs_player*> cs_planted_c4::defuser()
{
	return *reinterpret_cast<CHandle<cs_player*>*>(reinterpret_cast<DWORD>(this) + netvars::cs_planted_c4::m_hBombDefuser);
}
