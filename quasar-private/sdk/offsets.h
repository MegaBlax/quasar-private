#pragma once
#include "../stdafx.h"
#include <d3d9.h>

class offsets
{
public:
	offsets();
	bool init();
	DWORD* shaderapi() const;
	uintp present();
	uintp reset();
	DWORD matrix() const;
	DWORD reloading() const;
	//DWORD playerres() const;
	DWORD radarbase() const;
	//uintptr_t maptexture() const;
	uint8_t* hudelement() const;
	uint8_t* hud() const;
	uint8_t* smoke() const;
	uint8_t* hasc4() const;
	uint8_t* autoaccept() const;
	DWORD keyvaluessystem() const;
	DWORD loadfrombuffer() const;
	DWORD getstring() const;
	DWORD savetofile() const;
	uint8_t* setabsorigin() const;
	uint8_t* setabsangles() const;
	DWORD invalidbonecache() const;
	uint8_t* randomseed() const;
	DWORD currentcmd() const;
	uintp listleaves() const;

private:
	DWORD* _shaderapidx9_sig;
	uintp _present_sig;
	uintp _reset_sig;
	DWORD _matrix_sig;
	DWORD _reloading_sig;
	DWORD _maptexture_sig;
	DWORD _playerres_sig;
	DWORD _radarbase_sig;
	uint8_t* _hudelement_sig;
	uint8_t* _hud_sig;
	uint8_t* _smoke_sig;
	uint8_t* _has_c4_sig;
	uint8_t* _autoaccept_sig;
	DWORD _keyvaluessystem_sig;
	DWORD _loadfrombuffer_sig;
	DWORD _getstring_sig;
	DWORD _savetofile_sig;
	uint8_t* _setabsorigin_sig;
	uint8_t* _setabsangle_sig;
	DWORD _invalidbonecache_sig;
	uint8_t* _randomseed_sig;
	DWORD _currentcmd_sig;
	uintp _listleaves_sig;
};

extern std::unique_ptr<offsets> g_pOffsets;