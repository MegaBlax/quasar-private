#pragma once
#include "clientclass.h"
#include "inetchannelinfo.h"
#include "../vector.h"

class INetChannel
{
public:
	char pad_0000[20];           //0x0000
	bool m_bProcessingMessages;  //0x0014
	bool m_bShouldDelete;        //0x0015
	char pad_0016[2];            //0x0016
	int32_t m_nOutSequenceNr;    //0x0018 last send outgoing sequence number
	int32_t m_nInSequenceNr;     //0x001C last received incoming sequnec number
	int32_t m_nOutSequenceNrAck; //0x0020 last received acknowledge outgoing sequnce number
	int32_t m_nOutReliableState; //0x0024 state of outgoing reliable data (0/1) flip flop used for loss detection
	int32_t m_nInReliableState;  //0x0028 state of incoming reliable data
	int32_t m_nChokedPackets;    //0x002C number of choked packets
	char pad_0030[1044];         //0x0030
}; //Size: 0x0444

class CClientState
{
public:

	void ForceFullUpdate()
	{
		m_nDeltaTick2 = -1;
	}

	bool IsInGame() const
	{
		return m_nSignonState == SIGNONSTATE_FULL;
	}

	char pad_0000[156]; //0x0000
	INetChannel* m_NetChannel; //0x009C
	uint32_t m_nChallengeNr; //0x00A0
	char pad_00A0[100]; //0x00A4
	uint32_t m_nSignonState; //0x0108
	char pad_010C[8]; //0x010C
	float m_flNextCmdTime; //0x0114
	uint32_t m_nServerCount; //0x0118
	uint32_t m_nCurrentSequence; //0x011C
	char pad_0120[76]; //0x0120
	uint32_t m_nDeltaTick0; //0x016C
	uint32_t m_nDeltaTick1; //0x0170
	uint32_t m_nDeltaTick2; //0x0174
	bool m_bPaused; //0x0178
	char pad_0179[15]; //0x0179
	char m_szLevelName[260]; //0x0188
	char m_szLevelNameShort[40]; //0x028C
	char pad_02B4[212]; //0x02B4
	uint32_t m_nMaxClients; //0x0388
	char pad_038C[18844]; //0x038C
	uint32_t chokedcommands; //0x4D28
	uint32_t last_command_ack; //0x4D2C
	uint32_t command_ack; //0x4D30
	uint32_t m_nSoundSequence; //0x4D34
	char pad_4D38[80]; //0x4D38
	qangle viewangles; //0x4D88
	char pad_4D94[3760]; //0x4D94
}; //Size: 0x5C44