#pragma once
#include "../stdafx.h"
#include "../sdk/cs_player.h"
#include "../utils/utils.h"

class aimbot
{
public:
	void run(CUserCmd* cmd, byte*& packet);
	void clear();
	float get_fov() const;
	cs_player* get_target();
	void player_death(int index);
private:
	void count_shotsfired();
	void change_fov();
	void drop_target();
	bool find_target(cs_player*& data);
	bool find_hitbox(vector& vec);
	void change_angles(byte*& packet);
	void delay_before_first_shot();
private:
	CUserCmd* _cmd = nullptr;
	cs_player* _local_player = nullptr;
	cs_player* _entity_player = nullptr;
	cs_weapon* _local_weapon = nullptr;
	qangle _viewangles = qangle(0, 0, 0);
	vector _hitbox = vector(0, 0, 0);
	int _last_hitbox_index = -1;
	int _shotsfired = -1;
	int _last_itemindex = 0;
	float _time_after_kill = 0.f;
	float _time_before_first_shot = 0.f;
	float _last_shot_time = 0.f;
	float _current_fov = 0.f;
	bool _auto_delay_run = false;
	bool _in_crosshair = false;

	bool _active = false;
	bool _multi_points = false;
	bool _smart_hitbox = false;
	bool _auto_delay = false;
	bool _first_bullet_silent = false;
	int _hitbox_index = 0;
	int _silent_chance = 0;
	float _smooth = 0;
	float _fov = 0;
	float _kill_delay = 0;
	float _first_shot_delay = 0.f;
};

extern std::unique_ptr<aimbot> g_pAimbot;