#include "hooks.h"
#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx9.h"
#include "imgui_menu.h"
#include "imgui_render.h"
#include "imgui_freetype.h"

#include "../features/radar.h"

struct FreeType
{
    int	BuildMode;
    bool WantRebuild;
    float FontsMultiply;
    int	FontsPadding;
    unsigned int FontsFlags;

	FreeType()
    {
        BuildMode = 0;
        WantRebuild = true;
        FontsMultiply = 1.5f;
        FontsPadding = 1;
		FontsFlags = 0;
    }

    bool Rebuild()
    {
        if (!WantRebuild)
            return false;
        ImGuiIO& io = ImGui::GetIO();
        io.Fonts->TexGlyphPadding = FontsPadding;
        for (int n = 0; n < io.Fonts->ConfigData.Size; n++)
        {
            ImFontConfig* font_config = static_cast<ImFontConfig*>(&io.Fonts->ConfigData[n]);
            font_config->RasterizerMultiply = FontsMultiply;
            font_config->RasterizerFlags = FontsFlags;
        }
		ImGuiFreeType::BuildFontAtlas(io.Fonts, FontsFlags);
        WantRebuild = false;
        return true;
    }
};

FreeType freetype;

IDirect3DVertexDeclaration9* vert_dec; IDirect3DVertexShader9* vert_shader;
DWORD dwOld_D3DRS_COLORWRITEENABLE;
auto save(IDirect3DDevice9* device)
{
	device->GetRenderState(D3DRS_COLORWRITEENABLE, &dwOld_D3DRS_COLORWRITEENABLE);
	device->GetVertexDeclaration(&vert_dec);
	device->GetVertexShader(&vert_shader);
	device->SetRenderState(D3DRS_COLORWRITEENABLE, 0xffffffff);
	device->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
	device->SetSamplerState(NULL, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	device->SetSamplerState(NULL, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	device->SetSamplerState(NULL, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);
	device->SetSamplerState(NULL, D3DSAMP_SRGBTEXTURE, NULL);
}

auto restore(IDirect3DDevice9* device)
{
	device->SetRenderState(D3DRS_COLORWRITEENABLE, dwOld_D3DRS_COLORWRITEENABLE);
	device->SetRenderState(D3DRS_SRGBWRITEENABLE, 1);
	device->SetVertexDeclaration(vert_dec);
	device->SetVertexShader(vert_shader);
}

HRESULT hooks::present_h(IDirect3DDevice9* device, const RECT* pSourceRect, const RECT* pDestRect,
	HWND hDestWindowOverride, const RGNDATA* pDirtyRegion)
{
	if (!g_pInterfaces->engineclient()->IsActiveApp())
		return _o_present(device, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);

	save(device);

	ImGui::GetIO().MouseDrawCursor = globals::menu::active;
	ImGui_ImplDX9_NewFrame();
	ImGui_ImplWin32_NewFrame();
	if (freetype.Rebuild())
	{
		ImGui_ImplDX9_InvalidateDeviceObjects();
		ImGui_ImplDX9_CreateDeviceObjects();
	}
	ImGui::NewFrame();
	
	g_pMenu->toggle();
	g_pMenu->run();
	g_pRadar->find_maptexture();
	g_pRadar->run();
	
	ImDrawList* drawlist = g_pImRender->get_drawlist();
	
	ImGui::Render(drawlist);
	
	if (device->BeginScene() == D3D_OK) 
	{
		ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
		device->EndScene();
	}

	restore(device);
	return _o_present(device, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
}

HRESULT hooks::reset_h(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* pPresentationParameters)
{
	ImGui_ImplDX9_InvalidateDeviceObjects();
	const auto hr = _o_reset(device, pPresentationParameters);
	if (SUCCEEDED(hr))
	{
		ImGui_ImplDX9_CreateDeviceObjects();
	}

	return hr;
}