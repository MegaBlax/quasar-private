
#pragma once
#include "../stdafx.h"

namespace factory
{
	struct interface_info
	{
		DWORD(*m_CreateFn)();
		const char* m_pName;
	};

	/**
	 * \brief ��������� ���������� �� ����������� ����������, �������������� ����� ������� CreateInterface
	 */
	class cfactory
	{
	public:
		explicit cfactory(const HMODULE& module);
		explicit cfactory(void* module);
		void init(const ptrdiff_t& dwCreateInterface);
		std::vector<interface_info> dump() const;
		DWORD dwGetInterface(const char* pszName, int iVersion = 0) const;
		bool is_valid() const;

	private:
		class interface_reg
		{
		public:
			interface_info interface_info_;
			interface_reg* m_pNext;
		};

		interface_reg* s_pInterfaceRegs{};
	};

	bool init();

	extern cfactory* engine;
	extern cfactory* client;
	extern cfactory* vguimatsurface;
	extern cfactory* vstdlib;
	extern cfactory* materialsystem;
	extern cfactory* vphysics;
	extern cfactory* filesystem;
	extern cfactory* vgui2;
	extern cfactory* inputsystem;
	extern cfactory* matchmaking;
}