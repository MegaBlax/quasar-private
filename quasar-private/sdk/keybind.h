#pragma once
#include "../stdafx.h"
#include "buttoncode.h"

class CKeyBindings
{
public:
	void SetBinding(ButtonCode_t code, const char* pBinding);
	void SetBinding(const char* pButtonName, const char* pBinding);

	void Unbind(ButtonCode_t code);
	void Unbind(const char* pButtonName);
	void UnbindAll();

	int GetBindingCount() const;
	//void WriteBindings(CUtlBuffer& buf);
	const char* ButtonNameForBinding(const char* pBinding);
	const char* GetBindingForButton(ButtonCode_t code);

private:
	std::array<std::string, BUTTON_CODE_LAST> m_KeyInfo = {};
};

extern std::unique_ptr<CKeyBindings> keybind;