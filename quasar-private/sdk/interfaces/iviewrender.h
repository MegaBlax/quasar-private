#pragma once

class IViewRender
{
public:
	// Called to render just a particular setup ( for timerefresh and envmap creation )
	void RenderView(const CViewSetup& view, int nClearFlags, int whatToDraw) // 6
	{
		using fn = void(__thiscall*)(PVOID, const CViewSetup&, int, int);
		call_virtual<fn>(this, 6)(this, view, nClearFlags, whatToDraw);
	}

	// What are we currently rendering? Returns a combination of DF_ flags.
	int GetDrawFlags() // 7
	{
		using fn = int(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 7)(this);
	}

	const CViewSetup* GetPlayerViewSetup(void) // 12
	{
		using fn = const CViewSetup* (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 12)(this);
	}
	const CViewSetup* GetViewSetup(void) // 13
	{
		using fn = const CViewSetup* (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 13)(this);
	}

	// Draws another rendering over the top of the screen
	virtual void		QueueOverlayRenderView(const CViewSetup& view, int nClearFlags, int whatToDraw) = 0; // 26
};