#pragma once
#include "cmodel.h"
#include "trace.h"
#include "icliententity.h"
#include "icliententitylist.h"

class CGameTrace : public CBaseTrace
{
public:
	bool DidHitWorld() const;
	bool DidHitNonWorldEntity() const;
	int GetEntityIndex() const;
	bool DidHit() const;
	bool HitgroupHit() const;

	float				fractionleftsolid;	// time we left a solid, only valid if we started in solid
	csurface_t			surface;			// surface hit (impact surface)
	int					hitgroup;			// 0 == generic, non-zero is specific body part
	short				physicsbone;		// physics bone hit by trace in studio
	unsigned short		worldSurfaceIndex;	// Index of the msurface2_t, if applicable
	IClientEntity*		m_pEnt;
	int					hitbox;				// box hit by trace in studio
	CGameTrace() {}

	CGameTrace(const CGameTrace& other) :
		fractionleftsolid(other.fractionleftsolid),
		surface(other.surface),
		hitgroup(other.hitgroup),
		physicsbone(other.physicsbone),
		worldSurfaceIndex(other.worldSurfaceIndex),
		m_pEnt(other.m_pEnt),
		hitbox(other.hitbox)
	{
		startpos = other.startpos;
		endpos = other.endpos;
		plane = other.plane;
		fraction = other.fraction;
		contents = other.contents;
		dispFlags = other.dispFlags;
		allsolid = other.allsolid;
		startsolid = other.startsolid;
	}

	CGameTrace& operator=(const CGameTrace& other)
	{
		startpos = other.startpos;
		endpos = other.endpos;
		plane = other.plane;
		fraction = other.fraction;
		contents = other.contents;
		dispFlags = other.dispFlags;
		allsolid = other.allsolid;
		startsolid = other.startsolid;
		fractionleftsolid = other.fractionleftsolid;
		surface = other.surface;
		hitgroup = other.hitgroup;
		physicsbone = other.physicsbone;
		worldSurfaceIndex = other.worldSurfaceIndex;
		m_pEnt = other.m_pEnt;
		hitbox = other.hitbox;
		return *this;
	}
};


//-----------------------------------------------------------------------------
// Returns true if there was any kind of impact at all
//-----------------------------------------------------------------------------
inline bool CGameTrace::DidHitWorld() const
{
	return m_pEnt == entitylist->GetClientEntity(0);
}


inline bool CGameTrace::DidHitNonWorldEntity() const
{
	return m_pEnt != NULL && !DidHitWorld();
}


inline int CGameTrace::GetEntityIndex() const
{
	if (m_pEnt)
		return m_pEnt->EntIndex();
	
	return -1;
}
inline bool CGameTrace::DidHit() const
{
	return fraction < 1 || allsolid || startsolid;
}

inline bool CGameTrace::HitgroupHit() const
{
	return hitgroup > HITGROUP_GENERIC && hitgroup < HITGROUP_GEAR;
}

typedef CGameTrace trace_t;