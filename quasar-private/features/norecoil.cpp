#include "norecoil.h"
#include "../utils/utils.h"
#include "../sdk/cs_player.h"

std::unique_ptr<norecoil> g_pNorecoil;

void norecoil::run(CUserCmd* cmd)
{
	if (!globals::rcs::active)
		return;

	cs_player* local_player = cs_player::get_local_player();
	if (!local_player->is_alive())
		return;

	cs_weapon* weapon = local_player->weapon();
	if (!weapon)
		return;
	if (!weapon->is_heavy() && !weapon->is_smg() && !weapon->is_rifle() && !weapon->is_cz75())
		return;

	vector aimpunch = local_player->aimpunchangle();
	float length = aimpunch.NormalizeInPlace();
	length -= (12.f + length * 0.5f) * g_pInterfaces->globalvarsbase()->interval_per_tick;
	if (length > 0.f)
		aimpunch *= length;

	const int shotsfired = local_player->shotsfired();
	if (cmd->buttons & IN_ATTACK && shotsfired > globals::rcs::start && weapon->ammo() > 0)
	{
		vector delta = _old_punch - aimpunch;
		if (!g_pUtils->Clamp(delta))
			return;

		const float pitch = globals::rcs::pitch;
		const float yaw = globals::rcs::yaw;

		const qangle viewangles = g_pUtils->GetViewAngles();
		qangle angles = qangle(viewangles.x + delta.x * yaw, viewangles.y + delta.y * pitch, 0.0f);

		if (!g_pUtils->Clamp(angles))
			return;
		if (viewangles != angles)
		{
			cmd->viewangles = angles;
			g_pInterfaces->engineclient()->SetViewAngles(angles);
		}
	}

	_old_punch = aimpunch;
}

void norecoil::runcommand(IClientEntity* player)
{
	cs_player* local_player = cs_player::get_local_player();
	if (!player)
		return;
	if (player != local_player)
		return;
	if (!local_player->is_alive())
		return;

	m_aimPunchAngle[local_player->tickbase() % 128] = local_player->aimpunchangle();
}

void norecoil::fsn()
{
	cs_player* local_player = cs_player::get_local_player();
	if (!local_player)
		return;
	if (!local_player->is_alive())
		return;

	const qangle new_punch_angle = m_aimPunchAngle[local_player->tickbase() % 128];
	if (!new_punch_angle.IsValid())
		return;

	local_player->aimpunchangle() = new_punch_angle;
}