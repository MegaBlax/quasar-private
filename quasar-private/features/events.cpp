#include "events.h"
#include "sounds.h"
#include "aimbot.h"
#include "misc.h"
#include "../sdk/cs_player.h"
#include "../utils/fnv_hash.h"

event_listener* g_pEventmanager;
match_events* g_pMatcheventmanager;

constexpr auto round_start = FNV("round_start");
constexpr auto round_end = FNV("round_end");
constexpr auto player_death = FNV("player_death");
constexpr auto player_hurt = FNV("player_hurt");

void event_listener::FireGameEvent(IGameEvent* ev)
{
	const auto event_name = fnv::hash_runtime(ev->GetName());

	if (event_name == round_start)
	{
		g_pPlayerList->clear();
		g_pSounds->clear();
	}
	if (event_name == round_end)
		g_pPlayerList->remove_object(nullptr);
	
	if (event_name == player_death)
	{
		const int userid = g_pInterfaces->engineclient()->GetPlayerForUserID(ev->GetInt(xorstr_("userid")));
		const int attacker = g_pInterfaces->engineclient()->GetPlayerForUserID(ev->GetInt(xorstr_("attacker")));
		const int local = g_pInterfaces->engineclient()->GetLocalPlayer();
		if (attacker == local && userid != local)
		{
			cs_player* local_player = cs_player::get_local_player();
			if (!local_player || !local_player->is_alive())
				return;

			cs_weapon* weapon = local_player->weapon();
			if (!weapon || !weapon->is_shootable())
				return;
			
			g_pAimbot->player_death(userid);
		}
	}
	if (event_name == player_hurt)
	{
		if (!globals::misc::hitmarker)
			return;
		
		const int userid = g_pInterfaces->engineclient()->GetPlayerForUserID(ev->GetInt(xorstr_("userid")));
		const int attacker = g_pInterfaces->engineclient()->GetPlayerForUserID(ev->GetInt(xorstr_("attacker")));
		const int local = g_pInterfaces->engineclient()->GetLocalPlayer();
		if (attacker == local && userid != local)
			g_pMisc->player_hurt();
	}
}

constexpr auto OnMatchSessionUpdate = FNV("OnMatchSessionUpdate");

void match_events::OnEvent(KeyValues* pEvent)
{
	if (!globals::misc::autoaccept)
		return;
	
	const auto event_name = fnv::hash_runtime(pEvent->GetName());
	if (event_name == OnMatchSessionUpdate)
		g_pMisc->autoaccept();
}