#pragma once

class CGameUI
{
public:
	bool InGame() const
	{
		return m_nState == CSGO_GAME_UI_STATE_INGAME;
	}

	char pad_0000[0x1E4];
	unsigned int m_nState;
};