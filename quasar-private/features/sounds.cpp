#include "sounds.h"
#include "../sdk/cs_player.h"
#include <imgui_render.h>

std::unique_ptr<sounds> g_pSounds;

void sounds::update()
{
	_list.RemoveAll();
	g_pInterfaces->enginesound()->GetActiveSounds(_list);
	cs_player* local_player = cs_player::get_local_player();
	if (!local_player)
		return;

	for (int i = 0; i < _list.Count(); i++)
	{
		const SndInfo_t snd_info = _list[i];
		char name[1024];
		if (snd_info.m_nSoundSource == 0 || // world
			snd_info.m_nSoundSource > 64 || // invalid
			snd_info.m_nSoundSource == g_pInterfaces->engineclient()->GetLocalPlayer()) // local player
			continue;
		if (snd_info.m_nChannel != 4 && snd_info.m_nChannel != 6)
			continue;
		if (!g_pInterfaces->filesystem()->String(snd_info.m_FilenameHandle, name, 1024))
			continue;
		if (snd_info.m_nChannel == 6 && !strstr(name,xorstr_("player\\footsteps\\new\\land_")))
			continue;
		if (!snd_info.m_bUpdatePositions)
			continue;
		
		cs_player* player = cs_player::get_entity_player(snd_info.m_nSoundSource);
		if (!player)
			continue;
		if (!player->is_alive() || player->IsDormant())
			continue;

		const int teamid = local_player->teamindex() == player->teamindex() ? int(TEAM::ALLY) : int(TEAM::ENEMY);
		if (!globals::visuals::team_t[teamid].active)
			continue;
		if (!globals::visuals::team_t[teamid].steps)
			continue;
		if (!is_valid(snd_info))
			continue;
		if (!snd_info.m_pOrigin || !snd_info.m_pOrigin->IsValid())
			continue;
		if (snd_info.m_pOrigin->DistTo(local_player->vecorigin()) > 850.f)
			continue;
		
		const float time = g_pInterfaces->globalvarsbase()->curtime + 1.f;
		const ImVec4 color = globals::visuals::team_t[teamid].color[int(PCOLOR::STEPS)];
		const vector origin = snd_info.m_nChannel == 6 ? player->vecorigin() : *snd_info.m_pOrigin;
		
		_data.emplace_back(time, globals::visuals::team_t[globals::visuals::teamid].steps_size, snd_info.m_nGuid, origin, color);
	}
	
}

void sounds::clear()
{
	_list.RemoveAll();
	_data.clear();
}

void sounds::draw()
{
	cs_player* local_player = cs_player::get_local_player();
	for (size_t i = 0; i < _data.size(); i++)
	{
		const float dist = local_player->eye().DistTo(_data[i].position);
		const float diff = _data[i].time - g_pInterfaces->globalvarsbase()->curtime;
		_data[i].radius -= (_data[i].steps_size / 1.0f) * g_pInterfaces->globalvarsbase()->frametime;
		float points = _data[i].radius;
		if (dist < 200.f)
			points = points * (21 - dist / 10);
		if (diff <= 0.f || _data[i].radius <= 0.f)
		{
			_data.erase(_data.begin() + i);
			continue;
		}

		g_pImRender->add_circle3d(_data[i].position, points, _data[i].radius, 1.f, ImGui::GetColorU32(_data[i].color));
	}
}

bool sounds::is_valid(const SndInfo_t& sound)
{
	// Use only server dispatched sounds.
	//if (!sound.m_bFromServer)
	//	return false;

	for (auto& it : _data)
	{
		if (it.guid == sound.m_nGuid)
			return false;
	}

	return true;
}
