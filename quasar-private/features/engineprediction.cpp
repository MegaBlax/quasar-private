#include "engineprediction.h"

std::unique_ptr<eprediction> g_pPrediction;

eprediction::eprediction(): _last_cmd(nullptr), _old_cmd(nullptr), _old_curtime(0), _old_frametime(0), _tickbase(0),
                            _randomseed(0),
                            _old_flags(0)
{
}

void eprediction::run(CUserCmd* cmd)
{
    auto local_player = cs_player::get_local_player();
	
    if (!g_pInterfaces->movehelper() || !cmd || !local_player)
        return;

    if (_last_cmd) 
    {
        if (_last_cmd->hasbeenpredicted) {
            _tickbase = local_player->tickbase();
        }
        else {
            ++_tickbase;
        }
        globals::vars::server_time = _tickbase * g_pInterfaces->globalvarsbase()->interval_per_tick;
    }

    const auto getRandomSeed = [&cmd]()
	{
        using fn = unsigned int(__cdecl*)(unsigned int);
        static auto MD5_PseudoRandom = reinterpret_cast<fn>(g_pOffsets->randomseed());
        return MD5_PseudoRandom(cmd->command_number) & 0x7FFFFFFF;;
    };

    _last_cmd = cmd;
    _old_cmd = cmd;
    _old_curtime = g_pInterfaces->globalvarsbase()->curtime;
    _old_frametime = g_pInterfaces->globalvarsbase()->frametime;
    _old_flags = local_player->flags();

    g_pInterfaces->movehelper()->SetHost(local_player);
    _randomseed = getRandomSeed();
    g_pInterfaces->globalvarsbase()->curtime = globals::vars::server_time;
    g_pInterfaces->globalvarsbase()->frametime = g_pInterfaces->globalvarsbase()->interval_per_tick;
    local_player->currentcmd() = cmd;

    g_pInterfaces->gamemovement()->StartTrackPredictionErrors(local_player);

    CMoveData data;
    memset(&data, 0, sizeof(CMoveData));

    g_pInterfaces->prediction()->SetupMove(local_player, cmd, nullptr, &data);
    g_pInterfaces->gamemovement()->ProcessMovement(local_player, &data);
    g_pInterfaces->prediction()->FinishMove(local_player, cmd, &data);
}

void eprediction::end()
{
    auto local_player = cs_player::get_local_player();
    if (!local_player)
        return;

	g_pInterfaces->gamemovement()->FinishTrackPredictionErrors(local_player);
    local_player->currentcmd() = _old_cmd;
    local_player->flags() = _old_flags;
    g_pInterfaces->globalvarsbase()->curtime = _old_curtime;
    g_pInterfaces->globalvarsbase()->frametime = _old_frametime;
    g_pInterfaces->movehelper()->SetHost(nullptr);
}

void eprediction::clear()
{
    _last_cmd = nullptr;
    _old_cmd = nullptr;
    _old_curtime = 0.f;
    _old_frametime = 0.f;
    _tickbase = 0;
    _randomseed = 0;
    _old_flags = 0;
}

int eprediction::flags()
{
    return _old_flags;
}