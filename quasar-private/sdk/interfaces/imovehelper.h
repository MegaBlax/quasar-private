#pragma once

class IMoveHelper
{
public:
	virtual void UnknownVirtual() = 0;
	virtual void SetHost(IClientEntity* host) = 0;
};