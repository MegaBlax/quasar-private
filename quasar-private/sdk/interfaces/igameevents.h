#pragma once
#define MAX_EVENT_NAME_LENGTH	32		// max game event name length
#define EVENT_DEBUG_ID_INIT 42

class CGameEventCallback
{
public:
	void* m_pCallback; // callback pointer
	int m_nListenerType; // client or server side ?
};

class CGameEventDescriptor
{
public:
	char name[MAX_EVENT_NAME_LENGTH]; // name of this event
	int eventid; // network index number, -1 = not networked
	KeyValues* keys; // KeyValue describing data types, if NULL only name 
	bool local; // local event, never tell clients about that
	bool reliable; // send this event as reliable message
	CUtlVector<CGameEventCallback*> listeners; // registered listeners
};

class IGameEvent
{
public:
	virtual ~IGameEvent() {};
	virtual const char* GetName() const = 0;	// get event name

	virtual bool  IsReliable() const = 0; // if event handled reliable
	virtual bool  IsLocal() const = 0; // if event is never networked
	virtual bool  IsEmpty(const char* keyName = NULL) = 0; // check if data field exists

	// Data access
	virtual bool  GetBool(const char* keyName = NULL, bool defaultValue = false) = 0;
	virtual int   GetInt(const char* keyName = NULL, int defaultValue = 0) = 0;
	virtual uint64_t GetUint64(const char* keyname = NULL, unsigned long long default_value = 0) = 0;
	virtual float GetFloat(const char* keyname = nullptr, float default_value = 0.0f) = 0;
	virtual const char* GetString(const char* keyname = nullptr, const char* default_value = "") = 0;
	virtual const wchar_t* GetWString(const char* keyname = nullptr, const wchar_t* default_value = L"") = 0;
	virtual const void* GetPtr(const char* keyname = nullptr, const void* default_values = nullptr) = 0;

	virtual void SetBool(const char* keyname, bool value) = 0;
	virtual void SetInt(const char* keyname, int value) = 0;
	virtual void SetUint64(const char* keyname, uint64_t value) = 0;
	virtual void SetFloat(const char* keyname, float value) = 0;
	virtual void SetString(const char* keyname, const char* value) = 0;
	virtual void SetWString(const char* keyname, const wchar_t* value) = 0;
	virtual void SetPtr(const char* keyname, const void* value) = 0;
};

class CGameEvent : public IGameEvent
{
public:

	CGameEvent(CGameEventDescriptor* descriptor, const char* name)
	{
		m_pDescriptor = descriptor;
		m_pDataKeys = new KeyValues(name);
		m_pDataKeys->SetInt(("splitscreenplayer"), 0);
	}
	virtual ~CGameEvent() = 0;

	virtual const char* GetName() = 0;
	virtual bool  IsLocal() = 0;
	virtual bool  IsReliable() = 0;
	bool  IsEmpty(const char* keyName = NULL) override = 0; // check if data field exists

	// Data access
	bool  GetBool(const char* keyName = NULL, bool defaultValue = false) override = 0;
	int   GetInt(const char* keyName = NULL, int defaultValue = 0) override = 0;
	uint64_t GetUint64(const char* keyname = NULL, unsigned long long default_value = 0) override = 0;
	float GetFloat(const char* keyname = nullptr, float default_value = 0.0f) override = 0;
	const char* GetString(const char* keyname = nullptr, const char* default_value = "") override = 0;
	const wchar_t* GetWString(const char* keyname = nullptr, const wchar_t* default_value = L"") override = 0;
	const void* GetPtr(const char* keyname = nullptr, const void* default_values = nullptr) override = 0;

	void SetBool(const char* keyname, bool value) override = 0;
	void SetInt(const char* keyname, int value) override = 0;
	void SetUint64(const char* keyname, uint64_t value) override = 0;
	void SetFloat(const char* keyname, float value) override = 0;
	void SetString(const char* keyname, const char* value) override = 0;
	void SetWString(const char* keyname, const wchar_t* value) override = 0;
	void SetPtr(const char* keyname, const void* value) override = 0;

	CGameEventDescriptor* m_pDescriptor;
	KeyValues* m_pDataKeys;
};

class IGameEventListener2
{
public:
	virtual	~IGameEventListener2(void) {};

	// FireEvent is called by EventManager if event just occured
	// KeyValue memory will be freed by manager if not needed anymore
	virtual void FireGameEvent(IGameEvent* event) = 0;
	virtual int GetEventDebugID() = 0;
};

class IGameEventManager2
{
public:
	virtual	~IGameEventManager2(void) {};

	bool AddListener(IGameEventListener2* listener, const char* name, bool bServerSide)
	{
		typedef bool(__thiscall * fn)(void*, IGameEventListener2*, const char*, bool);
		return call_virtual<fn>(this, 3)(this, listener, name, bServerSide);
	}
	void RemoveListener(IGameEventListener2* listener)
	{
		typedef void(__thiscall * fn)(void*, IGameEventListener2*);
		call_virtual<fn>(this, 5)(this, listener);
	}
};