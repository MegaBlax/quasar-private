#include "cfactory.h"
#include "mem.h"

namespace factory
{
	cfactory::cfactory(const HMODULE& module)
	{
		init(reinterpret_cast<ptrdiff_t>(GetProcAddress(module, xorstr_("CreateInterface"))));
	}

	cfactory::cfactory(void* module)
	{
		init(reinterpret_cast<ptrdiff_t>(GetProcAddress(HMODULE(module), xorstr_("CreateInterface"))));
	}

	void cfactory::init(const ptrdiff_t& dwCreateInterface)
	{
		if (!dwCreateInterface)
			return;
		
		DWORD s_p_interface_regs = *reinterpret_cast<PDWORD>(dwCreateInterface + 5);

		if (s_p_interface_regs & 1 << 0x1F)
			s_p_interface_regs = 0xFFFFFFF1 - s_p_interface_regs;
		else
			s_p_interface_regs = 0;

		if (s_p_interface_regs != 0xfffffffA && s_p_interface_regs > 0x2000)
			return;
		if (!s_p_interface_regs)
			return;
		
		this->s_pInterfaceRegs = reinterpret_cast<interface_reg*>(**reinterpret_cast<PDWORD*>(dwCreateInterface - s_p_interface_regs));
	}

	std::vector<interface_info> cfactory::dump() const
	{
		std::vector<interface_info> ret;
		for (interface_reg* pCur = s_pInterfaceRegs; pCur; pCur = pCur->m_pNext)
			ret.emplace_back(pCur->interface_info_);
		
		return ret;
	}

	/**
	 * \brief �������� ��������� �� ���������
	 * \param pszName �������� ���������� ��� ������ ���� 000
	 * \param iVersion ���� ������ ���������� ��������� - ������� ����� ���������� ������������
	 * \return ��������� �� ����������� ������� (���������)
	 */
	DWORD cfactory::dwGetInterface(const char * pszName, int iVersion) const
	{
		const size_t iNameLen = strlen(pszName);

		for (interface_reg* pCur = s_pInterfaceRegs; pCur; pCur = pCur->m_pNext)
			if (!strncmp(pCur->interface_info_.m_pName, pszName, iNameLen))
				if (atoi(pCur->interface_info_.m_pName + iNameLen) > iVersion)
					return pCur->interface_info_.m_CreateFn();

		return NULL;
	}

	bool cfactory::is_valid() const
	{
		return s_pInterfaceRegs;
	}

	bool init()
	{
		engine = new cfactory(g_pMemory->engine().lpBaseOfDll);
		client = new cfactory(g_pMemory->client().lpBaseOfDll);
		vguimatsurface = new cfactory(g_pMemory->vguimatsurface().lpBaseOfDll);
		vstdlib = new cfactory(g_pMemory->vstdlib().lpBaseOfDll);
		materialsystem = new cfactory(g_pMemory->materialsystem().lpBaseOfDll);
		vphysics = new cfactory(g_pMemory->vphysics().lpBaseOfDll);
		filesystem = new cfactory(g_pMemory->filesystem().lpBaseOfDll);
		vgui2 = new cfactory(g_pMemory->vgui2().lpBaseOfDll);
		inputsystem = new cfactory(g_pMemory->inputsystem().lpBaseOfDll);
		matchmaking = new cfactory(g_pMemory->matchmaking().lpBaseOfDll);

		if (!engine->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize engine interface"));
			return false;
		}
		if (!client->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize client interface"));
			return false;
		}
		if (!vguimatsurface->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize vguimatsurface interface"));
			return false;
		}
		if (!vstdlib->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize vstdlib interface"));
			return false;
		}
		if (!materialsystem->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize materialsystem interface"));
			return false;
		}
		if (!vphysics->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize vphysics interface"));
			return false;
		}
		if (!filesystem->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize filesystem interface"));
			return false;
		}
		if (!vgui2->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize vgui2 interface"));
			return false;
		}
		if (!inputsystem->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize inputsystem interface"));
			return false;
		}
		if (!matchmaking->is_valid())
		{
			log_error(xorstr_(L"Failed to initialize matchmaking interface"));
			return false;
		}
		return true;
	}

	cfactory* engine = nullptr;
	cfactory* client = nullptr;
	cfactory* vguimatsurface = nullptr;
	cfactory* vstdlib = nullptr;
	cfactory* materialsystem = nullptr;
	cfactory* vphysics = nullptr;
	cfactory* filesystem = nullptr;
	cfactory* vgui2 = nullptr;
	cfactory* inputsystem = nullptr;
	cfactory* matchmaking = nullptr;
}
