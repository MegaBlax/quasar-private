#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"
#include "../imgui/imgui.h"

#define OVERVIEW_MAP_SIZE	1024	// an overview map is 1024x1024 pixels

enum class RADAR
{
	MAP = 0,
	ICON_PLAYER_GHOST,
	ICON_PLAYER_DEATH,
	C4_SMAL,
	C4_PING,
	C4_DEF_PING,
	MAX
};

class CMapOverview
{
public:
	
};

class radar
{
public:
	bool init();
	void find_maptexture();
	void run();
	
private:
	void load_texture();
	ImVec2 world_to_map(const vector& worldpos) const;
	ImVec2 map_to_radar(const ImVec2& mappos);
	void getbounds(float& x, float& y, float& wide, float& tall);
	ImVec2 is_in_panel(ImVec2& pos);
	ImVec2 world_to_radar(const vector& entity_origin, const vector& local_origin, float angle, float zoom);
	bool update();

	ImVec2 _center = ImVec2(512, 512);
	ImVec2 _pos = ImVec2();
	float _scale = 0.f;
	float _time = 0.f;
	float _ping_size = 5.f;
	bool _find = false;
	bool _ticking = false;
	bool _defusing = false;
	IDirect3DDevice9* _device = nullptr;
	IDirect3DTexture9* _texture[int(RADAR::MAX)] = {};
};

extern std::unique_ptr<radar> g_pRadar;