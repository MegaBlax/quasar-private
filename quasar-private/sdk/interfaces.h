#pragma once
#include "../stdafx.h"
#include "vector.h"
#include "mathlib.h"
#include "in_buttons.h"
#include "sndInfo.h"
#include "enums.h"
#include "utlvector.h"
#include "tier1/KeyValues.h"
#include "keybind.h"
#include "key_translation.h"

#include "interfaces/ehandle.h"
#include "interfaces/iappsystem.h"
#include "interfaces/icliententity.h"
#include "interfaces/iclientrenderable.h"
#include "interfaces/clientclass.h"
#include "interfaces/usercmd.h"
#include "interfaces/icliententitylist.h"
#include "interfaces/ibaseclientdll.h"
#include "interfaces/ivengineclient.h"
#include "interfaces/iclientmode.h"
#include "interfaces/icvar.h"
#include "interfaces/ipanel.h"
#include "interfaces/isurface.h"
#include "interfaces/ienginetrace.h"
#include "interfaces/ivmodelinfo.h"
#include "interfaces/globalvars.h"
#include "interfaces/igameevents.h"
#include "interfaces/ienginesound.h"
#include "interfaces/imovehelper.h"
#include "interfaces/iprediction.h"
#include "interfaces/imaterialsystem.h"
#include "interfaces/imaterial.h"
#include "interfaces/imaterialvar.h"
#include "interfaces/ivmodelrender.h"
#include "interfaces/view_shared.h"
#include "interfaces/iviewrender.h"
#include "interfaces/memalloc.h"
#include "interfaces/input.h"
#include "interfaces/filesystem.h"
#include "interfaces/iweaponsystem.h"
#include "interfaces/client.h"
#include "interfaces/iinputsystem.h"
#include "interfaces/isteamclient.h"
#include "interfaces/isteamuser.h"
#include "interfaces/imatchframework.h"
#include "interfaces/cgameui.h"
#include "interfaces/igametypes.h"
#include "interfaces/igamemovement.h"
#include "interfaces/inetsupport.h"
#include "interfaces/bsptreedata.h"
#include "interfaces/ivrenderview.h"

class interfaces
{
public:
	interfaces();
	bool init();
	IBaseClientDLL* baseclientdll() const;
	IVEngineClient* engineclient() const;
	IClientEntityList* cliententitylist() const;
	IClientMode* clientmode() const;
	ICvar* cvar() const;
	IPanel* panel() const;
	ISurface* surface() const;
	IEngineTrace* enginetrace() const;
	IVModelInfo* modelinfo() const;
	CGlobalVarsBase* globalvarsbase() const;
	IGameEventManager2* gameeventmanager() const;
	IEngineSound* enginesound() const;
	IPrediction* prediction() const;
	IMaterialSystem* materialsystem() const;
	IVModelRender* modelrender() const;
	IViewRender* viewrender() const;
	IMemAlloc* memalloc();
	CInput* input() const;
	IFileSystem* filesystem() const;
	IWeaponSystem* weaponsystem() const;
	CClientState* clientstate() const;
	IInputSystem* inputsystem() const;
	IMatchFramework* matchframework() const;
	ITextureManager* texturemanager() const;
	CGameUI* gameui() const;
	IGameTypes* gametypes() const;
	IMoveHelper* movehelper() const;
	CGameMovement* gamemovement() const;
	ISpatialQuery* spatialquery() const;
	IVRenderView* renderview() const;
	
private:
	IBaseClientDLL* _baseclientdll;
	IVEngineClient* _engineclient;
	IClientEntityList* _entitylist;
	IClientMode* _clientmode;
	ICvar* _cvar;
	IPanel* _panel;
	ISurface* _surface;
	IEngineTrace* _enginetrace;
	IVModelInfo* _modelinfo;
	CGlobalVarsBase* _globalvarsbase;
	IGameEventManager2* _gameeventmanager;
	IEngineSound* _enginesound;
	IPrediction* _prediction;
	IMaterialSystem* _materialsystem;
	IVModelRender* _modelrender;
	IViewRender* _viewrender;
	CInput* _input;
	IFileSystem* _filesystem;
	IWeaponSystem* _weaponsystem;
	CClientState* _clientstate;
	IInputSystem* _inputsystem;
	ISteamClient* _steamclient;
	ISteamUser* _steamuser;
	IMatchFramework* _matchframework;
	ITextureDictionary* _texturedictionary;
	ITextureManager* _texturemanager;
	CGameUI* _gameui;
	IGameTypes* _gametypes;
	IMoveHelper* _movehelper;
	CGameMovement* _gamemovement;
	INetSupport* _netsupport;
	ISpatialQuery* _spatialquery;
	IVRenderView* _renderview;
};

extern std::unique_ptr<interfaces> g_pInterfaces;