#include "netvarmanager.h"

std::unique_ptr<netvarmanager> g_pNetvarManager;

bool netvarmanager::init()
{
	if (!g_pInterfaces->baseclientdll())
		return false;

	for (auto clazz = g_pInterfaces->baseclientdll()->GetAllClasses(); clazz; clazz = clazz->m_pNext)
		if (clazz->m_pRecvTable)
			dump(clazz->m_pRecvTable);

	return true;
}

std::uint16_t netvarmanager::get_offset(const fnv::hash& hash)
{
	return _database.at(hash).offset;
}

void netvarmanager::dump(RecvTable* recv_table, const char* name, const std::uint16_t offset)
{
	const auto name_len = strlen(name);
	for (auto i = 0; i < recv_table->m_nProps; ++i)
	{
		const auto prop = &recv_table->m_pProps[i];
		const char* base_class = name_len > 0 ? name : recv_table->m_pNetTableName;
		if (!prop || isdigit(prop->m_pVarName[0]))
			continue;

		if (fnv::hash_runtime(prop->m_pVarName) == FNV("baseclass"))
			continue;

		if (prop->m_RecvType == DPT_DataTable &&
			prop->m_pDataTable != nullptr &&
			prop->m_pDataTable->m_pNetTableName[0] == 'D')
		{
			dump(prop->m_pDataTable, base_class, std::uint16_t(offset + prop->m_Offset));
		}

		char hash_name[256];
		
		strcpy_s(hash_name, base_class);
		strcat_s(hash_name, "->");
		strcat_s(hash_name, prop->m_pVarName);

		const auto hash = fnv::hash_runtime(hash_name);
		const auto total_offset = std::uint16_t(offset + prop->m_Offset);

		_database[hash] =
		{
			prop,
			total_offset
		};
	}
}
