#pragma once
#include "iappsystem.h"
#include "imatchevents.h"

class IMatchSession
{
public:
	// Get an internal pointer to session system-specific data
	virtual KeyValues* GetSessionSystemData() = 0;

	// Get an internal pointer to session settings
	virtual KeyValues* GetSessionSettings() = 0;

	// Update session settings, only changing keys and values need
	// to be passed and they will be updated
	virtual void UpdateSessionSettings(KeyValues* pSettings) = 0;

	// Issue a session command
	virtual void Command(KeyValues* pCommand) = 0;

	// Get the lobby or XSession ID 
	virtual unsigned __int64 GetSessionID() = 0;

	// Callback when team changes
	virtual void UpdateTeamProperties(KeyValues* pTeamProperties) = 0;
};

class IMatchFramework : public IAppSystem
{
public:
	// Get events container
	IMatchEventsSubscription* GetEventsSubscription()
	{
		//11
		using fn = IMatchEventsSubscription * (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 11)(this);
	}

	// Get the match session interface of the current match framework type
	IMatchSession* GetMatchSession()
	{
		//13
		using fn = IMatchSession * (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 13)(this);
	}
};