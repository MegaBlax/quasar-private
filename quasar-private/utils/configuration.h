#pragma once
#include "../stdafx.h"

class configuration
{
public:
	bool init();
	[[nodiscard]] std::vector<std::wstring> getarray() const;
	void del(int id) const;
	bool save(const std::wstring& name) const;
	bool load(const std::wstring& name) const;

private:
	std::wstring _path;
};

extern std::unique_ptr<configuration> g_pConfig;