#include "cs_player.h"
#include "netvars.h"
#include "offsets.h"
#include "../utils/utils.h"

cs_player* cs_player::get_local_player()
{
	return static_cast<cs_player*>(g_pInterfaces->cliententitylist()->GetClientEntity(
		g_pInterfaces->engineclient()->GetLocalPlayer()));
}

cs_player* cs_player::get_entity_player(const int index)
{
	return static_cast<cs_player*>(g_pInterfaces->cliententitylist()->GetClientEntity(index));
}

int cs_player::health()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_iHealth);
}

int cs_player::lifestate()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_lifeState);
}

int cs_player::teamindex()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_iTeamNum);
}

int cs_player::survivalteam()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_nSurvivalTeam);
}

int cs_player::tickbase()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_nTickBase);
}

int cs_player::shotsfired()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_iShotsFired);
}

int cs_player::movetype()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + 0x258);
}

int& cs_player::flags()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_fFlags);
}

int cs_player::observermode()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_iObserverMode);
}

int& cs_player::bspotted()
{
	return *reinterpret_cast<int*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_bSpotted);
}

float cs_player::flashalpha()
{
	return *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_flFlashMaxAlpha - 0x8);
}

float cs_player::simulationtime()
{
	return *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_flSimulationTime);
}

float cs_player::nextattack()
{
	return *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_weapon::m_flNextAttack);
}

bool cs_player::is_alive()
{
	return this->lifestate() == LIFE_ALIVE && this->health() > 0 && !this->is_ghost();
}

bool cs_player::is_player()
{
	return this->GetClientClass()->m_ClassID == CCSPlayer;
}

bool cs_player::is_enemy(cs_player* entity)
{
	if (globals::vars::game_type == DANGER_ZONE)
	{
		if (this->survivalteam() >= 0)
			return this->survivalteam() != entity->survivalteam();
		
		return true;
	}
	return this->teamindex() != entity->teamindex();
}

bool cs_player::in_smoke()
{
	static auto check = reinterpret_cast<GoesThroughSmoke>(g_pOffsets->smoke());
	cs_player* local_player = get_local_player();
	return check(local_player->eye(), this->eye());
}

bool cs_player::is_flashed()
{
	const float time = *reinterpret_cast<float*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_flFlashDuration - 0x14) - g_pInterfaces->globalvarsbase()->curtime;
	return time > 2.f;
}

bool cs_player::is_ghost()
{
	return *reinterpret_cast<bool*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_bIsPlayerGhost);
}

bool cs_player::has_c4()
{
	static auto check = reinterpret_cast<HasC4>(g_pOffsets->hasc4());
	return check(this);
}

bool cs_player::is_immunity()
{
	return *reinterpret_cast<bool*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_bGunGameImmunity);
}

bool cs_player::is_scoped()
{
	return *reinterpret_cast<bool*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_bIsScoped);
}

vector& cs_player::aimpunchangle()
{
	return *reinterpret_cast<vector*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_aimPunchAngle);
}

vector& cs_player::vecorigin()
{
	return *reinterpret_cast<vector*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_vecOrigin);
}

vector& cs_player::vecvelocity()
{
	return *reinterpret_cast<vector*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_vecVelocity);
}

vector cs_player::eye()
{
	return *reinterpret_cast<vector*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_vecViewOffset) + vecorigin();
}

vector& cs_player::eyeangles()
{
	return *reinterpret_cast<vector*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_angEyeAngles);
}

vector cs_player::gethitbox(const int index)
{
	std::array<matrix3x4_t, MAXSTUDIOBONES> bmatrix;
	if (this->SetupBones(bmatrix.data(), MAXSTUDIOBONES, BONE_USED_BY_HITBOX, this->simulationtime()))
	{
		const auto model = g_pInterfaces->modelinfo()->GetStudiomodel(this->GetModel());
		if (model)
		{
			const auto hbox = model->pHitbox(index);
			if (hbox)
			{
				auto min = vector(), max = vector();
				VectorTransform(hbox->bbmin, bmatrix[hbox->bone], min);
				VectorTransform(hbox->bbmax, bmatrix[hbox->bone], max);
				return (min + max) / 2.0f;
			}
		}
	}
	return vector();
}

std::vector<vector> cs_player::getmultipoint(const int index, int maxpoints, float scale)
{
	std::vector<vector> points;
	if (maxpoints > 74)
		return points;
	std::array<matrix3x4_t, MAXSTUDIOBONES> bmatrix;
	if (!this->SetupBones(bmatrix.data(), MAXSTUDIOBONES, BONE_USED_BY_HITBOX, this->simulationtime()))
		return points;
	if (!this->GetModel())
		return points;
	
	const auto studiomodel = g_pInterfaces->modelinfo()->GetStudiomodel(this->GetModel());
	if (studiomodel)
	{
		const auto hitbox = studiomodel->pHitbox(index);
		if (hitbox && hitbox->flCapsuleRadius != -1.f)
		{
			vector min, max;
			matrix3x4_t dmatrix, rmatrix;
			VectorTransform(hitbox->bbmin, bmatrix[hitbox->bone], min);
			VectorTransform(hitbox->bbmax, bmatrix[hitbox->bone], max);
			vector norm = (max - min);
			VectorNormalize(norm);
			VectorMatrix(vector(0.0f, 0.0f, .9f), rmatrix);
			VectorMatrix(norm, dmatrix);
			for (int i = 0; i < 74; i += (int)(74 / maxpoints))
			{
				if (i >= 74)
					i = 73;

				float flPoints[3];

				VectorRotate(g_capsuleVertPositions[i], rmatrix, &flPoints[0]);
				VectorRotate(&flPoints[0], dmatrix, &flPoints[0]);

				vector vecPoint = vector(flPoints) * (hitbox->flCapsuleRadius * scale);

				if (g_capsuleVertPositions[i][2] > 0.f)
					vecPoint += (min - max);

				points.push_back(max + vecPoint);
			}
		}
	}

	return points;
}

std::array<float, 24> & cs_player::poseparameter()
{
	return *reinterpret_cast<std::array<float, 24>*>(reinterpret_cast<DWORD>(this) + netvars::dt_base_animating::m_flPoseParameter);
}

std::array<matrix3x4_t, MAXSTUDIOBONES> cs_player::bonematrix()
{
	std::array<matrix3x4_t, MAXSTUDIOBONES> bone_matrix;
	this->SetupBones(bone_matrix.data(), MAXSTUDIOBONES, BONE_USED_BY_ANYTHING, this->simulationtime());
	return std::move(bone_matrix);
}

CUserCmd*& cs_player::currentcmd()
{
	return *reinterpret_cast<CUserCmd**>(reinterpret_cast<DWORD>(this) + g_pOffsets->currentcmd());
}

void cs_player::setabsorigin(const vector& origin)
{
	static auto _setabsorigin = reinterpret_cast<SetAbsOrigin>(g_pOffsets->setabsorigin());
	_setabsorigin(this, origin);
}

void cs_player::setabsangles(const qangle& angles)
{
	static auto _setabsangles = reinterpret_cast<SetAbsOrigin>(g_pOffsets->setabsangles());
	_setabsangles(this, angles);
}

void cs_player::invalidatebonecache()
{
	const auto model_bone_counter = **reinterpret_cast<unsigned long**>(g_pOffsets->invalidbonecache() + 0xA);
	*reinterpret_cast<unsigned int*>(reinterpret_cast<DWORD>(this) + 0x2924) = 0xFF7FFFFF; // m_flLastBoneSetupTime = -FLT_MAX;
	*reinterpret_cast<unsigned int*>(reinterpret_cast<DWORD>(this) + 0x2690) = model_bone_counter - 1; // m_iMostRecentModelBoneCounter = g_iModelBoneCounter - 1;
}

cs_player* cs_player::observertarget()
{
	const CHandle<cs_player> handle = *reinterpret_cast<CHandle<cs_player>*>(reinterpret_cast<DWORD>(this) + netvars::cs_player::m_hObserverTarget);
	return reinterpret_cast<cs_player*>(g_pInterfaces->cliententitylist()->GetClientEntityFromHandle(handle));
}

cs_weapon* cs_player::weapon()
{
	const auto weapon = *reinterpret_cast<CHandle<IClientEntity>*>(reinterpret_cast<DWORD>(this) + netvars::cs_weapon::m_hActiveWeapon);
	return reinterpret_cast<cs_weapon*>(g_pInterfaces->cliententitylist()->GetClientEntityFromHandle(weapon));
}

bool cs_player::is_visible()
{
	int scan_hitboxes[] = {
		HITBOX_HEAD,
		HITBOX_LEFT_FOOT,
		HITBOX_RIGHT_FOOT,
		HITBOX_LEFT_CALF,
		HITBOX_RIGHT_CALF,
		HITBOX_CHEST,
		HITBOX_STOMACH,
		HITBOX_RIGHT_HAND,
		HITBOX_LEFT_HAND,
	};

	cs_player* local_player = get_local_player();
	if (!local_player)
		return false;

	if (!local_player->is_alive())
	{
		cs_player* target = local_player->observertarget();
		if (local_player->observermode() == OBS_MODE_IN_EYE && target)
			if (target->is_alive() && !target->IsDormant())
				local_player = local_player->observertarget();
	}

	CTraceFilter filter;
	filter.pSkip = local_player;
	trace_t tr;
	Ray_t ray;

	for (auto& it : scan_hitboxes)
	{
		ray.Init(local_player->eye(), this->gethitbox(it));
		g_pInterfaces->enginetrace()->TraceRay(ray, MASK_IS_VISIBLE, &filter, &tr);
		if (tr.m_pEnt == static_cast<IClientEntity*>(this) && tr.DidHit())
			return true;
	}
	
	return false;
}

bool cs_player::hitbox_visible(const vector& hitbox)
{
	cs_player* local_player = get_local_player();
	if (!local_player)
		return false;
	
	CTraceFilter filter;
	filter.pSkip = local_player;
	trace_t tr;
	Ray_t ray;

	ray.Init(local_player->eye(), hitbox);
	g_pInterfaces->enginetrace()->TraceRay(ray, MASK_IS_VISIBLE, &filter, &tr);

	return tr.m_pEnt == static_cast<IClientEntity*>(this) && tr.DidHit();
}

bool cs_player::in_crosshair(const int index)
{
	cs_player* local = get_local_player();
	if (!local)
		return false;

	const vector start = local->eye();
	vector trace_end;
	const qangle view_angles = g_pUtils->GetViewAngles();
	const vector view_angles_rcs = view_angles + local->aimpunchangle() * 2.0f;
	AngleVectors(view_angles_rcs, &trace_end);
	trace_end = start + trace_end * 8192.0f;

	std::array<matrix3x4_t, MAXSTUDIOBONES> bmatrix;
	if (this->SetupBones(bmatrix.data(), MAXSTUDIOBONES, BONE_USED_BY_HITBOX, this->simulationtime()))
	{
		const auto model = g_pInterfaces->modelinfo()->GetStudiomodel(this->GetModel());
		if (model)
		{
			const auto hbox = model->pHitbox(index);
			if (hbox)
			{
				auto min = vector(), max = vector();
				VectorTransform(hbox->bbmin, bmatrix[hbox->bone], min);
				VectorTransform(hbox->bbmax, bmatrix[hbox->bone], max);
				return Intersect(start, trace_end, min, max, hbox->flCapsuleRadius);
			}
		}
	}
	return false;
}

float cs_player::eye_to_wall_dist()
{
	const vector start = this->eye();
	vector trace_end;
	const qangle view_angles = g_pUtils->GetViewAngles();
	const vector recoil_angle = view_angles + this->aimpunchangle() * 2.0f;
	AngleVectors(recoil_angle, &trace_end);
	trace_end = start + trace_end * 8192.0f;

	CTraceFilter filter;
	filter.pSkip = this;
	trace_t tr;
	Ray_t ray;

	ray.Init(start, trace_end);
	g_pInterfaces->enginetrace()->TraceRay(ray, MASK_ALL, &filter, &tr);
	return tr.DidHitWorld() && tr.DidHit() && tr.hitgroup == HITGROUP_GENERIC ? tr.startpos.DistTo(tr.endpos) : 0.0f;
}

float cs_player::get_hitchance(cs_player* entity)
{
	cs_weapon* weapon = this->weapon();
	if (!weapon || !weapon->is_shootable())
		return 0.f;

	const weapondata_t* weapon_data = g_pInterfaces->weaponsystem()->GetWeaponData(weapon->itemindex());
	
	weapon->update_accuracy_penalty();
	
	vector forward, right, up, dir;
	vector eye = this->eye();
	AngleVectors(g_pUtils->GetViewAngles(), &forward, &right, &up);

	const float fl_weapon_spread = weapon->get_spread();
	const float fl_weapon_inaccuracy = weapon->get_inaccuracy();

	int seeds_hit = 0;
	
	for (int i = 0; i < 256; i++) 
	{
		g_pUtils->RandomSeed(i + 1);
		
		float a = g_pUtils->RandomFloat(0.f, 1.f);
		float b = g_pUtils->RandomFloat(0.f, 2.f * M_PI_F);
		float c = g_pUtils->RandomFloat(0.f, 1.f);
		float d = g_pUtils->RandomFloat(0.f, 2.f * M_PI_F);

		float inaccuracy = a * fl_weapon_inaccuracy;
		float spread = c * fl_weapon_spread;

		vector v_spread((cos(b) * inaccuracy) + (cos(d) * spread), (sin(b) * inaccuracy) + (sin(d) * spread), 0);
		dir = forward + (right * v_spread) + (up * v_spread);

		vector TraceEnd = eye + (dir * weapon_data->flRange);

		trace_t tr;
		Ray_t ray;
		ray.Init(eye, TraceEnd);
		CTraceFilter filter;
		filter.pSkip = this;

		g_pInterfaces->enginetrace()->TraceRay(ray, MASK_SHOT, &filter, &tr);

		if (tr.m_pEnt == entity)
			seeds_hit++;
	}

	return float(seeds_hit) / 256.f * 100.f;
}

bool cs_player::getbox(ImVec2& out_min, ImVec2& out_max)
{
	const auto collision = this->GetCollideable();
	if (!collision)
		return false;
	ImVec2 flb, brt, blb, frt, frb, brb, blt, flt;
	const auto min = collision->OBBMins() + this->vecorigin();
	const auto max = collision->OBBMaxs() + this->vecorigin();

	vector points[] = {
		vector(min.x, min.y, min.z),
		vector(min.x, max.y, min.z),
		vector(max.x, max.y, min.z),
		vector(max.x, min.y, min.z),
		vector(max.x, max.y, max.z),
		vector(min.x, max.y, max.z),
		vector(min.x, min.y, max.z),
		vector(max.x, min.y, max.z) };
	if (!g_pUtils->WorldToScreen(points[3], flb) || !g_pUtils->WorldToScreen(points[5], brt)
		|| !g_pUtils->WorldToScreen(points[0], blb) || !g_pUtils->WorldToScreen(points[4], frt)
		|| !g_pUtils->WorldToScreen(points[2], frb) || !g_pUtils->WorldToScreen(points[1], brb)
		|| !g_pUtils->WorldToScreen(points[6], blt) || !g_pUtils->WorldToScreen(points[7], flt))
		return false;
	ImVec2 arr[] = { flb, brt, blb, frt, frb, brb, blt, flt };
	auto left = flb.x;
	auto top = flb.y;
	auto right = flb.x;
	auto bottom = flb.y;
	for (auto& it : arr)
	{
		if (left > it.x)
			left = it.x;
		if (bottom < it.y)
			bottom = it.y;
		if (right < it.x)
			right = it.x;
		if (top > it.y)
			top = it.y;
	}
	out_min.x = static_cast<float>(left);
	out_min.y = static_cast<float>(top);
	out_max.x = static_cast<float>(right - left);
	out_max.y = static_cast<float>(bottom - top);

	return true;
}

//================================ PLAYER LIST ================================//

std::unique_ptr<player_list> g_pPlayerList;

void player_list::update()
{
	for(auto& it : _players)
	{
		if (!it.entity || !it.index)
			continue;
		
		if (!it.entity->is_alive())
			continue;

		if (_data[it.index].health > float(it.entity->health()))
		{
			const float diff = _data[it.index].health - float(it.entity->health());
			const float step = 100.f / 1.f * g_pInterfaces->globalvarsbase()->frametime;
			_data[it.index].health -= (diff > step ? step : diff);
		}
		else
			_data[it.index].health = float(it.entity->health());
		
		if (it.entity->IsDormant())
		{
			if (_data[it.index].alpha > 0.f)
				_data[it.index].alpha -= 1.f / 1.25f * g_pInterfaces->globalvarsbase()->frametime;
		}
		else
			_data[it.index].alpha = 1.f;

		_data[it.index].can_drawing = it.entity->getbox(_data[it.index].min, _data[it.index].max);
		if (it.entity->weapon())
		{
			_data[it.index].itemindex = it.entity->weapon()->itemindex();
		}
	}
}

void player_list::clear()
{
	for(auto& it : _data)
	{
		it.itemindex = 0;
		it.alpha = 0;
		it.can_drawing = false;
		it.health = 0;
		it.max = ImVec2(0, 0);
		it.min = ImVec2(0, 0);
		it.radar_pos = vector(0, 0, 0);
		it.spotted_time = 0;
		it.radar_alive = false;
	}
}

void player_list::add_player(IClientEntity* entity, const int index)
{
	_players.emplace_back(player_count_t{static_cast<cs_player*>(entity), index });
}

void player_list::remove_player(IClientEntity* entity)
{
	for (auto i = 0U; i < _players.size(); ++i)
	{
		if (_players.at(i).entity == entity)
		{
			_players.erase(_players.begin() + i);
			break;
		}
	}
}

void player_list::add_object(IClientEntity* entity, const int index)
{
	_objects.emplace_back(object_count_t{ static_cast<cs_player*>(entity), index });
}

void player_list::remove_object(IClientEntity* entity)
{
	if (entity != nullptr)
	{
		for (auto i = 0U; i < _objects.size(); ++i)
		{
			if (_objects.at(i).entity == entity)
			{
				_objects.erase(_objects.begin() + i);
				break;
			}
		}
	}
	else
		_objects.clear();
}

player_data_t* player_list::get_player_data(const int index)
{
	return &_data[index];
}

std::vector<player_count_t> player_list::get_players() const
{
	return _players;
}

std::vector<object_count_t> player_list::get_objects() const
{
	return _objects;
}