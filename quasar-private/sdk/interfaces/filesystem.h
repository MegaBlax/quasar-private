#pragma once
#include "../platform.h"
#include "iappsystem.h"

class CUtlBuffer;
typedef void* FileHandle_t;
typedef void* FileNameHandle_t;

//-----------------------------------------------------------------------------
// File system allocation functions. Client must free on failure
//-----------------------------------------------------------------------------
typedef void* (*FSAllocFunc_t)(const char* pszFilename, unsigned nBytes);

enum FileSystemSeek_t
{
	FILESYSTEM_SEEK_HEAD = SEEK_SET,
	FILESYSTEM_SEEK_CURRENT = SEEK_CUR,
	FILESYSTEM_SEEK_TAIL = SEEK_END,
};

class IBaseFileSystem
{
public:
	virtual int				Read(void* pOutput, int size, FileHandle_t file) = 0;
	virtual int				Write(void const* pInput, int size, FileHandle_t file) = 0;

	// if pathID is NULL, all paths will be searched for the file
	virtual FileHandle_t	Open(const char* pFileName, const char* pOptions, const char* pathID = 0) = 0;
	virtual void			Close(FileHandle_t file) = 0;


	virtual void			Seek(FileHandle_t file, int pos, FileSystemSeek_t seekType) = 0;
	virtual unsigned int	Tell(FileHandle_t file) = 0;
	virtual unsigned int	Size(FileHandle_t file) = 0;
	virtual unsigned int	Size(const char* pFileName, const char* pPathID = 0) = 0;

	virtual void			Flush(FileHandle_t file) = 0;
	virtual bool			Precache(const char* pFileName, const char* pPathID = 0) = 0;

	virtual bool			FileExists(const char* pFileName, const char* pPathID = 0) = 0;
	virtual bool			IsFileWritable(char const* pFileName, const char* pPathID = 0) = 0;
	virtual bool			SetFileWritable(char const* pFileName, bool writable, const char* pPathID = 0) = 0;

	virtual long			GetFileTime(const char* pFileName, const char* pPathID = 0) = 0;

	//--------------------------------------------------------
	// Reads/writes files to utlbuffers. Use this for optimal read performance when doing open/read/close
	//--------------------------------------------------------
	virtual bool			ReadFile(const char* pFileName, const char* pPath, CUtlBuffer& buf, int nMaxBytes = 0, int nStartingByte = 0, FSAllocFunc_t pfnAlloc = NULL) = 0;
	virtual bool			WriteFile(const char* pFileName, const char* pPath, CUtlBuffer& buf) = 0;
	virtual bool			UnzipFile(const char* pFileName, const char* pPath, const char* pDestination) = 0;
};

class IFileSystem : public IAppSystem, public IBaseFileSystem
{
public:
	bool GetCurrentDirectory(char* pDirectory, int maxlen)
	{
		using fn = bool(__thiscall*)(PVOID, char*, int);
		return call_virtual<fn>(this, 40)(this, pDirectory, maxlen);
	}
	FileNameHandle_t FindOrAddFileName(char const* pFileName)
	{
		using fn = FileNameHandle_t(__thiscall*)(PVOID, char const*);
		return call_virtual<fn>(this, 41)(this, pFileName);
	}
	bool String(const FileNameHandle_t& handle, char* buf, int buflen)
	{
		using fn = bool(__thiscall*)(PVOID, const FileNameHandle_t&, char*, int);
		return call_virtual<fn>(this, 42)(this, handle, buf, buflen);
	}
	FileHandle_t OpenEx(const char* pFileName, const char* pOptions, unsigned flags = 0, const char* pathID = 0, char** ppszResolvedFilename = NULL)
	{
		using fn = FileHandle_t(__thiscall*)(PVOID, const char*, const char*, unsigned, const char*, char**);
		return call_virtual<fn>(this, 73)(this, pFileName, pOptions, flags, pathID, ppszResolvedFilename);
	}
	int ReadEx(PVOID pOutput, int sizeDest, int size, FileHandle_t file)
	{
		using fn = int(__thiscall*)(PVOID, PVOID,int,int,FileHandle_t);
		return call_virtual<fn>(this, 74)(this, pOutput, sizeDest, size, file);
	}
	FileNameHandle_t FindFileName(char const* pFileName)
	{
		using fn = FileNameHandle_t(__thiscall*)(PVOID, char const*);
		return call_virtual<fn>(this, 76)(this, pFileName);
	}
	bool GetOptimalIOConstraints(FileHandle_t hFile, unsigned* pOffsetAlign, unsigned* pSizeAlign, unsigned* pBufferAlign)
	{
		using fn = bool(__thiscall*)(PVOID, FileHandle_t, unsigned*, unsigned*, unsigned*);
		return call_virtual<fn>(this, 87)(this, hFile, pOffsetAlign, pSizeAlign, pBufferAlign);
	}
	inline unsigned GetOptimalReadSize(FileHandle_t hFile, unsigned nLogicalSize)
	{
		unsigned align;
		if (GetOptimalIOConstraints(hFile, &align, NULL, NULL))
			return AlignValue(nLogicalSize, align);
		
		return nLogicalSize;
	}
	PVOID AllocOptimalReadBuffer(FileHandle_t hFile, unsigned nSize = 0, unsigned nOffset = 0)
	{
		using fn = PVOID(__thiscall*)(PVOID, FileHandle_t, unsigned, unsigned);
		return call_virtual<fn>(this, 88)(this, hFile, nSize, nOffset);
	}
	void FreeOptimalReadBuffer(PVOID handle)
	{
		using fn = void(__thiscall*)(PVOID, PVOID);
		call_virtual<fn>(this, 89)(this, handle);
	}
	int	GetPathIndex( const FileNameHandle_t &handle ) //93
	{
		using fn = int(__thiscall*)(PVOID, const FileNameHandle_t&);
		return call_virtual<fn>(this, 93)(this, handle);
	}
};