#include "backtrack.h"
#include "chams.h"
#include "../utils/utils.h"

std::unique_ptr<backtrack> g_pBacktrack;

constexpr auto MAX_BACKTRACK_SIZE = 70;
constexpr auto INDEX_INVALID = -1;

backtrack::backtrack(): _cmd(nullptr), _local_player(nullptr), _best_target(0)
{
}

bool backtrack::is_tick_valid(const int tickcount)
{
    const auto netchannel = g_pInterfaces->engineclient()->GetNetChannelInfo();
    if (!netchannel)
        return false;
	
    const float latency = netchannel->GetLatency(FLOW_OUTGOING) + netchannel->GetLatency(FLOW_INCOMING) + lerp_time();
    const float correct = std::clamp(latency, 0.f, 1.f);
    const float delta = correct - (globals::vars::server_time - TICKS_TO_TIME(tickcount));
    return std::abs(delta) < 0.2f;
}

std::deque<record_ptr>& backtrack::record(const int index)
{
    return _records[index];
}

void backtrack::clear()
{
    for (size_t i = 0; i < _records.size(); ++i)
        _records[i].clear();
}

#pragma region from_hooks
void backtrack::fsn()
{
    cs_player* local_player = cs_player::get_local_player();
    if (!local_player || !local_player->is_alive())
        return;
	if (local_player->teamindex() < TEAM_TERRORIST) 
		return;
	
    for (auto& it : g_pPlayerList->get_players())
    {
        auto entity = it.entity;
        if (!entity || entity->IsDormant() || !entity->is_alive())
        {
            _records[it.index].clear();
            continue;
        }
        if (!entity->is_enemy(local_player))
            continue;

        auto record = record_t::get_new(entity);
        _records[it.index].push_front(record);

        while (_records[it.index].size() > 3 && int(_records[it.index].size()) > TIME_TO_TICKS(globals::backtrack::time / 1000.f))
            _records[it.index].pop_back();

        if (auto invalid = std::find_if(std::cbegin(_records[it.index]), std::cend(_records[it.index]),
            [](const record_ptr& rec)
            {
                return !g_pBacktrack->is_tick_valid(TIME_TO_TICKS(rec->simtime));
            }); invalid != std::cend(_records[it.index]))
            _records[it.index].erase(invalid, std::cend(_records[it.index]));
    }
}

void backtrack::create_move(CUserCmd* cmd)
{
    if (!globals::backtrack::active)
        return;

    _cmd = cmd;
    _local_player = cs_player::get_local_player();

    if (!_cmd || !_local_player || !_local_player->is_alive())
        return;

    int target_index = INDEX_INVALID;
    float max_fov = 60.f;

    cs_weapon* weapon = _local_player->weapon();
    const vector eye = _local_player->eye();

    if (!weapon || !weapon->is_shootable())
        return;

    for (auto& it : g_pPlayerList->get_players())
    {
        auto entity = it.entity;

        if (!entity || !entity->is_alive())
            continue;
        if (!entity->is_enemy(_local_player))
            continue;
        if (entity->IsDormant())
            continue;

        const auto fov = g_pUtils->GetFoV(cmd->viewangles, eye, entity->eye());
        if (max_fov > fov)
        {
            max_fov = fov;
            target_index = it.index;
        }
    }
    if (target_index == INDEX_INVALID)
        return;

    _best_target = target_index;
    _best_record = nullptr;

    if (!get_best_tick(target_index, max_fov, &_best_record))
        return;
    if (!(_cmd->buttons & IN_ATTACK) || !_best_record)
        return;
    if (weapon->can_fire(_local_player->nextattack()))
        cmd->tick_count = TIME_TO_TICKS(_best_record->simtime + lerp_time());
}
#pragma endregion

float backtrack::lerp_time()
{
    static const auto& cl_interp = g_pInterfaces->cvar()->FindVar(xorstr_("cl_interp"));
    static const auto& cl_interp_ratio = g_pInterfaces->cvar()->FindVar(xorstr_("cl_interp_ratio"));
    static const auto& cl_updaterate = g_pInterfaces->cvar()->FindVar(xorstr_("cl_updaterate"));
    return max(cl_interp->GetFloat(), cl_interp_ratio->GetFloat() / cl_updaterate->GetFloat());
}

#pragma region get_ticks

bool backtrack::get_best_tick(const int index, const float best_fov, record_ptr* record)
{
    const auto eye = _local_player->eye();
    auto& records = _records.at(index);
    float max_fov = best_fov;
    bool tick_found = false;
    for(auto& it : records)
    {
        it->valid = is_tick_valid(TIME_TO_TICKS(it->simtime));
    	if (!it->valid)
            continue;
    	
        const float fov = g_pUtils->GetFoV(_cmd->viewangles, eye, it->hitbox);
    	if (fov <= max_fov)
    	{
            max_fov = fov;
            *record = it;
            tick_found = true;
    	}
    }
    return tick_found;
}

bool backtrack::get_last_tick(const int index, record_ptr* record)
{
    auto& records = _records.at(index);
    std::for_each(records.rbegin(), records.rend(), [&](record_ptr& it)
    {
            it->valid = is_tick_valid(TIME_TO_TICKS(it->simtime));
            if (it->valid)
            {
                *record = it;
                return true;
            }
            return false;
    });
    return false;
}

#pragma endregion
