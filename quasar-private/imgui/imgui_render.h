#pragma once
#include <imgui.h>
#include "../stdafx.h"
#include "../sdk/vector.h"

enum text_flags_t
{
	NO_FLAGS = 0,
	CENTERED_X = 1,
	CENTERED_Y = 2,
	OUTLINED = 4,
	DROPSHADOW = 8,
};

enum ImCustomFont_
{
	ImCustomFont_Default = 0,
	ImCustomFont_Tabs,
	ImCustomFont_Hotkey,
	ImCustomFont_COUNT
};

enum ImCustomColor_
{
	ImCustomColor_WindowBg_TL = 0,
	ImCustomColor_WindowBg_TR,
	ImCustomColor_WindowBg_BR,
	ImCustomColor_WindowBg_BL,
	ImCustomColor_Line,
	ImCustomColor_Line_Light,
	ImCustomColor_Line_Dark,
	ImCustomColor_Line_Blue,
	ImCustomColor_Text_Shadow,
	ImCustomColor_Hotkey,
	ImCustomColor_Hotkey_Border,
	ImCustomColor_Hotkey_Active,
	ImCustomColor_COUNT
};

enum ImCustomTexture_
{
	ImCustomTexture_Logo = 0,
	ImCustomTexture_Checkmark,
	ImCustomTexture_Empty,
	ImCustomTexture_Legit = ImCustomTexture_Empty + 1,
	ImCustomTexture_Visuals,
	ImCustomTexture_Misc,
	ImCustomTexture_Config,
	ImCustomTexture_COUNT
};

class imgui_render
{
public:
	imgui_render();
	bool init();
	void begin_scene() const;
	void end_scene();
	ImDrawList* get_drawlist();
	ImFont* get_font(ImCustomFont_ id);
	ImVec4 get_color(ImCustomColor_ id);
	ImTextureID get_texture(ImCustomTexture_ id);

	void add_line(const ImVec2& a, const ImVec2& b, const ImU32& col, float thickness = 1.f) const;
	void add_rect(const ImVec2& a, const ImVec2& b, const ImU32& col, float thickness = 1.f, float rounding = 0.f,
	                     int rounding_corners_flags = ImDrawCornerFlags_All) const;
	void add_rect_filled(const ImVec2& a, const ImVec2& b, const ImU32& col, float rounding = 0.f,
	                            int rounding_corners_flags = ImDrawCornerFlags_All) const;
	void add_circle(const ImVec2& center, float radius, const ImU32& col, int segments = 12, float thickness = 1.f) const;
	void add_circle_filled(const ImVec2& center, const float radius, const ImU32& col, int segments = 12) const;
	void add_corner_box(const ImVec2& a, const ImVec2& b, const ImU32& col, float size, float thickness = 1.f) const;
	void add_circle3d(const vector& centre, float points, float radius, float thickness, const ImU32& col) const;
	void add_text(const ImFont* font, const ImU32& color, const ImU32& shadow_color, const ImVec2& pos, int flags, const char* format, ...) const;
private:
	std::mutex _mutex;
	ImDrawList* _drawlist;
	ImDrawList* _drawlist2;
	ImDrawList* _drawlist3;
	ImFont* _fonts[ImCustomFont_COUNT];
	ImVec4 _colors[ImCustomColor_COUNT];
	IDirect3DTexture9* _textures[ImCustomTexture_COUNT];
};

extern std::unique_ptr<imgui_render> g_pImRender;