#pragma once
#include "../../stdafx.h"
#include "studio.h"
#include "imaterial.h"
struct model_t;
struct studiohdr_t;

class IVModelInfo
{
public:
	const model_t* GetModel(int modelindex)
	{
		typedef const model_t* (__thiscall * fn)(void*, int);
		return call_virtual<fn>(this, 1)(this, modelindex);
	}

	const char* GetModelName(const model_t* model)
	{
		typedef const char* (__thiscall * fn)(void*, const model_t*);
		return call_virtual<fn>(this, 3)(this, model);
	}
	int	GetModelMaterialCount(const model_t* model)
	{
		using fn = int(__thiscall*)(PVOID, const model_t*);
		return call_virtual<fn>(this, 18)(this, model);
	}
	int GetModelMaterials(const model_t* model, int count, IMaterial** ppMaterial)
	{
		using fn = int(__thiscall*)(PVOID, const model_t*, int, IMaterial**);
		return call_virtual<fn>(this, 19)(this, model, count, ppMaterial);
	}
	studiohdr_t* GetStudiomodel(const model_t* mod)
	{
		typedef studiohdr_t* (__thiscall * fn)(void*, const model_t*);
		return call_virtual<fn>(this, 32)(this, mod);
	}
};