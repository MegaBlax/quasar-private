#include "hooks.h"
#include "../utils/utils.h"

void hooks::overrideview_h(IClientMode* thisptr, PVOID, CViewSetup* pSetup)
{
	if (!_o_overrideview)
		return;
	
	_o_overrideview(thisptr, pSetup);
	if (!g_pInterfaces->engineclient()->IsActiveApp())
		return;
	
	if (g_pInterfaces->clientstate()->IsInGame()) {
		globals::vars::fov = pSetup->fov;
	}
}