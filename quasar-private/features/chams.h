#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"
#include "../hooks/hooks.h"

enum class MATERIAL : int
{
	TEXTURED = 0,
	TEXTURED_I,
	FLAT,
	FLAT_I,
	MAX
};

class chams
{
public:
	chams();
	bool init(const drawmodelexecute_t& original);
	void create_new_mat();
	void run(IVModelRender* thisptr, IMatRenderContext* ctx, DrawModelState_t& state, const ModelRenderInfo_t& pInfo, matrix3x4_t* pCustomBoneToWorld);
private:
	void draw_backtrack(IVModelRender* thisptr, IMatRenderContext* ctx, DrawModelState_t& state, const ModelRenderInfo_t& pInfo, int teamid);

private:
	IMaterial* _material[int(MATERIAL::MAX)];
	drawmodelexecute_t _original;
};

extern std::unique_ptr<chams> g_pChams;