#pragma once
#include <imgui.h>
#include "../stdafx.h"
#include "../sdk/vector.h"
#include "../sdk/mathlib.h"
#include "../sdk/interfaces/usercmd.h"

using RandomIntEx = int(__cdecl*)(int, int);
using RandomFloatEx = float(__cdecl*)(float, float);
using RandomSeedEx = void(__cdecl*)(int);
using FindHudElementEx = DWORD(__thiscall*)(DWORD*, const char*);

template <int X> struct EnsureCompileTime {
	enum : int {
		Value = X
	};
};

//Use Compile-Time as seed
#define Seed ((__TIME__[7] - '0') * 1  + (__TIME__[6] - '0') * 10  + \
              (__TIME__[4] - '0') * 60   + (__TIME__[3] - '0') * 600 + \
              (__TIME__[1] - '0') * 3600 + (__TIME__[0] - '0') * 36000)

constexpr int LinearCongruentGenerator(int Rounds) {
	return 1013904223 + 1664525 * ((Rounds > 0) ? LinearCongruentGenerator(Rounds - 1) : Seed & 0xFFFFFFFF);
}
#define Random() EnsureCompileTime<LinearCongruentGenerator(10)>::Value //10 Rounds
#define RandomNumber(Min, Max) (Min + (Random() % (Max - Min + 1)))

constexpr int XORKEY = RandomNumber(0, 0xFF);

class utils
{
public:
	void SinCos(float radians, float* sine, float* cosine);
	std::wstring GetFolderPath();
	std::wstring GetLogsPath();
	std::wstring GetConfigsPath();
	std::wstring GetIconsPath();
	void CreateAllDirectories();
	std::wstring UTF8ToWstring(const std::string& str);
	std::string WstringToUTF8(const std::wstring& str);
	std::uint8_t* PatternScan(const MODULEINFO& mod, const char* signature);
	void KeyIsDown(int key, int type, bool* v);
	bool WorldToScreen(const vector& origin, ImVec2& screen);
	ImVec2 CenterOnScreen();
	ImVec2 GetScreenSize();
	qangle GetViewAngles();
	qangle CalculateAngle(const vector& src, const vector& dst);
	bool Clamp(qangle& angles);
	float GetFoV(const qangle& viewangle, const qangle& eye, const qangle& spot);
	float GetRealDistanceFOV(const qangle& viewangle, const qangle& eye, const qangle& spot);
	void FixMovement(CUserCmd* cmd, const qangle& angle);
	int RandomInt(int min, int max);
	float RandomFloat(float min, float max);
	void RandomSeed(int vl);
	DWORD FindHudElement(const char* name);
	const char* GetWeaponName(int index);
	char const* EncryptString(char const* szAddress, uint64 ullCrypt = XORKEY);
	char const* DecryptString(char const* szAddress, uint64 ullCrypt = XORKEY);
};

extern std::unique_ptr<utils> g_pUtils;
