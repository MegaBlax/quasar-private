#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces/memalloc.h"
#include "custom_winapi.h"

inline bool valid_code_ptr(uintptr_t addr)
{
	if (!addr)
		return false;
	MEMORY_BASIC_INFORMATION mbi;
	if (!g_pWinApi->dwVirtualQuery(reinterpret_cast<const void*>(addr), &mbi, sizeof(MEMORY_BASIC_INFORMATION)))
		return false;
	if (!(mbi.Protect & PAGE_EXECUTE_READWRITE || mbi.Protect & PAGE_EXECUTE_READ))
		return false;
	return true;
}

class virtual_hook
{
	[[nodiscard]] static size_t vtable_length(uintp* base)
	{
		size_t len = 0;
		while (valid_code_ptr(base[len]))
			++len;
		return len;
	}
public:
	virtual_hook()
	{
		_setup.clear();
		_original.clear();
	}
	~virtual_hook()
	{
		for(auto& it : _setup)
		{
			DWORD lpflOldProtect;
			VirtualProtect((PVOID)it.from, 4, 0x40, &lpflOldProtect);
			it.from = it.old;
			VirtualProtect((PVOID)it.from, 4, lpflOldProtect, &lpflOldProtect);
		}
	}
	template<typename T>
	void setup(fnv::hash hash, PVOID base, int index, T to)
	{
		uintp* vt_base = *(uintp**)base;
		const size_t len = vtable_length(vt_base) * 4U;
		uintp* vt_old = (uintp*)_memalloc->Alloc(len);
		memcpy(vt_old, vt_base, len);

		_setup.push_back(hook_t{ hash, vt_base, vt_old, (uintp)to, index });
		_original[hash] = vt_base[index];
	}

	void hook()
	{
		for(auto& it : _setup)
		{
			DWORD lpflOldProtect;
			VirtualProtect((PVOID)it.from, 4, 0x40, &lpflOldProtect);
			it.from[it.index] = it.to;
			VirtualProtect((PVOID)it.from, 4, lpflOldProtect, &lpflOldProtect);
		}
	}

	void unhook()
	{
		for (auto& it : _setup)
		{
			DWORD lpflOldProtect;
			VirtualProtect((PVOID)it.from, 4, 0x40, &lpflOldProtect);
			it.from[it.index] = _original[it.hash];
			VirtualProtect((PVOID)it.from, 4, lpflOldProtect, &lpflOldProtect);
		}
	}

	template<typename T>
	T original(fnv::hash hash)
	{
		return (T)_original[hash];
	}

private:
	struct hook_t
	{
		fnv::hash hash;
		uintp* from;
		uintp* old;
		uintp to;
		int index;
	};
	std::vector<hook_t> _setup;
	std::unordered_map<fnv::hash, uintp> _original;
};