#include "hooks.h"
#include "imgui_impl_dx9.h"
#include "imgui_impl_win32.h"

#include "../sdk/mem.h"
#include "../sdk/offsets.h"
#include "../sdk/interfaces.h"
#include "../features/aimbot.h"
#include "../features/backtrack.h"
#include "../features/chams.h"
#include "../features/engineprediction.h"
#include "../features/entitylistener.h"
#include "../features/events.h"
#include "../features/misc.h"
#include "../features/norecoil.h"
#include "../features/radar.h"
#include "../features/sounds.h"
#include "../features/triggerbot.h"
#include "../features/visuals.h"

std::unique_ptr<hooks> g_pHooks;

hooks::hooks()
{
	_o_wndproc = nullptr;
	_o_present = nullptr;
	_o_reset = nullptr;
	_o_createmove = nullptr;
	_o_painttraverse = nullptr;
	_o_framestagenotify = nullptr;
	_o_levelinitpostentity = nullptr;
	_o_lockcursor = nullptr;
	_o_runcommand = nullptr;
	_o_drawmodelexecute = nullptr;
	_o_listleavesinbox = nullptr;

	_hooks = std::make_unique<virtual_hook>();

	g_pAimbot = std::make_unique<aimbot>(); 
	g_pBacktrack = std::make_unique<backtrack>();
	g_pChams = std::make_unique<chams>();
	g_pPrediction = std::make_unique<eprediction>();
	g_pMisc = std::make_unique<misc>();
	g_pNorecoil = std::make_unique<norecoil>();
	g_pRadar = std::make_unique<radar>();
	g_pSounds = std::make_unique<sounds>();
	g_pTriggerbot = std::make_unique<triggerbot>();
	g_pVisuals = std::make_unique<visuals>();
}

bool hooks::init() const
{
	_o_wndproc = reinterpret_cast<WNDPROC>(SetWindowLongPtr(globals::vars::window, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(wndproc_h)));

	_o_present = **reinterpret_cast<present_t**>(g_pOffsets->present());
	**reinterpret_cast<present_t**>(g_pOffsets->present()) = present_h;
	_o_reset = **reinterpret_cast<reset_t**>(g_pOffsets->reset());
	**reinterpret_cast<reset_t**>(g_pOffsets->reset()) = reset_h;

	_hooks->setup(FNV("createmove"), g_pInterfaces->baseclientdll(), CREATEMOVE, createmove_proxy_h);
	_hooks->setup(FNV("painttraverse"), g_pInterfaces->panel(), PAINTTRAVERSE, painttraverse_h);
	_hooks->setup(FNV("framestagenotify"), g_pInterfaces->baseclientdll(), FRAMESTAGENOTIFY, framestagenotify_h);
	_hooks->setup(FNV("levelinitpostentity"), g_pInterfaces->baseclientdll(), LEVELINITPOSTENTITY, levelinitpostentity_h);
	_hooks->setup(FNV("overrideview"), g_pInterfaces->clientmode(), OVERRIDEVIEW, overrideview_h);
	_hooks->setup(FNV("lockcursor"), g_pInterfaces->surface(), LOCKCURSOR, lockcursor_h);
	_hooks->setup(FNV("runcommand"), g_pInterfaces->prediction(), RUNCOMMAND, runcommand_h);
	_hooks->setup(FNV("drawmodelexecute"), g_pInterfaces->modelrender(), DRAWMODELEXECUTE, drawmodelexecute_h);
	_hooks->setup(FNV("listleavesinbox"), g_pInterfaces->spatialquery(), LISTLEAVESINBOX, listleavesinbox_h);
	
	//_hooks->setup(FNV("renderview"), g_pInterfaces->viewrender(), RENDERVIEW, renderview_h);
	//_hooks->setup(FNV("render2deffectsprehud"), g_pInterfaces->viewrender(), RENDER2DEFFECTSPREHUD, render2deffectsprehud_h);
	
	_hooks->hook();

	_o_createmove = _hooks->original<createmove_t>(FNV("createmove"));
	_o_painttraverse = _hooks->original<painttraverse_t>(FNV("painttraverse"));
	_o_framestagenotify = _hooks->original<framestagenotify_t>(FNV("framestagenotify"));
	_o_levelinitpostentity = _hooks->original<levelinitpostentity_t>(FNV("levelinitpostentity"));
	_o_overrideview = _hooks->original<overrideview_t>(FNV("overrideview"));
	_o_lockcursor = _hooks->original<lockcursor_t>(FNV("lockcursor"));
	_o_runcommand = _hooks->original<runcommand_t>(FNV("runcommand"));
	_o_drawmodelexecute = _hooks->original<drawmodelexecute_t>(FNV("drawmodelexecute"));
	_o_listleavesinbox = _hooks->original<listleavesinbox_t>(FNV("listleavesinbox"));
	
	//_o_renderview = _hooks->original<renderview_t>(FNV("renderview"));
	//_o_render2deffectsprehud = _hooks->original<render2deffectsprehud>(FNV("render2deffectsprehud"));
	
	const std::vector<const char*> events = { xorstr_("round_start"), xorstr_("round_end"), xorstr_("player_death"), xorstr_("player_hurt") };
	g_pEntitymanager = new entity_listener();
	g_pEventmanager = new event_listener(events);
	g_pMatcheventmanager = new match_events();
	
	//keybind = std::make_unique<CKeyBindings>();

	keytranslation = std::make_unique<key_translation>();
	
	if (!g_pRadar->init())
	{
		log_error(xorstr_(L"Failed to initialize radar"));
		return false;
	}

	g_pChams->init(_o_drawmodelexecute);
#ifdef _DEBUG
	g_pInterfaces->cvar()->ConsoleColorPrintf(Color::Green(), xorstr_("Hooked ok!\n"));
#endif
	return true;
}

bool hooks::reset() const
{
	_hooks->unhook();

	g_pEntitymanager->remove();
	g_pEventmanager->remove();
	g_pMatcheventmanager->remove();

	g_pEntitymanager = nullptr;
	g_pEventmanager = nullptr;
	g_pMatcheventmanager = nullptr;
	
	SetWindowLongPtr(globals::vars::window, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(_o_wndproc));

	**reinterpret_cast<void***>(g_pOffsets->present()) = _o_present;
	**reinterpret_cast<void***>(g_pOffsets->reset()) = _o_reset;

	ImGui_ImplDX9_Shutdown();
	ImGui_ImplWin32_Shutdown();
	ImGui::DestroyContext();

	return true;
}
