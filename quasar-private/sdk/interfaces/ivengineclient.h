#pragma once
#include "inetchannelinfo.h"
#include "bsptreedata.h"

typedef struct player_info_s
{
	__int64         unknown;            //0x0000 
	union
	{
		__int64       steamID64;          //0x0008 - SteamID64
		struct
		{
			__int32     xuid_low;
			__int32     xuid_high;
		};
	};
	char            szName[128];        //0x0010 - Player Name
	int             userId;             //0x0090 - Unique Server Identifier
	char            szSteamID[20];      //0x0094 - STEAM_X:Y:Z
	char            pad_0x00A8[0x10];   //0x00A8
	unsigned long   iSteamID;           //0x00B8 - SteamID 
	char            szFriendsName[128];
	bool            fakeplayer;
	bool            ishltv;
	unsigned int    customfiles[4];
	unsigned char   filesdownloaded;
} player_info_t;

class IVEngineClient
{
public:
	void GetScreenSize(int& width, int& height)
	{
		using fn = void(__thiscall*)(PVOID, int&, int&);
		call_virtual<fn>(this, 5)(this, width, height);
	}
	bool GetPlayerInfo(int ent_num, player_info_t* pinfo)
	{
		using fn = bool(__thiscall*)(PVOID, int, player_info_t*);
		return call_virtual<fn>(this, 8)(this, ent_num, pinfo);
	}
	int GetPlayerForUserID(int userID)
	{
		using fn = int(__thiscall*)(PVOID, int);
		return call_virtual<fn>(this, 9)(this, userID);
	}
	bool Con_IsVisible()
	{
		using fn = bool(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 11)(this);
	}
	int GetLocalPlayer()
	{
		using fn = int(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 12)(this);
	}
	void GetViewAngles(qangle& va)
	{
		using fn = void(__thiscall*)(PVOID, qangle&);
		call_virtual<fn>(this, 18)(this, va);
	}
	void SetViewAngles(qangle& va)
	{
		using fn = void(__thiscall*)(PVOID, qangle&);
		call_virtual<fn>(this, 19)(this, va);
	}
	int GetMaxClients()
	{
		using fn = int(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 20)(this);
	}
	/*bool IsInGame()
	{
		typedef bool(__thiscall * fn)(void*);
		return call_virtual<fn>(this, 26)(this);
	}
	bool IsConnected()
	{
		typedef bool(__thiscall * fn)(void*);
		return call_virtual<fn>(this, 27)(this);
	}*/
	//virtual const char			*GetGameDirectory( void ) = 0; // 36
	ISpatialQuery* GetBSPTreeQuery()
	{
		using fn = ISpatialQuery * (__thiscall*)(PVOID);
		return call_virtual<fn>(this, 43)(this);
	}
	char const* GetLevelNameShort()
	{
		using fn = char const*(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 53)(this);
	}
	INetChannelInfo* GetNetChannelInfo()
	{
		using fn = INetChannelInfo*(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 78)(this);
	}
	bool IsPlayingDemo()
	{
		using fn = bool(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 82)(this);
	}
	bool IsTakingScreenshot()
	{
		using fn = bool(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 92)(this);
	}
	unsigned int GetEngineBuildNumber()
	{
		using fn = uintp(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 104)(this);
	}
	void ExecuteClientCmd(const char* szCmdString)
	{
		using fn = void(__thiscall*)(PVOID, const char*);
		call_virtual<fn>(this, 108)(this, szCmdString);
	}
	bool IsActiveApp()
	{
		using fn = bool(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 196)(this);
	}
	const char* GetModDirectory()
	{
		using fn = const char*(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 209)(this);
	}
	int GetClientVersion()
	{
		using fn = int(__thiscall*)(PVOID);
		return call_virtual<fn>(this, 219)(this);
	}
};