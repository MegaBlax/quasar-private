#pragma once
#include "../vector.h"

#define MAXSTUDIOBONES		128		// total bones actually used

#define BONE_USED_MASK				0x0007FF00
#define BONE_USED_BY_ANYTHING		0x0007FF00
#define BONE_USED_BY_HITBOX			0x00000100	// bone (or child) is used by a hit box
#define BONE_USED_BY_ATTACHMENT		0x00000200	// bone (or child) is used by an attachment point
#define BONE_USED_BY_VERTEX_MASK	0x0003FC00
#define BONE_USED_BY_VERTEX_LOD0	0x00000400	// bone (or child) is used by the toplevel model via skinned vertex
#define BONE_USED_BY_VERTEX_LOD1	0x00000800	
#define BONE_USED_BY_VERTEX_LOD2	0x00001000  
#define BONE_USED_BY_VERTEX_LOD3	0x00002000
#define BONE_USED_BY_VERTEX_LOD4	0x00004000
#define BONE_USED_BY_VERTEX_LOD5	0x00008000
#define BONE_USED_BY_VERTEX_LOD6	0x00010000
#define BONE_USED_BY_VERTEX_LOD7	0x00020000
#define BONE_USED_BY_BONE_MERGE		0x00040000	// bone is available for bone merge to occur against it

struct model_t
{
	void* fnHandle; //0x0000
	char name[260]; //0x0004
	int32_t nLoadFlags; //0x0108
	int32_t nServerCount; //0x010C
	int32_t type; //0x0110
	int32_t flags; //0x0114
	vector mins; //0x0118
	vector maxs; //0x0124
	float radius; //0x0130
	char pad_0134[28]; //0x0134
};

struct mstudiobbox_t
{
	int					bone;
	int					group;				// intersection group
	vector				bbmin;				// bounding box, or the ends of the capsule if flCapsuleRadius > 0 
	vector				bbmax;
	int					szhitboxnameindex;	// offset to the name of the hitbox.
	qangle				angOffsetOrientation;
	float				flCapsuleRadius;
	__int32				unused[4];

	const char* pszHitboxName() const
	{
		if (szhitboxnameindex == 0)
			return "";

		return ((const char*)this) + szhitboxnameindex;
	}

	mstudiobbox_t(): bone(0), group(0), szhitboxnameindex(0), flCapsuleRadius(0), unused{}
	{
	}
};

struct mstudiohitboxset_t {
	int sznameindex;

	char* pszName() const
	{
		return (char*)this + sznameindex;
	}

	int numhitboxes;
	int hitboxindex;

	mstudiobbox_t* pHitbox(int i) const
	{
		if (i > numhitboxes) return nullptr;
		return reinterpret_cast<mstudiobbox_t*>((uint8_t*)this + hitboxindex) + i;
	}
};

struct studiohdr_t
{
	int id = 0;
	int version = 0;
	int checksum = 0;
	char name[64] = {};
	int length = 0;
	vector eyeposition = {};
	vector illumposition = {};
	vector hull_min = {};
	vector hull_max = {};
	vector view_bbmin = {};
	vector view_bbmax = {};
	int flags = 0;
	int numbones = 0;
	int boneindex = 0;
	int numbonecontrollers = 0;
	int bonecontrollerindex = 0;
	int numhitboxsets = 0;
	int hitboxsetindex = 0;
	int	numlocalanim;			// animations/poses
	int	localanimindex;		// animation descriptions
	int	numlocalseq;				// sequences
	int	localseqindex;
	mutable int	activitylistversion;	// initialization flag - have the sequences been indexed?
	mutable int	eventsindexed;
	// raw textures
	int	numtextures;
	int	textureindex;

	// Look up hitbox set by index
	mstudiohitboxset_t* pHitboxSet(int i) const
	{
		Assert(i >= 0 && i < numhitboxsets);
		return reinterpret_cast<mstudiohitboxset_t*>((byte*)this + hitboxsetindex) + i;
	}
	// Calls through to hitbox to determine size of specified set
	mstudiobbox_t* pHitbox(const int i, const int set = 0) const
	{
		const mstudiohitboxset_t* s = pHitboxSet(set);

		if (!s)
			return nullptr;

		return s->pHitbox(i);
	}
	// Calls through to set to get hitbox count for set
	int	iHitboxCount(int set) const
	{
		mstudiohitboxset_t const* s = pHitboxSet(set);
		if (!s)
			return 0;

		return s->numhitboxes;
	};
};