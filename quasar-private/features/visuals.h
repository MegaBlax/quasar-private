#pragma once
#include "../stdafx.h"
#include "../sdk/cs_player.h"

#include "../imgui/imgui.h"

struct ImVec2;

class visuals
{
public:
	visuals();
	void run();

private:
	void box(const ImVec2& min, const ImVec2& max, bool is_visible) const;
	void healthbar(const ImVec2& min, const ImVec2& max, const float& health) const;
	void name(const ImVec2& min, const ImVec2& max, const char* name) const;
	void weapon(const ImVec2& min, const ImVec2& max, cs_player* player) const;
	void misc() const;

	cs_player* _local_player;
	float _alpha;
	int _teamid;
	int _itemindex;
};

extern std::unique_ptr<visuals> g_pVisuals;