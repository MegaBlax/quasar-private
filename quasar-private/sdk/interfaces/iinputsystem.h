#pragma once
#include "iappsystem.h"
#include "../buttoncode.h"

enum InputEventType_t
{
	IE_ButtonPressed = 0,	// m_nData contains a ButtonCode_t
	IE_ButtonReleased,		// m_nData contains a ButtonCode_t
	IE_ButtonDoubleClicked,	// m_nData contains a ButtonCode_t
	IE_AnalogValueChanged,	// m_nData contains an AnalogCode_t, m_nData2 contains the value

	IE_FirstSystemEvent = 100,
	IE_Quit = IE_FirstSystemEvent,
	IE_ControllerInserted,	// m_nData contains the controller ID
	IE_ControllerUnplugged,	// m_nData contains the controller ID

	IE_FirstVguiEvent = 1000,	// Assign ranges for other systems that post user events here
	IE_FirstAppEvent = 2000,
};

struct InputEvent_t // 20, 21
{
	int m_nType;				// Type of the event (see InputEventType_t)
	int m_nTick;				// Tick on which the event occurred
	int m_nData;				// Generic 32-bit data, what it contains depends on the event
	int m_nData2;				// Generic 32-bit data, what it contains depends on the event
	int m_nData3;				// Generic 32-bit data, what it contains depends on the event
};

class IInputSystem : public IAppSystem
{
public:
	// Is a button down? "Buttons" are binary-state input devices (mouse buttons, keyboard keys)
	bool IsButtonDown(ButtonCode_t code)
	{
		using fn = bool(__thiscall*)(PVOID, ButtonCode_t);
		return call_virtual<fn>(this, 15)(this, code);
	}

	// Returns the tick at which the button was pressed and released
	int GetButtonPressedTick(ButtonCode_t code)
	{
		using fn = int(__thiscall*)(PVOID, ButtonCode_t);
		return call_virtual<fn>(this, 16)(this, code);
	}
	
	int GetButtonReleasedTick(ButtonCode_t code)
	{
		using fn = int(__thiscall*)(PVOID, ButtonCode_t);
		return call_virtual<fn>(this, 17)(this, code);
	}
	
	const char* ButtonCodeToString(ButtonCode_t code)
	{
		using fn = const char*(__thiscall*)(PVOID, ButtonCode_t);
		return call_virtual<fn>(this, 40)(this, code);
	}
	
	ButtonCode_t StringToButtonCode(const char* pString)
	{
		using fn = ButtonCode_t(__thiscall*)(PVOID, const char*);
		return call_virtual<fn>(this, 42)(this, pString);
	}
	
	ButtonCode_t VirtualKeyToButtonCode(int nVirtualKey)
	{
		using fn = ButtonCode_t(__thiscall*)(PVOID, int);
		return call_virtual<fn>(this, 45)(this, nVirtualKey);
	}
	
	int ButtonCodeToVirtualKey(ButtonCode_t code)
	{
		using fn = int(__thiscall*)(PVOID, ButtonCode_t);
		return call_virtual<fn>(this, 46)(this, code);
	}

	bool IsButtonReleased(ButtonCode_t code)
	{
		released_tick[code] = GetButtonReleasedTick(code);
		if (released_tick[code] > old_released_tick[code])
		{
			old_released_tick[code] = released_tick[code];
			return true;
		}

		return false;
	}

private:
	int released_tick[BUTTON_CODE_COUNT] = {};
	int old_released_tick[BUTTON_CODE_COUNT] = {};
};
