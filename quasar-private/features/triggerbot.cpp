#include "triggerbot.h"
#include "backtrack.h"

std::unique_ptr<triggerbot> g_pTriggerbot;

void triggerbot::run(CUserCmd* cmd, byte*& packet)
{
	if (!globals::triggerbot::active)
		return;
	
	_local_player = cs_player::get_local_player();
	_cmd = cmd;

	if (!_local_player || !_cmd)
		return;
	if (!_local_player->is_alive())
		return;
	cs_weapon* weapon = _local_player->weapon();
	if (!weapon || !weapon->is_shootable() || weapon->is_reloading())
		return;
	if (globals::triggerbot::flash_check && _local_player->is_flashed())
		return;
	if (globals::triggerbot::zoom_check && weapon->is_sniper() && !_local_player->is_scoped())
		return;
	if (globals::hotkey::triggerbot) {
		if (globals::triggerbot::key_mode == 1) {
			if (!g_pInterfaces->inputsystem()->IsButtonDown((ButtonCode_t)globals::hotkey::triggerbot))
				return;
		}
		else if (globals::triggerbot::key_mode == 2) {
			static bool toggle = true;
			if (g_pInterfaces->inputsystem()->IsButtonReleased((ButtonCode_t)globals::hotkey::triggerbot))
				toggle = !toggle;
			if (!toggle)
				return;
		}
	}

	if (!find_target(_entity_player))
		return;
	if (!_entity_player || !_entity_player->is_alive())
		return;
	
	if (weapon->can_fire(_local_player->nextattack()))
	{
		_cmd->buttons |= IN_ATTACK;
	}
	else
	{
		_cmd->buttons &= ~IN_ATTACK;
	}
}

bool triggerbot::find_target(cs_player*& data)
{
	const auto eye = _local_player->eye();
	const auto viewangles = g_pUtils->GetViewAngles() + _local_player->aimpunchangle() * 2.f;
	vector rend;
	AngleVectors(viewangles, &rend);
	rend = eye + rend * 8192.f;
	CTraceFilter filter;
	filter.pSkip = _local_player;
	trace_t tr;
	Ray_t ray;
	ray.Init(eye, rend);
	g_pInterfaces->enginetrace()->TraceRay(ray, MASK_SHOT, &filter, &tr);
	if (!tr.DidHit() || !tr.HitgroupHit())
		return false;
	if (tr.m_pEnt->GetClientClass()->m_ClassID != CCSPlayer)
		return false;

	data = cs_player::get_entity_player(tr.m_pEnt->EntIndex());
	if (!data || !data->is_alive())
		return false;
	if (!data->is_enemy(_local_player))
		return false;
	if (globals::triggerbot::smoke_check && data->in_smoke())
		return false;
	
	return true;
}
