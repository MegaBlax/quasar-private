#pragma once
#include "recv.h"

#include "iclientrenderable.h"
#include "iclientnetworkable.h"

class ClientClass;
// The serial number that gets passed in is used for ehandles.
typedef IClientNetworkable* (*CreateClientClassFn)(int entnum, int serialNum);
typedef IClientNetworkable* (*CreateEventFn)();

class ClientClass
{
public:

	CreateClientClassFn m_pCreateFn;
	CreateEventFn m_pCreateEventFn;
	const char *m_pNetworkName;
	RecvTable *m_pRecvTable;
	ClientClass *m_pNext;
	int m_ClassID;
};