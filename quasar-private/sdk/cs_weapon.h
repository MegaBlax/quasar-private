#pragma once
#include "../stdafx.h"

class cs_weapon
{
public:
	short itemindex();
	int ammo();
	int type();
	float nextprimaryattack();
	float lastshottime();
	bool is_reloading();
	bool can_fire(float nextattack);
	float get_inaccuracy();
	float get_spread();
	void update_accuracy_penalty();
	bool is_shootable();
	bool is_cz75();
	bool is_revolver();
	bool is_pistol();
	bool is_shotgun();
	bool is_heavy();
	bool is_smg();
	bool is_rifle();
	bool is_sniper();
	bool is_autosniper();
	const char* name();
};