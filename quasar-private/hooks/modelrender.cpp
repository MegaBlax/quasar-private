#include "hooks.h"
#include "../features/chams.h"

void hooks::drawmodelexecute_h(IVModelRender* thisptr, PVOID, IMatRenderContext* ctx, DrawModelState_t& state, const ModelRenderInfo_t& pInfo, matrix3x4_t* pCustomBoneToWorld)
{
	if (!_o_drawmodelexecute)
		return;
	
	if (!g_pInterfaces->engineclient()->IsActiveApp())
		return _o_drawmodelexecute(thisptr, ctx, state, pInfo, pCustomBoneToWorld);

	g_pChams->run(thisptr, ctx, state, pInfo, pCustomBoneToWorld);
	_o_drawmodelexecute(thisptr, ctx, state, pInfo, pCustomBoneToWorld);
	thisptr->ForcedMaterialOverride(nullptr);
}