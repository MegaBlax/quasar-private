#pragma once
#include "../stdafx.h"
#include "../offsets.h"
#include "../interfaces/filesystem.h"

typedef int HKeySymbol;
constexpr HKeySymbol INVALID_KEY_SYMBOL = -1;
class KeyValuesSystem
{
public:
	virtual void RegisterSizeofKeyValues(int size) = 0;
	virtual PVOID AllocKeyValuesMemory(int size) = 0;
	virtual void FreeKeyValuesMemory(PVOID pMem) = 0;
	virtual HKeySymbol GetSymbolForString(const char* name, bool bCreate = true) = 0;
	virtual const char* GetStringForSymbol(HKeySymbol symbol) = 0;
	virtual void AddKeyValuesToMemoryLeakList(PVOID pMem, HKeySymbol name) = 0;
	virtual void RemoveKeyValuesFromMemoryLeakList(PVOID pMem) = 0;
	virtual void SetKeyValuesExpressionSymbol(const char*, bool) = 0;
	virtual void GetKeyValuesExpressionSymbol(const char*) = 0;
	virtual HKeySymbol GetSymbolForStringCaseSensitive(int*, const char*, bool) = 0;
	virtual ~KeyValuesSystem() = 0; // NOT EXIST IN REAL! ONLY FOR WARNING SILIENCE!
};

class KeyValues
{
public:
	static KeyValuesSystem* GetKeyValuesSystem()
	{
		using KeyValuesSystem_t = KeyValuesSystem * (__cdecl*)();
		return reinterpret_cast<KeyValuesSystem_t>(g_pOffsets->keyvaluessystem())();
	}
	
	explicit KeyValues(const char* setName) : m_iKeyName(0), m_iKeyNameCaseSensitive1(0), m_sValue(nullptr),
		m_wsValue(nullptr),
		m_iValue(0),
		m_iDataType(),
		m_bHasEscapeSequences(false), m_iKeyNameCaseSensitive2(0),
		m_pPeer(nullptr),
		m_pSub(nullptr),
		m_pChain(nullptr)
	{
		int param = -1;
		const HKeySymbol symbol = GetKeyValuesSystem()->GetSymbolForStringCaseSensitive(&param, setName, true);
		m_iKeyName ^= (param ^ m_iKeyName) & 0xFFFFFF;
		m_iKeyNameCaseSensitive1 = symbol;
		m_iKeyNameCaseSensitive2 = symbol >> 8;
	}

	bool LoadFromBuffer(const char* resourceName, const char* pBuffer, IBaseFileSystem* pFileSystem = nullptr, const char* pPathID = nullptr)
	{
		using fn = bool(__thiscall*)(PVOID, const char*, const char*, IBaseFileSystem*, const char*, DWORD, PVOID);
		return reinterpret_cast<fn>(g_pOffsets->loadfrombuffer())(this, resourceName, pBuffer, pFileSystem, pPathID, 0, nullptr);
	}

	bool LoadFromFile(IBaseFileSystem* filesystem, const char* resourceName, const char* pathID = NULL)
	{
		FileHandle_t f = filesystem->Open(resourceName, "rb", pathID);
		if (!f)
			return false;

		// load file into a null-terminated buffer
		int fileSize = (int)filesystem->Size(f);
		unsigned bufSize = ((IFileSystem*)filesystem)->GetOptimalReadSize(f, fileSize + 2);

		char* buffer = (char*)((IFileSystem*)filesystem)->AllocOptimalReadBuffer(f, bufSize);
		Assert(buffer);

		// read into local buffer
		bool bRetOK = (((IFileSystem*)filesystem)->ReadEx(buffer, (int)bufSize, fileSize, f) != 0);

		filesystem->Close(f);	// close file after reading

		if (bRetOK)
		{
			buffer[fileSize] = 0; // null terminate file as EOF
			buffer[fileSize + 1] = 0; // double NULL terminating in case this is a unicode file
			bRetOK = LoadFromBuffer(resourceName, buffer, filesystem, pathID);
		}

		((IFileSystem*)filesystem)->FreeOptimalReadBuffer(buffer);

		return bRetOK;
	}

	const char* GetString(const char* keyName = nullptr, const char* defaultValue = "")
	{
		using fn = const char* (__thiscall*)(PVOID, const char*, const char*);
		return reinterpret_cast<fn>(g_pOffsets->getstring())(this, keyName, defaultValue);
	}

	PVOID operator new(size_t iAllocSize)
	{
		return GetKeyValuesSystem()->AllocKeyValuesMemory(iAllocSize);
	}

	void operator delete(PVOID pMem)
	{
		return GetKeyValuesSystem()->FreeKeyValuesMemory(pMem);
	}
	const char* GetName() const
	{
		return GetKeyValuesSystem()->GetStringForSymbol(m_iKeyName);
	}
	
	KeyValues* FindKey(int keySymbol) const
	{
		for (KeyValues* dat = m_pSub; dat != nullptr; dat = dat->m_pPeer)
		{
			if (dat->m_iKeyName == keySymbol)
				return dat;
		}

		return nullptr;
	}

	KeyValues* FindKey(const char* keyName, bool bCreate)
	{
		// return the current key if a NULL subkey is asked for
		if (!keyName || !keyName[0])
			return this;

		// look for '/' characters deliminating sub fields
		char szBuf[256];
		const char* subStr = strchr(keyName, '/');
		const char* searchStr = keyName;

		// pull out the substring if it exists
		if (subStr)
		{
			const int size = subStr - keyName;
			memcpy(szBuf, keyName, size);
			szBuf[size] = 0;
			searchStr = szBuf;
		}

		// lookup the symbol for the search string
		const HKeySymbol iSearchStr = GetKeyValuesSystem()->GetSymbolForString(searchStr, bCreate);
		if (iSearchStr == INVALID_KEY_SYMBOL)
		{
			// not found, couldn't possibly be in key value list
			return nullptr;
		}

		KeyValues* lastItem = nullptr;
		KeyValues* dat;
		// find the searchStr in the current peer list
		for (dat = m_pSub; dat != nullptr; dat = dat->m_pPeer)
		{
			lastItem = dat;	// record the last item looked at (for if we need to append to the end of the list)

			// symbol compare
			if (dat->m_iKeyName == iSearchStr)
			{
				break;
			}
		}

		if (!dat && m_pChain)
		{
			dat = m_pChain->FindKey(keyName, false);
		}

		// make sure a key was found
		if (!dat)
		{
			if (bCreate)
			{
				// we need to create a new key
				dat = new KeyValues(searchStr);
				//			Assert(dat != NULL);

							// insert new key at end of list
				if (lastItem)
				{
					lastItem->m_pPeer = dat;
				}
				else
				{
					m_pSub = dat;
				}
				dat->m_pPeer = nullptr;

				// a key graduates to be a submsg as soon as it's m_pSub is set
				// this should be the only place m_pSub is set
				m_iDataType = TYPE_NONE;
			}
			else
			{
				return nullptr;
			}
		}

		// if we've still got a subStr we need to keep looking deeper in the tree
		if (subStr)
		{
			// recursively chain down through the paths in the string
			return dat->FindKey(subStr + 1, bCreate);
		}

		return dat;
	}

	int GetInt(const char* keyName, const int default_value = 0)
	{
		auto found = FindKey(keyName, false);
		if (found)
		{
			switch (found->m_iDataType)
			{
			case TYPE_STRING:
				return atol(found->m_sValue);
			case TYPE_WSTRING:
				return _wtoi(found->m_wsValue);
			case TYPE_FLOAT:
				return static_cast<int>(floor(found->m_flValue));
			case TYPE_UINT64:
				return 0;
			case TYPE_PTR:
				return reinterpret_cast<int>(found->m_pValue);
			default:
				return found->m_iValue;
			}
		}

		return default_value;
	}

	float GetFloat(const char* keyName, float defaultValue = 0.f)
	{
		KeyValues* dat = FindKey(keyName, false);
		if (dat)
		{
			switch (dat->m_iDataType)
			{
			case TYPE_STRING:
				return (float)atof(dat->m_sValue);
			case TYPE_WSTRING:
				return (float)_wtof(dat->m_wsValue);		// no wtof
			case TYPE_FLOAT:
				return dat->m_flValue;
			case TYPE_INT:
				return (float)dat->m_iValue;
			case TYPE_UINT64:
				return (float)(*((uint64*)dat->m_sValue));
			case TYPE_PTR:
			default:
				return 0.0f;
			}
		}
		return defaultValue;
	}

	void SetInt(const char* keyName, const int value)
	{
		KeyValues* dat = FindKey(keyName, true);

		if (dat)
		{
			dat->m_iValue = value;
			dat->m_iDataType = TYPE_INT;
		}
	}

	void SetFloat(const char* keyName, const float value)
	{
		KeyValues* dat = FindKey(keyName, true);

		if (dat)
		{
			dat->m_flValue = value;
			dat->m_iDataType = TYPE_FLOAT;
		}
	}
	
	void SetColor(const char* keyName, const char r, const char g, const char b, const char a)
	{
		KeyValues* dat = FindKey(keyName, true);

		if (dat)
		{
			dat->m_Color[0] = r;
			dat->m_Color[1] = g;
			dat->m_Color[2] = b;
			dat->m_Color[3] = a;
			dat->m_iDataType = TYPE_COLOR;
		}
	}

	void SetString(const char* keyName, const char* value)
	{
		KeyValues* dat = FindKey(keyName, true);

		if (dat)
		{
			auto memalloc = _memalloc;
			memalloc->Free(dat->m_sValue);
			memalloc->Free(dat->m_wsValue);
			const size_t size = strlen(value) + 1;
			if (size > 1)
			{
				dat->m_sValue = static_cast<char*>(_memalloc->Alloc(size));
				memset(dat->m_sValue, 0, size);
				strcpy_s(dat->m_sValue, size, value);
				dat->m_iDataType = TYPE_STRING;
			}
		}
	}
	
	bool SaveToFile(IBaseFileSystem* filesystem, const char* resourceName, const char* pathID, bool bWriteEmptySubkeys = false)
	{
		using fn = bool(__thiscall*)(PVOID, IBaseFileSystem*, const char*, const char*, bool);
		return reinterpret_cast<fn>(g_pOffsets->savetofile())(this, filesystem, resourceName, pathID, bWriteEmptySubkeys);
	}

	// Data type
	enum types_t : char
	{
		TYPE_NONE = 0,
		TYPE_STRING,
		TYPE_INT,
		TYPE_FLOAT,
		TYPE_PTR,
		TYPE_WSTRING,
		TYPE_COLOR,
		TYPE_UINT64,
		TYPE_COMPILED_INT_BYTE,			// hack to collapse 1 byte ints in the compiled format
		TYPE_COMPILED_INT_0,			// hack to collapse 0 in the compiled format
		TYPE_COMPILED_INT_1,			// hack to collapse 1 in the compiled format
		TYPE_NUMTYPES,
	};

	enum MergeKeyValuesOp_t
	{
		MERGE_KV_ALL,
		MERGE_KV_UPDATE,
		MERGE_KV_DELETE,
		MERGE_KV_BORROW
	};
	
	uint32 m_iKeyName : 24;	// keyname is a symbol defined in KeyValuesSystem
	uint32 m_iKeyNameCaseSensitive1 : 8;	// 1st part of case sensitive symbol defined in KeyValueSystem

	// These are needed out of the union because the API returns string pointers
	char* m_sValue;
	wchar_t* m_wsValue;

	// we don't delete these
	union
	{
		int m_iValue;
		float m_flValue{};
		PVOID m_pValue;
		unsigned char m_Color[4];
	};

	types_t	   m_iDataType;
	bool	   m_bHasEscapeSequences; // true, if while parsing this KeyValue, Escape Sequences are used (default false)
	uint16	   m_iKeyNameCaseSensitive2;	// 2nd part of case sensitive symbol defined in KeyValueSystem;

	KeyValues* m_pPeer;	// pointer to next key in list
	KeyValues* m_pSub;	// pointer to Start of a new sub key list
	KeyValues* m_pChain;// Search here if it's not in our list
};