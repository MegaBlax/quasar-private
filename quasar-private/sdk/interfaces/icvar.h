#pragma once
#include "convar.h"
#include "iappsystem.h"
#include "color.h"

class ICvar : public IAppSystem
{
public:
	ConVar* FindVar(const char* var_name)
	{
		using fn = ConVar * (__thiscall*)(void*, const char*);
		return call_virtual<fn>(this, 15)(this, var_name);
	}

	template <typename... Vl>
	void ConsoleColorPrintf(const Color& clr, const char* szMsg, Vl... p)
	{
		typedef void(__cdecl *fn)(void*, const Color&, const char*, ...);
		return call_virtual<fn>(this, 25)(this, clr, szMsg, p...);
	}
};
