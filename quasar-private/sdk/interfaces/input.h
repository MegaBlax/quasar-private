#pragma once
#include "../checksum_crc.h"

#define MULTIPLAYER_BACKUP 150

class CVerifiedUserCmd
{
public:
	CUserCmd	m_cmd;
	CRC32_t		m_crc;
};

class CInput
{
public:
	CInput(void);
	
	void CreateMove(int sequence_number, float input_sample_frametime, bool active)
	{
		using fn = void(__thiscall*)(void*, int, float, bool);
		call_virtual<fn>(this, 3)(this, sequence_number, input_sample_frametime, active);
	}
	
	void ExtraMouseSample(float frametime, bool active)
	{
		using fn = void(__thiscall*)(float, bool);
		call_virtual<fn>(this, 4)(frametime, active);
	}

	CUserCmd* GetUserCmd(int sequence_number)
	{
		using OriginalFn = CUserCmd * (__thiscall*)(void*, int, int);
		return call_virtual<OriginalFn>(this, 8)(this, 0, sequence_number);
	}
	
	CVerifiedUserCmd* GetVerifiedUserCmd(int sequence_number)
	{
		const auto m_pVerifiedCommands = *(CVerifiedUserCmd**)(reinterpret_cast<uint32_t>(this) + 0xF8);
		return &m_pVerifiedCommands[sequence_number % MULTIPLAYER_BACKUP];
	}
};