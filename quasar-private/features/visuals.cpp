#include "../utils/utils.h"
#include "../imgui/imgui.h"
#include "../imgui/imgui_internal.h"
#include "../imgui/imgui_render.h"

#include "visuals.h"
#include "sounds.h"
#include "aimbot.h"
#include "misc.h"

std::unique_ptr<visuals> g_pVisuals;

visuals::visuals(): _local_player(nullptr), _alpha(0), _teamid(0), _itemindex(0)
{
}

void visuals::run()
{
	if (!g_pInterfaces->clientstate()->IsInGame())
		return;

	_local_player = cs_player::get_local_player();
	if (!_local_player || _local_player->teamindex() < TEAM_TERRORIST )
		return;
	
	player_data_t* local_data = g_pPlayerList->get_player_data(g_pInterfaces->engineclient()->GetLocalPlayer());
	if (!local_data)
		return;

	_itemindex = local_data->itemindex;

	for (auto& it : g_pPlayerList->get_players())
	{
		player_data_t* data = g_pPlayerList->get_player_data(it.index);
		if (!data)
			continue;
		if (it.index == _local_player->EntIndex())
			continue;
		if (!data->can_drawing) 
			continue;
		if (!it.entity->is_alive())
			continue;
		if (data->alpha <= 0.f)
			continue;

		_teamid = it.entity->is_enemy(_local_player) ? int(TEAM::ENEMY) : int(TEAM::ALLY) ;
		_alpha = data->alpha;
		const bool is_visible = it.entity->is_visible() && !it.entity->in_smoke();
		
		if (!globals::visuals::team_t[_teamid].active)
			continue;
		if (globals::visuals::team_t[_teamid].visible_check && !is_visible)
			continue;
		
		player_info_t info;
		if (!g_pInterfaces->engineclient()->GetPlayerInfo(it.index,&info))
			continue;
		
		if (globals::visuals::team_t[_teamid].box)
			box(data->min, data->max, is_visible);
		if (globals::visuals::team_t[_teamid].health)
			healthbar(data->min, data->max, data->health);
		if (globals::visuals::team_t[_teamid].name)
			name(data->min, data->max, info.szName);
		if (globals::visuals::team_t[_teamid].weapon)
			weapon(data->min, data->max, it.entity);
	}

	g_pSounds->draw();
	
	if (_local_player->is_alive())
	{
		misc();
	}
	
}

void visuals::box(const ImVec2& min, const ImVec2& max, bool is_visible) const
{
	const ImU32 col = ImGui::QGetColorU32(globals::visuals::team_t[_teamid].color[int(is_visible ? PCOLOR::BOX_V : PCOLOR::BOX_I)],_alpha);
	g_pImRender->add_corner_box(min, min + max, col, globals::visuals::team_t[_teamid].corner_size);	
}

void visuals::healthbar(const ImVec2& min, const ImVec2& max, const float& health) const
{
	const float p = health / 100.f;
	const float height = max.y * p;
	const ImColor col = ImColor::HSV(0.03f * p * 10.f, 1.f, 1.f, _alpha);
	g_pImRender->add_rect_filled(ImVec2(min.x + max.x + 3.f, min.y + max.y - height),
	                              ImVec2(min.x + max.x + 6.f, min.y + max.y), col);
}

void visuals::name(const ImVec2& min, const ImVec2& max, const char* name) const
{
	const auto col1 = ImGui::QGetColorU32(globals::visuals::team_t[_teamid].color[int(PCOLOR::NAME)], _alpha);
	const auto col2 = ImGui::GetColorU32(ImVec4(0.f, 0.f, 0.f, _alpha));

	g_pImRender->add_text(g_pImRender->get_font(ImCustomFont_Default), col1, col2,
	                       ImVec2(min.x + max.x / 2.f, min.y - g_pImRender->get_font(ImCustomFont_Default)->FontSize),
	                       CENTERED_X | DROPSHADOW, "%s", name);
}

void visuals::weapon(const ImVec2& min, const ImVec2& max, cs_player* player) const
{
	cs_weapon* weapon = player->weapon();
	if (!weapon)
		return;
	const char* name = weapon->name();
	
	const auto col1 = ImGui::QGetColorU32(globals::visuals::team_t[_teamid].color[int(PCOLOR::WEAPON)], _alpha);
	const auto col2 = ImGui::GetColorU32(ImVec4(0.f, 0.f, 0.f, _alpha));

	g_pImRender->add_text(g_pImRender->get_font(ImCustomFont_Default), col1, col2,
		ImVec2(min.x + max.x / 2.f, min.y + max.y),
		CENTERED_X | DROPSHADOW, "%s", name);
}

void visuals::misc() const
{
	cs_weapon* weapon = _local_player->weapon(); // no weapon = no FOV!
	if (!weapon)
		return;
	
	const ImVec2 center = g_pUtils->CenterOnScreen();
	const ImVec2 size = g_pUtils->GetScreenSize();
	const ImVec2 div = size / globals::vars::fov;
	
	if (globals::visuals::fov)
	{
		if (globals::aimbot::active && globals::aimbot::weapon_t[_itemindex].active)
		{
			const float fov = div.y * g_pAimbot->get_fov();
			const ImVec2 pos = ImVec2(center.x - div.x * (_local_player->aimpunchangle().y / 2), center.y + div.y * _local_player->aimpunchangle().x);
			const ImVec4 vec_col = globals::visuals::color[int(COLOR::FOV)];
			const ImU32 colf = ImGui::GetColorU32(ImVec4(vec_col.x, vec_col.y, vec_col.z, 0.1f));
			const ImU32 col = ImGui::GetColorU32(ImVec4(vec_col.x, vec_col.y, vec_col.z, 0.5f));
			g_pImRender->add_circle_filled(pos, fov, colf, 128);
			g_pImRender->add_circle(pos, fov, col, 128);
		}
	}
	
	if (globals::visuals::sniper_crosshair)
	{
		if (!_local_player->is_scoped() && (weapon->is_autosniper() || weapon->is_sniper()))
		{
			const ImU32 col = ImGui::GetColorU32(globals::visuals::color[int(COLOR::CROSSHAIR)]);
			g_pImRender->add_line(center - ImVec2(8, 0), center - ImVec2(1, 0), col, 2.f);
			g_pImRender->add_line(center + ImVec2(1, 0), center + ImVec2(8, 0), col, 2.f);
			g_pImRender->add_line(center - ImVec2(0, 8), center - ImVec2(0, 1), col, 2.f);
			g_pImRender->add_line(center + ImVec2(0, 1), center + ImVec2(0, 8), col, 2.f);
		}
	}
	
	if (globals::visuals::recoil_crosshair)
	{
		if (weapon->is_smg() || weapon->is_heavy() || weapon->is_rifle() || weapon->is_cz75())
		{
			const ImVec2 pos = ImVec2(center.x - div.x * (_local_player->aimpunchangle().y / 2), center.y + div.y * _local_player->aimpunchangle().x);
			const ImU32 col = ImGui::GetColorU32(globals::visuals::color[int(COLOR::RECOIL)]);
			g_pImRender->add_circle_filled(pos, 3.f, col);
		}
	}

	if (globals::misc::hitmarker)
		g_pMisc->hitmarker();
}
