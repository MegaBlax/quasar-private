#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"
#include "../sdk/cs_player.h"

struct steps_s
{
	steps_s(const float _time, const float _radius, const int _guid, vector _position, ImVec4 _color):
		time(_time), radius(_radius),
		steps_size(_radius),
		guid(_guid), position(_position), color(_color)
	{
	}

	float time;
	float radius;
	float steps_size;
	int guid;
	vector position;
	ImVec4 color;
};

class sounds
{
public:
	void update();
	void clear();
	void draw();
	bool is_valid(const SndInfo_t& sound);
private:
	CUtlVector<SndInfo_t> _list;
	std::vector<steps_s> _data;
};

extern std::unique_ptr<sounds> g_pSounds;