#include "hooks.h"
#include <imgui.h>
#include "../sdk/key_translation.h"

LRESULT WndProcHandler(UINT msg, WPARAM wParam, LPARAM lParam)
{
	ImGuiIO& io = ImGui::GetIO();
	switch (msg)
	{
	case WM_LBUTTONDOWN: case WM_LBUTTONDBLCLK:
	case WM_RBUTTONDOWN: case WM_RBUTTONDBLCLK:
	case WM_MBUTTONDOWN: case WM_MBUTTONDBLCLK:
	case WM_XBUTTONDOWN: case WM_XBUTTONDBLCLK:
	{
		int button = MOUSE_LEFT;
		if (msg == WM_LBUTTONDOWN || msg == WM_LBUTTONDBLCLK) { button = MOUSE_LEFT; }
		if (msg == WM_RBUTTONDOWN || msg == WM_RBUTTONDBLCLK) { button = MOUSE_RIGHT; }
		if (msg == WM_MBUTTONDOWN || msg == WM_MBUTTONDBLCLK) { button = MOUSE_MIDDLE; }
		if (msg == WM_XBUTTONDOWN || msg == WM_XBUTTONDBLCLK) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? MOUSE_4 : MOUSE_5; }

		globals::vars::key_pressed[button] = true;
		return 0;
	}
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
	case WM_XBUTTONUP:
	{
		int button = MOUSE_LEFT;
		if (msg == WM_LBUTTONUP) { button = MOUSE_LEFT; }
		if (msg == WM_RBUTTONUP) { button = MOUSE_RIGHT; }
		if (msg == WM_MBUTTONUP) { button = MOUSE_MIDDLE; }
		if (msg == WM_XBUTTONUP) { button = (GET_XBUTTON_WPARAM(wParam) == XBUTTON1) ? MOUSE_4 : MOUSE_5; }

		globals::vars::key_pressed[button] = false;
		return 0;
	}
	case WM_KEYDOWN:
	case WM_SYSKEYDOWN:
		if (wParam < 256)
		{
			io.KeysDown[wParam] = true;
			if (!(lParam & (1 << 30)))
			{
				ButtonCode_t button = keytranslation->ButtonCode_VirtualKeyToButtonCode(wParam);
				globals::vars::key_pressed[button] = true;
			}
		}
		
		return 0;
	case WM_KEYUP:
	case WM_SYSKEYUP:
		if (wParam < 256)
		{
			io.KeysDown[wParam] = false;
			ButtonCode_t button = keytranslation->ButtonCode_VirtualKeyToButtonCode(wParam);
			globals::vars::key_pressed[button] = false;
		}
		return 0;
	default: 
		return 0;
	}
}

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT hooks::wndproc_h(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (globals::menu::active)
	{
		if (!g_pInterfaces->engineclient()->IsActiveApp() || globals::vars::window != g_pWinApi->dwGetForegroundWindow())
			return CallWindowProc(_o_wndproc, hWnd, msg, wParam, lParam);
	}
	WndProcHandler(msg, wParam, lParam);
	
	if (globals::menu::active && !ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
		return true;

	return CallWindowProc(_o_wndproc, hWnd, msg, wParam, lParam);
}