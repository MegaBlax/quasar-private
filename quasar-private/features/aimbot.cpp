#include "aimbot.h"
#include "engineprediction.h"

std::unique_ptr<aimbot> g_pAimbot;

std::vector<int> hitboxes = { HITBOX_STOMACH, HITBOX_LEFT_THIGH, HITBOX_RIGHT_THIGH, HITBOX_PELVIS, HITBOX_LOWER_CHEST, HITBOX_CHEST,HITBOX_UPPER_CHEST,HITBOX_NECK,HITBOX_HEAD };

vector velocity_prediction(cs_player* player, const vector& pos)
{
	return pos + player->vecvelocity() * g_pInterfaces->globalvarsbase()->interval_per_tick;
}

void aimbot::run(CUserCmd* cmd, byte*& packet)
{
	if (!globals::aimbot::active)
		return;

	count_shotsfired();
	change_fov();
	if (!find_target(_entity_player)) {
		drop_target();
		return;
	}
	if (globals::hotkey::aimbot) {
		if (globals::aimbot::key_mode == 1) {
			if (!g_pInterfaces->inputsystem()->IsButtonDown((ButtonCode_t)globals::hotkey::aimbot) && !_auto_delay_run) {
				clear();
				return;
			}
		}
		else if (globals::aimbot::key_mode == 2) {
			static bool toggle = true;
			if (g_pInterfaces->inputsystem()->IsButtonReleased((ButtonCode_t)globals::hotkey::aimbot))
				toggle = !toggle;
			if (!toggle) {
				clear();
				return;
			}
		}
	}
	if (!find_hitbox(_hitbox))
		return;
	change_angles(packet);
	delay_before_first_shot();
}

void aimbot::clear()
{
}

float aimbot::get_fov() const
{
	return _current_fov;
}

cs_player* aimbot::get_target()
{
	return _entity_player;
}

void aimbot::player_death(const int index)
{
}

void aimbot::count_shotsfired()
{
}

void aimbot::change_fov()
{
}

void aimbot::drop_target()
{
}

bool aimbot::find_target(cs_player*& data)
{
	return false;
}

bool aimbot::find_hitbox(vector& vec)
{
	return false;
}

void aimbot::change_angles(byte*& packet)
{
}

void aimbot::delay_before_first_shot()
{
}