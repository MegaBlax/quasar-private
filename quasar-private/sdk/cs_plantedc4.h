#pragma once
#include "../stdafx.h"
#include "interfaces.h"
#include "cs_player.h"

class cs_planted_c4
{
public:
	static cs_planted_c4* get_entity(cs_player* entity);
	float defuselength();
	float explosiontimer();
	float defusetimer();
	bool ticking();
	bool defused();
	CHandle<cs_player*> defuser();
};
