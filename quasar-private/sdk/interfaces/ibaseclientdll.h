#pragma once
class ClientClass;
class CGlobalVarsBase;

class IBaseClientDLL
{
public:
    virtual int              Connect(CreateInterfaceFn appSystemFactory, CGlobalVarsBase *pGlobals) = 0;
    virtual int              Disconnect(void) = 0;
    virtual int              Init(CreateInterfaceFn appSystemFactory, CGlobalVarsBase *pGlobals) = 0;
    virtual void             PostInit() = 0;
    virtual void             Shutdown(void) = 0;
    virtual void             LevelInitPreEntity(char const* pMapName) = 0;
    virtual void             LevelInitPostEntity() = 0;
    virtual void             LevelShutdown(void) = 0;
    virtual ClientClass*     GetAllClasses(void) = 0;
    bool			         DispatchUserMessage(int msg_type, int32 nFlags, int size, const PVOID msg)
    {
        using fn = bool(__thiscall*)(PVOID, int, int32, int, PVOID);
        return call_virtual<fn>(this, 38)(this, msg_type, nFlags, size, msg);
    }
};