#include "interfaces.h"
#include "cfactory.h"
#include "mem.h"
#include "../utils/utils.h"

IClientEntityList* entitylist;
IMemAlloc* _memalloc;
std::unique_ptr<interfaces> g_pInterfaces;

interfaces::interfaces()
{
	_baseclientdll = nullptr;
	_engineclient = nullptr;
	_entitylist = nullptr;
	_clientmode = nullptr;
	_cvar = nullptr;
	_panel = nullptr;
	_surface = nullptr;
	_enginetrace = nullptr;
	_modelinfo = nullptr;
	_globalvarsbase = nullptr;
	_gameeventmanager = nullptr;
	_enginesound = nullptr;
	_prediction = nullptr;
	_materialsystem = nullptr;
	_modelrender = nullptr;
	_viewrender = nullptr;
	_input = nullptr;
	_filesystem = nullptr;
	_weaponsystem = nullptr;
	_clientstate = nullptr;
	_inputsystem = nullptr;
	_steamclient = nullptr;
	_steamuser = nullptr;
	_matchframework = nullptr;
	_texturedictionary = nullptr;
	_texturemanager = nullptr;
	_gameui = nullptr;
	_gametypes = nullptr;
	_movehelper = nullptr;
	_gamemovement = nullptr;
	_netsupport = nullptr;
	_spatialquery = nullptr;
}

bool interfaces::init()
{
	_baseclientdll = reinterpret_cast<IBaseClientDLL*>(factory::client->dwGetInterface(xorstr_("VClient")));
	_engineclient = reinterpret_cast<IVEngineClient*>(factory::engine->dwGetInterface(xorstr_("VEngineClient")));
	_entitylist = reinterpret_cast<IClientEntityList*>(factory::client->dwGetInterface(xorstr_("VClientEntityList")));
	entitylist = _entitylist;
	_clientmode = **reinterpret_cast<IClientMode***>((*reinterpret_cast<DWORD**>(_baseclientdll))[10] + 0x5);
	_cvar = reinterpret_cast<ICvar*>(factory::vstdlib->dwGetInterface(xorstr_("VEngineCvar")));
	_panel = reinterpret_cast<IPanel*>(factory::vgui2->dwGetInterface(xorstr_("VGUI_Panel")));
	_surface = reinterpret_cast<ISurface*>(factory::vguimatsurface->dwGetInterface(xorstr_("VGUI_Surface")));
	_enginetrace = reinterpret_cast<IEngineTrace*>(factory::engine->dwGetInterface(xorstr_("EngineTraceClient")));
	_modelinfo = reinterpret_cast<IVModelInfo*>(factory::engine->dwGetInterface(xorstr_("VModelInfoClient")));
	_globalvarsbase = **reinterpret_cast<CGlobalVarsBase ***>((*reinterpret_cast<DWORD **>(_baseclientdll))[11] + 10);
	_gameeventmanager = reinterpret_cast<IGameEventManager2*>(factory::engine->dwGetInterface(xorstr_("GAMEEVENTSMANAGER"), 1));
	_enginesound = reinterpret_cast<IEngineSound*>(factory::engine->dwGetInterface(xorstr_("IEngineSoundClient")));
	_prediction = reinterpret_cast<IPrediction*>(factory::client->dwGetInterface(xorstr_("VClientPrediction")));
	_materialsystem = reinterpret_cast<IMaterialSystem*>(factory::materialsystem->dwGetInterface(xorstr_("VMaterialSystem")));
	_modelrender = reinterpret_cast<IVModelRender*>(factory::engine->dwGetInterface(xorstr_("VEngineModel")));
	_viewrender = **reinterpret_cast<IViewRender***>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("FF 50 4C 8B 06 8D 4D F4")) - 0x6);
	_memalloc = *reinterpret_cast<IMemAlloc**>(GetProcAddress(HMODULE(g_pMemory->tier0().lpBaseOfDll), xorstr_("g_pMemAlloc")));
	_input = *reinterpret_cast<CInput**>(g_pUtils->PatternScan(g_pMemory->client(), "B9 ? ? ? ? F3 0F 11 04 24 FF 50 10") + 0x1);
	_filesystem = reinterpret_cast<IFileSystem*>(factory::filesystem->dwGetInterface(xorstr_("VFileSystem")));
	_weaponsystem = *reinterpret_cast<IWeaponSystem**>(g_pUtils->PatternScan(g_pMemory->client(), "8B 35 ? ? ? ? FF 10 0F B7 C0") + 0x2);
	_clientstate = **reinterpret_cast<CClientState***>(g_pUtils->PatternScan(g_pMemory->engine(), "A1 ? ? ? ? 33 D2 6A ? 6A ? 33 C9 89 B0") + 0x1);
	_inputsystem = reinterpret_cast<IInputSystem*>(factory::inputsystem->dwGetInterface(xorstr_("InputSystemVersion")));
	_matchframework = reinterpret_cast<IMatchFramework*>(factory::matchmaking->dwGetInterface("MATCHFRAMEWORK_"));
	_texturemanager = *reinterpret_cast<ITextureManager**>(g_pUtils->PatternScan(g_pMemory->materialsystem(), "A1 ? ? ? ? B9 ? ? ? ? FF 50 40 8B") + 0x1);
	_gameui = *reinterpret_cast<CGameUI**>(g_pUtils->PatternScan(g_pMemory->client(), xorstr_("A1 ? ? ? ? B9 ? ? ? ? 6A 00 6A 00 6A 00")) + 0x1);
	_gametypes = reinterpret_cast<IGameTypes*>(factory::matchmaking->dwGetInterface(xorstr_("VENGINE_GAMETYPES_VERSION"), 1));
	_movehelper = **reinterpret_cast<IMoveHelper***>(g_pUtils->PatternScan(g_pMemory->client(), "8B 0D ? ? ? ? 8B 45 ? 51 8B D4 89 02 8B 01") + 0x2);
	_gamemovement = reinterpret_cast<CGameMovement*>(factory::client->dwGetInterface(xorstr_("GameMovement")));
	_renderview = reinterpret_cast<IVRenderView*>(factory::engine->dwGetInterface(xorstr_("VEngineRenderView")));
	
	/*_steamclient = reinterpret_cast<ISteamClient* (__cdecl*)()>(GetProcAddress(
		static_cast<HMODULE>(g_pMemory->steamapi().lpBaseOfDll), xorstr_("SteamClient")))();
	const SteamUserHandle hSteamUser = reinterpret_cast<SteamUserHandle(__cdecl*)()>(GetProcAddress(
		static_cast<HMODULE>(g_pMemory->steamapi().lpBaseOfDll), xorstr_("SteamAPI_GetHSteamUser")))();
	const SteamPipeHandle hSteamPipe = reinterpret_cast<SteamPipeHandle(__cdecl*)()>(GetProcAddress(
		static_cast<HMODULE>(g_pMemory->steamapi().lpBaseOfDll), xorstr_("SteamAPI_GetHSteamPipe")))();
	
	_steamuser = _steamclient->GetISteamUser(hSteamUser, hSteamPipe, xorstr_("SteamUser019"));*/

	_spatialquery = reinterpret_cast<ISpatialQuery*>(engineclient()->GetBSPTreeQuery());

	if (!_baseclientdll)
	{
		log_error(xorstr_(L"failed to initialize IBaseClientDLL"));
		return false;
	}
	if (!_engineclient)
	{
		log_error(xorstr_(L"Failed to initialize IVEngineClient"));
		return false;
	}
	if (!_entitylist || !entitylist)
	{
		log_error(xorstr_(L"Failed to initialize IClientEntityList"));
		return false;
	}
	if (!_clientmode)
	{
		log_error(xorstr_(L"Failed to initialize IClientMode"));
		return false;
	}
	if (!_cvar)
	{
		log_error(xorstr_(L"Failed to initialize ICvar"));
		return false;
	}
	if (!_panel)
	{
		log_error(xorstr_(L"Failed to initialize IPanel"));
		return false;
	}
	if (!_surface)
	{
		log_error(xorstr_(L"Failed to initialize ISurface"));
		return false;
	}
	if (!_enginetrace)
	{
		log_error(xorstr_(L"Failed to initialize IEngineTrace"));
		return false;
	}
	if (!_modelinfo)
	{
		log_error(xorstr_(L"Failed to initialize IVModelInfo"));
		return false;
	}
	if (!_globalvarsbase)
	{
		log_error(xorstr_(L"Failed to initialize CGlobalVarsBase"));
		return false;
	}
	if (!_gameeventmanager)
	{
		log_error(xorstr_(L"Failed to initialize IGameEventManager2"));
		return false;
	}
	if (!_enginesound)
	{
		log_error(xorstr_(L"Failed to initialize IEngineSound"));
		return false;
	}
	if (!_prediction)
	{
		log_error(xorstr_(L"Failed to initialize IPrediction"));
		return false;
	}
	if (!_materialsystem)
	{
		log_error(xorstr_(L"Failed to initialize IMaterialSystem"));
		return false;
	}
	if (!_viewrender)
	{
		log_error(xorstr_(L"Failed to initialize IViewRender"));
		return false;
	}
	if (!_memalloc)
	{
		log_error(xorstr_(L"Failed to initialize IMemAlloc"));
		return false;
	}
	if (!_input)
	{
		log_error(xorstr_(L"Failed to initialize CInput"));
		return false;
	}
	if (!_filesystem)
	{
		log_error(xorstr_(L"Failed to initialize IFileSystem"));
		return false;
	}
	if (!_weaponsystem)
	{
		log_error(xorstr_(L"Failed to initialize IWeaponSystem"));
		return false;
	}
	if (!_clientstate)
	{
		log_error(xorstr_(L"Failed to initialize CClientState"));
		return false;
	}
	if (!_inputsystem)
	{
		log_error(xorstr_(L"Failed to initialize IInputSystem"));
		return false;
	}
	/*if (!_steamclient)
	{
		log_error(xorstr_(L"Failed to initialize ISteamClient"));
		return false;
	}*/
	/*if (!_steamuser)
	{
		log_error(xorstr_(L"Failed to initialize ISteamUser"));
		return false;
	}*/
	if (!_matchframework)
	{
		log_error(xorstr_(L"Failed to initialize IMatchFramework"));
		return false;
	}
	if (!_texturemanager)
	{
		log_error(xorstr_(L"Failed to initialize ITextureManager"));
		return false;
	}
	if (!_gameui)
	{
		log_error(xorstr_(L"Failed to initialize CGameUI"));
		return false;
	}
	if (!_gametypes)
	{
		log_error(xorstr_(L"Failed to initialize IGameTypes"));
		return false;
	}
	if (!_movehelper)
	{
		log_error(xorstr_(L"Failed to initialize IMoveHelper"));
		return false;
	}
	if (!_gamemovement)
	{
		log_error(xorstr_(L"Failed to initialize CGameMovement"));
		return false;
	}
	if (!_spatialquery)
	{
		log_error(xorstr_(L"Failed to initialize ISpatialQuery"));
		return false;
	}
	if (!_renderview)
	{
		log_error(xorstr_(L"Failed to initialize IVRenderView"));
		return false;
	}
	return true;
}

IBaseClientDLL* interfaces::baseclientdll() const
{
	return _baseclientdll;
}

IVEngineClient* interfaces::engineclient() const
{
	return _engineclient;
}

IClientEntityList* interfaces::cliententitylist() const
{
	return _entitylist;
}

IClientMode* interfaces::clientmode() const
{
	return _clientmode;
}

ICvar* interfaces::cvar() const
{
	return _cvar;
}

IPanel* interfaces::panel() const
{
	return _panel;
}

ISurface* interfaces::surface() const
{
	return _surface;
}

IEngineTrace* interfaces::enginetrace() const
{
	return _enginetrace;
}

IVModelInfo* interfaces::modelinfo() const
{
	return _modelinfo;
}

CGlobalVarsBase* interfaces::globalvarsbase() const
{
	return _globalvarsbase;
}

IGameEventManager2* interfaces::gameeventmanager() const
{
	return _gameeventmanager;
}

IEngineSound* interfaces::enginesound() const
{
	return _enginesound;
}

IPrediction* interfaces::prediction() const
{
	return _prediction;
}

IMaterialSystem* interfaces::materialsystem() const
{
	return _materialsystem;
}

IVModelRender* interfaces::modelrender() const
{
	return _modelrender;
}

IViewRender* interfaces::viewrender() const
{
	return _viewrender;
}

IMemAlloc* interfaces::memalloc()
{
	return _memalloc;
}

CInput* interfaces::input() const
{
	return _input;
}

IFileSystem* interfaces::filesystem() const
{
	return _filesystem;
}

IWeaponSystem* interfaces::weaponsystem() const
{
	return _weaponsystem;
}

CClientState* interfaces::clientstate() const
{
	return _clientstate;
}

IInputSystem* interfaces::inputsystem() const
{
	return _inputsystem;
}

IMatchFramework* interfaces::matchframework() const
{
	return _matchframework;
}

ITextureManager* interfaces::texturemanager() const
{
	return _texturemanager;
}

CGameUI* interfaces::gameui() const
{
	return _gameui;
}

IGameTypes* interfaces::gametypes() const
{
	return _gametypes;
}

IMoveHelper* interfaces::movehelper() const
{
	return _movehelper;
}

CGameMovement* interfaces::gamemovement() const
{
	return _gamemovement;
}

ISpatialQuery* interfaces::spatialquery() const
{
	return _spatialquery;
}

IVRenderView* interfaces::renderview() const
{
	return _renderview;
}
