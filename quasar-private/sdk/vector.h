#pragma once
#include "../stdafx.h"

class vector
{
public:
	// Members
	float x{}, y{}, z{};

	// Construction/destruction:
	vector() = default;
	vector(const vector&) = default;
	vector(vector&&) = default;
	~vector() = default;
	
	vector(const float _x, const float _y, const float _z): x(_x), y(_y), z(_z)
	{
	}

	vector(const float* clr): x(clr[0]), y(clr[1]), z(clr[2])
	{
	}

	// Initialization
	void Init(const float ix = 0.0f, const float iy = 0.0f, const float iz = 0.0f)
	{
		x = ix; y = iy; z = iz;
	}

	// Got any nasty NAN's?
	[[nodiscard]] bool IsValid() const
	{
		return !isinf(x) && !isinf(y) && !isinf(z) && !IsZero();
	}
	void Invalidate()
	{
		x = y = z = std::numeric_limits<float>::infinity();
	}

	// array access...
	float operator[](const int i) const
	{
		return ((float*)(this))[i];
	}
	float& operator[](const int i)
	{
		return reinterpret_cast<float*>(this)[i];
	}

	// Base address...
	[[nodiscard]] float* Base()
	{
		return reinterpret_cast<float*>(this);
	}
	[[nodiscard]] float const* Base() const
	{
		return reinterpret_cast<float const*>(this);
	}

	// Initialization methods
	void RandomV(float minVal, float maxVal)
	{
		x = minVal + static_cast<float>(rand()) / RAND_MAX * (maxVal - minVal);
		y = minVal + static_cast<float>(rand()) / RAND_MAX * (maxVal - minVal);
		z = minVal + static_cast<float>(rand()) / RAND_MAX * (maxVal - minVal);
	}
	void Zero() ///< zero out a vector
	{
		x = y = z = 0.0f;
	}

	// equality
	[[nodiscard]] bool operator==(const vector& v) const
	{
		return v.x == x && v.y == y && v.z == z;
	}
	[[nodiscard]] bool operator!=(const vector& v) const
	{
		return (v.x != x) || (v.y != y) || (v.z != z);
	}

	// arithmetic operations
	vector& operator+=(const vector& v)
	{
		x += v.x; y += v.y; z += v.z;
		return *this;
	}

	vector& operator-=(const vector& v)
	{
		x -= v.x; y -= v.y; z -= v.z;
		return *this;
	}

	vector& operator*=(const float fl)
	{
		x *= fl;
		y *= fl;
		z *= fl;
		return *this;
	}

	vector& operator*=(const vector& v)
	{
		x *= v.x;
		y *= v.y;
		z *= v.z;
		return *this;
	}

	vector& operator/=(const vector& v)
	{
		x /= v.x;
		y /= v.y;
		z /= v.z;
		return *this;
	}

	// this ought to be an opcode.
	vector&	operator+=(const float fl)
	{
		x += fl;
		y += fl;
		z += fl;
		return *this;
	}

	// this ought to be an opcode.
	vector&	operator/=(const float fl)
	{
		x /= fl;
		y /= fl;
		z /= fl;
		return *this;
	}
	vector&	operator-=(const float fl)
	{
		x -= fl;
		y -= fl;
		z -= fl;
		return *this;
	}

	// negate the vector components
	void	Negate()
	{
		x = -x; y = -y; z = -z;
	}

	// Get the vector's magnitude.
	[[nodiscard]] float	Length() const
	{
		const V4SF vx =
		{
			{
			this->LengthSqr(),
			0.f,
			0.f,
			0.f
			},
		};
		const v4sf mmsqrt = _mm_rsqrt_ss(vx.v);
		const v4sf cache = _mm_mul_ss(vx.v, mmsqrt);
		return _mm_cvtss_f32(cache);
	}

	// Get the vector's magnitude squared.
	[[nodiscard]] float LengthSqr() const
	{
		return x * x + y * y + z * z;
	}

	// return true if this vector is (0,0,0) within tolerance
	[[nodiscard]] bool IsZero(float tolerance = 0.01f) const
	{
		return x > -tolerance && x < tolerance &&
			y > -tolerance && y < tolerance &&
			z > -tolerance && z < tolerance;
	}

	static float Normalizevector(vector& v)
	{
		const float l = v.Length();
		if (l != 0.0f) {
			v /= l;
		}
		else {
			v.x = v.y = v.z = 0.0f;
		}
		return l;
	}
	[[nodiscard]] float	NormalizeInPlace()
	{
		return Normalizevector(*this);
	}
	[[nodiscard]] vector	Normalized() const
	{
		vector res = *this;
		const float l = res.Length();
		if (l != 0.0f) {
			res /= l;
		}
		else {
			res.x = res.y = res.z = 0.0f;
		}
		return res;
	}

	bool	IsLengthLessThan(float val) const
	{
		return LengthSqr() < val * val;
	}

	// check if a vector is within the box defined by two other vectors
	[[nodiscard]] bool WithinAABox(vector const &boxmin, vector const &boxmax) const
	{
		return (
			(x >= boxmin.x) && (x <= boxmax.x) &&
			(y >= boxmin.y) && (y <= boxmax.y) &&
			(z >= boxmin.z) && (z <= boxmax.z)
			);
	}

	// Get the distance from this vector to the other one.
	[[nodiscard]] float	DistTo(const vector &vOther) const
	{
		const vector delta = *this - vOther;
		return delta.Length();
	}

	// Get the distance from this vector to the other one squared.
	// NJS: note, VC wasn't inlining it correctly in several deeply nested inlines due to being an 'out of line' .  
	// may be able to tidy this up after switching to VC7
	[[nodiscard]] float DistToSqr(const vector &vOther) const
	{
		const vector delta = *this - vOther;

		return delta.LengthSqr();
	}

	// Copy
	void	CopyToArray(float* rgfl) const
	{
		rgfl[0] = x, rgfl[1] = y, rgfl[2] = z;
	}

	void Mul(float scalar)
	{
		x *= scalar;
		y *= scalar;
		z *= scalar;
	}

	// Multiply, add, and assign to this (ie: *this = a + b * scalar). This
	// is about 12% faster than the actual vector equation (because it's done per-component
	// rather than per-vector).
	void	MulAdd(const vector& a, const vector& b, float scalar)
	{
		x = a.x + b.x * scalar;
		y = a.y + b.y * scalar;
		z = a.z + b.z * scalar;
	}

	[[nodiscard]] static vector CrossProduct(const vector& a, const vector& b)
	{
		return vector(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
	}
	vector Cross(const vector& vOther) const
	{
		return CrossProduct(*this, vOther);
	}

	static float DotProduct(const vector& a, const vector& b)
	{
		return(a.x * b.x + a.y * b.y + a.z * b.z);
	}

	// Dot product.
	[[nodiscard]] float	Dot(const vector& b) const
	{
		return DotProduct(*this, b);
	}

	// assignment
	vector& operator=(const vector &vOther)
		= default;

	// 2d
	[[nodiscard]] float	Length2D() const
	{
		const float value = x * x + y * y;
		const V4SF vx =
		{
			{
			value,
			0.f,
			0.f,
			0.f
			},
		};
		const v4sf mmsqrt = _mm_rsqrt_ss(vx.v);
		const v4sf cache = _mm_mul_ss(vx.v, mmsqrt);
		return _mm_cvtss_f32(cache);
	}

	/// get the component of this vector parallel to some other given vector
	[[nodiscard]] vector  ProjectOnto(const vector& onto) const
	{
		return onto * (this->Dot(onto) / (onto.LengthSqr()));
	}

	// copy constructors
	//	vector(const vector &vOther);

	// arithmetic operations
	vector	operator-() const
	{
		return vector(-x, -y, -z);
	}

	vector	operator+(const int& v) const
	{
		const auto cast = static_cast<float>(v);
		return vector(x + cast, y + cast, z + cast);
	}
	vector	operator+(const vector& v) const
	{
		return vector(x + v.x, y + v.y, z + v.z);
	}
	vector	operator-(const vector& v) const
	{
		return vector(x - v.x, y - v.y, z - v.z);
	}
	vector	operator*(const vector& v) const
	{
		return vector(x * v.x, y * v.y, z * v.z);
	}
	vector	operator/(const vector& v) const
	{
		return vector(x / v.x, y / v.y, z / v.z);
	}
	vector	operator*(const float fl) const
	{
		return vector(x * fl, y * fl, z * fl);
	}
	vector	operator/(const float fl) const
	{
		return vector(x / fl, y / fl, z / fl);
	}
};

typedef vector qangle;

class __declspec(align(16)) vector_aligned : public vector
{
public:
	vector_aligned() : w(0.f)
	{
	};
	vector_aligned(const float X, const float Y, const float Z) : w(0.f)
	{
		vector(X, Y, Z);
	}

	explicit vector_aligned(const vector &vOther) : w(0)
	{
		if(*this != vOther)
			Init(vOther.x, vOther.y, vOther.z);
	}

	vector_aligned& operator=(const vector &vOther)
	{
		if (*this == vOther)
			return *this;
		Init(vOther.x, vOther.y, vOther.z);
		return *this;
	}

	vector_aligned& operator=(const vector_aligned &vOther)
	{
		if (*this == vOther)
			return *this;
		_mm_store_ps(Base(), _mm_load_ps(vOther.Base()));
		return *this;
	}

	float w;
};

static void VectorAdd(const vector& a, const vector& b, vector& c)
{
	c.x = a.x + b.x;
	c.y = a.y + b.y;
	c.z = a.z + b.z;
}

static void VectorCopy(const vector& src, vector& dst)
{
	dst.x = src.x;
	dst.y = src.y;
	dst.z = src.z;
}

static void VectorSubtract(const vector& a, const vector& b, vector& c)
{
	c.x = a.x - b.x;
	c.y = a.y - b.y;
	c.z = a.z - b.z;
}

static void VectorClear(vector& a)
{
	a.x = a.y = a.z = 0.0f;
}