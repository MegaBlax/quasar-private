#pragma once
#include "../stdafx.h"
#include "../sdk/interfaces.h"

using AutoAccept = bool(__stdcall*)(const char*);

class misc
{
public:
	misc();
	void reveal_ranks(CUserCmd* cmd);
	void bunnyhop(CUserCmd* cmd);
	void strafe(CUserCmd* cmd);
	void autoaccept();
	void player_hurt();
	void hitmarker() const;
	void killeffect() const;
private:
	AutoAccept _autoaccept;
	float _hit_time;
};

extern std::unique_ptr<misc> g_pMisc;