#pragma once

class IMatchEventsSink
{
public:
	virtual void OnEvent(KeyValues* pEvent) = 0;
};

class IMatchEventsSubscription
{
public:
	virtual void Subscribe(IMatchEventsSink* pSink) = 0;
	virtual void Unsubscribe(IMatchEventsSink* pSink) = 0;

	virtual void BroadcastEvent(KeyValues* pEvent) = 0;

	virtual void RegisterEventData(KeyValues* pEventData) = 0;
	virtual KeyValues* GetEventData(char const* szEventDataKey) = 0;
};