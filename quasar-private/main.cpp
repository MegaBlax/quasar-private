#include "stdafx.h"
#include "utils/log.h"
#include "utils/utils.h"
#include "utils/configuration.h"
#include "utils/custom_winapi.h"
#include "imgui/imgui_render.h"
#include "imgui/imgui_menu.h"
#include "sdk/mem.h"
#include "sdk/cfactory.h"
#include "sdk/interfaces.h"
#include "sdk/netvars.h"
#include "sdk/offsets.h"
#include "sdk/cs_player.h"
#include "sdk/cs_gamerules.h"
#include "hooks/hooks.h"

HANDLE thread;

void runtime_error(const wchar_t* msg)
{
	log_error(msg);
	g_pWinApi->dwMessageBoxW(nullptr, msg, nullptr, MB_OK | MB_ICONERROR | MB_TOPMOST);
}

uintp init_thread (PVOID arg)
{
	//VMProtectBeginMutation(__FUNCTION__);
	while (!globals::vars::window)
	{
		globals::vars::window = g_pWinApi->dwFindWindowW(xorstr_(L"Valve001"), xorstr_(L"Counter-Strike: Global Offensive"));
		g_pWinApi->dwSleep(250);
	}
	for (;;)
	{
		if (spoofed_GetModuleHandleW(xorstr_(L"GameOverlayRenderer.dll")) && spoofed_GetModuleHandleW(xorstr_(L"serverbrowser.dll")))
			break;
		g_pWinApi->dwSleep(250);
	}

	g_pUtils = std::make_unique<utils>();
	g_pUtils->CreateAllDirectories();

	g_pLogManager = std::make_unique<logmanager>();
	g_pMemory = std::make_unique<mem>();
	g_pInterfaces = std::make_unique<interfaces>();
	g_pOffsets = std::make_unique<offsets>();
	g_pGameRules = std::make_unique<CSGameRulesProxy>();
	g_pImRender = std::make_unique<imgui_render>();
	g_pMenu = std::make_unique<imgui_menu>();
	g_pHooks = std::make_unique<hooks>();
	g_pConfig = std::make_unique<configuration>();
	g_pPlayerList = std::make_unique<player_list>();

	if (!g_pMemory || !g_pMemory->init())
	{
		runtime_error(xorstr_(L"Failed to initialize modules"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
	if (!factory::init())
	{
		runtime_error(xorstr_(L"Failed to initialize factory"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
	if (!g_pInterfaces || !g_pInterfaces->init())
	{
		runtime_error(xorstr_(L"Failed to initialize interface"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
#if 0
	if (g_pInterfaces->engineclient()->GetClientVersion() != globals::vars::client_version ||
		(int)g_pInterfaces->engineclient()->GetEngineBuildNumber() != globals::vars::engine_build)
	{
		runtime_error(xorstr_(L"Outdated"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
#endif
	if (!netvars::init())
	{
		runtime_error(xorstr_(L"Failed to initialize netvars"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
	if (!g_pOffsets || !g_pOffsets->init())
	{
		runtime_error(xorstr_(L"Failed to initialize offsets"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
	if (!g_pGameRules || !g_pGameRules->init())
	{
		runtime_error(xorstr_(L"Failed to initialize CSGameRulesProxy"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
	if (!g_pImRender || !g_pImRender->init())
	{
		runtime_error(xorstr_(L"Failed to initialize imgui"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
	if (!g_pHooks || !g_pHooks->init())
	{
		runtime_error(xorstr_(L"Failed to initialize hooks"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}
	if (!g_pConfig || !g_pConfig->init())
	{
		runtime_error(xorstr_(L"Failed to initialize configuration"));
		g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);
		return 0;
	}

	const auto first_load = g_pConfig->load(xorstr_(L"default"));
	if (!first_load)
		g_pInterfaces->cvar()->ConsoleColorPrintf(Color::Red(), xorstr_("Failed to load default configuration!\n"));

	g_pWinApi->dwReleaseThreadHandle(thread);
	//VMProtectEnd();
	return 0;
}

uintp cheat_free(PVOID arg)
{
	for(; !globals::vars::unload; std::this_thread::sleep_for(std::chrono::milliseconds(250))){}
		g_pWinApi->dwSleep(250);
	
	g_pInterfaces->cvar()->ConsoleColorPrintf(Color::Red(), xorstr_("unload!\n"));

	if (!g_pHooks->reset()) // unable to safely unload dll.
		std::abort();

	g_pWinApi->dwSleep(250);
	g_pWinApi->dwBeep(500, 350);
	g_pWinApi->dwFreeLibraryAndExitThread(static_cast<HMODULE>(arg), 0);

	return 0;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, const DWORD fdwReason, const LPVOID lpvReserved) 
{
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		g_pWinApi = std::make_unique<winapi>();
		g_pWinApi->dwDisableThreadLibraryCalls(hinstDLL);
		thread = g_pWinApi->dwCreateSimpleThread(init_thread, hinstDLL, 0);
		//g_pWinApi->dwCreateSimpleThread(cheat_free, hinstDLL, 0);
		if (!thread)
			return FALSE;
		
#ifdef _DEBUG
		
		g_pWinApi->dwAllocConsole();
		g_pWinApi->dwAttachConsole(g_pWinApi->dwGetCurrentProcessId());
		freopen_s((FILE**)stdout, "CONOUT$", "w", stdout);
#endif
		return  TRUE;
	}
	if (fdwReason == DLL_PROCESS_DETACH)
	{
#ifdef _DEBUG
		fclose((FILE*)stdin);
		FreeConsole();
#endif
		return TRUE;
	}
	return TRUE;
}