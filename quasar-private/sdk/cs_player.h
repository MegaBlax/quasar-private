#pragma once
#include "../stdafx.h"
#include "interfaces.h"
#include "cs_weapon.h"
#include <imgui.h>

constexpr auto CAPSULE_VERTS = 74;
inline float g_capsuleVertPositions[CAPSULE_VERTS][3] = {
	{ -0.01, -0.01, 1.0 },	{ 0.51, 0.0, 0.86 },	{ 0.44, 0.25, 0.86 },	{ 0.25, 0.44, 0.86 },	{ -0.01, 0.51, 0.86 },	{ -0.26, 0.44, 0.86 },	{ -0.45, 0.25, 0.86 },	{ -0.51, 0.0, 0.86 },	{ -0.45, -0.26, 0.86 },
	{ -0.26, -0.45, 0.86 },	{ -0.01, -0.51, 0.86 },	{ 0.25, -0.45, 0.86 },	{ 0.44, -0.26, 0.86 },	{ 0.86, 0.0, 0.51 },	{ 0.75, 0.43, 0.51 },	{ 0.43, 0.75, 0.51 },	{ -0.01, 0.86, 0.51 },	{ -0.44, 0.75, 0.51 },
	{ -0.76, 0.43, 0.51 },	{ -0.87, 0.0, 0.51 },	{ -0.76, -0.44, 0.51 },	{ -0.44, -0.76, 0.51 },	{ -0.01, -0.87, 0.51 },	{ 0.43, -0.76, 0.51 },	{ 0.75, -0.44, 0.51 },	{ 1.0, 0.0, 0.01 },		{ 0.86, 0.5, 0.01 },
	{ 0.49, 0.86, 0.01 },	{ -0.01, 1.0, 0.01 },	{ -0.51, 0.86, 0.01 },	{ -0.87, 0.5, 0.01 },	{ -1.0, 0.0, 0.01 },	{ -0.87, -0.5, 0.01 },	{ -0.51, -0.87, 0.01 },	{ -0.01, -1.0, 0.01 },	{ 0.49, -0.87, 0.01 },
	{ 0.86, -0.51, 0.01 },	{ 1.0, 0.0, -0.02 },	{ 0.86, 0.5, -0.02 },	{ 0.49, 0.86, -0.02 },	{ -0.01, 1.0, -0.02 },	{ -0.51, 0.86, -0.02 },	{ -0.87, 0.5, -0.02 },	{ -1.0, 0.0, -0.02 },	{ -0.87, -0.5, -0.02 },
	{ -0.51, -0.87, -0.02 },{ -0.01, -1.0, -0.02 },	{ 0.49, -0.87, -0.02 },	{ 0.86, -0.51, -0.02 },	{ 0.86, 0.0, -0.51 },	{ 0.75, 0.43, -0.51 },	{ 0.43, 0.75, -0.51 },	{ -0.01, 0.86, -0.51 },	{ -0.44, 0.75, -0.51 },
	{ -0.76, 0.43, -0.51 },	{ -0.87, 0.0, -0.51 },	{ -0.76, -0.44, -0.51 },{ -0.44, -0.76, -0.51 },{ -0.01, -0.87, -0.51 },{ 0.43, -0.76, -0.51 },	{ 0.75, -0.44, -0.51 },	{ 0.51, 0.0, -0.87 },	{ 0.44, 0.25, -0.87 },
	{ 0.25, 0.44, -0.87 },	{ -0.01, 0.51, -0.87 },	{ -0.26, 0.44, -0.87 },	{ -0.45, 0.25, -0.87 },	{ -0.51, 0.0, -0.87 },	{ -0.45, -0.26, -0.87 },{ -0.26, -0.45, -0.87 },{ -0.01, -0.51, -0.87 },{ 0.25, -0.45, -0.87 },
	{ 0.44, -0.26, -0.87 },	{ 0.0, 0.0, -1.0 },
};

using GoesThroughSmoke = bool(__cdecl*)(vector, vector);
using HasC4 = bool(__thiscall*)(void*);
using SetAbsOrigin = void(__thiscall*)(PVOID, const vector&);
using SetAbsAngles = void(__thiscall*)(PVOID, const qangle&);

class cs_player : public IClientEntity
{
public:
	static cs_player* get_local_player();
	static cs_player* get_entity_player(int index);

	int health();
	int lifestate();
	int teamindex();
	int survivalteam();
	int tickbase();
	int shotsfired();
	int movetype();
	int& flags();
	int observermode();
	int& bspotted();
	float flashalpha();
	float simulationtime();
	float nextattack();
	bool is_alive();
	bool is_player();
	bool is_enemy(cs_player* entity);
	bool in_smoke();
	bool is_flashed();
	bool is_ghost();
	bool has_c4();
	bool is_immunity();
	bool is_scoped();
	vector& aimpunchangle();
	vector& vecorigin();
	vector& vecvelocity();
	vector eye();
	vector& eyeangles();
	vector gethitbox(int index);
	std::vector<vector> getmultipoint(int index, int maxpoints = 4, float scale = 1.f);
	std::array<float, 24> & poseparameter();
	std::array<matrix3x4_t, MAXSTUDIOBONES> bonematrix();
	CUserCmd*& currentcmd();
	void setabsorigin(const vector& origin);
	void setabsangles(const qangle& angles);
	void invalidatebonecache();
	cs_player* observertarget();
	cs_weapon* weapon();
	bool is_visible();
	bool hitbox_visible(const vector& hitbox);
	bool in_crosshair(int index);
	float eye_to_wall_dist();
	float get_hitchance(cs_player* entity);
	bool getbox(ImVec2& out_min, ImVec2& out_max);
};

class RadarPlayer_t
{
};

class CCSGO_HudRadar
{
};

typedef struct player_data_s
{
	float alpha = 0;
	float health = 0;
	float spotted_time = 0;
	int itemindex = 0;
	bool can_drawing = false;
	bool radar_alive = false;
	ImVec2 min = ImVec2(0,0), max = ImVec2(0,0);
	vector radar_pos = vector(0,0,0);
	vector radar_old_pos = vector(0, 0, 0);
}player_data_t;

typedef struct player_count_s
{
	cs_player* entity = nullptr;
	int index = 0;
} player_count_t;

typedef struct object_count_s
{
	cs_player* entity = nullptr;
	int index = 0;
} object_count_t;

class player_list
{
public:
	player_list()
	{
		_players.reserve(64);
		_objects.reserve(32);
		_data = {};
	}

	~player_list()
	{
		_players.clear();
		_objects.clear();
	}

	void update();
	void clear();
	void add_player(IClientEntity* entity, int index);
	void remove_player(IClientEntity* entity);
	void add_object(IClientEntity* entity, int index);
	void remove_object(IClientEntity* entity);
	player_data_t* get_player_data(int index);
	std::vector<player_count_t> get_players() const;
	std::vector<object_count_t> get_objects() const;
private:
	std::vector<player_count_s> _players;
	std::vector<object_count_t> _objects;
	std::array<player_data_t, 64> _data;
};

extern std::unique_ptr<player_list> g_pPlayerList;