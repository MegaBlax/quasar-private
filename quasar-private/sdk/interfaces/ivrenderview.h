#pragma once
//#define FFADE_IN			0x0001		// Just here so we don't pass 0 into the function
//#define FFADE_OUT			0x0002		// Fade out (not in)
//#define FFADE_MODULATE		0x0004		// Modulate (don't blend)
//#define FFADE_STAYOUT		0x0008		// ignores the duration, stays faded out until new ScreenFade message received
//#define FFADE_PURGE			0x0010		// Purges all other fades, replacing them with this one
//#define SCREENFADE_FRACBITS		9		// which leaves 16-this for the integer part
//struct ScreenFade_t
//{
//	unsigned short 	duration;		// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, seconds duration
//	unsigned short 	holdTime;		// FIXED 16 bit, with SCREENFADE_FRACBITS fractional, seconds duration until reset (fade & hold)
//	short			fadeFlags;		// flags
//	byte			r, g, b, a;		// fade to color ( max alpha )
//};
//
//// Commands for the screen shake effect.
//enum ShakeCommand_t
//{
//	SHAKE_START = 0,		// Starts the screen shake for all players within the radius.
//	SHAKE_STOP,				// Stops the screen shake for all players within the radius.
//	SHAKE_AMPLITUDE,		// Modifies the amplitude of an active screen shake for all players within the radius.
//	SHAKE_FREQUENCY,		// Modifies the frequency of an active screen shake for all players within the radius.
//	SHAKE_START_RUMBLEONLY,	// Starts a shake effect that only rumbles the controller, no screen effect.
//	SHAKE_START_NORUMBLE,	// Starts a shake that does NOT rumble the controller.
//};
//
//struct ScreenShake_t
//{
//	ShakeCommand_t		command;
//	float	amplitude;
//	float	frequency;
//	float	duration;
//	vector  direction;
//
//	inline ScreenShake_t() : direction(0, 0, 0) {};
//	inline ScreenShake_t(ShakeCommand_t _command, float _amplitude, float _frequency,
//		float _duration, const vector& _direction);
//};
//
//inline ScreenShake_t::ScreenShake_t(ShakeCommand_t _command, float _amplitude, float _frequency,
//	float _duration, const vector& _direction) :
//	command(_command), amplitude(_amplitude), frequency(_frequency),
//	duration(_duration), direction(_direction)
//{}
//
//class IViewEffects
//{
//public:
//	// Initialize subsystem
//	virtual void	Init(void) = 0;
//	// Initialize after each level change
//	virtual void	LevelInit(void) = 0;
//	// Called each frame to determine the current view fade parameters ( color and alpha )
//	virtual void	GetFadeParams(unsigned char* r, unsigned char* g, unsigned char* b, unsigned char* a, bool* blend) = 0;
//	// Apply directscreen shake
//	virtual void	Shake(const ScreenShake_t& data) = 0;
//	// Apply direct screen fade
//	virtual void	Fade(ScreenFade_t& data) = 0;
//	// Clear all permanent fades in our fade list
//	virtual void	ClearPermanentFades(void) = 0;
//	// Clear all fades in our fade list
//	virtual void	ClearAllFades(void) = 0;
//	// Compute screen shake values for this frame
//	virtual void	CalcShake(void) = 0;
//	// Apply those values to the passed in vector(s).
//	virtual void	ApplyShake(vector& origin, qangle& angles, float factor) = 0;
//	// Compute screen tilt values for this frame
//	virtual void	CalcTilt(void) = 0;
//	// Apply those values to the passed in vector(s).
//	virtual void	ApplyTilt(qangle& angles, float factor) = 0;
//	// Save / Restore
//	//virtual void	Save(ISave* pSave) = 0;
//	//virtual void	Restore(IRestore* pRestore, bool) = 0;
//	//virtual void	ClearAllShakes(void) = 0;
//};
//extern IViewEffects* effects;

class IVRenderView
{
public:
	void SetBlend(float blend)
	{
		using fn = void(__thiscall*)(PVOID, float);
		call_virtual<fn>(this, 4)(this, blend);
		
	}
	void SetColorModulation(float const* blend)
	{
		using fn = void(__thiscall*)(PVOID, float const*);
		call_virtual<fn>(this, 6)(this, blend);
	}
};