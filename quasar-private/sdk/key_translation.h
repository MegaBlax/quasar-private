//===== Copyright � 1996-2005, Valve Corporation, All rights reserved. ======//
//
// Purpose: 
//
//===========================================================================//

#ifndef KEY_TRANSLATION_H
#define KEY_TRANSLATION_H
#ifdef _WIN32
#pragma once
#endif

#include "buttoncode.h"

class key_translation
{
public:
	key_translation()
	{
		ButtonCode_InitKeyTranslationTable();
		ButtonCode_UpdateScanCodeLayout();
	}
	
	// Convert from Windows scan codes to Button codes.
	ButtonCode_t ButtonCode_ScanCodeToButtonCode(int lParam);

	// Convert from Windows virtual key codes to Button codes.
	ButtonCode_t ButtonCode_VirtualKeyToButtonCode(int keyCode);
	int ButtonCode_ButtonCodeToVirtualKey(ButtonCode_t code);

	// Convert back + forth between ButtonCode + strings
	const char* ButtonCode_ButtonCodeToString(ButtonCode_t code);
	ButtonCode_t ButtonCode_StringToButtonCode(const char* pString);
private:
	// Call this to initialize the system
	void ButtonCode_InitKeyTranslationTable();
	// Update scan codes for foreign keyboards
	void ButtonCode_UpdateScanCodeLayout();
};

extern std::unique_ptr<key_translation> keytranslation;

#endif // KEY_TRANSLATION_H
