#include "cs_gamerules.h"
#include "netvars.h"
#include "mem.h"
#include "../utils/utils.h"

std::unique_ptr<CSGameRulesProxy> g_pGameRules;

CSGameRulesProxy::CSGameRulesProxy()
{
	thisptr = 0;
}

bool CSGameRulesProxy::init()
{
	thisptr = reinterpret_cast<DWORD>(g_pUtils->PatternScan(g_pMemory->client(), "83 3D ? ? ? ? 0 74 ? A1 ? ? ? ? B9 ? ? ? ? FF 50 ? 85 C0 75 ? 8B 0D ? ? ? ?") + 0x2);
	if (!thisptr)
	{
		log_error(xorstr_(L"Failed to initialize CSGameRulesProxy"));
		return false;
	}
	return true;
}

bool CSGameRulesProxy::IsValveDS() const
{
	return *reinterpret_cast<bool*>(thisptr + netvars::DT_CSGameRulesProxy::m_bIsValveDS);
}
