#pragma once
#include "isteamuser.h"

typedef uint32_t SteamPipeHandle;
typedef uint32_t SteamUserHandle;
class ISteamClient
{
public:
	ISteamUser* GetISteamUser(SteamUserHandle hSteamUser, SteamPipeHandle hSteamPipe, const char* pchVersion)
	{
		using fn = ISteamUser * (__thiscall*)(void*, SteamUserHandle, SteamPipeHandle, const char*);
		return call_virtual<fn>(this, 5)(this, hSteamUser, hSteamPipe, pchVersion);
	}
};