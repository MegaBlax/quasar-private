#pragma once
#include <cassert>
constexpr auto MAX_BUFFER_SIZE = 1024;
#define _USE_MATH_DEFINES
#define Assert( _exp ) assert(_exp)

#include <Windows.h>
#include <iostream>
#include <thread>
#include <process.h>
#include <cmath>
#include <limits>
#include <vector>
#include <Psapi.h>
#include <d3d9.h>
#include <mutex>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <any>
#include <deque>
#include <array>
#include <memory>
#include <unordered_map>
#include <map>
#include <sstream>
#include <filesystem>
#include <functional>
#include <intrin.h>

#include "globals.h"
#include "xorstr.h"
#include "sse_mathfun.h"
#include "sdk/platform.h"
#include "utils/log.h"
#include "utils/fnv_hash.h"
#include "vmp/VMProtectSDK.h"

#define M_PI_F 3.14159265358979323846f
#define log_message(msg) g_pLogManager->message(msg)
#define log_error(msg) g_pLogManager->error(msg)

template<typename T>
T call_virtual(void* pp_class, const DWORD index)
{
	return (T)(*static_cast<PDWORD*>(pp_class))[index];
}

template < typename T >
T clamp(T in, T low, T high)
{
	return min(max(in, low), high);
}